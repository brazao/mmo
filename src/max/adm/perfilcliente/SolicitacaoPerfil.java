package max.adm.perfilcliente;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.math.BigDecimal;

import com.google.common.base.Objects;

public class SolicitacaoPerfil implements Serializable {
	 
	private static final long serialVersionUID = 1L;
	
	private BigDecimal codigoCliente;
	private Long loja;
	
	private SolicitacaoPerfil(BigDecimal codigoCliente, Long loja) {
		this.codigoCliente = codigoCliente;
		this.loja = loja;
	}
	
	public static SolicitacaoPerfil newInstance(BigDecimal codigoCliente, Long loja) {
		checkNotNull(codigoCliente);
		checkNotNull(loja);
		return new SolicitacaoPerfil(codigoCliente, loja);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof SolicitacaoPerfil) {
			SolicitacaoPerfil other = (SolicitacaoPerfil) obj;
			return Objects.equal(this.codigoCliente, other.codigoCliente) &&
				   Objects.equal(this.loja, other.loja);	
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(codigoCliente, loja);
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("cliente", codigoCliente).add("loja", loja).toString();
	}
	
	public BigDecimal getCodigoCliente() {
		return codigoCliente;
	}
	
	public Long getLoja() {
		return loja;
	}
	
}
