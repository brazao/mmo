package max.adm.perfilcliente;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.entidade.NotaFiscalCapa;
import max.adm.entidade.PerfilCliente;
import max.adm.entidade.Pessoa;
import max.adm.repositorio.RepositorioContaReceber;
import max.adm.repositorio.RepositorioNotaFiscalCapa;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class RepositorioPerfilCliente {

	private Context context;
	private SQLiteDatabase db;

	public RepositorioPerfilCliente(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public PerfilCliente buscaPerfil(Pessoa pessoa) {

		RepositorioNotaFiscalCapa repoNota = new RepositorioNotaFiscalCapa(context);

		// **DADOS DA PRIMEIRA COMPRA
		NotaFiscalCapa notaFiscalCapaPrimeiraCompra = repoNota.buscaPrimeiraCompra(pessoa.getCodigo());

		// **DADOS DA ULTIMA COMPRA
		NotaFiscalCapa notaFiscalCapaUltimaCompra = repoNota.buscaUltimaCompra(pessoa.getCodigo());

		// **DADOS DO TOTAL DE PAGAMENTOS
		BigDecimal valorTotalEmCompra = new BigDecimal(0);
		List<NotaFiscalCapa> notas = repoNota.buscaPorCliente(pessoa.getCodigo());
		for (NotaFiscalCapa notaFiscalCapa : notas) {
			valorTotalEmCompra.add(valorTotalEmCompra).add(notaFiscalCapa.getValorTotal().getValor());
		}

		RepositorioContaReceber repositorioContaReceber = new RepositorioContaReceber(context);

		BigDecimal totalEmAberto = new BigDecimal(String.valueOf(repositorioContaReceber.getValorTotalAberto(
				pessoa.getCodigo(), pessoa.getEmpresa().getNumeroLoja())));
		BigDecimal totalVencido = new BigDecimal(String.valueOf(repositorioContaReceber.getValorTotalVencido(
				pessoa.getCodigo(), pessoa.getEmpresa().getNumeroLoja())));
		BigDecimal totalVencer = new BigDecimal(String.valueOf(repositorioContaReceber.getValorTotalVencer(
				pessoa.getCodigo(), pessoa.getEmpresa().getNumeroLoja())));
		
		BigDecimal valorMedioPorPedido = new BigDecimal(0);
		BigDecimal percentualEmAberto = new BigDecimal(0);
		Date dataSincronizacao = new Date();

		try {
			PerfilCliente perfilCliente = PerfilCliente.newInstance(
					0, 
					pessoa.getCodigo(), 
					pessoa.getEmpresa().getNumeroLoja(), 
					notaFiscalCapaPrimeiraCompra.getDataemissao(), 
					notaFiscalCapaPrimeiraCompra.getItens().size(), 
					notaFiscalCapaPrimeiraCompra.getValorTotal().getValor(), 
					null, null,	null, null, null, null, 
					notaFiscalCapaUltimaCompra.getDataemissao(),
					notaFiscalCapaUltimaCompra.getItens().size(),
					notaFiscalCapaUltimaCompra.getValorTotal().getValor(),
					valorTotalEmCompra, 
					totalEmAberto, 
					totalVencido, 
					totalVencer,
					valorMedioPorPedido, 
					percentualEmAberto, 
					dataSincronizacao);
			
			return perfilCliente;
		} catch (Exception e) {
			return null;
		}
	}

}
