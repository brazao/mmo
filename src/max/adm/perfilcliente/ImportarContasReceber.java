package max.adm.perfilcliente;

import static com.google.common.base.Preconditions.checkNotNull;

import java.lang.reflect.Type;
import java.util.List;

import max.adm.Servico;
import max.adm.entidade.ContaReceber;
import max.adm.entidade.Empresa;
import max.adm.entidade.Parametro;
import max.adm.entidade.PerfilCliente;
import max.adm.pessoa.repositorio.RepositorioPessoa;
import max.adm.service.ConexaoHttp;
import max.adm.utilidades.Comprimir;
import max.adm.utilidades.Notificacao;
import max.adm.utilidades.NotificacaoFactory;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.common.reflect.TypeToken;

public class ImportarContasReceber {

	public static final String TAG = "ImportarContasReceber";

	Context ctx;
	Empresa empresa;
	Notificacao notificacao;

	private ImportarContasReceber(Context ctx, Empresa empresa) {
		this.ctx = ctx;
		this.empresa = empresa;
	}

	public static ImportarContasReceber newInstance(Context ctx, Empresa empresa) {
		checkNotNull(ctx, "não e possivel intanciar ImportarContasReceber com contexto nulo");
		checkNotNull(empresa, "não e possivel intanciar ImportarContasReceber com empresa nulo");
		return new ImportarContasReceber(ctx, empresa);
	}

	public void execute() {
		try {
			Log.d("importacao de dados", "importando Dados financeiros");
			notifica();
			Parametro param = Parametro.buscaPorLoja(ctx, empresa.getNumeroLoja());
			ConexaoHttp conexao = ConexaoHttp.newInstance(param.getUrlWebService() + Servico.buscaContasReceber.get());
			ContaReceber.deleteAll(this.ctx);

			//dao.delete(PerfilCliente.class, null, null);
			List<SolicitacaoPerfil> solicitacoes = geraSolicitacoes();
			Type tipo = new TypeToken<List<SolicitacaoPerfil>>() {
			}.getType();
			
			Type tipoPerfil = new TypeToken<List<PerfilCliente>>() {
			}.getType();
			
			byte[] bytes = Comprimir.listToByteArray(solicitacoes, tipo);

			List<PerfilCliente> response = conexao.executaRequisicao(bytes, PerfilCliente.class, tipoPerfil);
			for(PerfilCliente perfil : response) {
				if(perfil != null) {
					System.out.println(perfil);
					//dao.insert(perfil);
				}
			}
			notificaSucesso();
		} catch (Exception e) {
			e.printStackTrace();
			notificaFalha();
		}

	}

	private List<SolicitacaoPerfil> geraSolicitacoes() {
		RepositorioPessoa repo = new RepositorioPessoa(ctx);
		List<SolicitacaoPerfil> solicitacoes = repo.buildSolicitacaoDePerfil();
		repo.fechar();
		return solicitacoes;
	}
	
	
	
	

	private void notifica() {
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoInfo(this.ctx, "Dados financeiros", "Importando Dados Financeiros", new Intent());
	}

	private void notificaFalha() {
		notificacao.cancelaNotificacao();
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoFalha(this.ctx, "Dados Financeiros", "Falha ao importar", new Intent());
	}

	private void notificaSucesso() {
		notificacao.cancelaNotificacao();
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoSucesso(this.ctx, "Dados Financeiros", "importação concluida", new Intent());
	}
	
	

}
