package max.adm.relatorios;

import java.io.Serializable;
import java.math.BigDecimal;

import max.adm.Dinheiro;
import max.adm.utilidades.MascaraDecimal;

import org.joda.time.DateMidnight;

public class ItemRelatorioListaPedido implements Serializable {

	private static final long serialVersionUID = 1L;
	public int idPedido;
	public int numeroPedido;
	public String nomeCliente;
	public DateMidnight dataEmissaoPedido;
	public double percentualDescontoPedido;
	public double valorBruto;

	public String getIdPedido() {
		return String.valueOf(idPedido);
	}

	public int getId() {
		return this.idPedido;
	}
	public String getNumeroPedido() {
		return String.valueOf(numeroPedido);
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public String getDataEmissaoPedido() {
		StringBuilder sb = new StringBuilder();
		sb.append(dataEmissaoPedido.getDayOfMonth()).append("/").append(dataEmissaoPedido.getMonthOfYear()).append("/").append(dataEmissaoPedido.getYear());
		return sb.toString();
	}

	public String getPercentualDescontoPedido() {
		return MascaraDecimal.mascaradecimal(this.percentualDescontoPedido, MascaraDecimal.DECIMAL_DUAS_CASAS) + "%";
	}
	public String getValorBruto() {
		return Dinheiro.newInstance(new BigDecimal(this.valorBruto)).formatToScreen();
	}
	public String getValorDesconto() {
		double desconto = (this.valorBruto * (this.percentualDescontoPedido / 100));
		return "R$ " + MascaraDecimal.mascaradecimal(desconto, MascaraDecimal.DECIMAL_DUAS_CASAS);
	}

	public String getValorLiquido() {
		double desconto = (this.valorBruto * (this.percentualDescontoPedido / 100));
		double liquido = this.valorBruto - desconto;
		return "R$ " + MascaraDecimal.mascaradecimal(liquido, MascaraDecimal.DECIMAL_DUAS_CASAS);
	}

}
