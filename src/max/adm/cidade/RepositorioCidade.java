package max.adm.cidade;

import java.util.ArrayList;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.entidade.Cidade;
import max.adm.estado.RepositorioEstado;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RepositorioCidade {

	private Context ctx;

	private static final String NOME_TABELA = Cidade.NOME_TABELA;

	private SQLiteDatabase db;

	public RepositorioCidade(Context ctx) {
		db = new DBHelper(ctx).getWritableDatabase();
		this.ctx = ctx;
	}

	public int salvar(Cidade cidade) {
		int id = cidade.getCodigo();
		if (id != 0) {
			atualizar(cidade);
		} else {
			id = inserir(cidade);
		}

		return id;
	}
	
	public boolean salvarLista(List<Cidade> cidades) {

		try {
			for (Cidade cidade : cidades) {
				inserir(cidade);
			}
		} catch (Exception e) {
			Log.d("SalvarListaEstados", "excecao");
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public int inserir(Cidade cidade) {
		ContentValues values = new ContentValues();
		values.put(Cidade.NOME, cidade.getNome());
		values.put(Cidade.CODIGO, cidade.getCodigo());
		values.put(Cidade.CODIGOIBGE, cidade.getCodigoIbge());
		values.put(Cidade.CEP, cidade.getCep());
		values.put(Cidade.ESTADO, cidade.getEstado().getSigla());

		int id = inserir(values);

		return id;
	}

	public int inserir(ContentValues values) {
		int id = (int) db.insert(Cidade.NOME_TABELA, "", values);
		return id;
	}

	public int atualizar(Cidade cidade) {
		ContentValues values = new ContentValues();
		values.put(Cidade.NOME, cidade.getNome());
		values.put(Cidade.CODIGO, cidade.getCodigo());
		values.put(Cidade.CODIGOIBGE, cidade.getCodigoIbge());
		values.put(Cidade.CEP, cidade.getCep());
		values.put(Cidade.ESTADO, cidade.getEstado().getSigla());

		String codigo = String.valueOf(cidade.getCodigo());
		String where = Cidade.CODIGO + "=?";
		String[] whereArgs = new String[] { codigo };
		
		return atualizar(values, where, whereArgs);
	}

	public int atualizar(ContentValues values, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, values, where, whereArgs);

		return count;
	}

	public int deletar(int id) {
		String where = Cidade.CODIGO + "=?";
		String codigo = String.valueOf(id);
		String[] whereArgs = new String[] { codigo };

		return deletar(where, whereArgs);
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(NOME_TABELA, where, whereArgs);
		return count;
	}

	public Cidade busca(int id) {
		Cursor c = db.query(false, 
				NOME_TABELA, 
				Cidade.COLUNAS, 
				Cidade.CODIGO + "="	+ id, 
				null, null, null, null, null);
		
		Cidade cidade = null;

		if (c.moveToFirst()) {
			cidade = new Cidade();
			cidade.setCodigo(c.getInt(c.getColumnIndex(Cidade.CODIGO)));
			cidade.setNome(c.getString(c.getColumnIndex(Cidade.NOME)));
			cidade.setCep(c.getString(c.getColumnIndex(Cidade.CEP)));
			cidade.setCodigoIbge(c.getInt(c.getColumnIndex(Cidade.CODIGOIBGE)));
			RepositorioEstado repoEstado = new RepositorioEstado(ctx);
			cidade.setEstado(repoEstado.busca(c.getString(c.getColumnIndex(Cidade.ESTADO))));
			repoEstado.fechar();
		}
		c.close();
		return cidade;
	}

	public List<Cidade> buscaPorNome(String nome) {

		Cursor cursor = db.query(NOME_TABELA, 
				Cidade.COLUNAS, 
				Cidade.NOME +" like '%" + nome + "%'", 
				null, null, null, 
				Cidade.NOME);

		return listar(cursor);
	}
	
	public List<Cidade> buscaPorEstado(String sigla) {

		Cursor cursor = db.query(NOME_TABELA, 
				Cidade.COLUNAS, 
				Cidade.ESTADO +" = '" + sigla + "'", 
				null, null, null, 
				Cidade.NOME);

		return listar(cursor);
	}

	/**
	 * 
	 * @return retorna um cursor com todas as colunas
	 */
	public Cursor getCursor() {

		try {

			Cursor c = db.query(false, NOME_TABELA, Cidade.COLUNAS, null, null,
					null, null, null, null);

			return c;

		} catch (SQLException e) {
			Log.e("Cidade", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	
	private List<Cidade> listar(Cursor cursor) {
		List<Cidade> cidades = new ArrayList<Cidade>();

		RepositorioEstado repoEstado = new RepositorioEstado(ctx);
		while (cursor.moveToNext()) {

			Cidade cidade = new Cidade();

			cidade.setCodigo(cursor.getInt(cursor.getColumnIndex(Cidade.CODIGO)));
			cidade.setNome(cursor.getString(cursor.getColumnIndex(Cidade.NOME)));
			cidade.setCodigoIbge(cursor.getInt(cursor.getColumnIndex(Cidade.CODIGOIBGE)));
			cidade.setCep(cursor.getString(cursor.getColumnIndex(Cidade.CEP)));
			cidade.setEstado(repoEstado.busca(cursor.getString(cursor.getColumnIndex(Cidade.ESTADO))));

			cidades.add(cidade);
		}
		repoEstado.fechar();

		return cidades;
	}
	
	public List<Cidade> listarTodas() {
		Cursor cursor = getCursor();
		
		return listar(cursor);
	}

	public boolean tabelaVazia() {
		Cursor c = db.query(NOME_TABELA, Cidade.COLUNAS, null, null, null, null, null, "1");
		
		return c.moveToFirst();
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

}
