package max.adm.cidade;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import com.google.common.reflect.TypeToken;

import max.adm.Servico;
import max.adm.entidade.Cidade;
import max.adm.service.ConexaoHttp;
import max.adm.utilidades.Notificacao;
import max.adm.utilidades.NotificacaoFactory;
import android.content.Context;
import android.content.Intent;

public class ImportarCidades {
	private Context ctx;
	private String chave;
	private String url;
	private Notificacao notificacao;
	
	private ImportarCidades(Context ctx, String chave, String url) {
		this.ctx = ctx;
		this.chave = chave;
		this.url = url;
	}
	
	public static ImportarCidades newInstance(Context ctx, String chave, String url){
		checkNotNull(ctx);
		checkNotNull(chave);
		checkNotNull(url);
		return new ImportarCidades(ctx, chave, url);
	}
	
	public boolean execute(){
		notifica();
		try{
			ConexaoHttp conexao = ConexaoHttp.newInstance(url+Servico.buscaCidades.get());
			List<Cidade> cidades = conexao.executaRequisicao(chave, Cidade.class, new TypeToken<List<Cidade>>() {}.getType());
			Cidade.salvarLista(ctx, cidades);
			notificaSucesso();
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			notificaFalha();
			return false;
		}
	}
	
	private void notifica(){
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoInfo(this.ctx, "Cidades", "Importando Cidades", new Intent());
	}
	
	private void notificaFalha(){
		notificacao.cancelaNotificacao();
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoFalha(this.ctx, "Cidades", "Falha ao importar", new Intent());
	}

	private void notificaSucesso(){
		notificacao.cancelaNotificacao();
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoSucesso(this.ctx, "Cidades", "importação concluida", new Intent());
	}
	
}
