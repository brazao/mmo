package max.adm.repositorio;

import java.util.ArrayList;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.entidade.GrupoDeProdutos;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RepositorioGrupoDeProdutos {

	private static final String NOME_TABELA = GrupoDeProdutos.NOME_TABELA;

	protected SQLiteDatabase db;

	public RepositorioGrupoDeProdutos(Context context) {
		db = new DBHelper(context).getWritableDatabase();
	}

	public int salvar(GrupoDeProdutos grupoDeProdutos) {
		int id = grupoDeProdutos.getCodigo();
		if (id != 0) {
			atualizar(grupoDeProdutos);
		} else {
			id = inserir(grupoDeProdutos);
		}

		return id;
	}

	public static int salvar(GrupoDeProdutos grupoDeProdutos, SQLiteDatabase db) {
		int id = grupoDeProdutos.getCodigo();
		if (id != 0) {
			atualizar(grupoDeProdutos, db);
		} else {
			id = inserir(grupoDeProdutos, db);
		}

		return id;
	}

	public int inserir(GrupoDeProdutos grupoDeProdutos) {
		ContentValues values = new ContentValues();
		values.put(GrupoDeProdutos.CODIGO, grupoDeProdutos.getCodigo());
		values.put(GrupoDeProdutos.DESCRICAO, grupoDeProdutos.getDescricao());

		int id = inserir(values);

		return id;
	}

	public static int inserir(GrupoDeProdutos grupoDeProdutos, SQLiteDatabase db) {
		ContentValues values = new ContentValues();
		values.put(GrupoDeProdutos.CODIGO, grupoDeProdutos.getCodigo());
		values.put(GrupoDeProdutos.DESCRICAO, grupoDeProdutos.getDescricao());

		int id = inserir(values, db);

		return id;
	}

	public int inserir(ContentValues values) {
		int id = (int) db.insert(GrupoDeProdutos.NOME_TABELA, "", values);
		return id;
	}

	public static int inserir(ContentValues values, SQLiteDatabase db) {
		int id = (int) db.insert(GrupoDeProdutos.NOME_TABELA, "", values);
		return id;
	}

	public int atualizar(GrupoDeProdutos grupoDeProdutos) {
		ContentValues values = new ContentValues();
		values.put(GrupoDeProdutos.CODIGO, grupoDeProdutos.getCodigo());
		values.put(GrupoDeProdutos.DESCRICAO, grupoDeProdutos.getDescricao());

		String _id = String.valueOf(grupoDeProdutos.getCodigo());
		String where = GrupoDeProdutos.CODIGO + "=?";
		String[] whereArgs = new String[]{_id};
		int count = atualizar(values, where, whereArgs);

		return count;
	}

	public static int atualizar(GrupoDeProdutos grupoDeProdutos, SQLiteDatabase db) {
		ContentValues values = new ContentValues();
		values.put(GrupoDeProdutos.CODIGO, grupoDeProdutos.getCodigo());
		values.put(GrupoDeProdutos.DESCRICAO, grupoDeProdutos.getDescricao());

		String _id = String.valueOf(grupoDeProdutos.getCodigo());
		String where = GrupoDeProdutos.CODIGO + "=?";
		String[] whereArgs = new String[]{_id};
		int count = atualizar(values, where, whereArgs, db);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public static int atualizar(ContentValues valores, String where, String[] whereArgs, SQLiteDatabase db) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);
		return count;
	}

	public int deletar(int id) {
		String where = GrupoDeProdutos.CODIGO + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[]{_id};
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(NOME_TABELA, where, whereArgs);
		return count;
	}

	public GrupoDeProdutos busca(int id) {
		Cursor c = db.query(true, NOME_TABELA, GrupoDeProdutos.COLUNAS, GrupoDeProdutos.CODIGO + "=" + id, null, null, null, null, null);
		if (c.getCount() > 0) {
			// pocisiona no primeiro elemento
			c.moveToFirst();
			GrupoDeProdutos grupoDeProdutos = new GrupoDeProdutos();
			grupoDeProdutos.setCodigo(c.getInt(c.getColumnIndex(GrupoDeProdutos.CODIGO)));
			grupoDeProdutos.setCodigo(c.getInt(c.getColumnIndex(GrupoDeProdutos.CODIGO)));
			grupoDeProdutos.setDescricao(c.getString(c.getColumnIndex(GrupoDeProdutos.DESCRICAO)));

			c.close();

			return grupoDeProdutos;

		}
		c.close();
		return null;
	}

	// retorna cursor com todos os GrupoDeProdutoses
	public Cursor getCursor() {
		try {
			// select * frm carros
			return db.query(NOME_TABELA, GrupoDeProdutos.COLUNAS, null, null, null, null, null);
		} catch (SQLException e) {
			Log.e("GrupoDeProdutos", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<GrupoDeProdutos> listar() {
		Cursor c = getCursor();
		List<GrupoDeProdutos> grupoDeProdutoss = new ArrayList<GrupoDeProdutos>();

		if (c.moveToFirst()) {
			// recupera os �ndices das colunas
			int indexId = c.getColumnIndex(GrupoDeProdutos.CODIGO);
			int indexNome = c.getColumnIndex(GrupoDeProdutos.DESCRICAO);

			// loop at� o final

			do {
				GrupoDeProdutos grupoDeProdutos = new GrupoDeProdutos();
				grupoDeProdutoss.add(grupoDeProdutos);

				grupoDeProdutos.setCodigo(c.getInt(indexId));
				grupoDeProdutos.setCodigo(c.getInt(c.getColumnIndex(GrupoDeProdutos.CODIGO)));
				grupoDeProdutos.setDescricao(c.getString(indexNome));

			} while (c.moveToNext());
		}

		return grupoDeProdutoss;
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

	public boolean salvarLista(List<GrupoDeProdutos> grupos) {
		for (GrupoDeProdutos grupo : grupos) {
			try {
				salvar(grupo);
			} catch (Exception e) {
				// ja inserido
			}
		}
		return true;
	}

}
