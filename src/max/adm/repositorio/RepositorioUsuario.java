package max.adm.repositorio;

import java.util.ArrayList;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.dominio.Usuario;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RepositorioUsuario {
	
	private Context context;
	
	private static final String NOME_TABELA = Usuario.NOME_TABELA;

	protected SQLiteDatabase db;

	public RepositorioUsuario(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public int salvar(Usuario usuario) {
		db.delete(NOME_TABELA, null, null);
		int id = usuario.getId();
		if (id != 0) {
			atualizar(usuario);
		} else {
			id = inserir(usuario);
		}

		return id;
	}

	public int inserir(Usuario usuario) {
		ContentValues values = new ContentValues();
		values.put(Usuario.LOGIN, usuario.getLogin());
		values.put(Usuario.NOME, usuario.getNome());
		values.put(Usuario.SENHA, usuario.getSenha());
		values.put(Usuario.CODIGOREPRESENTANTE, usuario.getCodigoRepresentante());

		int id = inserir(values);

		return id;
	}

	public int inserir(ContentValues values) {
		int id = (int) db.insert(Usuario.NOME_TABELA, "", values);
		return id;
	}

	public int atualizar(Usuario usuario) {
		ContentValues values = new ContentValues();
		values.put(Usuario.LOGIN, usuario.getLogin());
		values.put(Usuario.NOME, usuario.getNome());
		values.put(Usuario.SENHA, usuario.getSenha());
		values.put(Usuario.CODIGOREPRESENTANTE, usuario.getCodigoRepresentante());

		String _id = String.valueOf(usuario.getId());
		String where = Usuario.ID + "=?";
		String[] whereArgs = new String[] { _id };
		int count = atualizar(values, where, whereArgs);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public int deletar(int id) {
		String where = Usuario.ID + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[] { _id };
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(NOME_TABELA, where, whereArgs);
		return count;
	}

	public Usuario busca(int id) {
		Cursor c = db.query(true, NOME_TABELA, Usuario.COLUNAS, Usuario.ID + "="
				+ id, null, null, null, null, null);
		
		if (c.getCount() > 0) {
			// pocisiona no primeiro elemento
			c.moveToFirst();
			Usuario usuario = new Usuario();
			usuario.setId(c.getInt(c.getColumnIndex(Usuario.ID)));
			usuario.setLogin(c.getString(c.getColumnIndex(Usuario.LOGIN)));
			usuario.setNome(c.getString(c.getColumnIndex(Usuario.NOME)));
			usuario.setSenha(c.getString(c.getColumnIndex(Usuario.SENHA)));
			usuario.setCodigoRepresentante(c.getString(c.getColumnIndex(Usuario.CODIGOREPRESENTANTE)));

			return usuario;

		}
		return null;
	}

	// retorna cursor com todos os Usuarioes
	public Cursor getCursor() {
		try {
			// select * frm carros
			return db.query(NOME_TABELA, Usuario.COLUNAS, null, null, null,
					null, null);
		} catch (SQLException e) {
			Log.e("Usuario", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<Usuario> listar() {
		Cursor c = getCursor();
		List<Usuario> usuarios = new ArrayList<Usuario>();
		if (c.moveToFirst()) {
			
			// loop at� o final

			do {
				Usuario usuario = new Usuario();
				usuarios.add(usuario);
				
				usuario.setId(c.getInt(c.getColumnIndex(Usuario.ID)));
				usuario.setLogin(c.getString(c.getColumnIndex(Usuario.LOGIN)));
				usuario.setNome(c.getString(c.getColumnIndex(Usuario.NOME)));
				usuario.setSenha(c.getString(c.getColumnIndex(Usuario.SENHA)));
				usuario.setCodigoRepresentante(c.getString(c.getColumnIndex(Usuario.CODIGOREPRESENTANTE)));
				

			} while (c.moveToNext());
			
		}
		c.close();
		return usuarios;
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

}
