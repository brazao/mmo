package max.adm.repositorio;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import max.adm.Dinheiro;
import max.adm.database.DBHelper;
import max.adm.entidade.PedidoCapa;
import max.adm.entidade.PedidoItens;
import max.adm.entidade.TabelaPrecoProdutoReduzido;
import max.adm.produtoreduzido.repositorio.RepositorioProdutoReduzido;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.common.collect.Lists;

public class RepositorioPedidoItem {
	
	private Context context;
	
	private static final String NOME_TABELA = PedidoItens.NOME_TABELA;

	protected SQLiteDatabase db;

	public RepositorioPedidoItem(Context context) { 
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public int salvar(PedidoItens pedidoItens) {
		int id = pedidoItens.getId();
		if (id != 0) {
			atualizar(pedidoItens);
		} else {
			id = inserir(pedidoItens);
		}

		return id;
	}

	public int inserir(PedidoItens pedidoItens) {
		
		ContentValues values = new ContentValues();
		values.put(PedidoItens.NUMEROCONJUNTO, pedidoItens.getNumeroConjunto());
		values.put(PedidoItens.PEDIDOCAPA, pedidoItens.getPedidoCapa().getId());
		values.put(PedidoItens.PRECO, pedidoItens.getPreco().getValor().doubleValue());
		values.put(PedidoItens.PRODUTOREDUZIDO, pedidoItens.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getRed_produto_key());
		values.put(PedidoItens.QUANTIDADE, pedidoItens.getQuantidade());
		
		int id = inserir(values);

		return id;
	}

	public int inserir(ContentValues values) {
		int id = (int) db.insert(PedidoItens.NOME_TABELA, "", values);
		return id;
	}

	public int atualizar(PedidoItens pedidoItens) {
		ContentValues values = new ContentValues();
		values.put(PedidoItens.NUMEROCONJUNTO, pedidoItens.getNumeroConjunto());
		values.put(PedidoItens.PEDIDOCAPA, pedidoItens.getPedidoCapa().getId());
		values.put(PedidoItens.PRECO, pedidoItens.getPreco().getValor().doubleValue());
		values.put(PedidoItens.PRODUTOREDUZIDO, pedidoItens.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getRed_produto_key());
		values.put(PedidoItens.QUANTIDADE, pedidoItens.getQuantidade());

		String _id = String.valueOf(pedidoItens.getId());
		String where = PedidoItens.ID + "=?";
		String[] whereArgs = new String[] { _id };
		int count = atualizar(values, where, whereArgs);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public int deletar(int id) {
		String where = PedidoItens.ID + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[] { _id };
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(NOME_TABELA, where, whereArgs);
		return count;
	}


	// retorna cursor com todos os PedidoItenses
	public Cursor getCursor() {
		try {
			// select * frm carros
			return db.query(NOME_TABELA, PedidoItens.COLUNAS, null, null, null,
					null, null);
		} catch (SQLException e) {
			Log.e("PedidoItens", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<PedidoItens> listarPorPedido(PedidoCapa pedido){
		Cursor c = db.query(PedidoItens.NOME_TABELA, PedidoItens.COLUNAS, PedidoItens.PEDIDOCAPA+"=?", new String[]{String.valueOf(pedido.getId())}, null, null, null);
		
		List<PedidoItens> pedidoItenss = new ArrayList<PedidoItens>();
		
		if (c.moveToFirst()) {
			
			RepositorioTabelaDePrecoProdutoReduzido repoTabPrecoProdReduzido = new RepositorioTabelaDePrecoProdutoReduzido(context);
			do {
				TabelaPrecoProdutoReduzido precoProdutoReduzido = 
						repoTabPrecoProdReduzido.busca(
								pedido.getEmpresa().getNumeroLoja(),
								pedido.getTabelaDePreco().getCodigo(),
								c.getInt(c.getColumnIndex(PedidoItens.PRODUTOREDUZIDO)));
				
				PedidoItens pedidoItens = PedidoItens.newPedidoitens(c.getInt(c.getColumnIndex(PedidoItens.ID)), 
						c.getInt(c.getColumnIndex(PedidoItens.QUANTIDADE)), 
						Dinheiro.newInstance(new BigDecimal(c.getDouble(c.getColumnIndex(PedidoItens.PRECO)))), 
						c.getInt(c.getColumnIndex(PedidoItens.NUMEROCONJUNTO)), 
						pedido, 
						precoProdutoReduzido);	
				
				pedidoItenss.add(pedidoItens);

			} while (c.moveToNext());
			repoTabPrecoProdReduzido.fechar();
		}
		c.close();
		
		return pedidoItenss;
		
		
	}
	
	public double qtdePecasPorPedido(int idPedidocapa){
		String sql = "SELECT SUM("+PedidoItens.QUANTIDADE+") FROM "+PedidoItens.NOME_TABELA+ " WHERE "+PedidoItens.PEDIDOCAPA+" = "+idPedidocapa;
		Cursor c = db.rawQuery(sql, null);
		double qtdePecas = 0;
		if (c.getCount() > 0){
			c.moveToFirst();
			qtdePecas = c.getDouble(c.getColumnIndex("SUM("+PedidoItens.QUANTIDADE+")"));
		}
		return qtdePecas;
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

	public List<PedidoItens> listarPorPedido(int empresaId, int tabelaPrecoId, int idPedido) {
		List<PedidoItens> itens = Lists.newLinkedList();
		Cursor c = db.query(PedidoItens.NOME_TABELA, PedidoItens.COLUNAS, PedidoItens.PEDIDOCAPA+"="+idPedido, null, null, null, null);
		if(c.moveToFirst()){
			do{
				PedidoItens item = montaItem(empresaId, tabelaPrecoId, c);
				itens.add(item);
			}while(c.moveToNext());
		}
		c.close();
		return itens;
	}


	private PedidoItens montaItem(int empresaId, int tabelaPrecoId, Cursor c){
		RepositorioTabelaDePrecoProdutoReduzido repoTabPrecoProdReduzido = new RepositorioTabelaDePrecoProdutoReduzido(context);
		TabelaPrecoProdutoReduzido tabelaPrecoProdutoReduzido = 
				repoTabPrecoProdReduzido.busca(empresaId, tabelaPrecoId, c.getInt(c.getColumnIndex(PedidoItens.RED_PRODUTO_KEY)));
		
		PedidoItens item = PedidoItens.newPedidoitens(c.getInt(c.getColumnIndex(PedidoItens.ID)), 
				c.getInt(c.getColumnIndex(PedidoItens.QUANTIDADE)),
				Dinheiro.newInstance(new BigDecimal(c.getDouble(c.getColumnIndex(PedidoItens.PRECO)))),
				c.getInt(c.getColumnIndex(PedidoItens.NUMEROCONJUNTO)),
				null, 
				tabelaPrecoProdutoReduzido);
		
		repoTabPrecoProdReduzido.fechar();
		return item;
	}
	

}
