package max.adm.repositorio;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.entidade.Lote;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RepositorioLote {

	public static String NOME_TABELA = "FATFECHALOTE";

	public static String ID = "LOTE_ID";
	public static String LOTE = "NUMERO_LOTE";
	public static String DATAPREVISAO = "DATA_PREVISAO";
	public static String DATAFECHAMENTO = "DATA_FECHAMENTO";

	public static String[] COLUNAS = { ID, LOTE, DATAPREVISAO, DATAFECHAMENTO };

	private Context context;

	protected SQLiteDatabase db;

	public RepositorioLote(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public long salvar(Lote lote) {
		long id = lote.getId();
		if (id != 0) {
			atualizar(lote);
		} else {
			Log.d("Lote", "inserindo lote");
			id = inserir(lote);
		}

		return id;
	}

	public int inserir(Lote lote) {
		ContentValues values = new ContentValues();
		values.put(LOTE, lote.getNumeroLote());
		if (lote.getDataPrevisao() != null) {
			values.put(DATAPREVISAO, lote.getDataPrevisao().getTime());
		}
		if (lote.getDataFechamento() != null) {
			values.put(DATAFECHAMENTO, lote.getDataFechamento().getTime());
		}


		int id = inserir(values);

		return id;
	}

	public int inserir(ContentValues values) {
		Log.d("Lote", "executando insercao");
		int id = (int) db.insert(NOME_TABELA, "", values);
		return id;
	}

	public int atualizar(Lote lote) {
		ContentValues values = new ContentValues();

		values.put(LOTE, lote.getNumeroLote());

		if (lote.getDataPrevisao() != null) {
			values.put(DATAPREVISAO, lote.getDataFechamento().getTime());
		}
		if (lote.getDataFechamento() != null) {
			values.put(DATAFECHAMENTO, lote.getDataFechamento().getTime());
		}


		String _id = String.valueOf(lote.getId());
		String where = ID + "=?";
		String[] whereArgs = new String[] { _id };
		int count = atualizar(values, where, whereArgs);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public int deletar(int id) {
		String where = ID + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[] { _id };
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(NOME_TABELA, where, whereArgs);
		return count;
	}

	public Lote busca(int id) {
		Cursor c = db.query(true, NOME_TABELA, COLUNAS,
				ID + "=" + id, null, null, null, null, null);
		if (c.getCount() > 0) {
			c.moveToFirst();
			Lote lote = Lote.newInstance(c.getLong(c.getColumnIndex(ID)), c.getInt(c.getColumnIndex(LOTE)), 
					new Date(c.getLong(c.getColumnIndex(DATAPREVISAO))), new Date(c.getLong(c.getColumnIndex(DATAFECHAMENTO))));
			c.close();
			return lote;
		}
		c.close();
		return null;
	}

	// retorna cursor com todos os Lotees
	public Cursor getCursor() {
		try {
			// select * frm carros
			return db.query(NOME_TABELA, COLUNAS, null, null, null, null,
					null);
		} catch (SQLException e) {
			Log.e("Lote", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<Lote> listar() {
		Cursor c = getCursor();
		List<Lote> lotes = new ArrayList<Lote>();

		if (c.moveToFirst()) {
			do {
				Lote lote = Lote.newInstance(c.getLong(c.getColumnIndex(ID)), c.getInt(c.getColumnIndex(LOTE)), 
						new Date(c.getLong(c.getColumnIndex(DATAPREVISAO))), new Date(c.getLong(c.getColumnIndex(DATAFECHAMENTO))));
				lotes.add(lote);
			} while (c.moveToNext());
		}

		return lotes;
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

	public boolean salvarLista(List<Lote> lotes) {
		Log.d("Lote", "salvando lista");
		for (Lote lote : lotes) {
			try {
				this.salvar(lote);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return true;
	}

}
