package max.adm.repositorio;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.entidade.ContaReceber;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RepositorioContaReceber {

	private static final String NOME_TABELA = ContaReceber.NOME_TABELA;

	protected SQLiteDatabase db;

	public RepositorioContaReceber(Context context) {
		db = new DBHelper(context).getWritableDatabase();
	}

	public int inserir(ContaReceber contaReceber) {
		ContentValues values = new ContentValues();
		values.put(ContaReceber.LJA_CODIGO, contaReceber.getLja_codigo());
		values.put(ContaReceber.REC_SERIE, contaReceber.getRec_serie());
		values.put(ContaReceber.REC_NOTA, contaReceber.getRec_nota());
		values.put(ContaReceber.REC_PARCEL, contaReceber.getRec_parcel());
		values.put(ContaReceber.CLF_CODIGO, contaReceber.getClf_codigo());
		values.put(ContaReceber.REC_VALOR, contaReceber.getRec_valor());
		values.put(ContaReceber.REC_DTEMIS, contaReceber.getRec_dtemis().getTime());
		values.put(ContaReceber.REC_DTVENC, contaReceber.getRec_dtvenc().getTime());
		values.put(ContaReceber.REC_VALORPAGO, contaReceber.getRec_valorpago());
		values.put(ContaReceber.REC_DTULTPGTO, contaReceber.getRec_dtultpgto().getTime());
		values.put(ContaReceber.TIPOCOBRANCA, contaReceber.getTipocobranca());
		values.put(ContaReceber.CPE_NUMERO_PEDIDO, contaReceber.getCpe_numero_pedido());
		values.put(ContaReceber.CPE_NR_ENTREGA, contaReceber.getCpe_nr_entrega());
		values.put(ContaReceber.REC_POSCOBR, contaReceber.getRec_poscobr());
		values.put(ContaReceber.REC_BAIXA, contaReceber.getRec_baixa());
		values.put(ContaReceber.REC_JUROS, contaReceber.getRec_juros());
		values.put(ContaReceber.REC_MULTA, contaReceber.getRec_multa());
		values.put(ContaReceber.REC_DESCONTO, contaReceber.getRec_desconto());

		int id = inserir(values);

		return id;
	}

	public int inserir(ContentValues values) {
		int id = (int) db.insert(ContaReceber.NOME_TABELA, "", values);
		return id;
	}

	public int atualizar(ContaReceber contaReceber) {
		ContentValues values = new ContentValues();
		values.put(ContaReceber.LJA_CODIGO, contaReceber.getLja_codigo());
		values.put(ContaReceber.REC_SERIE, contaReceber.getRec_serie());
		values.put(ContaReceber.REC_NOTA, contaReceber.getRec_nota());
		values.put(ContaReceber.REC_PARCEL, contaReceber.getRec_parcel());
		values.put(ContaReceber.CLF_CODIGO, contaReceber.getClf_codigo());
		values.put(ContaReceber.REC_VALOR, contaReceber.getRec_valor());
		values.put(ContaReceber.REC_DTEMIS, contaReceber.getRec_dtemis().getTime());
		values.put(ContaReceber.REC_DTVENC, contaReceber.getRec_dtvenc().getTime());
		values.put(ContaReceber.REC_VALORPAGO, contaReceber.getRec_valorpago());
		values.put(ContaReceber.REC_DTULTPGTO, contaReceber.getRec_dtultpgto().getTime());
		values.put(ContaReceber.TIPOCOBRANCA, contaReceber.getTipocobranca());
		values.put(ContaReceber.CPE_NUMERO_PEDIDO, contaReceber.getCpe_numero_pedido());
		values.put(ContaReceber.CPE_NR_ENTREGA, contaReceber.getCpe_nr_entrega());
		values.put(ContaReceber.REC_POSCOBR, contaReceber.getRec_poscobr());
		values.put(ContaReceber.REC_BAIXA, contaReceber.getRec_baixa());
		values.put(ContaReceber.REC_JUROS, contaReceber.getRec_juros());
		values.put(ContaReceber.REC_MULTA, contaReceber.getRec_multa());
		values.put(ContaReceber.REC_DESCONTO, contaReceber.getRec_desconto());

		String empresaId = String.valueOf(contaReceber.getLja_codigo());
		String serie = contaReceber.getRec_serie();
		String nota = String.valueOf(contaReceber.getRec_nota());
		String parcela = String.valueOf(contaReceber.getRec_parcel());
		String pedidoId = String.valueOf(contaReceber.getCpe_numero_pedido());

		StringBuilder where = new StringBuilder().append(ContaReceber.LJA_CODIGO + "=? and ")
				.append(ContaReceber.REC_SERIE + "=? and ").append(ContaReceber.REC_NOTA + "=? and ")
				.append(ContaReceber.REC_PARCEL + "=? and ").append(ContaReceber.CPE_NUMERO_PEDIDO + "=?");

		String[] whereArgs = new String[] { empresaId, serie, nota, parcela, pedidoId };
		int count = atualizar(values, where.toString(), whereArgs);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public ContaReceber busca(int pedidoId) {
		Cursor c = db.query(true, NOME_TABELA, ContaReceber.COLUNAS, ContaReceber.CPE_NUMERO_PEDIDO + "="
				+ pedidoId, null, null, null, null, null);

		if (c.getCount() > 0) {

			// pocisiona no primeiro elemento
			c.moveToFirst();

			ContaReceber contaReceber = new ContaReceber();
			contaReceber.setLja_codigo(c.getInt(c.getColumnIndex(ContaReceber.LJA_CODIGO)));
			contaReceber.setRec_serie(c.getString(c.getColumnIndex(ContaReceber.REC_SERIE)));
			contaReceber.setRec_nota(c.getInt(c.getColumnIndex(ContaReceber.REC_NOTA)));
			contaReceber.setRec_parcel(c.getInt(c.getColumnIndex(ContaReceber.REC_PARCEL)));
			contaReceber.setClf_codigo(c.getInt(c.getColumnIndex(ContaReceber.CLF_CODIGO)));
			contaReceber.setRec_valor(c.getDouble(c.getColumnIndex(ContaReceber.REC_VALOR)));
			contaReceber.setRec_dtemis(new Date(c.getLong(c.getColumnIndex(ContaReceber.REC_DTEMIS))));
			contaReceber.setRec_dtvenc(new Date(c.getLong(c.getColumnIndex(ContaReceber.REC_DTVENC))));
			contaReceber.setRec_valorpago(c.getDouble(c.getColumnIndex(ContaReceber.REC_VALORPAGO)));
			contaReceber.setRec_dtultpgto(new Date(c.getLong(c.getColumnIndex(ContaReceber.REC_DTULTPGTO))));
			contaReceber.setTipocobranca(c.getString(c.getColumnIndex(ContaReceber.TIPOCOBRANCA)));
			contaReceber.setCpe_numero_pedido(c.getInt(c.getColumnIndex(ContaReceber.CPE_NUMERO_PEDIDO)));
			contaReceber.setCpe_nr_entrega(c.getInt(c.getColumnIndex(ContaReceber.CPE_NR_ENTREGA)));
			contaReceber.setRec_poscobr(c.getString(c.getColumnIndex(ContaReceber.REC_POSCOBR)));
			contaReceber.setRec_baixa(c.getString(c.getColumnIndex(ContaReceber.REC_BAIXA)));
			contaReceber.setRec_juros(c.getDouble(c.getColumnIndex(ContaReceber.REC_JUROS)));
			contaReceber.setRec_multa(c.getDouble(c.getColumnIndex(ContaReceber.REC_MULTA)));
			contaReceber.setRec_desconto(c.getDouble(c.getColumnIndex(ContaReceber.REC_DESCONTO)));

			db.close();

			return contaReceber;

		}
		return null;
	}

	public ContaReceber buscaPrimeiraCompra(int codigoCliente) {
		String sql = "SELECT " + "ID " + ",LJA_CODIGO " + ",REC_SERIE " + ",REC_NOTA	 " + ",REC_PARCEL  "
				+ ",CLF_CODIGO	 " + ",REC_VALOR	 " + ",min(REC_DTEMIS) " + ",REC_DTVENC	 "
				+ ",REC_VALORPAGO	 " + ",REC_DTULTPGTO	 " + ",TIPOCOBRANCA	 " + ",CPE_NUMERO_PEDIDO "
				+ ",CPE_NR_ENTREGA	 " + ",REC_POSCOBR	 " + ",REC_BAIXA	 " + ",REC_JUROS	 " + ",REC_MULTA	 "
				+ ",REC_DESCONTO from " + ContaReceber.NOME_TABELA + "	WHERE " + ContaReceber.CLF_CODIGO
				+ " = " + codigoCliente;

		Cursor c = db.rawQuery(sql, null);

		if (c.moveToFirst()) {

			ContaReceber contaReceber = new ContaReceber();
			contaReceber.setLja_codigo(c.getInt(c.getColumnIndex(ContaReceber.LJA_CODIGO)));
			contaReceber.setRec_serie(c.getString(c.getColumnIndex(ContaReceber.REC_SERIE)));
			contaReceber.setRec_nota(c.getInt(c.getColumnIndex(ContaReceber.REC_NOTA)));
			contaReceber.setRec_parcel(c.getInt(c.getColumnIndex(ContaReceber.REC_PARCEL)));
			contaReceber.setClf_codigo(c.getInt(c.getColumnIndex(ContaReceber.CLF_CODIGO)));
			contaReceber.setRec_valor(c.getDouble(c.getColumnIndex(ContaReceber.REC_VALOR)));
			contaReceber.setRec_dtemis(new Date(c.getLong(c.getColumnIndex("min(REC_DTEMIS)"))));
			contaReceber.setRec_dtvenc(new Date(c.getLong(c.getColumnIndex(ContaReceber.REC_DTVENC))));
			contaReceber.setRec_valorpago(c.getDouble(c.getColumnIndex(ContaReceber.REC_VALORPAGO)));
			contaReceber.setRec_dtultpgto(new Date(c.getLong(c.getColumnIndex(ContaReceber.REC_DTULTPGTO))));
			contaReceber.setTipocobranca(c.getString(c.getColumnIndex(ContaReceber.TIPOCOBRANCA)));
			contaReceber.setCpe_numero_pedido(c.getInt(c.getColumnIndex(ContaReceber.CPE_NUMERO_PEDIDO)));
			contaReceber.setCpe_nr_entrega(c.getInt(c.getColumnIndex(ContaReceber.CPE_NR_ENTREGA)));
			contaReceber.setRec_poscobr(c.getString(c.getColumnIndex(ContaReceber.REC_POSCOBR)));
			contaReceber.setRec_baixa(c.getString(c.getColumnIndex(ContaReceber.REC_BAIXA)));
			contaReceber.setRec_juros(c.getDouble(c.getColumnIndex(ContaReceber.REC_JUROS)));
			contaReceber.setRec_multa(c.getDouble(c.getColumnIndex(ContaReceber.REC_MULTA)));
			contaReceber.setRec_desconto(c.getDouble(c.getColumnIndex(ContaReceber.REC_DESCONTO)));

			db.close();

			return contaReceber;

		}
		return null;
	}

	// retorna cursor com todos os ContaReceberes
	public Cursor getCursor() {
		try {
			// select * frm carros
			return db.query(NOME_TABELA, ContaReceber.COLUNAS, null, null, null, null, null);
		} catch (SQLException e) {
			Log.e("ContaReceber", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<ContaReceber> listar() {
		Cursor cursor = getCursor();
		List<ContaReceber> contaRecebers = new ArrayList<ContaReceber>();

		montaLista(cursor, contaRecebers);

		return contaRecebers;
	}

	public List<ContaReceber> listarContas(int codigoCliente, int codigoLoja) {
		String sql = "SELECT " + ContaReceber.REC_VALOR + ", " + ContaReceber.REC_DTEMIS + ", "
				+ ContaReceber.REC_DTVENC + ", " + ContaReceber.REC_VALORPAGO + ", "
				+ ContaReceber.REC_DTULTPGTO + ", " + ContaReceber.REC_BAIXA + " FROM "
				+ ContaReceber.NOME_TABELA + " WHERE " + ContaReceber.CLF_CODIGO + " = " + codigoCliente
				+ "  and " + ContaReceber.LJA_CODIGO + " = " + codigoLoja;

		Cursor cursor = db.rawQuery(sql, null);
		List<ContaReceber> contaRecebers = new ArrayList<ContaReceber>();

		montaLista(cursor, contaRecebers);

		return contaRecebers;

	}

	public List<ContaReceber> listaTotalEmAberto(int codigoCliente, int codigoLoja) {
		List<ContaReceber> lista = new ArrayList<ContaReceber>();

		Cursor cursor = db.query(true, ContaReceber.NOME_TABELA, ContaReceber.COLUNAS, ContaReceber.REC_BAIXA
				+ "=? and " + ContaReceber.LJA_CODIGO + "=? and " + ContaReceber.CLF_CODIGO + "=?",
				new String[] { "N", String.valueOf(codigoLoja), String.valueOf(codigoCliente) }, null, null,
				null, null);

		montaLista(cursor, lista);

		return lista;
	}
	
	public List<ContaReceber> listaTotalVencido(int codigoCliente, int codigoLoja) {
		List<ContaReceber> lista = new ArrayList<ContaReceber>();

		Date hoje = new Date();
		
		Cursor cursor = db.query(true, ContaReceber.NOME_TABELA, ContaReceber.COLUNAS, ContaReceber.REC_BAIXA
				+ "=? and " + ContaReceber.LJA_CODIGO + "=? and " 
					+ ContaReceber.CLF_CODIGO + "=? and "+ ContaReceber.REC_DTVENC+"<?",
				new String[] { "N", 
							String.valueOf(codigoLoja), 
							String.valueOf(codigoCliente), 
							String.valueOf(hoje.getTime()) }, 
				null, null,	null, null);

		montaLista(cursor, lista);

		return lista;
	}
	
	public List<ContaReceber> listaTotalVencer(int codigoCliente, int codigoLoja) {
		List<ContaReceber> lista = new ArrayList<ContaReceber>();
		
		Date hoje = new Date();
		
		Cursor cursor = db.query(true, ContaReceber.NOME_TABELA, ContaReceber.COLUNAS, ContaReceber.REC_BAIXA
				+ "=? and " + ContaReceber.LJA_CODIGO + "=? and " 
				+ ContaReceber.CLF_CODIGO + "=? and "+ ContaReceber.REC_DTVENC+">=?",
				new String[] { "N", 
						String.valueOf(codigoLoja), 
						String.valueOf(codigoCliente), 
						String.valueOf(hoje.getTime()) },
				null, null, null, null);

		montaLista(cursor, lista);

		return lista;
	}
	
	public Double getValorTotalVencer(int codigoCliente, int codigoLoja) {
		List<ContaReceber> contasReceber = listaTotalVencer(codigoCliente, codigoLoja);
		
		Double total = 0D;
		for (ContaReceber contaReceber : contasReceber) {
			total += contaReceber.getRec_valor();
		}
		
		return total;
	}
	
	public Double getValorTotalVencido(int codigoCliente, int codigoLoja) {
		List<ContaReceber> contasReceber = listaTotalVencido(codigoCliente, codigoLoja);
		
		Double total = 0D;
		for (ContaReceber contaReceber : contasReceber) {
			total += contaReceber.getRec_valor();
		}
		
		return total;
	}
	
	public Double getValorTotalAberto(int codigoCliente, int codigoLoja) {
		List<ContaReceber> contasReceber = listaTotalEmAberto(codigoCliente, codigoLoja);
		
		Double total = 0D;
		for (ContaReceber contaReceber : contasReceber) {
			total += contaReceber.getRec_valor();
		}
		
		return total;
	}

	private void montaLista(Cursor cursor, List<ContaReceber> lista) {
		while (cursor.moveToNext()) {
			ContaReceber contaReceber = new ContaReceber();

			contaReceber.setLja_codigo(cursor.getInt(cursor.getColumnIndex(ContaReceber.LJA_CODIGO)));
			contaReceber.setRec_serie(cursor.getString(cursor.getColumnIndex(ContaReceber.REC_SERIE)));
			contaReceber.setRec_nota(cursor.getInt(cursor.getColumnIndex(ContaReceber.REC_NOTA)));
			contaReceber.setRec_parcel(cursor.getInt(cursor.getColumnIndex(ContaReceber.REC_PARCEL)));
			contaReceber.setClf_codigo(cursor.getInt(cursor.getColumnIndex(ContaReceber.CLF_CODIGO)));
			contaReceber.setRec_valor(cursor.getDouble(cursor.getColumnIndex(ContaReceber.REC_VALOR)));
			contaReceber.setRec_dtemis(new Date(cursor.getLong(cursor.getColumnIndex(ContaReceber.REC_DTEMIS))));
			contaReceber.setRec_dtvenc(new Date(cursor.getLong(cursor.getColumnIndex(ContaReceber.REC_DTVENC))));
			contaReceber.setRec_valorpago(cursor.getDouble(cursor.getColumnIndex(ContaReceber.REC_VALORPAGO)));
			contaReceber.setRec_dtultpgto(new Date(cursor.getLong(cursor.getColumnIndex(ContaReceber.REC_DTULTPGTO))));
			contaReceber.setTipocobranca(cursor.getString(cursor.getColumnIndex(ContaReceber.TIPOCOBRANCA)));
			contaReceber.setCpe_numero_pedido(cursor.getInt(cursor.getColumnIndex(ContaReceber.CPE_NUMERO_PEDIDO)));
			contaReceber.setCpe_nr_entrega(cursor.getInt(cursor.getColumnIndex(ContaReceber.CPE_NR_ENTREGA)));
			contaReceber.setRec_poscobr(cursor.getString(cursor.getColumnIndex(ContaReceber.REC_POSCOBR)));
			contaReceber.setRec_baixa(cursor.getString(cursor.getColumnIndex(ContaReceber.REC_BAIXA)));
			contaReceber.setRec_juros(cursor.getDouble(cursor.getColumnIndex(ContaReceber.REC_JUROS)));
			contaReceber.setRec_multa(cursor.getDouble(cursor.getColumnIndex(ContaReceber.REC_MULTA)));
			contaReceber.setRec_desconto(cursor.getDouble(cursor.getColumnIndex(ContaReceber.REC_DESCONTO)));

			lista.add(contaReceber);
		}
	}

	public int atrasoMedio(int codigoCliente, int codigoLoja) {
		int atrasoMedio = 0;
		long ultimosPgtos = 0;
		long vencimentos = 0;
		int quantidadeDeContas = 0;

		int diasUltimosPgtos = 0;
		int diasVencimento = 0;

		String sql = "";
		Cursor c;

		db.beginTransaction();
		try {
			// quantidade de contas
			sql = "select " + ContaReceber.COLUNAS + ", " + ContaReceber.REC_DTULTPGTO + ", "
					+ ContaReceber.REC_DTVENC + " from " + ContaReceber.NOME_TABELA + " where "
					+ ContaReceber.CLF_CODIGO + " = " + codigoCliente + " and " + ContaReceber.LJA_CODIGO
					+ " = " + codigoLoja;
			c = db.rawQuery(sql, null);

			Log.d("Dados financeiros", sql);

			List<ContaReceber> contas = new ArrayList<ContaReceber>();

			if (c.moveToFirst()) {
				do {
					ContaReceber conta = new ContaReceber();
					conta.setCpe_numero_pedido(c.getInt(c.getColumnIndex(ContaReceber.CPE_NUMERO_PEDIDO)));

					contas.add(conta);
					// considerar que esta atrasado ate hj as contas sem data de
					// ultimo pagameto.
					if (c.getLong(c.getColumnIndex(ContaReceber.REC_DTULTPGTO)) == 0) {
						conta.setRec_dtultpgto(new Date());
					} else {
						conta.setRec_dtultpgto(new Date(c.getLong(c
								.getColumnIndex(ContaReceber.REC_DTULTPGTO))));
					}

					conta.setRec_dtvenc(new Date(c.getLong(c.getColumnIndex(ContaReceber.REC_DTVENC))));

				} while (c.moveToNext());

			}

			for (ContaReceber contareceber : contas) {
				quantidadeDeContas++;
				ultimosPgtos += contareceber.getRec_dtultpgto().getTime();
				vencimentos += contareceber.getRec_dtvenc().getTime();
			}

			// trasformar ultimos pagtos em dias
			// dividir por 1000 milisegundos / dividir por 60 segundos / por 60
			// minutos / 24 horas
			diasUltimosPgtos = (int) ((((ultimosPgtos / 1000) / 60) / 60) / 24);

			// trasformar vencimentos em dias
			diasVencimento = (int) ((((vencimentos / 1000) / 60) / 60) / 24);

			// se o atraso medio for negativo, entao o cliente para adiantado,
			// ou seja.. nao tem atrasos
			if (quantidadeDeContas > 0) {
				atrasoMedio = (int) ((diasUltimosPgtos - diasVencimento) / quantidadeDeContas);
			} else {
				atrasoMedio = 0;
			}

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}

		if (atrasoMedio > 0) {
			return atrasoMedio;
		} else {
			return 0;
		}

	}

	public int prazoMedio(int codigoCliente, int codigoLoja) {
		int prazoMedio = 0;
		long vencimentos = 0;
		long emissoes = 0;
		int quantidadeDeContas = 0;

		int diasVencimetos = 0;
		int diasEmissoes = 0;

		String sql = "";
		Cursor c;

		db.beginTransaction();
		try {
			// quantidade de contas
			sql = "select count(" + ContaReceber.CPE_NUMERO_PEDIDO + ") from " + ContaReceber.NOME_TABELA
					+ " where " + ContaReceber.CLF_CODIGO + " = " + codigoCliente + " and "
					+ ContaReceber.LJA_CODIGO + " = " + codigoLoja;
			c = db.rawQuery(sql, null);
			if (c.getCount() > 0) {
				c.moveToFirst();
				quantidadeDeContas = c.getInt(c.getColumnIndex("count(" + ContaReceber.CPE_NUMERO_PEDIDO
						+ ")"));
			}

			// soma todos os ultimos pagamentos
			sql = "SELECT SUM(" + ContaReceber.REC_DTVENC + ") FROM " + ContaReceber.NOME_TABELA + " WHERE "
					+ ContaReceber.CLF_CODIGO + " = " + codigoCliente + " AND " + ContaReceber.LJA_CODIGO
					+ " = " + codigoLoja;

			c = db.rawQuery(sql, null);

			if (c.getCount() > 0)
				;
			{
				c.moveToFirst();
				vencimentos = c.getLong(c.getColumnIndex("SUM(" + ContaReceber.REC_DTVENC + ")"));
			}

			// soma todos os ultimos vencmentos
			sql = "SELECT SUM(" + ContaReceber.REC_DTEMIS + ") FROM " + ContaReceber.NOME_TABELA + " WHERE "
					+ ContaReceber.CLF_CODIGO + " = " + codigoCliente + " AND " + ContaReceber.LJA_CODIGO
					+ " = " + codigoLoja;
			c = db.rawQuery(sql, null);
			if (c.getCount() > 0)
				;
			{
				c.moveToFirst();
				emissoes = c.getLong(c.getColumnIndex("SUM(" + ContaReceber.REC_DTEMIS + ")"));
			}

			// trasformar ultimos pagtos em dias
			// dividir por 1000 milisegundos / dividir por 60 segundos / por 60
			// minutos / 24 horas
			diasVencimetos = (int) ((((vencimentos / 1000) / 60) / 60) / 24);

			// trasformar vencimentos em dias
			diasEmissoes = (int) ((((emissoes / 1000) / 60) / 60) / 24);

			if (quantidadeDeContas > 0) {
				prazoMedio = (int) ((diasVencimetos - diasEmissoes) / quantidadeDeContas);
			} else {
				prazoMedio = 0;
			}

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}

		return prazoMedio;

	}

	public Date vencimentoUltimoAtraso(int codigoPessoa, int codigoLoja) {
		Date vencUltimoAtrado = null;

		String sql = "select max(" + ContaReceber.REC_DTVENC + ") from " + ContaReceber.NOME_TABELA
				+ " where (" + ContaReceber.REC_DTULTPGTO + " - " + ContaReceber.REC_DTVENC + ") > 0 and "
				+ ContaReceber.CLF_CODIGO + " = " + codigoPessoa + " and " + ContaReceber.LJA_CODIGO + " = "
				+ codigoLoja;

		Cursor c = db.rawQuery(sql, null);

		if (c.getCount() > 0) {
			c.moveToFirst();
			vencUltimoAtrado = new Date(c.getLong(c.getColumnIndex("max(" + ContaReceber.REC_DTVENC + ")")));

		}

		return vencUltimoAtrado;
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

	public void deleteAll() {
		db.delete(ContaReceber.NOME_TABELA, null, null);
	}
}
