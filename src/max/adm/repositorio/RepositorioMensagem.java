package max.adm.repositorio;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.empresa.RepositorioEmpresa;
import max.adm.entidade.Mensagem;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RepositorioMensagem {
	
	private Context context;
	
	private static final String NOME_TABELA = Mensagem.NOME_TABELA;

	protected SQLiteDatabase db;

	public RepositorioMensagem(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public int salvar(Mensagem mensagem) {
		int id = mensagem.getId();
		if (id != 0) {
			atualizar(mensagem);
		} else {
			id = inserir(mensagem);
		}

		return id;
	}

	public int inserir(Mensagem mensagem) {
		ContentValues values = new ContentValues();
		values.put(Mensagem.CODIGO, mensagem.getCodigo());
		values.put(Mensagem.REMETENTE, mensagem.getRemetente());
		values.put(Mensagem.DATAENVIO, mensagem.getDataEnvio().getTime());
		values.put(Mensagem.DATALEITURA, mensagem.getDataLeitura().getTime());
		values.put(Mensagem.CODIGOPESSOA, mensagem.getCodigoPessoa());
		values.put(Mensagem.MENSAGEM, mensagem.getMensagem());
		values.put(Mensagem.RESPOSTA, mensagem.getResposta());
		values.put(Mensagem.EMPRESA, mensagem.getEmpresa().getNumeroLoja());

		int id = inserir(values);

		return id;
	}

	public int inserir(ContentValues values) {
		int id = (int) db.insert(Mensagem.NOME_TABELA, "", values);
		return id;
	}

	public int atualizar(Mensagem mensagem) {
		ContentValues values = new ContentValues();
		values.put(Mensagem.CODIGO, mensagem.getCodigo());
		values.put(Mensagem.REMETENTE, mensagem.getRemetente());
		if (mensagem.getDataEnvio() != null)
			values.put(Mensagem.DATAENVIO, mensagem.getDataEnvio().getTime());
		if (mensagem.getDataLeitura() != null)
			values.put(Mensagem.DATALEITURA, mensagem.getDataLeitura().getTime());
		if (mensagem.getDataResposta() != null)
			values.put(Mensagem.DATARESPOSTA, mensagem.getDataResposta().getTime());
		values.put(Mensagem.CODIGOPESSOA, mensagem.getCodigoPessoa());
		values.put(Mensagem.MENSAGEM, mensagem.getMensagem());
		values.put(Mensagem.RESPOSTA, mensagem.getResposta());
		values.put(Mensagem.EMPRESA, mensagem.getEmpresa().getNumeroLoja());

		String _id = String.valueOf(mensagem.getId());
		String where = Mensagem.ID + "=?";
		String[] whereArgs = new String[] { _id };
		int count = atualizar(values, where, whereArgs);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}
	
	public void deletarVarias(String[] ids){
		String sql = "DELETE FROM "+NOME_TABELA+" WHERE "+Mensagem.ID+" in( ";
		sql+= ids[0];
		for (int i = 1; i< ids.length; i++){
			sql+= ", "+ids[i];
		}
		sql +=")";
		
		db.execSQL(sql);
	}

	public int deletar(int id) {
		String where = Mensagem.ID + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[] { _id };
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(NOME_TABELA, where, whereArgs);
		return count;
	}

	public Mensagem busca(int id) {
		Cursor c = db.query(true, NOME_TABELA, Mensagem.COLUNAS, Mensagem.ID + "="
				+ id, null, null, null, null, null);
		RepositorioEmpresa repoEmpresa = new RepositorioEmpresa(context);
		if (c.getCount() > 0) {
			// pocisiona no primeiro elemento
			c.moveToFirst();
			Mensagem mensagem = new Mensagem();
			mensagem.setId(c.getInt(c.getColumnIndex(Mensagem.ID)));
			mensagem.setCodigo(c.getInt(c.getColumnIndex(Mensagem.CODIGO)));
			mensagem.setRemetente(c.getString(c.getColumnIndex(Mensagem.REMETENTE)));
			mensagem.setDataEnvio(new Date(c.getLong(c.getColumnIndex(Mensagem.DATAENVIO))));
			mensagem.setDataLeitura(new Date(c.getLong(c.getColumnIndex(Mensagem.DATALEITURA))));
			mensagem.setDataResposta(new Date(c.getLong(c.getColumnIndex(Mensagem.DATARESPOSTA))));
			mensagem.setCodigoPessoa(c.getInt(c.getColumnIndex(Mensagem.CODIGOPESSOA)));
			mensagem.setMensagem(c.getString(c.getColumnIndex(Mensagem.MENSAGEM)));
			mensagem.setResposta(c.getString(c.getColumnIndex(Mensagem.RESPOSTA)));
			mensagem.setEmpresa(repoEmpresa.busca(c.getInt(c.getColumnIndex(Mensagem.EMPRESA))));
			repoEmpresa.fechar();

			return mensagem;

		}
		return null;
	}

	// retorna cursor com todos os Mensagemes
	public Cursor getCursor() {
		try {
			// select * frm carros
			return db.query(NOME_TABELA, Mensagem.COLUNAS, null, null, null,
					null, null);
		} catch (SQLException e) {
			Log.e("Mensagem", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<Mensagem> listar() {
		Cursor c = getCursor();
		List<Mensagem> mensagems = new ArrayList<Mensagem>();
		
		if (c.moveToFirst()) {

			// loop at� o final

			do {
				RepositorioEmpresa repoEmpresa = new RepositorioEmpresa(context);
				Mensagem mensagem = new Mensagem();
				mensagems.add(mensagem);

				mensagem.setId(c.getInt(c.getColumnIndex(Mensagem.ID)));
				mensagem.setCodigo(c.getInt(c.getColumnIndex(Mensagem.CODIGO)));
				mensagem.setRemetente(c.getString(c.getColumnIndex(Mensagem.REMETENTE)));
				if (c.getLong(c.getColumnIndex(Mensagem.DATAENVIO)) > 0){
					mensagem.setDataEnvio(new Date(c.getLong(c.getColumnIndex(Mensagem.DATAENVIO))));
				}
				if (c.getLong(c.getColumnIndex(Mensagem.DATALEITURA)) > 0){
					mensagem.setDataEnvio(new Date(c.getLong(c.getColumnIndex(Mensagem.DATALEITURA))));
				}
				if (c.getLong(c.getColumnIndex(Mensagem.DATARESPOSTA)) > 0){
					mensagem.setDataResposta(new Date(c.getLong(c.getColumnIndex(Mensagem.DATARESPOSTA))));
				}
				
				mensagem.setCodigoPessoa(c.getInt(c.getColumnIndex(Mensagem.CODIGOPESSOA)));
				mensagem.setMensagem(c.getString(c.getColumnIndex(Mensagem.MENSAGEM)));
				mensagem.setResposta(c.getString(c.getColumnIndex(Mensagem.RESPOSTA)));
				mensagem.setEmpresa(repoEmpresa.busca(c.getInt(c.getColumnIndex(Mensagem.EMPRESA))));
				repoEmpresa.fechar();

			} while (c.moveToNext());
		}

		return mensagems;
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}
	
	

}
