package max.adm.repositorio;

import java.util.ArrayList;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.entidade.TamanhoProduto;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RepositorioTamanho {

	private static final String NOME_TABELA = TamanhoProduto.NOME_TABELA;

	protected SQLiteDatabase db;

	public RepositorioTamanho(Context context) {
		db = new DBHelper(context).getWritableDatabase();
	}

	public boolean salvarLista(List<TamanhoProduto> tamanhos) {

		for (TamanhoProduto t : tamanhos) {
			try {
				salvar(t);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	public String salvar(TamanhoProduto tamanho) {
		String id = tamanho.getCodigo();
		if (!id.equals("")) {
			atualizar(tamanho);
		} else {
			id = inserir(tamanho);
		}

		return id;
	}

	public static String salvar(TamanhoProduto tamanho, SQLiteDatabase db) {
		String id = tamanho.getCodigo();
		if (!id.equals("")) {
			atualizar(tamanho, db);
		} else {
			id = inserir(tamanho, db);
		}

		return id;
	}

	public String inserir(TamanhoProduto tamanho) {
		ContentValues values = new ContentValues();
		values.put(TamanhoProduto.DESCRICAO, tamanho.getDescricao());
		values.put(TamanhoProduto.TAMANHOCLASSIFICACAO, tamanho.getTamanhoClassificacao());

		String id = inserir(values);

		return id;
	}

	public static String inserir(TamanhoProduto tamanho, SQLiteDatabase db) {
		ContentValues values = new ContentValues();
		values.put(TamanhoProduto.DESCRICAO, tamanho.getDescricao());
		values.put(TamanhoProduto.TAMANHOCLASSIFICACAO, tamanho.getTamanhoClassificacao());

		String id = inserir(values, db);

		return id;
	}

	public String inserir(ContentValues values) {
		String id = String.valueOf(db.insert(TamanhoProduto.NOME_TABELA, "", values));
		return id;
	}

	public static String inserir(ContentValues values, SQLiteDatabase db) {
		String id = String.valueOf(db.insert(TamanhoProduto.NOME_TABELA, "", values));
		return id;
	}

	public int atualizar(TamanhoProduto tamanho) {
		ContentValues values = new ContentValues();
		values.put(TamanhoProduto.DESCRICAO, tamanho.getDescricao());
		values.put(TamanhoProduto.TAMANHOCLASSIFICACAO, tamanho.getTamanhoClassificacao());

		String _id = String.valueOf(tamanho.getCodigo());
		String where = TamanhoProduto.CODIGO + "=?";
		String[] whereArgs = new String[]{_id};
		int count = atualizar(values, where, whereArgs);

		return count;
	}

	public static int atualizar(TamanhoProduto tamanho, SQLiteDatabase db) {
		ContentValues values = new ContentValues();
		values.put(TamanhoProduto.DESCRICAO, tamanho.getDescricao());
		values.put(TamanhoProduto.TAMANHOCLASSIFICACAO, tamanho.getTamanhoClassificacao());

		String _id = String.valueOf(tamanho.getCodigo());
		String where = TamanhoProduto.CODIGO + "=?";
		String[] whereArgs = new String[]{_id};
		int count = atualizar(values, where, whereArgs, db);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public static int atualizar(ContentValues valores, String where, String[] whereArgs, SQLiteDatabase db) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public int deletar(int id) {
		String where = TamanhoProduto.CODIGO + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[]{_id};
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(NOME_TABELA, where, whereArgs);
		return count;
	}

	public TamanhoProduto busca(String id) {
		Cursor c = db.query(
				true, 
				NOME_TABELA, 
				TamanhoProduto.COLUNAS, 
				TamanhoProduto.CODIGO + "='" + id +"'", 
				null, null, null, null, null);

		if (c.moveToFirst()) {
			TamanhoProduto tamanho = TamanhoProduto.newTamanho(
					c.getString(c.getColumnIndex(TamanhoProduto.CODIGO)),
					c.getString(c.getColumnIndex(TamanhoProduto.DESCRICAO)), 
					c.getInt(c.getColumnIndex(TamanhoProduto.TAMANHOCLASSIFICACAO)));
			c.close();
			return tamanho;
		}
		c.close();
		return null;
	}

	// retorna cursor com todos os Tamanhoes
	public Cursor getCursor() {
		try {
			// select * frm carros
			return db.query(NOME_TABELA, TamanhoProduto.COLUNAS, null, null, null, null, TamanhoProduto.TAMANHOCLASSIFICACAO);
		} catch (SQLException e) {
			Log.e("Tamanho", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<TamanhoProduto> listar() {
		Cursor c = getCursor();
		List<TamanhoProduto> tamanhos = new ArrayList<TamanhoProduto>();
		while (c.moveToNext()) {
			TamanhoProduto tamanho = TamanhoProduto.newTamanho(
					c.getString(c.getColumnIndex(TamanhoProduto.CODIGO)),
					c.getString(c.getColumnIndex(TamanhoProduto.DESCRICAO)),
					c.getInt(c.getColumnIndex(TamanhoProduto.TAMANHOCLASSIFICACAO)));

			tamanhos.add(tamanho);
		}
		return tamanhos;
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

}
