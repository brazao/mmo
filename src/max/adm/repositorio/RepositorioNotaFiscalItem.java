package max.adm.repositorio;

import java.util.ArrayList;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.entidade.NotaFiscalItem;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RepositorioNotaFiscalItem {

	private Context context;

	private static final String NOME_TABELA = NotaFiscalItem.NOME_TABELA;

	protected SQLiteDatabase db;

	public RepositorioNotaFiscalItem(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public int inserir(NotaFiscalItem itemNotaFiscal) {
		ContentValues values = new ContentValues();
		values.put(NotaFiscalItem.REDPRODUTOKEY, itemNotaFiscal.getRedProdutoKey());
		values.put(NotaFiscalItem.QUANTIDADE, itemNotaFiscal.getQuantidade());
		values.put(NotaFiscalItem.PRECOUNITARIO, itemNotaFiscal.getPrecoUnitario());
		values.put(NotaFiscalItem.NOTAFISCAL, itemNotaFiscal.getNotaFiscal().getId());

		int id = inserir(values);

		return id;
	}

	public int inserir(ContentValues values) {
		int id = (int) db.insert(NotaFiscalItem.NOME_TABELA, "", values);
		return id;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(NOME_TABELA, where, whereArgs);
		return count;
	}

	public NotaFiscalItem busca(int redProdutoKey, int notaFiscalCapaId) {
		Cursor c = db.query(true, NOME_TABELA, NotaFiscalItem.COLUNAS, NotaFiscalItem.REDPRODUTOKEY + "="
				+ redProdutoKey + "and " + NotaFiscalItem.NOTAFISCAL + " = " + notaFiscalCapaId, null, null,
				null, null, null);
		RepositorioNotaFiscalCapa repoNota = new RepositorioNotaFiscalCapa(context);
		if (c.getCount() > 0) {
			// pocisiona no primeiro elemento
			c.moveToFirst();
			NotaFiscalItem itemNotaFiscal = new NotaFiscalItem();
			itemNotaFiscal.setRedProdutoKey(c.getInt(c.getColumnIndex(NotaFiscalItem.REDPRODUTOKEY)));
			itemNotaFiscal.setQuantidade(c.getDouble(c.getColumnIndex(NotaFiscalItem.QUANTIDADE)));
			itemNotaFiscal.setPrecoUnitario(c.getInt(c.getColumnIndex(NotaFiscalItem.PRECOUNITARIO)));
			itemNotaFiscal
					.setNotaFiscal(repoNota.busca(c.getInt(c.getColumnIndex(NotaFiscalItem.NOTAFISCAL))));
			repoNota.fechar();

			return itemNotaFiscal;

		}
		return null;
	}

	// retorna cursor com todos os ItemNotaFiscais
	public Cursor getCursor() {
		try {
			// select * frm carros
			return db.query(NOME_TABELA, NotaFiscalItem.COLUNAS, null, null, null, null, null);
		} catch (SQLException e) {
			Log.e("ItemNotaFiscal", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<NotaFiscalItem> listar() {
		Cursor c = getCursor();
		List<NotaFiscalItem> itemNotaFiscals = new ArrayList<NotaFiscalItem>();
		RepositorioNotaFiscalCapa repoNota = new RepositorioNotaFiscalCapa(context);
		while (c.moveToNext()) {
			NotaFiscalItem itemNotaFiscal = new NotaFiscalItem();
			itemNotaFiscals.add(itemNotaFiscal);

			itemNotaFiscal.setRedProdutoKey(c.getInt(c.getColumnIndex(NotaFiscalItem.REDPRODUTOKEY)));
			itemNotaFiscal.setQuantidade(c.getDouble(c.getColumnIndex(NotaFiscalItem.QUANTIDADE)));
			itemNotaFiscal.setPrecoUnitario(c.getInt(c.getColumnIndex(NotaFiscalItem.PRECOUNITARIO)));
			itemNotaFiscal.setNotaFiscal(repoNota.busca(c.getInt(c.getColumnIndex(NotaFiscalItem.NOTAFISCAL))));
			repoNota.fechar();
		}

		return itemNotaFiscals;
	}

	/**
	 * 
	 * @param idNotaCapa
	 * @return item com id, redProdutoKey, quantidade, precoUnitario nao atribui
	 *         a capa da nota no item (isso foi feito para otimiza��o de
	 *         pesquisa)
	 */
	public List<NotaFiscalItem> buscaItensPorNota(int idNotaCapa) {
		String sql = "SELECT * FROM " + NotaFiscalItem.NOME_TABELA + " WHERE " + NotaFiscalItem.NOTAFISCAL
				+ " = " + idNotaCapa;

		Cursor c = db.rawQuery(sql, null);

		List<NotaFiscalItem> itemNotaFiscals = new ArrayList<NotaFiscalItem>();

		while (c.moveToNext()) {
			NotaFiscalItem itemNotaFiscal = new NotaFiscalItem();
			itemNotaFiscals.add(itemNotaFiscal);

			itemNotaFiscal.setRedProdutoKey(c.getInt(c.getColumnIndex(NotaFiscalItem.REDPRODUTOKEY)));
			itemNotaFiscal.setQuantidade(c.getDouble(c.getColumnIndex(NotaFiscalItem.QUANTIDADE)));
			itemNotaFiscal.setPrecoUnitario(c.getInt(c.getColumnIndex(NotaFiscalItem.PRECOUNITARIO)));

			// retirado para otimizao de busca
			// itemNotaFiscal.setNotaFiscal(repoNota.busca(c.getInt(c.getColumnIndex(ItemNotaFiscal.NOTAFISCAL))));
			// repoNota.fechar();
		}

		return itemNotaFiscals;
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

}
