package max.adm.repositorio;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.entidade.ImagemProduto;
import max.adm.entidade.Produto;
import max.adm.entidade.TabelaDePreco;
import max.adm.produto.ItemGalleryProduto;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import com.google.common.collect.Lists;

public class RepositorioProImagem {

	private Context context;

	private static final String NOME_TABELA = ImagemProduto.NOME_TABELA;

	protected SQLiteDatabase db;

	public RepositorioProImagem(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public int salvar(ImagemProduto proImagem) {
		int id = proImagem.getCodigo();
		if (id != 0) {
			atualizar(proImagem);
		} else {
			id = inserir(proImagem);
		}

		return id;
	}

	public int inserir(ImagemProduto proImagem) {
		ContentValues values = new ContentValues();
		values.put(ImagemProduto.CODIGO, proImagem.getCodigo());
		values.put(ImagemProduto.IMAGEM, proImagem.getImagem());
		values.put(ImagemProduto.PRODUTO, proImagem.getProduto().getCodigo());

		int id = inserir(values);

		return id;
	}

	public int inserir(ContentValues values) {
		int id = (int) db.insert(ImagemProduto.NOME_TABELA, "", values);
		return id;
	}

	public int atualizar(ImagemProduto proImagem) {
		ContentValues values = new ContentValues();
		values.put(ImagemProduto.PRODUTO, proImagem.getProduto().getCodigo());
		values.put(ImagemProduto.IMAGEM, proImagem.getImagem());

		String _id = String.valueOf(proImagem.getCodigo());
		String where = ImagemProduto.CODIGO + "=?";
		String[] whereArgs = new String[] { _id };
		int count = atualizar(values, where, whereArgs);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public int deletar(int id) {
		String where = ImagemProduto.CODIGO + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[] { _id };
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(NOME_TABELA, where, whereArgs);
		return count;
	}

	public ImagemProduto busca(int codigo) {
		Cursor c = db.query(true, NOME_TABELA, ImagemProduto.COLUNAS, ImagemProduto.CODIGO + "=" + codigo,
				null, null, null, null, null);

		if (c.moveToFirst()) {
			RepositorioProduto repositorioProduto = new RepositorioProduto(context);

			ImagemProduto proImagem = new ImagemProduto();
			proImagem.setCodigo(c.getInt(c.getColumnIndex(ImagemProduto.CODIGO)));
			proImagem.setProduto(repositorioProduto.busca(c.getInt(c.getColumnIndex(ImagemProduto.PRODUTO))));
			proImagem.setImagem(c.getBlob(c.getColumnIndex(ImagemProduto.IMAGEM)));

			repositorioProduto.fechar();
			return proImagem;

		}
		return null;
	}

	public ImagemProduto buscaPorProduto(int codigoProduto) {
		String sql = "SELECT distinct(" + ImagemProduto.IMAGEM + ") as " + ImagemProduto.IMAGEM + " FROM "
				+ ImagemProduto.NOME_TABELA + " WHERE " + ImagemProduto.PRODUTO + " = " + codigoProduto;
		
		
		Cursor c = db.query(true, 
				ImagemProduto.NOME_TABELA, 
				ImagemProduto.COLUNAS, 
				ImagemProduto.PRODUTO+"=?", 
				new String[] {String.valueOf(codigoProduto)}, 
				null, null, null, null);

		if (c.moveToFirst()) {
			RepositorioProduto repositorioProduto = new RepositorioProduto(context);

			ImagemProduto proImagem = new ImagemProduto();
			proImagem.setCodigo(c.getInt(c.getColumnIndex(ImagemProduto.CODIGO)));
			proImagem.setProduto(repositorioProduto.busca(c.getInt(c.getColumnIndex(ImagemProduto.PRODUTO))));
			proImagem.setImagem(c.getBlob(c.getColumnIndex(ImagemProduto.IMAGEM)));

			repositorioProduto.fechar();
			return proImagem;
		}

		return null;

	}

	// retorna cursor com todos os ProImagemes
	public Cursor getCursor() {
		try {
			// select * frm carros
			return db.query(NOME_TABELA, ImagemProduto.COLUNAS, null, null, null, null, null);
		} catch (SQLException e) {
			Log.e("ProImagem", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<ImagemProduto> listar() {
		Cursor c = getCursor();
		List<ImagemProduto> proImagems = new ArrayList<ImagemProduto>();

		RepositorioProduto repositorioProduto = new RepositorioProduto(context);
		while (c.moveToNext()) {

			ImagemProduto proImagem = new ImagemProduto();
			proImagems.add(proImagem);

			proImagem.setCodigo(c.getInt(c.getColumnIndex(ImagemProduto.CODIGO)));
			proImagem.setProduto(repositorioProduto.busca(c.getInt(c.getColumnIndex(ImagemProduto.PRODUTO))));
			proImagem.setImagem(c.getBlob(c.getColumnIndex(ImagemProduto.IMAGEM)));

		}

		repositorioProduto.fechar();
		return proImagems;
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

	public boolean salvarLista(List<ImagemProduto> imagens) {
		if (imagens != null && imagens.size() > 0) {
			criaDiretorio(String.valueOf(imagens.get(0).getProduto().getCodigo()));

			for (ImagemProduto img : imagens) {
				try {
					ContentValues values = new ContentValues();
					values.put(ImagemProduto.CODIGO, img.getCodigo());
					values.put(ImagemProduto.PRODUTO, img.getProduto().getCodigo());
					values.put(ImagemProduto.IMAGEM, img.getImagem());

					db.insert(ImagemProduto.NOME_TABELA, "", values);
					saveImage(img);
					saveImageMini(img);
				} catch (Exception e) {
					Log.d(RepositorioProImagem.class.getName(), "Erro ao inserir imagem");
				}
			}
		}
		return true;
	}

	private void deleteDirectory(File file) throws IOException {

		if (file.isDirectory()) {

			// directory is empty, then delete it
			if (file.list().length == 0) {

				file.delete();
				System.out.println("Directory is deleted : " + file.getAbsolutePath());

			} else {

				// list all the directory contents
				String files[] = file.list();

				for (String temp : files) {
					// construct the file structure
					File fileDelete = new File(file, temp);

					// recursive delete
					deleteDirectory(fileDelete);
				}

				// check the directory again, if empty then delete it
				if (file.list().length == 0) {
					file.delete();
					System.out.println("Directory is deleted : " + file.getAbsolutePath());
				}
			}

		} else {
			// if file, then delete it
			file.delete();
			System.out.println("File is deleted : " + file.getAbsolutePath());
		}
	}
	
	
	public void apagaDiretorioPrincipal() {

		String state = Environment.getExternalStorageState();
		Log.i("state sd", state);
		
		File file = new File(String.format("%s/%s", 
				Environment.getExternalStorageDirectory().getPath(), 
				"maxMobile"));
		if (file.exists()) {
			try {
				deleteDirectory(file);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void criaDiretorio(String codigoDoProduto) {

		String state = Environment.getExternalStorageState();
		Log.i("state sd", state);
		
		File f = new File(String.format("%s/%s/%s/normal", Environment.getExternalStorageDirectory()
				.getPath(), "maxMobile/produtos", codigoDoProduto));
		
		if (f.mkdirs()) {
			Log.d("IMAGEM", "diretório criado");
		} else {
			Log.d("IMAGEM", "Não foi possível criar diretório: "+f.getPath());
		}
		f = new File(String.format("%s/%s/%s/mini", Environment.getExternalStorageDirectory().getPath(),
				"maxMobile/produtos", codigoDoProduto));
		if (f.mkdirs()) {
			Log.d("IMAGEM", "diretŕio mini criado");
		} else {
			Log.d("IMAGEM", "Não foi possível criar diretório mini");
		}
	}

	public void saveImage(ImagemProduto img) {
		String path = String.format("%s/%s/%d/normal", Environment.getExternalStorageDirectory().getPath(),
				"maxMobile/produtos", img.getProduto().getCodigo());
		int numeroDaImagem = 1;
		String fileName = String.format("%d%s%d.jpg", img.getProduto().getCodigo(), "_", numeroDaImagem);
		File file = new File(path, fileName);
		while (file.exists()) {
			numeroDaImagem++;
			fileName = String.format("%d%s%d.jpg", img.getProduto().getCodigo(), "_", numeroDaImagem);
			file = new File(path, fileName);
		}
		try {
			System.out.println("tamanho - " + img.getImagem().length / 1024);
			FileOutputStream out = new FileOutputStream(file);
			img.getBitmap().compress(Bitmap.CompressFormat.JPEG, 90, out);
			out.flush();
			out.close();
		} catch (Exception e) {
			Log.d(RepositorioProImagem.class.getName(), "Erro ao salvar imagem");
			Log.d(RepositorioProImagem.class.getName(), e.getCause().getMessage());
		}

	}

	public void saveImageMini(ImagemProduto img) {
		String path = String.format("%s/%s/%d/mini", Environment.getExternalStorageDirectory().getPath(),
				"maxMobile/produtos", img.getProduto().getCodigo());
		String fileName = String.format("%dmini.jpg", img.getProduto().getCodigo());
		File file = new File(path, fileName);

		try {
			FileOutputStream out = new FileOutputStream(file);
			img.getMiniBitmap().compress(Bitmap.CompressFormat.JPEG, 90, out);
			out.flush();
			out.close();
		} catch (Exception e) {
			Log.d(RepositorioProImagem.class.getName(), "Erro ao salvar imagem miniatura");
			Log.d(RepositorioProImagem.class.getName(), e.getCause().getMessage());
		}

	}

	public List<ItemGalleryProduto> listarParaGaleria(TabelaDePreco tabela) {
		List<ItemGalleryProduto> itens = Lists.newLinkedList();
		StringBuilder sb = new StringBuilder("SELECT DISTINCT(produto.codigo) CODIGO "
				+ " FROM produto "
				+ " INNER JOIN imagemproduto ON imagemproduto.produto_id = produto.codigo "
				+ " INNER JOIN produtoreduzido ON  produtoreduzido.produto_id = produto.codigo "
				+ " INNER JOIN tabelapreco_produtoreduzido ON tabelapreco_produtoreduzido.reduzidoproduto_id = produtoreduzido.red_produto_key "
				+ " INNER JOIN tabelapreco ON   tabelapreco.codigo = tabelapreco_produtoreduzido.tabelapreco_id "
				+ " WHERE   tabelapreco.codigo = ");
		sb.append(tabela.getCodigo());

		Log.d("sql", sb.toString());

		Cursor c = db.rawQuery(sb.toString(), null);
		while (c.moveToNext()) {

			itens.add(ItemGalleryProduto.newInstance(buscaPorProduto(c.getInt(c.getColumnIndex(Produto.CODIGO)))));
		}
		c.close();
		return itens;
	}
}
