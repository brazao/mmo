package max.adm.repositorio;

import java.util.ArrayList;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.entidade.CondicaoPagamento;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RepositorioCondicaoPagamento {

	private static final String NOME_TABELA = CondicaoPagamento.NOME_TABELA;

	protected SQLiteDatabase db;

	public RepositorioCondicaoPagamento(Context context) {
		db = new DBHelper(context).getWritableDatabase();
	}

	public int salvar(CondicaoPagamento condicaoPagamento) {
		int id = condicaoPagamento.getCodigo();
		if (id != 0) {
			atualizar(condicaoPagamento);
		} else {
			id = inserir(condicaoPagamento);
		}

		return id;
	}

	public int inserir(CondicaoPagamento condicaoPagamento) {
		ContentValues values = new ContentValues();
		values.put(CondicaoPagamento.DESCRICAO, condicaoPagamento.getDescricao());
		values.put(CondicaoPagamento.CODIGO, condicaoPagamento.getCodigo());

		int id = inserir(values);

		return id;
	}

	public int inserir(ContentValues values) {
		int id = (int) db.insert(CondicaoPagamento.NOME_TABELA, "", values);
		return id;
	}

	public int atualizar(CondicaoPagamento condicaoPagamento) {
		ContentValues values = new ContentValues();
		values.put(CondicaoPagamento.DESCRICAO, condicaoPagamento.getDescricao());
		values.put(CondicaoPagamento.CODIGO, condicaoPagamento.getCodigo());

		String _id = String.valueOf(condicaoPagamento.getCodigo());
		String where = CondicaoPagamento.CODIGO + "=?";
		String[] whereArgs = new String[]{_id};
		int count = atualizar(values, where, whereArgs);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public int deletar(int id) {
		String where = CondicaoPagamento.CODIGO + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[]{_id};
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(NOME_TABELA, where, whereArgs);
		return count;
	}

	public CondicaoPagamento busca(int id) {
		Cursor c = db.query(true, NOME_TABELA, CondicaoPagamento.COLUNAS, CondicaoPagamento.CODIGO + "=" + id, null, null, null, null, null);

		if (c.getCount() > 0) {
			// pocisiona no primeiro elemento
			c.moveToFirst();
			CondicaoPagamento condicaoPagamento = new CondicaoPagamento();
			condicaoPagamento.setCodigo(c.getInt(c.getColumnIndex(CondicaoPagamento.CODIGO)));
			condicaoPagamento.setDescricao(c.getString(c.getColumnIndex(CondicaoPagamento.DESCRICAO)));
			c.close();
			return condicaoPagamento;

		}
		c.close();
		return null;
	}

	// retorna cursor com todos os CondicaoPagamentoes
	public Cursor getCursor() {
		try {
			// select * frm carros
			return db.query(NOME_TABELA, CondicaoPagamento.COLUNAS, null, null, null, null, null);
		} catch (SQLException e) {
			Log.e("CondicaoPagamento", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<CondicaoPagamento> listar() {
		Cursor c = getCursor();
		List<CondicaoPagamento> condicaoPagamentos = new ArrayList<CondicaoPagamento>();

		if (c.moveToFirst()) {
			// recupera os �ndices das colunas
			int indexNome = c.getColumnIndex(CondicaoPagamento.DESCRICAO);

			// loop at� o final

			do {
				CondicaoPagamento condicaoPagamento = new CondicaoPagamento();
				condicaoPagamentos.add(condicaoPagamento);

				condicaoPagamento.setCodigo(c.getInt(c.getColumnIndex(CondicaoPagamento.CODIGO)));
				condicaoPagamento.setDescricao(c.getString(indexNome));

			} while (c.moveToNext());
		}
		c.close();
		return condicaoPagamentos;
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

	public void salvarLista(List<CondicaoPagamento> condicaoPagamentos) {
		db.beginTransaction();
		try {
			for (CondicaoPagamento cond : condicaoPagamentos) {
				try {
					this.salvar(cond);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	public void deleteAll() {
		db.delete(NOME_TABELA, null, null);
	}

}
