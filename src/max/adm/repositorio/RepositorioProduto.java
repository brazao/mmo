package max.adm.repositorio;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import max.adm.Dinheiro;
import max.adm.database.DBHelper;
import max.adm.entidade.Empresa;
import max.adm.entidade.GrupoDeProdutos;
import max.adm.entidade.Produto;
import max.adm.entidade.ProdutoReduzido;
import max.adm.entidade.TabelaDePreco;
import max.adm.entidade.TabelaPrecoProdutoReduzido;
import max.adm.produtoreduzido.repositorio.RepositorioProdutoReduzido;
import max.adm.tabelaDePreco.RepositorioTabelaDePreco;
import max.adm.utilidades.adapters.ProdutoItemPedidoParaListar;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.common.collect.Lists;

public class RepositorioProduto {

	private Context context;

	private static final String NOME_TABELA = Produto.NOME_TABELA;

	protected SQLiteDatabase db;

	public RepositorioProduto(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public int salvar(Produto produto) {
		int id = produto.getCodigo();

		if (id != 0) {
			atualizar(produto);
		} else {
			id = inserir(produto);
		}

		return id;
	}

	public static int salvar(Produto produto, SQLiteDatabase db) {
		int id = produto.getCodigo();

		if (id != 0) {
			atualizar(produto, db);
		} else {
			id = inserir(produto, db);
		}

		return id;
	}

	public int inserir(Produto produto) {
		ContentValues values = new ContentValues();
		values.put(Produto.DESCRICAO, produto.getDescricao());
		values.put(Produto.CODIGO, produto.getCodigo());
		values.put(Produto.MASCARACODIGO, produto.getMascaraCodigo());
		values.put(Produto.DESCRICAOREDUZIDA, produto.getDescricaoReduzida());
		values.put(Produto.GRUPODEPRODUTO, produto.getGrupoDeProduto().getCodigo());

		int id = inserir(values, produto);

		return id;
	}

	public static int inserir(Produto produto, SQLiteDatabase db) {
		ContentValues values = new ContentValues();
		values.put(Produto.DESCRICAO, produto.getDescricao());
		values.put(Produto.CODIGO, produto.getCodigo());
		values.put(Produto.MASCARACODIGO, produto.getMascaraCodigo());
		values.put(Produto.DESCRICAOREDUZIDA, produto.getDescricaoReduzida());
		values.put(Produto.GRUPODEPRODUTO, produto.getGrupoDeProduto().getCodigo());

		int id = inserir(values, produto, db);

		return id;
	}

	public int inserir(ContentValues values, Produto produto) {
		int id = 0;
		db.beginTransaction();
		try {

			id = (int) db.insert(Produto.NOME_TABELA, "", values);

			db.setTransactionSuccessful();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			db.endTransaction();
		}

		return id;
	}

	public static int inserir(ContentValues values, Produto produto, SQLiteDatabase db) {
		int id = 0;
		db.beginTransaction();
		try {

			id = (int) db.insert(Produto.NOME_TABELA, "", values);

			db.setTransactionSuccessful();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			db.endTransaction();
		}

		return id;
	}

	public int atualizar(Produto produto) {
		ContentValues values = new ContentValues();
		values.put(Produto.DESCRICAO, produto.getDescricao());
		values.put(Produto.MASCARACODIGO, produto.getMascaraCodigo());
		values.put(Produto.DESCRICAOREDUZIDA, produto.getDescricaoReduzida());
		values.put(Produto.GRUPODEPRODUTO, produto.getGrupoDeProduto().getCodigo());

		String _id = String.valueOf(produto.getCodigo());
		String where = Produto.CODIGO + "=?";
		String[] whereArgs = new String[] { _id };
		int count = atualizar(values, where, whereArgs, produto);

		return count;
	}

	public static int atualizar(Produto produto, SQLiteDatabase db) {
		ContentValues values = new ContentValues();
		values.put(Produto.DESCRICAO, produto.getDescricao());
		values.put(Produto.MASCARACODIGO, produto.getMascaraCodigo());
		values.put(Produto.DESCRICAOREDUZIDA, produto.getDescricaoReduzida());
		values.put(Produto.GRUPODEPRODUTO, produto.getGrupoDeProduto().getCodigo());

		String _id = String.valueOf(produto.getCodigo());
		String where = Produto.CODIGO + "=?";
		String[] whereArgs = new String[] { _id };
		int count = atualizar(values, where, whereArgs, produto, db);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs, Produto produto) {
		int count;
		db.beginTransaction();
		try {
			count = db.update(NOME_TABELA, valores, where, whereArgs);
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}

		return count;
	}

	public static int atualizar(ContentValues valores, String where, String[] whereArgs, Produto produto,
			SQLiteDatabase db) {
		int count;
		db.beginTransaction();
		try {
			count = db.update(NOME_TABELA, valores, where, whereArgs);
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}

		return count;
	}

	public int deletar(int id) {
		String where = Produto.CODIGO + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[] { _id };
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(NOME_TABELA, where, whereArgs);
		return count;
	}

	public Produto busca(String maskCod) {
		Cursor c = db.query(true, NOME_TABELA, Produto.COLUNAS, Produto.MASCARACODIGO + "='" + maskCod + "'",
				null, null, null, null, null);
		RepositorioGrupoDeProdutos repoGrupoProd = new RepositorioGrupoDeProdutos(context);
		if (c.getCount() > 0) {
			// pocisiona no primeiro elemento
			c.moveToFirst();
			Produto produto = new Produto();
			produto.setCodigo(c.getInt(c.getColumnIndex(Produto.CODIGO)));
			produto.setDescricao(c.getString(c.getColumnIndex(Produto.DESCRICAO)));
			produto.setMascaraCodigo(c.getString(c.getColumnIndex(Produto.MASCARACODIGO)));
			produto.setDescricaoReduzida(c.getString(c.getColumnIndex(Produto.DESCRICAOREDUZIDA)));
			produto.setGrupoDeProduto(repoGrupoProd.busca(c.getInt(c.getColumnIndex(Produto.GRUPODEPRODUTO))));
			repoGrupoProd.fechar();

			return produto;

		}
		return null;
	}

	public Produto busca(int id) {
		Cursor c = db.query(true, NOME_TABELA, Produto.COLUNAS, Produto.CODIGO + "=" + id, null, null, null,
				null, null);
		RepositorioGrupoDeProdutos repoGrupoProd = new RepositorioGrupoDeProdutos(context);
		if (c.getCount() > 0) {
			// pocisiona no primeiro elemento
			c.moveToFirst();
			Produto produto = new Produto();
			produto.setCodigo(c.getInt(c.getColumnIndex(Produto.CODIGO)));
			produto.setDescricao(c.getString(c.getColumnIndex(Produto.DESCRICAO)));
			produto.setMascaraCodigo(c.getString(c.getColumnIndex(Produto.MASCARACODIGO)));
			produto.setDescricaoReduzida(c.getString(c.getColumnIndex(Produto.DESCRICAOREDUZIDA)));
			produto.setGrupoDeProduto(repoGrupoProd.busca(c.getInt(c.getColumnIndex(Produto.GRUPODEPRODUTO))));
			repoGrupoProd.fechar();

			c.close();
			return produto;

		}
		c.close();
		return null;
	}

	// retorna cursor com todos os Produtoes
	public Cursor getCursor() {
		try {
			// select * frm carros
			return db.query(NOME_TABELA, Produto.COLUNAS, null, null, null, null, null);
		} catch (SQLException e) {
			Log.e("Produto", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<Integer> listarProCodigos() {
		List<Integer> codigos = new ArrayList<Integer>();
		Cursor c = db.query(NOME_TABELA, new String[] { Produto.CODIGO }, null, null, null, null, null);
		if (c.moveToFirst()) {
			do {
				codigos.add(c.getInt(c.getColumnIndex(Produto.CODIGO)));

			} while (c.moveToNext());
		}
		c.close();
		return codigos;
	}

	public List<Produto> listar() {
		Cursor c = getCursor();
		List<Produto> produtos = new ArrayList<Produto>();

		RepositorioGrupoDeProdutos repoGrupoProd = new RepositorioGrupoDeProdutos(context);

		if (c.moveToFirst()) {

			// loop ate o final

			do {
				Produto produto = new Produto();
				produtos.add(produto);

				produto.setCodigo(c.getInt(c.getColumnIndex(Produto.CODIGO)));
				produto.setDescricao(c.getString(c.getColumnIndex(Produto.DESCRICAO)));
				produto.setMascaraCodigo(c.getString(c.getColumnIndex(Produto.MASCARACODIGO)));
				produto.setDescricaoReduzida(c.getString(c.getColumnIndex(Produto.DESCRICAOREDUZIDA)));
				produto.setGrupoDeProduto(repoGrupoProd.busca(c.getInt(c
						.getColumnIndex(Produto.GRUPODEPRODUTO))));

			} while (c.moveToNext());
		}

		return produtos;
	}

	public List<Produto> listarParaTela(int idTabelaPreco) {
		String sql = "SELECT distinct(" + Produto.MASCARACODIGO + ") as " + Produto.MASCARACODIGO + ", "
				+ Produto.CODIGO + ", " + Produto.DESCRICAO + ", " + Produto.CODIGO + " FROM "
				+ Produto.NOME_TABELA +

				" INNER JOIN " + NOME_TABELA + " on " + Produto.CODIGO + " = " + ProdutoReduzido.PRODUTO +

				" WHERE " + TabelaPrecoProdutoReduzido.TABELAPRECO + " = " + idTabelaPreco;

		Cursor c = db.rawQuery(sql, null);

		List<Produto> produtos = new ArrayList<Produto>();

		while (c.moveToNext()) {
			Produto produto = new Produto();
			produtos.add(produto);

			produto.setMascaraCodigo(c.getString(c.getColumnIndex(Produto.MASCARACODIGO)));
			produto.setCodigo(c.getInt(c.getColumnIndex(Produto.CODIGO)));
			produto.setDescricao(c.getString(c.getColumnIndex(Produto.DESCRICAO)));
			produto.setCodigo(c.getInt(c.getColumnIndex(Produto.CODIGO)));
		}
		System.out.println(produtos.size());
		c.close();
		return produtos;
	}

	public List<Produto> listarPorTabelas(String[] tabelas) {
		String where = "";
		for (int i = 0; i < tabelas.length; i++) {
			if (i < tabelas.length - 1) {
				where += tabelas[i] + ",";
			} else {
				where += tabelas[i];
			}
		}

		String sql = "SELECT distinct(" + Produto.MASCARACODIGO + ") as '" + Produto.MASCARACODIGO + "', "
				+ Produto.CODIGO + ", " + Produto.DESCRICAO + ", " + Produto.CODIGO + ", "
				+ GrupoDeProdutos.CODIGO + ", " + GrupoDeProdutos.DESCRICAO + " FROM " + Produto.NOME_TABELA
				+ " INNER JOIN " + ProdutoReduzido.NOME_TABELA + " on " + Produto.CODIGO + " = "
				+ ProdutoReduzido.PRODUTO + " INNER JOIN " + GrupoDeProdutos.NOME_TABELA + " on "
				+ Produto.GRUPODEPRODUTO + " = " + GrupoDeProdutos.CODIGO + " WHERE "
				+ TabelaPrecoProdutoReduzido.TABELAPRECO + " in (" + where + ")";

		Log.d("sql", sql);

		Cursor c = db.rawQuery(sql, null);

		List<Produto> produtos = new ArrayList<Produto>();

		if (c.moveToFirst()) {
			do {
				Produto produto = new Produto();
				produtos.add(produto);

				produto.setMascaraCodigo(c.getString(c.getColumnIndex(Produto.MASCARACODIGO)));
				produto.setCodigo(c.getInt(c.getColumnIndex(Produto.CODIGO)));
				produto.setDescricao(c.getString(c.getColumnIndex(Produto.DESCRICAO)));

				GrupoDeProdutos grupo = new GrupoDeProdutos();
				grupo.setCodigo(c.getInt(c.getColumnIndex(GrupoDeProdutos.CODIGO)));
				grupo.setDescricao(c.getString(c.getColumnIndex(GrupoDeProdutos.DESCRICAO)));
				produto.setGrupoDeProduto(grupo);

			} while (c.moveToNext());
		}

		return produtos;
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

	public List<ProdutoItemPedidoParaListar> listarPorTabelas(List<TabelaDePreco> tabelas, Empresa empresa) {
		List<ProdutoItemPedidoParaListar> proParaListarList = Lists.newLinkedList();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT TABELAPRECO_PRODUTOREDUZIDO.* ")
		.append("FROM  PRODUTOREDUZIDO ")
		.append("INNER JOIN TABELAPRECO_PRODUTOREDUZIDO ON TABELAPRECO_PRODUTOREDUZIDO.REDUZIDOPRODUTO_ID = PRODUTOREDUZIDO.RED_PRODUTO_KEY  ")
		.append("where ")
		.append("TABELAPRECO_PRODUTOREDUZIDO.TABELAPRECO_ID in ( ");
		
		for (int i = 0; i < tabelas.size(); i++) {
			TabelaDePreco t = tabelas.get(i);
			if (i == 0)
				sb.append(t.getCodigo());
			else
				sb.append("," + t.getCodigo());
		}
		sb.append(")");
		sb.append(" order by "+ProdutoReduzido.PRODUTO);
		sb.append(" LIMIT 50 ");

		Log.d("debug", sb.toString());
		
		Cursor cursor = db.rawQuery(sb.toString(), null);

		RepositorioProdutoReduzido repoProdutoReduzido = new RepositorioProdutoReduzido(context);
		RepositorioTabelaDePreco repoTabelaDePreco = new RepositorioTabelaDePreco(context);
		
		int idProd = 0;
		
		while (cursor.moveToNext()) {
			Log.i("entrou no while", "aaaaa");
			
			ProdutoReduzido produtoReduzido = repoProdutoReduzido.busca(
					cursor.getInt(cursor.getColumnIndex(TabelaPrecoProdutoReduzido.PRODUTOREDUZIDO)));
			
			TabelaDePreco tabelaDePreco = repoTabelaDePreco.busca(cursor.getInt(cursor.getColumnIndex(TabelaPrecoProdutoReduzido.TABELAPRECO)));
			
			
			Double precoD = cursor.getDouble(cursor.getColumnIndex(TabelaPrecoProdutoReduzido.PRECO));
			Dinheiro preco = Dinheiro.newInstance(new BigDecimal(String.valueOf(precoD)));
			
			TabelaPrecoProdutoReduzido tabelaPrecoProdutoReduzido = 
					TabelaPrecoProdutoReduzido.newInstance(empresa, 
							preco, 
							produtoReduzido, 
							tabelaDePreco);
			
			//Integer reduzidoId = cursor.getInt(cursor.getColumnIndex(TabelaPrecoProdutoReduzido.PRODUTOREDUZIDO));
			
			if (idProd == tabelaPrecoProdutoReduzido.getProdutoReduzido().getProduto().getCodigo()) {
				proParaListarList.get(proParaListarList.size() - 1).getReduzidos().add(tabelaPrecoProdutoReduzido);
			} else {
				List<TabelaPrecoProdutoReduzido> tabelaPrecoProdutoReduzidos = Lists.newLinkedList();
				tabelaPrecoProdutoReduzidos.add(tabelaPrecoProdutoReduzido);
				proParaListarList.add(ProdutoItemPedidoParaListar.newInstance(produtoReduzido.getProduto(), tabelaPrecoProdutoReduzidos));
			}

			idProd = tabelaPrecoProdutoReduzido.getProdutoReduzido().getProduto().getCodigo();
			Log.i("entrou no while", "bbbbb");

		}
		repoProdutoReduzido.fechar();
		repoTabelaDePreco.fechar();
		cursor.close();
		
		return proParaListarList;
	}

	public List<Integer> buscaCodigos() {
		List<Integer> codigos = Lists.newLinkedList();
		Cursor c = db.query(NOME_TABELA, new String[] { Produto.CODIGO }, null, null, null, null, null);
		if (c.moveToFirst()) {
			do {
				codigos.add(c.getInt(c.getColumnIndex(Produto.CODIGO)));
			} while (c.moveToNext());
		}
		c.close();
		return codigos;
	}

}
