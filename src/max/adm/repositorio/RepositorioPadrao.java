package max.adm.repositorio;

import java.util.ArrayList;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.entidade.PadraoProduto;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RepositorioPadrao {

	private SQLiteDatabase db;

	private static final String NOME_TABELA = PadraoProduto.NOME_TABELA;

	public RepositorioPadrao(Context context) {
		db = new DBHelper(context).getWritableDatabase();
	}

	public int salvar(PadraoProduto padrao) {
		int id = padrao.getCodigo();
		if (id != 0) {
			atualizar(padrao);
		} else {
			id = inserir(padrao);
		}

		return id;
	}

	public static int salvar(PadraoProduto padrao, SQLiteDatabase db) {
		int id = padrao.getCodigo();
		if (id != 0) {
			atualizar(padrao, db);
		} else {
			id = inserir(padrao, db);
		}

		return id;
	}

	public boolean salvarLista(List<PadraoProduto> padroes) {
		for (PadraoProduto p : padroes) {
			try {
				salvar(p);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return true;
	}

	public int inserir(PadraoProduto padrao) {
		ContentValues values = new ContentValues();
		values.put(PadraoProduto.CODIGO, padrao.getCodigo());
		values.put(PadraoProduto.DESCRICAO, padrao.getDescricao());
		values.put(PadraoProduto.SIGLA, padrao.getSigla());

		int id = inserir(values);

		return id;
	}

	public static int inserir(PadraoProduto padrao, SQLiteDatabase db) {
		ContentValues values = new ContentValues();
		values.put(PadraoProduto.CODIGO, padrao.getCodigo());
		values.put(PadraoProduto.DESCRICAO, padrao.getDescricao());
		values.put(PadraoProduto.SIGLA, padrao.getSigla());

		int id = inserir(values, db);

		return id;
	}

	public int inserir(ContentValues values) {
		int id = (int) db.insert(PadraoProduto.NOME_TABELA, "", values);
		return id;
	}

	public static int inserir(ContentValues values, SQLiteDatabase db) {
		int id = (int) db.insert(PadraoProduto.NOME_TABELA, "", values);
		return id;
	}

	public int atualizar(PadraoProduto padrao) {
		ContentValues values = new ContentValues();
		values.put(PadraoProduto.CODIGO, padrao.getCodigo());
		values.put(PadraoProduto.DESCRICAO, padrao.getDescricao());
		values.put(PadraoProduto.SIGLA, padrao.getSigla());

		String _id = String.valueOf(padrao.getCodigo());
		String where = PadraoProduto.CODIGO + "=?";
		String[] whereArgs = new String[] { _id };
		int count = atualizar(values, where, whereArgs);

		return count;
	}

	public static int atualizar(PadraoProduto padrao, SQLiteDatabase db) {
		ContentValues values = new ContentValues();
		values.put(PadraoProduto.CODIGO, padrao.getCodigo());
		values.put(PadraoProduto.DESCRICAO, padrao.getDescricao());
		values.put(PadraoProduto.SIGLA, padrao.getSigla());

		String _id = String.valueOf(padrao.getCodigo());
		String where = PadraoProduto.CODIGO + "=?";
		String[] whereArgs = new String[] { _id };
		int count = atualizar(values, where, whereArgs, db);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public static int atualizar(ContentValues valores, String where, String[] whereArgs, SQLiteDatabase db) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public int deletar(int id) {
		String where = PadraoProduto.CODIGO + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[] { _id };
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(NOME_TABELA, where, whereArgs);
		return count;
	}

	public PadraoProduto busca(int id) {
		Cursor c = db.query(true, NOME_TABELA, PadraoProduto.COLUNAS, PadraoProduto.CODIGO + "=" + id, null,
				null, null, null, null);

		if (c.moveToFirst()) {
			PadraoProduto padrao = PadraoProduto.newPadrao(c.getInt(c.getColumnIndex(PadraoProduto.CODIGO)),
					c.getString(c.getColumnIndex(PadraoProduto.DESCRICAO)),
					c.getString(c.getColumnIndex(PadraoProduto.SIGLA)));

			c.close();
			return padrao;
		}
		c.close();
		return null;
	}

	// retorna cursor com todos os Padraoes
	public Cursor getCursor() {
		try {
			// select * frm carros
			return db.query(NOME_TABELA, PadraoProduto.COLUNAS, null, null, null, null, null);
		} catch (SQLException e) {
			Log.e("Padrao", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<PadraoProduto> listar() {
		Cursor c = getCursor();
		List<PadraoProduto> padraos = new ArrayList<PadraoProduto>();
		while (c.moveToNext()) {

			PadraoProduto padrao = PadraoProduto.newPadrao(c.getInt(c.getColumnIndex(PadraoProduto.CODIGO)),
					c.getString(c.getColumnIndex(PadraoProduto.DESCRICAO)),
					c.getString(c.getColumnIndex(PadraoProduto.SIGLA)));
			padraos.add(padrao);

		}

		return padraos;
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}
}
