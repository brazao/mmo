package max.adm.repositorio;

import java.util.ArrayList;
import java.util.List;

import max.adm.classificacaoPessoa.RepositorioClassificacaoPessoa;
import max.adm.database.DBHelper;
import max.adm.empresa.RepositorioEmpresa;
import max.adm.entidade.ClassifcacaoPessoa;
import max.adm.entidade.Empresa;
import max.adm.entidade.Pessoa;
import max.adm.entidade.PessoaClassifcacaoPessoa;
import max.adm.pessoa.repositorio.RepositorioPessoa;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class RepositorioPessoaClassificacaoPessoa {

	private Context context;
	private SQLiteDatabase db;

	public RepositorioPessoaClassificacaoPessoa(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public List<PessoaClassifcacaoPessoa> listar() {
		List<PessoaClassifcacaoPessoa> lista = new ArrayList<PessoaClassifcacaoPessoa>();

		Cursor cursor = db.query(PessoaClassifcacaoPessoa.NOME_TABELA, PessoaClassifcacaoPessoa.COLUNAS,
				null, null, null, null, null);

		RepositorioPessoa repositorioPessoa = new RepositorioPessoa(context);
		RepositorioEmpresa repositorioEmpresa = new RepositorioEmpresa(context);
		RepositorioClassificacaoPessoa repositorioClassificacaoPessoa = new RepositorioClassificacaoPessoa(
				context);

		while (cursor.moveToNext()) {
			
			Empresa empresa = repositorioEmpresa.busca(cursor.getInt(cursor.getColumnIndex(PessoaClassifcacaoPessoa.EMPRESA)));
			Pessoa pessoa = repositorioPessoa.busca(cursor.getInt(cursor.getColumnIndex(PessoaClassifcacaoPessoa.PESSOA)), empresa.getNumeroLoja());
			ClassifcacaoPessoa classifcacaoPessoa = repositorioClassificacaoPessoa.busca(cursor.getInt(cursor
					.getColumnIndex(PessoaClassifcacaoPessoa.CLASSIFICACAOPESSOA)));

			PessoaClassifcacaoPessoa pessoaClassifcacaoPessoa = new PessoaClassifcacaoPessoa();
			pessoaClassifcacaoPessoa.setPessoa(pessoa);
			pessoaClassifcacaoPessoa.setEmpresa(empresa);
			pessoaClassifcacaoPessoa.setClassifcacaoPessoa(classifcacaoPessoa);

			lista.add(pessoaClassifcacaoPessoa);
		}
		repositorioClassificacaoPessoa.fechar();
		repositorioEmpresa.fechar();
		repositorioPessoa.fechar();

		return lista;
	}

	public List<PessoaClassifcacaoPessoa> busca(int pessoaId, int empresaId) {
		List<PessoaClassifcacaoPessoa> lista = new ArrayList<PessoaClassifcacaoPessoa>();

		Cursor cursor = db.query(PessoaClassifcacaoPessoa.NOME_TABELA, PessoaClassifcacaoPessoa.COLUNAS,
				PessoaClassifcacaoPessoa.PESSOA + "=? and " + PessoaClassifcacaoPessoa.EMPRESA + "=?",
				new String[] { String.valueOf(pessoaId), String.valueOf(empresaId) }, null, null, null);

		RepositorioPessoa repositorioPessoa = new RepositorioPessoa(context);
		RepositorioEmpresa repositorioEmpresa = new RepositorioEmpresa(context);
		RepositorioClassificacaoPessoa repositorioClassificacaoPessoa = new RepositorioClassificacaoPessoa(
				context);

		while (cursor.moveToNext()) {			
			Empresa empresa = repositorioEmpresa.busca(cursor.getInt(cursor.getColumnIndex(PessoaClassifcacaoPessoa.EMPRESA)));
			Pessoa pessoa = repositorioPessoa.busca(cursor.getInt(cursor.getColumnIndex(PessoaClassifcacaoPessoa.PESSOA)), empresa.getNumeroLoja());
			ClassifcacaoPessoa classifcacaoPessoa = repositorioClassificacaoPessoa.busca(cursor.getInt(cursor
					.getColumnIndex(PessoaClassifcacaoPessoa.CLASSIFICACAOPESSOA)));

			PessoaClassifcacaoPessoa pessoaClassifcacaoPessoa = new PessoaClassifcacaoPessoa();
			pessoaClassifcacaoPessoa.setPessoa(pessoa);
			pessoaClassifcacaoPessoa.setEmpresa(empresa);
			pessoaClassifcacaoPessoa.setClassifcacaoPessoa(classifcacaoPessoa);

			lista.add(pessoaClassifcacaoPessoa);
		}
		repositorioClassificacaoPessoa.fechar();
		repositorioEmpresa.fechar();
		repositorioPessoa.fechar();

		return lista;
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

	public void salvar(PessoaClassifcacaoPessoa pessoaClassifcacaoPessoa) {
		ContentValues values = new ContentValues();

		values.put(PessoaClassifcacaoPessoa.CLASSIFICACAOPESSOA, pessoaClassifcacaoPessoa.getClassifcacaoPessoa().getCodigo());
		values.put(PessoaClassifcacaoPessoa.EMPRESA, pessoaClassifcacaoPessoa.getEmpresa().getNumeroLoja());
		values.put(PessoaClassifcacaoPessoa.PESSOA, pessoaClassifcacaoPessoa.getPessoa().getCodigo());

		if (existeRegistro(pessoaClassifcacaoPessoa)) {
			db.update(
					PessoaClassifcacaoPessoa.NOME_TABELA,
					values,
					PessoaClassifcacaoPessoa.PESSOA + "=? and " + PessoaClassifcacaoPessoa.EMPRESA + "=? and "
							+ PessoaClassifcacaoPessoa.CLASSIFICACAOPESSOA + "=?",
					new String[] { String.valueOf(pessoaClassifcacaoPessoa.getPessoa().getCodigo()),
							String.valueOf(pessoaClassifcacaoPessoa.getEmpresa().getNumeroLoja()),
							String.valueOf(pessoaClassifcacaoPessoa.getClassifcacaoPessoa().getCodigo()) });
		} else {
			db.insert(PessoaClassifcacaoPessoa.NOME_TABELA, null, values);
		}
	}

	private Boolean existeRegistro(PessoaClassifcacaoPessoa pessoaClassifcacaoPessoa) {
		Cursor cursor = db.query(
				PessoaClassifcacaoPessoa.NOME_TABELA,
				PessoaClassifcacaoPessoa.COLUNAS,
				PessoaClassifcacaoPessoa.PESSOA + "=? and " + PessoaClassifcacaoPessoa.EMPRESA + "=? and "
						+ PessoaClassifcacaoPessoa.CLASSIFICACAOPESSOA + "=?",
				new String[] { String.valueOf(pessoaClassifcacaoPessoa.getPessoa().getCodigo()),
						String.valueOf(pessoaClassifcacaoPessoa.getEmpresa().getNumeroLoja()),
						String.valueOf(pessoaClassifcacaoPessoa.getClassifcacaoPessoa().getCodigo()) }, null,
				null, null);

		return cursor.moveToFirst();

	}

}
