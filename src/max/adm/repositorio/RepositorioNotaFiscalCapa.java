package max.adm.repositorio;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import max.adm.Dinheiro;
import max.adm.database.DBHelper;
import max.adm.empresa.RepositorioEmpresa;
import max.adm.entidade.NotaFiscalCapa;
import max.adm.entidade.NotaFiscalItem;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RepositorioNotaFiscalCapa {

	private Context context;
	protected SQLiteDatabase db;

	public RepositorioNotaFiscalCapa(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public NotaFiscalCapa busca(int id) {
		Cursor c = db.query(true, NotaFiscalCapa.NOME_TABELA, NotaFiscalCapa.COLUNAS, NotaFiscalCapa.CODIGO + "=" + id,
				null, null, null, null, null);

		RepositorioEmpresa repoEmpresa = new RepositorioEmpresa(context);
		if (c.moveToFirst()) {
			int notaFiscalId = c.getInt(c.getColumnIndex(NotaFiscalCapa.CODIGO));
			
			NotaFiscalCapa notaFiscalCapa = new NotaFiscalCapa();
			notaFiscalCapa.setId(notaFiscalId);
			notaFiscalCapa.setNumeroNota(c.getInt(c.getColumnIndex(NotaFiscalCapa.NUMERONOTA)));
			notaFiscalCapa.setSerie(c.getString(c.getColumnIndex(NotaFiscalCapa.SERIE)));
			notaFiscalCapa.setPessoa(c.getInt(c.getColumnIndex(NotaFiscalCapa.PESSOA)));
			notaFiscalCapa.setNumeroPedido(c.getInt(c.getColumnIndex(NotaFiscalCapa.PEDIDONUMERO)));
			notaFiscalCapa.setDataemissao(new Date(c.getLong(c.getColumnIndex(NotaFiscalCapa.DATAEMISSAO))));
			notaFiscalCapa.setEmpresa(c.getInt(c.getColumnIndex(NotaFiscalCapa.EMPRESA)));
			Double valorTotalD = c.getDouble(c.getColumnIndex(NotaFiscalCapa.VALORTOTAL));
			notaFiscalCapa.setValorTotal(Dinheiro.newInstance(new BigDecimal(String.valueOf(valorTotalD))));
			
			RepositorioNotaFiscalItem repoNotaFiscalItem = new RepositorioNotaFiscalItem(context);
			List<NotaFiscalItem> itens = repoNotaFiscalItem.buscaItensPorNota(notaFiscalId);
			repoNotaFiscalItem.fechar();
			notaFiscalCapa.setItens(itens);
			
			repoEmpresa.fechar();

			return notaFiscalCapa;
		}
		return null;
	}

	// retorna cursor com todos os NotaFiscalCapaes
	public Cursor getCursor() {
		try {
			return db.query(NotaFiscalCapa.NOME_TABELA, NotaFiscalCapa.COLUNAS, null, null, null, null, null);
		} catch (SQLException e) {
			Log.e("NotaFiscalCapa", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<NotaFiscalCapa> listar() {
		Cursor c = getCursor();
		List<NotaFiscalCapa> notaFiscalCapas = new ArrayList<NotaFiscalCapa>();

		if (c.moveToFirst()) {

			// loop at� o final

			do {
				NotaFiscalCapa notaFiscalCapa = new NotaFiscalCapa();
				notaFiscalCapas.add(notaFiscalCapa);

				notaFiscalCapa.setId(c.getInt(c.getColumnIndex(NotaFiscalCapa.CODIGO)));
				notaFiscalCapa.setNumeroNota(c.getInt(c.getColumnIndex(NotaFiscalCapa.NUMERONOTA)));
				notaFiscalCapa.setSerie(c.getString(c.getColumnIndex(NotaFiscalCapa.SERIE)));
				notaFiscalCapa.setPessoa(c.getInt(c.getColumnIndex(NotaFiscalCapa.PESSOA)));
				notaFiscalCapa.setNumeroPedido(c.getInt(c.getColumnIndex(NotaFiscalCapa.PEDIDONUMERO)));
				notaFiscalCapa.setDataemissao(new Date(c.getLong(c.getColumnIndex(NotaFiscalCapa.DATAEMISSAO))));
				notaFiscalCapa.setEmpresa(c.getInt(c.getColumnIndex(NotaFiscalCapa.EMPRESA)));
				Double valorTotalD = c.getDouble(c.getColumnIndex(NotaFiscalCapa.VALORTOTAL));
				notaFiscalCapa.setValorTotal(Dinheiro.newInstance(new BigDecimal(String.valueOf(valorTotalD))));
				

			} while (c.moveToNext());
		}

		return notaFiscalCapas;
	}

	public NotaFiscalCapa buscaUltimaCompra(int codigoCliente) {

		String sql = "SELECT " + NotaFiscalCapa.CODIGO + ", " + NotaFiscalCapa.NUMERONOTA + ", "
				+ NotaFiscalCapa.SERIE + ", " + NotaFiscalCapa.PESSOA + ", " + NotaFiscalCapa.PEDIDONUMERO
				+ ", " + NotaFiscalCapa.DATAEMISSAO + ", " + NotaFiscalCapa.EMPRESA + ", " + NotaFiscalCapa.VALORTOTAL  
				+ "  FROM "	+ NotaFiscalCapa.NOME_TABELA + " WHERE " + NotaFiscalCapa.PESSOA + " = " + codigoCliente
				+ " AND " + NotaFiscalCapa.DATAEMISSAO + " = (SELECT MAX(" + NotaFiscalCapa.DATAEMISSAO
				+ ") FROM " + NotaFiscalCapa.NOME_TABELA + " where " + NotaFiscalCapa.PESSOA + " = "
				+ codigoCliente + ")" + "group by " + NotaFiscalCapa.DATAEMISSAO;

		Cursor c = db.rawQuery(sql, null);

		if (c.moveToFirst()) {
			
			int notaFiscalId = c.getInt(c.getColumnIndex(NotaFiscalCapa.CODIGO));
			
			NotaFiscalCapa notaFiscalCapa = new NotaFiscalCapa();
			notaFiscalCapa.setId(notaFiscalId);
			notaFiscalCapa.setNumeroNota(c.getInt(c.getColumnIndex(NotaFiscalCapa.NUMERONOTA)));
			notaFiscalCapa.setSerie(c.getString(c.getColumnIndex(NotaFiscalCapa.SERIE)));
			notaFiscalCapa.setPessoa(c.getInt(c.getColumnIndex(NotaFiscalCapa.PESSOA)));
			notaFiscalCapa.setNumeroPedido(c.getInt(c.getColumnIndex(NotaFiscalCapa.PEDIDONUMERO)));
			notaFiscalCapa.setDataemissao(new Date(c.getLong(c.getColumnIndex(NotaFiscalCapa.DATAEMISSAO))));
			notaFiscalCapa.setEmpresa(c.getInt(c.getColumnIndex(NotaFiscalCapa.EMPRESA)));
			Double valorTotalD = c.getDouble(c.getColumnIndex(NotaFiscalCapa.VALORTOTAL));
			notaFiscalCapa.setValorTotal(Dinheiro.newInstance(new BigDecimal(String.valueOf(valorTotalD))));
			
			RepositorioNotaFiscalItem repoNotaFiscalItem = new RepositorioNotaFiscalItem(context);
			List<NotaFiscalItem> itens = repoNotaFiscalItem.buscaItensPorNota(notaFiscalId);
			repoNotaFiscalItem.fechar();
			notaFiscalCapa.setItens(itens);

			return notaFiscalCapa;

		}
		return null;

	}

	/**
	 * busca a primeira nota (da base de dados ), considerando a data de
	 * emiss�o.
	 * 
	 * @param codigoCliente
	 *            int
	 * @return NotaFiscalCapa notafiscal
	 */
	public NotaFiscalCapa buscaPrimeiraCompra(int codigoCliente) {

		String sql = "SELECT " + NotaFiscalCapa.CODIGO + ", " + NotaFiscalCapa.NUMERONOTA + ", "
				+ NotaFiscalCapa.SERIE + ", " + NotaFiscalCapa.PESSOA + ", " + NotaFiscalCapa.PEDIDONUMERO
				+ ", " + NotaFiscalCapa.DATAEMISSAO + ", " + NotaFiscalCapa.EMPRESA + ", "+ NotaFiscalCapa.VALORTOTAL  
				+ "  FROM " + NotaFiscalCapa.NOME_TABELA + " WHERE " + NotaFiscalCapa.PESSOA + " = " + codigoCliente
				+ " AND " + NotaFiscalCapa.DATAEMISSAO + " = (SELECT MIN(" + NotaFiscalCapa.DATAEMISSAO
				+ ") FROM " + NotaFiscalCapa.NOME_TABELA + " where " + NotaFiscalCapa.PESSOA + " = "
				+ codigoCliente + ")" + " group by " + NotaFiscalCapa.DATAEMISSAO;

		Cursor c = db.rawQuery(sql, null);

		if (c.moveToFirst()) {
			int notaFiscalId = c.getInt(c.getColumnIndex(NotaFiscalCapa.CODIGO));

			NotaFiscalCapa notaFiscalCapa = new NotaFiscalCapa();
			notaFiscalCapa.setId(notaFiscalId);
			notaFiscalCapa.setNumeroNota(c.getInt(c.getColumnIndex(NotaFiscalCapa.NUMERONOTA)));
			notaFiscalCapa.setSerie(c.getString(c.getColumnIndex(NotaFiscalCapa.SERIE)));
			notaFiscalCapa.setPessoa(c.getInt(c.getColumnIndex(NotaFiscalCapa.PESSOA)));
			notaFiscalCapa.setNumeroPedido(c.getInt(c.getColumnIndex(NotaFiscalCapa.PEDIDONUMERO)));
			notaFiscalCapa.setDataemissao(new Date(c.getLong(c.getColumnIndex(NotaFiscalCapa.DATAEMISSAO))));
			notaFiscalCapa.setEmpresa(c.getInt(c.getColumnIndex(NotaFiscalCapa.EMPRESA)));
			Double valorTotalD = c.getDouble(c.getColumnIndex(NotaFiscalCapa.VALORTOTAL));
			notaFiscalCapa.setValorTotal(Dinheiro.newInstance(new BigDecimal(String.valueOf(valorTotalD))));
			
			RepositorioNotaFiscalItem repoNotaFiscalItem = new RepositorioNotaFiscalItem(context);
			List<NotaFiscalItem> itens = repoNotaFiscalItem.buscaItensPorNota(notaFiscalId);
			repoNotaFiscalItem.fechar();
			notaFiscalCapa.setItens(itens);

			return notaFiscalCapa;
		}
		return null;

	}
	
	public List<NotaFiscalCapa> buscaPorCliente(int codigoCliente) {
		List<NotaFiscalCapa> notas = new ArrayList<NotaFiscalCapa>();
		Cursor c = db.query(
				true, 
				NotaFiscalCapa.NOME_TABELA, 
				NotaFiscalCapa.COLUNAS, 
				NotaFiscalCapa.PESSOA+"=?", 
				new String[] {String.valueOf(codigoCliente)}, 
				null, null, NotaFiscalCapa.CODIGO, null);
		
		RepositorioNotaFiscalItem repoNotaFiscalItem = new RepositorioNotaFiscalItem(context);
		while (c.moveToNext()) {

			int notaFiscalId = c.getInt(c.getColumnIndex(NotaFiscalCapa.CODIGO));

			NotaFiscalCapa notaFiscalCapa = new NotaFiscalCapa();
			notaFiscalCapa.setId(notaFiscalId);
			notaFiscalCapa.setNumeroNota(c.getInt(c.getColumnIndex(NotaFiscalCapa.NUMERONOTA)));
			notaFiscalCapa.setSerie(c.getString(c.getColumnIndex(NotaFiscalCapa.SERIE)));
			notaFiscalCapa.setPessoa(c.getInt(c.getColumnIndex(NotaFiscalCapa.PESSOA)));
			notaFiscalCapa.setNumeroPedido(c.getInt(c.getColumnIndex(NotaFiscalCapa.PEDIDONUMERO)));
			notaFiscalCapa.setDataemissao(new Date(c.getLong(c.getColumnIndex(NotaFiscalCapa.DATAEMISSAO))));
			notaFiscalCapa.setEmpresa(c.getInt(c.getColumnIndex(NotaFiscalCapa.EMPRESA)));
			Double valorTotalD = c.getDouble(c.getColumnIndex(NotaFiscalCapa.VALORTOTAL));
			notaFiscalCapa.setValorTotal(Dinheiro.newInstance(new BigDecimal(String.valueOf(valorTotalD))));

			List<NotaFiscalItem> itens = repoNotaFiscalItem.buscaItensPorNota(notaFiscalId);
			notaFiscalCapa.setItens(itens);
			
			notas.add(notaFiscalCapa);
		}
		repoNotaFiscalItem.fechar();
		return notas;

	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

	public void deleteAll() {
		db.delete(NotaFiscalCapa.NOME_TABELA, null, null);

	}

}
