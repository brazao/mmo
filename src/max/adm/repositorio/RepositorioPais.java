package max.adm.repositorio;

import java.util.ArrayList;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.entidade.Pais;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RepositorioPais  {
	
	private static final String NOME_TABELA = Pais.TABELA_NOME;
	
	protected SQLiteDatabase db;
	
	
	public RepositorioPais(Context context){
		db = new DBHelper(context).getWritableDatabase();
	}
	
	public int buscaIdBrasil(){
		Cursor c = db.query(true, NOME_TABELA, Pais.COLUNAS, Pais.NOME + "= 'BRASIL'", null, null, null, null, null);
		if (c.getCount() > 0){
			//pocisiona no primeiro elemento
			c.moveToFirst();
			
			Pais pais = new Pais();
			pais.setCodigo(c.getLong(0));
			pais.setNomePais(c.getString(1));
			pais.setCodigoBacen(c.getInt(2));
			
			c.close();
			return (int) pais.getCodigo();
		}
		c.close();
		return 0;
	}
	
	public long salvar(Pais pais) {
		long id = pais.getCodigo();
		if (id != 0){
			atualizar(pais);
		}
		else {
			id = inserir(pais);
		}
		
		return id;
	}


	public long inserir(Pais pais) {
		ContentValues values = new ContentValues();
		values.put(Pais.CODIGOBACEN, pais.getCodigoBacen());
		values.put(Pais.NOME, pais.getNomePais());
		
		long id = inserir(values);
		
		return id;
	}

	
	public long inserir(ContentValues values) {
		long id = db.insert(Pais.TABELA_NOME, "", values);
		return id;
	}

	
	public int atualizar(Pais pais) {
		ContentValues values = new ContentValues();
		values.put(Pais.CODIGOBACEN, pais.getCodigoBacen());
		values.put(Pais.NOME, pais.getNomePais());
		
		String _id = String.valueOf(pais.getCodigo());
		String where = Pais.CODIGO + "=?";
		String[] whereArgs = new String[] {_id};
		int count = atualizar(values, where, whereArgs);
		
		return count;
	}

	
	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);
		
		return count;
	}

	
	public int deletar(long id) {
		String where = Pais.CODIGO + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[] {_id};
		int count = deletar(where, whereArgs);
		return count;
	}

	
	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(NOME_TABELA, where, whereArgs);
		return count;
	}

	
	public Pais busca(int id) {
		Cursor c = db.query(true, NOME_TABELA, Pais.COLUNAS, Pais.CODIGO + "=" + id, null, null, null, null, null);
		if (c.getCount() > 0){
			//pocisiona no primeiro elemento
			c.moveToFirst();
			Pais pais = new Pais();
			pais.setCodigo(c.getLong(0));
			pais.setNomePais(c.getString(1));
			pais.setCodigoBacen(c.getInt(2));
			c.close();
			return pais;
			
		}
		c.close();
		return null;
	}

	// retorna cursor com todos os paises
	public Cursor getCursor() {
		 try {
			 // select * frm carros
			 return db.query(NOME_TABELA, Pais.COLUNAS, null, null, null, null, null);
		 }
		 catch (SQLException e){
			 Log.e("Pais", "Erro ao fazer busca: "+ e.toString());
			 return null;
		 }
		
	}

	
	public List<Pais> listar() {
		Cursor c = getCursor();
		List<Pais> paises = new ArrayList<Pais>();
		if (c.moveToFirst()){
			// recupera os �ndices das colunas
			int indexId = c.getColumnIndex(Pais.CODIGO);
			int indexNome = c.getColumnIndex(Pais.NOME);
			int indexBacen = c.getColumnIndex(Pais.CODIGOBACEN);
			
			// loop at� o final
			
			do {
				Pais pais = new Pais();
				paises.add(pais);
				
				pais.setCodigo(c.getLong(indexId));
				pais.setNomePais(c.getString(indexNome));
				pais.setCodigoBacen(c.getInt(indexBacen));
				
			}while(c.moveToNext());
		}
		
		return paises;
	}

	
	public void fechar(){
		if(db != null){
			db.close();
		}
	}
	
	

}
