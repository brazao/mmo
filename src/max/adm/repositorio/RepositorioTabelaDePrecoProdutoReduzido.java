package max.adm.repositorio;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import max.adm.Dinheiro;
import max.adm.database.DBHelper;
import max.adm.empresa.RepositorioEmpresa;
import max.adm.entidade.Empresa;
import max.adm.entidade.ProdutoReduzido;
import max.adm.entidade.TabelaDePreco;
import max.adm.entidade.TabelaPrecoProdutoReduzido;
import max.adm.produtoreduzido.repositorio.RepositorioProdutoReduzido;
import max.adm.tabelaDePreco.RepositorioTabelaDePreco;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class RepositorioTabelaDePrecoProdutoReduzido {

	private Context context;
	private SQLiteDatabase db;

	public RepositorioTabelaDePrecoProdutoReduzido(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public TabelaPrecoProdutoReduzido busca(int empresaId, int tabelaPrecoId, int reduzidoId) {
		Cursor cursor = db.query(TabelaPrecoProdutoReduzido.NOME_TABELA, 
				TabelaPrecoProdutoReduzido.COLUNAS, 
				TabelaPrecoProdutoReduzido.EMPRESA +"=? and "+ 
				TabelaPrecoProdutoReduzido.TABELAPRECO +"=? and "+ 
				TabelaPrecoProdutoReduzido.PRODUTOREDUZIDO +"=?", 
				new String[] {String.valueOf(empresaId), String.valueOf(tabelaPrecoId), String.valueOf(reduzidoId)}, 
				null, null, null);
		
		if (cursor.moveToFirst()) {
			Empresa empresa = new RepositorioEmpresa(context).busca(empresaId);
			TabelaDePreco tabelaDePreco = new RepositorioTabelaDePreco(context).busca(tabelaPrecoId);
			ProdutoReduzido produtoReduzido = new RepositorioProdutoReduzido(context).busca(reduzidoId);
			
			BigDecimal precoBD = new BigDecimal(cursor.getString(cursor.getColumnIndex(TabelaPrecoProdutoReduzido.PRECO)));
			
			return TabelaPrecoProdutoReduzido.newInstance(empresa, 
					Dinheiro.newInstance(precoBD), 
					produtoReduzido, 
					tabelaDePreco);
		}
		
		return null;
	}
	
	public List<TabelaPrecoProdutoReduzido> buscaPorProdutoTabela(int empresaId, int tabelaPrecoId, int produtoId) {
		List<TabelaPrecoProdutoReduzido> tabelaPrecoProdutoReduzidos = new ArrayList<TabelaPrecoProdutoReduzido>();
		
		StringBuilder sb = new StringBuilder();
		sb.append("select "+TabelaPrecoProdutoReduzido.NOME_TABELA+".* ")
		.append(" from "+TabelaPrecoProdutoReduzido.NOME_TABELA)
		.append(" join produtoreduzido on produtoreduzido.red_produto_key = tabelapreco_produtoreduzido.reduzidoproduto_id ")
		.append(" join produto on produto.codigo = produtoreduzido.produto_id")
		.append(" where produto.codigo = "+produtoId)
		.append(" and tabelapreco_produtoreduzido.empresa_id = "+empresaId)
		.append(" and tabelapreco_produtoreduzido.tabelapreco_id = "+ tabelaPrecoId);
		
		Cursor cursor = db.rawQuery(sb.toString(), null);
		
		while (cursor.moveToNext()) {
			
			Empresa empresa = new RepositorioEmpresa(context).busca(empresaId);
			TabelaDePreco tabelaDePreco = new RepositorioTabelaDePreco(context).busca(tabelaPrecoId);
			ProdutoReduzido produtoReduzido = 
					new RepositorioProdutoReduzido(context).busca(cursor.getInt(cursor.getColumnIndex(TabelaPrecoProdutoReduzido.PRODUTOREDUZIDO)));
			
			BigDecimal precoBD = new BigDecimal(cursor.getString(cursor.getColumnIndex(TabelaPrecoProdutoReduzido.PRECO)));
			
			tabelaPrecoProdutoReduzidos.add(TabelaPrecoProdutoReduzido.newInstance(empresa, 
					Dinheiro.newInstance(precoBD), 
					produtoReduzido, 
					tabelaDePreco));
		}
		
		return tabelaPrecoProdutoReduzidos;
	}
	
	public void fechar() {
		if (db != null) {
			db.close();
		}
	}
	
}
