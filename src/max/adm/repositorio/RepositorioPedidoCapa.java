package max.adm.repositorio;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.empresa.RepositorioEmpresa;
import max.adm.entidade.CondicaoPagamento;
import max.adm.entidade.Empresa;
import max.adm.entidade.FaixaCodigoPedido;
import max.adm.entidade.PedidoCapa;
import max.adm.entidade.PedidoItens;
import max.adm.entidade.Pessoa;
import max.adm.entidade.TabelaDePreco;
import max.adm.entidade.TipoCobranca;
import max.adm.pedido.PedidoDoc;
import max.adm.pedido.PedidoDoc.Builder;
import max.adm.pedido.PedidoDocItens;
import max.adm.pessoa.repositorio.RepositorioPessoa;
import max.adm.relatorios.ItemRelatorioListaPedido;
import max.adm.tabelaDePreco.RepositorioTabelaDePreco;
import max.adm.tipoCobranca.RepositorioTipoCobranca;

import org.joda.time.DateMidnight;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.common.collect.Lists;

public class RepositorioPedidoCapa {

	private Context context;

	private SQLiteDatabase db;

	public RepositorioPedidoCapa(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public int salvar(PedidoCapa pedidoCapa) {
		int id = pedidoCapa.getId();
		if (id != 0) {
			atualizar(pedidoCapa);
		} else {
			id = inserir(pedidoCapa);
		}

		return id;
	}

	private ContentValues montaValuesCodigoPedido(PedidoCapa pedido) {
		ContentValues values = new ContentValues();
		values.put(FaixaCodigoPedido.CODIGOCLIENTE, pedido.getPessoa().getCodigo());
		values.put(FaixaCodigoPedido.PEDIDOID, pedido.getDataEmissao().getTime());
		values.put(FaixaCodigoPedido.VALOR, pedido.getValor());
		return values;
	}

	public int inserir(PedidoCapa pedidoCapa) {
		ContentValues values = montarValues(pedidoCapa);
		int id = 0;
		db.beginTransaction();
		try {
			id = (int) db.insert(PedidoCapa.NOME_TABELA, "", values);
			pedidoCapa.setId(id);
			for (PedidoItens item : pedidoCapa.getItens()) {
				ContentValues valuesItem = montarValuesItens(item, id);
				db.insert(PedidoItens.NOME_TABELA, "", valuesItem);
			}
			db.update(FaixaCodigoPedido.TABELA_FAIXA, montaValuesCodigoPedido(pedidoCapa), FaixaCodigoPedido.EMPRESAID + " = " + pedidoCapa.getEmpresa().getNumeroLoja() + " and "
					+ FaixaCodigoPedido.PEDIDOID + " = " + pedidoCapa.getNumeroPedido(), null);
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
		return id;
	}

	public int inserir(ContentValues values) {
		int id = (int) db.insert(PedidoCapa.NOME_TABELA, "", values);
		return id;
	}

	private ContentValues montarValues(PedidoCapa pedidoCapa) {
		ContentValues values = new ContentValues();

		values.put(PedidoCapa.NUMEROPEDIDO, pedidoCapa.getNumeroPedido());
		values.put(PedidoCapa.NUMEROENTREGA, pedidoCapa.getNumeroEntrega());
		values.put(PedidoCapa.CODIGOCOLECAO, pedidoCapa.getCodigoColecao());
		values.put(PedidoCapa.DATAEMISSAO, pedidoCapa.getDataEmissao().getTime());
		values.put(PedidoCapa.DATAPREVISAO, pedidoCapa.getDataPrevisao().getTime());
		values.put(PedidoCapa.VALOR, pedidoCapa.getValor());
		values.put(PedidoCapa.PERCENTUALDESCONTO, pedidoCapa.getPercentualDesconto());
		values.put(PedidoCapa.STATUS, "W");
		values.put(PedidoCapa.VALORLIQUIDO, pedidoCapa.getValorLiquido());
		values.put(PedidoCapa.OBSERVACAO, pedidoCapa.getObservacao());
		values.put(PedidoCapa.SYNC, pedidoCapa.isSync() ? "S" : "N");
		values.put(PedidoCapa.EMPRESA, pedidoCapa.getEmpresa().getNumeroLoja());
		values.put(PedidoCapa.EMPRESANUMERO, pedidoCapa.getEmpresa().getNumeroLoja());
		values.put(PedidoCapa.TIPOCOBRANCA, pedidoCapa.getTipoCobranca().getCodigo());
		values.put(PedidoCapa.TIPOCOBRANCACODIGO, pedidoCapa.getTipoCobranca().getCodigo());
		values.put(PedidoCapa.TIPOCOBRANCADESCRICAO, pedidoCapa.getTipoCobranca().getDescricao());
		if (pedidoCapa.getTipoCobranca2() != null) {
			values.put(PedidoCapa.TIPOCOBRANCA2, pedidoCapa.getTipoCobranca2().getCodigo());
			values.put(PedidoCapa.TIPOCOBRANCACODIGO2, pedidoCapa.getTipoCobranca2().getCodigo());
			values.put(PedidoCapa.TIPOCOBRANCADESCRICAO2, pedidoCapa.getTipoCobranca2().getDescricao());
		}
		values.put(PedidoCapa.PESSOA, pedidoCapa.getPessoa().getCodigo());
		values.put(PedidoCapa.PESSOACODIGO, pedidoCapa.getPessoa().getCodigo());
		values.put(PedidoCapa.PESSOANOME, pedidoCapa.getPessoa().getRazaoSocial());
		values.put(PedidoCapa.TABELADEPRECO, pedidoCapa.getTabelaDePreco().getCodigo());
		values.put(PedidoCapa.TABELADEPRECONUMERO, pedidoCapa.getTabelaDePreco().getCodigo());
		values.put(PedidoCapa.TABELADEPRECODESCRICAO, pedidoCapa.getTabelaDePreco().getDescricao());
		values.put(PedidoCapa.CONDICAODEPAGAMENTO, pedidoCapa.getCondicaoDePagamento().getCodigo());
		values.put(PedidoCapa.CONDICAODEPAGAMENTOCODIGO, pedidoCapa.getCondicaoDePagamento().getCodigo());
		values.put(PedidoCapa.CONDICAODEPAGAMENTODESCRICAO, pedidoCapa.getCondicaoDePagamento().getDescricao());
		values.put(PedidoCapa.ASSINATURA, pedidoCapa.getAssinatura());

		return values;
	}

	private ContentValues montarValues(PedidoDoc pedidoDoc) {
		ContentValues values = new ContentValues();

		values.put(PedidoCapa.NUMEROPEDIDO, pedidoDoc.getCPE_NUMERO_PEDIDO());
		values.put(PedidoCapa.NUMEROENTREGA, 0);
		values.put(PedidoCapa.DATAEMISSAO, pedidoDoc.getCPE_DATA_EMISSAO().getTime());
		values.put(PedidoCapa.DATAPREVISAO, pedidoDoc.getCPE_DATA_PREVISAO_ENT().getTime());
		values.put(PedidoCapa.VALOR, pedidoDoc.getValor().doubleValue());
		values.put(PedidoCapa.PERCENTUALDESCONTO, pedidoDoc.getCPE_PERC_DESCONTO().doubleValue());
		values.put(PedidoCapa.STATUS, "W");
		values.put(PedidoCapa.OBSERVACAO, pedidoDoc.getCPE_OBS());
		values.put(PedidoCapa.SYNC, pedidoDoc.getCPE_SYNC());
		values.put(PedidoCapa.EMPRESANUMERO, pedidoDoc.getEMPRESANUMERO());
		values.put(PedidoCapa.TIPOCOBRANCACODIGO, pedidoDoc.getTIPOCOBRANCACODIGO());
		values.put(PedidoCapa.TIPOCOBRANCADESCRICAO, pedidoDoc.getTIPOCOBRANCADESCRICAO());

		return values;
	}

	private ContentValues montarValuesItens(PedidoItens pedidoItens, int idPedido) {
		ContentValues values = new ContentValues();
		values.put(PedidoItens.NUMEROCONJUNTO, pedidoItens.getNumeroConjunto());
		values.put(PedidoItens.PEDIDOCAPA, idPedido);
		values.put(PedidoItens.PRECO, pedidoItens.getPreco().getValor().doubleValue());
		values.put(PedidoItens.PRODUTOREDUZIDO, pedidoItens.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getRed_produto_key());
		values.put(PedidoItens.QUANTIDADE, pedidoItens.getQuantidade());
		values.put(PedidoItens.DESCRICAO, pedidoItens.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto().getDescricao());
		values.put(PedidoItens.TAMANHODESCRICAO, pedidoItens.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getTamanho().getCodigo());
		values.put(PedidoItens.PADRAOCODIGO, pedidoItens.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getPadrao().getCodigo());
		values.put(PedidoItens.PADRAODESCRICAO, pedidoItens.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getPadrao().getDescricao());
		values.put(PedidoItens.RED_PRODUTO_KEY, pedidoItens.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getRed_produto_key());
		return values;
	}

	private boolean faixaJaInserida(int cod, int loja) {
		Cursor c = db.query(
				FaixaCodigoPedido.TABELA_FAIXA,
				new String[] { FaixaCodigoPedido.PEDIDOID },
				FaixaCodigoPedido.PEDIDOID + " = "
						+ String.valueOf(cod) + " and "
						+ FaixaCodigoPedido.EMPRESAID + " = "
						+ String.valueOf(loja), 
				null, null, null, null);

		if (c.getCount() > 0) {
			c.close();
			return true;
		}
		c.close();
		return false;
	}

	public boolean salvarFaixa(List<FaixaCodigoPedido> faixa) {
		db.beginTransaction();
		try {
			for (FaixaCodigoPedido cod : faixa) {
				try {
					if (!faixaJaInserida(cod.getPedidoId(), cod.getEmpresaId())) {
						ContentValues values = new ContentValues();
						values.put(FaixaCodigoPedido.PEDIDOID, cod.getPedidoId());
						values.put(FaixaCodigoPedido.EMPRESAID, cod.getEmpresaId());
						db.insert(FaixaCodigoPedido.TABELA_FAIXA, "", values);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
		return true;
	}

	public int atualizar(PedidoCapa pedidoCapa) {
		ContentValues values = montarValues(pedidoCapa);
		String _id = String.valueOf(pedidoCapa.getId());
		String where = PedidoCapa.ID + "=?";
		String[] whereArgs = new String[]{_id};
		int count = atualizar(values, where, whereArgs);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(PedidoCapa.NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public int deletar(int id) {
		String where = PedidoCapa.ID + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[]{_id};
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(PedidoCapa.NOME_TABELA, where, whereArgs);
		return count;
	}

	public int deletar(PedidoCapa pedido) {
		int count = 0;
		try {
			db.beginTransaction();

			db.delete(PedidoItens.NOME_TABELA, PedidoItens.PEDIDOCAPA + "=" + pedido.getId(), null);
			count = db.delete(PedidoCapa.NOME_TABELA, PedidoCapa.ID + "=" + pedido.getId(), null);

			db.setTransactionSuccessful();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			db.endTransaction();
		}
		return count;
	}

	// retorna cursor com todos os PedidoCapaes
	public Cursor getCursor() {
		try {
			// select * frm carros
			return db.query(PedidoCapa.NOME_TABELA, PedidoCapa.COLUNAS, null, null, null, null, null);
		} catch (SQLException e) {
			Log.e("PedidoCapa", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<ItemRelatorioListaPedido> listarParaTela(int idLoja) {

		String sql = "select * from pedidocapa";

		Cursor c = db.rawQuery(sql, null);

		List<ItemRelatorioListaPedido> pedidos = new ArrayList<ItemRelatorioListaPedido>();

		if (c.moveToFirst()) {
			do {
				ItemRelatorioListaPedido pedido = new ItemRelatorioListaPedido();
				pedidos.add(pedido);

				pedido.idPedido = (c.getInt(c.getColumnIndex("CPE_ID")));
				pedido.numeroPedido = (c.getInt(c.getColumnIndex("CPE_NUMERO_PEDIDO")));
				pedido.nomeCliente = (c.getString(c.getColumnIndex("PESSOANOME")));
				pedido.dataEmissaoPedido = (new DateMidnight(c.getLong(c.getColumnIndex("CPE_DATA_EMISSAO"))));
				pedido.valorBruto = c.getDouble(c.getColumnIndex("CPE_VALOR"));
				pedido.percentualDescontoPedido = (c.getDouble(c.getColumnIndex("CPE_PERC_DESCONTO")));

			} while (c.moveToNext());

		}
		c.close();
		return pedidos;
	}

	public int leCodigoDisponivel(int codigoLoja) {
		int codigo = 0;
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT min(").append(FaixaCodigoPedido.PEDIDOID)
				.append(") as CODIGO ").append(" from ")
				.append(FaixaCodigoPedido.TABELA_FAIXA).append(" where ")
				.append(FaixaCodigoPedido.CODIGOCLIENTE)
				.append(" is null and ")
				.append(FaixaCodigoPedido.EMPRESAID).append(" = ")
				.append(codigoLoja);

		Cursor c = db.rawQuery(sb.toString(), null);

		if (c.moveToFirst()) {
			codigo = c.getInt(c.getColumnIndex("CODIGO"));
		}
		c.close();
		return codigo;
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

	public void updateLista(List<PedidoDoc> pedidos) {
		db.beginTransaction();
		try {
			for (PedidoDoc p : pedidos) {
				ContentValues values = this.montarValues(p);
				db.update(PedidoCapa.NOME_TABELA, values, PedidoCapa.ID + "=" + p.getCPE_ID(), null);
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}

	}

	public List<PedidoCapa> listarNaoSincronizados(Empresa loja) {
		Cursor c = db.query(PedidoCapa.NOME_TABELA, PedidoCapa.COLUNAS, PedidoCapa.EMPRESA + "=" + loja.getNumeroLoja() + " and " + PedidoCapa.SYNC + "= 'N'", null, null, null, null);
		List<PedidoCapa> pedidos = Lists.newLinkedList();
		if (c.moveToFirst()) {
			do {
				PedidoCapa.Builder builder = montaBuilder(c, loja);
				pedidos.add(builder.build());
			} while (c.moveToNext());
		}
		c.close();
		return pedidos;
	}

	public PedidoCapa buscaPedidoCompleto(int id) {
		Cursor c = db.query(PedidoCapa.NOME_TABELA, PedidoCapa.COLUNAS, PedidoCapa.ID + "=" + id, null, null, null, null);
		PedidoCapa.Builder builder = PedidoCapa.builder();
		if (c.getCount() > 0) {
			c.moveToFirst();

			RepositorioEmpresa repositorioEmpresa = new RepositorioEmpresa(context);
			Empresa empresa = repositorioEmpresa.busca(c.getInt(c.getColumnIndex(PedidoCapa.EMPRESA)));
			repositorioEmpresa.fechar();

			RepositorioTabelaDePreco repositorioTabelaDePreco = new RepositorioTabelaDePreco(context);
			TabelaDePreco tabPreco = repositorioTabelaDePreco.busca(c.getInt(c.getColumnIndexOrThrow(PedidoCapa.TABELADEPRECO)));
			repositorioTabelaDePreco.fechar();

			RepositorioTipoCobranca repoTipoCobranca = new RepositorioTipoCobranca(context);
			TipoCobranca tpCob = repoTipoCobranca.busca(c.getInt(c.getColumnIndex(PedidoCapa.TIPOCOBRANCA)));
			TipoCobranca tpCob2 = repoTipoCobranca.busca(c.getInt(c.getColumnIndex(PedidoCapa.TIPOCOBRANCA2)));
			repoTipoCobranca.fechar();

			RepositorioPessoa repositorioPessoa = new RepositorioPessoa(context);
			Pessoa pessoa = repositorioPessoa.busca(c.getInt(c.getColumnIndex(PedidoCapa.PESSOA)), c.getInt(c.getColumnIndex(Pessoa.EMPRESA)));
			repositorioPessoa.fechar();

			RepositorioCondicaoPagamento repositorioCondicaoPagamento = new RepositorioCondicaoPagamento(context);
			CondicaoPagamento condicaoPagamento = repositorioCondicaoPagamento.busca(c.getInt(c.getColumnIndex(PedidoCapa.CONDICAODEPAGAMENTO)));
			repositorioCondicaoPagamento.fechar();

			RepositorioPedidoItem repositorioPedidoItem = new RepositorioPedidoItem(context);
			List<PedidoItens> itens = repositorioPedidoItem.listarPorPedido(
					empresa.getNumeroLoja(), 
					tabPreco.getCodigo(), 
					c.getInt(c.getColumnIndex(PedidoCapa.ID)));
			repositorioPedidoItem.fechar();

			builder.comId(c.getInt(c.getColumnIndex(PedidoCapa.ID)));
			builder.comNumeroPedido(c.getInt(c.getColumnIndex(PedidoCapa.NUMEROPEDIDO)));
			builder.comNumeroEntrega(c.getInt(c.getColumnIndex(PedidoCapa.NUMEROENTREGA)));
			builder.comCodigoColecao(c.getInt(c.getColumnIndex(PedidoCapa.CODIGOCOLECAO)));
			builder.comDataEmissao(new Date(c.getLong(c.getColumnIndex(PedidoCapa.DATAEMISSAO))));
			builder.comDataPrevisao(new Date(c.getLong(c.getColumnIndex(PedidoCapa.DATAPREVISAO))));
			builder.comPercentualDesconto(c.getDouble(c.getColumnIndex(PedidoCapa.PERCENTUALDESCONTO)));
			builder.comStatus(c.getString(c.getColumnIndex(PedidoCapa.STATUS)));
			builder.comObservacao(c.getString(c.getColumnIndex(PedidoCapa.OBSERVACAO)));
			builder.comSync(c.getString(c.getColumnIndex(PedidoCapa.SYNC)));
			builder.comEmpresa(empresa);
			builder.comTipoCobranca(tpCob);
			builder.comTipoCobranca2(tpCob2);
			builder.comCondicaoDePagamento(condicaoPagamento);
			builder.comPessoa(pessoa);
			builder.comTabelaDePreco(tabPreco);
			builder.comItens(itens);
			builder.comAssinatura(c.getBlob(c.getColumnIndex(PedidoCapa.ASSINATURA)));

		}
		c.close();
		try {
			return builder.build();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private PedidoCapa.Builder montaBuilder(Cursor c, Empresa loja) {
		PedidoCapa.Builder builder = PedidoCapa.builder();

		int idPedido = c.getInt(c.getColumnIndex(PedidoCapa.ID));
		builder.comId(idPedido);
		builder.comNumeroPedido(c.getInt(c.getColumnIndex(PedidoCapa.NUMEROPEDIDO)));
		builder.comNumeroEntrega(c.getInt(c.getColumnIndex(PedidoCapa.NUMEROENTREGA)));
		builder.comCodigoColecao(c.getInt(c.getColumnIndex(PedidoCapa.CODIGOCOLECAO)));
		builder.comDataEmissao(new Date(c.getLong(c.getColumnIndex(PedidoCapa.DATAEMISSAO))));
		builder.comDataPrevisao(new Date(c.getLong(c.getColumnIndex(PedidoCapa.DATAPREVISAO))));
		builder.comPercentualDesconto(c.getDouble(c.getColumnIndex(PedidoCapa.PERCENTUALDESCONTO)));
		builder.comStatus(c.getString(c.getColumnIndex(PedidoCapa.STATUS)));
		builder.comObservacao(c.getString(c.getColumnIndex(PedidoCapa.OBSERVACAO)));
		builder.comSync(c.getString(c.getColumnIndex(PedidoCapa.SYNC)));
		
		RepositorioTabelaDePreco repositorioTabelaDePreco = new RepositorioTabelaDePreco(context);
		TabelaDePreco tabelaDePreco = repositorioTabelaDePreco.busca(c.getInt(c.getColumnIndex(PedidoCapa.TABELADEPRECO)));
		repositorioTabelaDePreco.fechar();
		builder.comTabelaDePreco(tabelaDePreco);

		RepositorioPedidoItem repoItens = new RepositorioPedidoItem(context);
		List<PedidoItens> itens = repoItens.listarPorPedido(loja.getNumeroLoja(), tabelaDePreco.getCodigo(), idPedido);
		repoItens.fechar();

		builder.comItens(itens);

		return builder;

	}

	public List<PedidoDoc> buscaPedidosDocs(Empresa empresa) {
		List<PedidoDoc> docs = Lists.newLinkedList();
		Cursor c = db.query(PedidoCapa.NOME_TABELA, PedidoCapa.COLUNAS, PedidoCapa.SYNC + "= 'N' and " + PedidoCapa.EMPRESANUMERO + " = " + empresa.getNumeroLoja(), null, null, null, null);

		if (c.moveToFirst()) {
			do {
				Builder doc = PedidoDoc.builder();
				doc.setCPE_ID(c.getLong(c.getColumnIndex(PedidoCapa.ID)));
				doc.setCPE_NUMERO_PEDIDO(c.getLong(c.getColumnIndex("CPE_NUMERO_PEDIDO")));
				doc.setCOL_CODIGO(c.getLong(c.getColumnIndex("COL_CODIGO")));
				doc.setCPE_DATA_EMISSAO(new Date(c.getLong(c.getColumnIndex("CPE_DATA_EMISSAO"))));
				Log.i("data cpe_data_emissao", String.valueOf(new Date(c.getLong(c.getColumnIndex("CPE_DATA_EMISSAO")))));
				doc.setCPE_DATA_PREVISAO_ENT(new Date(c.getLong(c.getColumnIndex("CPE_DATA_PREVISAO_ENT"))));
				doc.setCPE_PERC_DESCONTO(new BigDecimal(c.getLong(c.getColumnIndex("CPE_PERC_DESCONTO"))));
				doc.setCPE_STATUS(c.getString(c.getColumnIndex("CPE_STATUS")));
				doc.setCPE_OBS(c.getString(c.getColumnIndex("CPE_OBS")));
				doc.setCPE_SYNC(c.getString(c.getColumnIndex("CPE_SYNC")));
				doc.setEMPRESANUMERO(c.getLong(c.getColumnIndex("EMPRESANUMERO")));
				doc.setTIPOCOBRANCACODIGO(c.getLong(c.getColumnIndex("TIPOCOBRANCACODIGO")));
				doc.setTIPOCOBRANCADESCRICAO(c.getString(c.getColumnIndex("TIPOCOBRANCADESCRICAO")));
				doc.setTIPOCOBRANCACODIGO2(c.getLong(c.getColumnIndex("TIPOCOBRANCACODIGO2")));
				doc.setTIPOCOBRANCADESCRICAO2(c.getString(c.getColumnIndex("TIPOCOBRANCADESCRICAO2")));
				doc.setPESSOACODIGO(c.getLong(c.getColumnIndex("PESSOACODIGO")));
				doc.setPESSOANOME(c.getString(c.getColumnIndex("PESSOANOME")));
				doc.setTABELADEPRECONUMERO(c.getLong(c.getColumnIndex("TABELADEPRECONUMERO")));
				doc.setTABELADEPRECODESCRICAO(c.getString(c.getColumnIndex("TABELADEPRECODESCRICAO")));
				doc.setCONDICAODEPAGAMENTOCODIGO(c.getLong(c.getColumnIndex("CONDICAODEPAGAMENTOCODIGO")));
				doc.setCONDICAODEPAGAMENTODESCRICAO(c.getString(c.getColumnIndex("CONDICAODEPAGAMENTODESCRICAO")));
				PedidoDoc pedidoDoc = doc.build();

				Cursor ci = db.query(PedidoItens.NOME_TABELA, PedidoItens.COLUNAS, PedidoItens.PEDIDOCAPA + " = " + c.getLong(c.getColumnIndex(PedidoCapa.ID)), null, null, null, null);
				if (ci.moveToFirst()) {
					do {
						max.adm.pedido.PedidoDocItens.Builder it = PedidoDocItens.builder();
						it.setDESCRICAO(ci.getString(ci.getColumnIndex(PedidoItens.DESCRICAO)));
						it.setPADRAOCODIGO(ci.getLong(ci.getColumnIndex(PedidoItens.PADRAOCODIGO)));
						it.setPADRAODESCRICAO(ci.getString(ci.getColumnIndex(PedidoItens.PADRAODESCRICAO)));
						it.setPEDIDOCAPA_CPE_ID(ci.getLong(ci.getColumnIndex(PedidoItens.PEDIDOCAPA)));
						it.setPEDIDOITEM_ID(ci.getLong(ci.getColumnIndex(PedidoItens.ID)));
						it.setPEDITEM_NRCONJUNTO(ci.getLong(ci.getColumnIndex(PedidoItens.NUMEROCONJUNTO)));
						it.setPEDITEM_PRECO(new BigDecimal(ci.getDouble(ci.getColumnIndex(PedidoItens.PRECO))));
						it.setPEDITEM_QUANTIDADE(ci.getLong(ci.getColumnIndex(PedidoItens.QUANTIDADE)));
						it.setPRO_REDUZIDO_PRODUTOS_REDUZIDO_ID(ci.getLong(ci.getColumnIndex(PedidoItens.PRODUTOREDUZIDO)));
						it.setRED_PRODUTO_KEY(ci.getLong(ci.getColumnIndex(PedidoItens.RED_PRODUTO_KEY)));
						it.setTAMANHODESCRICAO(ci.getString(ci.getColumnIndex(PedidoItens.TAMANHODESCRICAO)));
						pedidoDoc.addItens(it.build());
					} while (ci.moveToNext());
				}
				ci.close();

				docs.add(pedidoDoc);

			} while (c.moveToNext());

		}
		c.close();
		return docs;
	}

}
