package max.adm.repositorio;

import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;


public abstract class Repositorio {
	
	
	public abstract long salvar(Object obj);
	
	public abstract long inserir (Object obj);
	
	public abstract long inserir (ContentValues values);
	
	public abstract int atualizar (Object obj);
	
	public abstract int atualizar(ContentValues valores, String where, String[] whereArgs);
	
	public abstract int deletar(long id);
	
	public abstract int deletar (String where, String[] whereArgs);
	
	public abstract Object busca(long id);
	
	public abstract Cursor getCursor();
	
	public abstract List<Object> listar();
	
}
