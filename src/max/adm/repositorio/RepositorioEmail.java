package max.adm.repositorio;

import java.util.ArrayList;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.entidade.Email;
import max.adm.pessoa.repositorio.RepositorioPessoa;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RepositorioEmail {

	private Context context;
	
	private static final String NOME_TABELA = Email.NOME_TABELA;

	protected SQLiteDatabase db;

	public RepositorioEmail(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public int salvar(Email email) {
		int id = email.getCodigo();
		if (id != 0) {
			atualizar(email);
		} else {
			id = inserir(email);
		}

		return id;
	}

	public int inserir(Email email) {
		ContentValues values = new ContentValues();
		values.put(Email.EMAIL, email.getEmail());
		values.put(Email.PESSOA, email.getPessoa().getCodigo());
		

		int id = inserir(values);

		return id;
	}

	public int inserir(ContentValues values) {
		int id = (int) db.insert(Email.NOME_TABELA, "", values);
		return id;
	}

	public int atualizar(Email email) {
		ContentValues values = new ContentValues();
		values.put(Email.EMAIL, email.getEmail());
		values.put(Email.PESSOA, email.getPessoa().getCodigo());

		String _id = String.valueOf(email.getCodigo());
		String where = Email.CODIGO + "=?";
		String[] whereArgs = new String[] { _id };
		int count = atualizar(values, where, whereArgs);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public int deletar(int id) {
		String where = Email.CODIGO + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[] { _id };
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(NOME_TABELA, where, whereArgs);
		return count;
	}

	public Email busca(int id) {
		Cursor c = db.query(true, NOME_TABELA, Email.COLUNAS, Email.CODIGO + "="
				+ id, null, null, null, null, null);
		RepositorioPessoa repoPessoa = new RepositorioPessoa(context);
		if (c.getCount() > 0) {
			// pocisiona no primeiro elemento
			c.moveToFirst();
			Email email = new Email();
			email.setCodigo(c.getInt(c.getColumnIndex(Email.CODIGO)));
			email.setEmail(c.getString(c.getColumnIndex(Email.EMAIL)));
			repoPessoa.fechar();


			return email;

		}
		return null;
	}
	
	public List<Email> buscaPorPessoa(int idPessoa) {
		Cursor c = db.query(true, NOME_TABELA, Email.COLUNAS, Email.PESSOA + "="
				+ idPessoa, null, null, null, null, null);
		List<Email> emails = new ArrayList<Email>();
		
		//RepositorioPessoa repoPessoa = new RepositorioPessoa(context);
		if (c.moveToFirst()) {
			
			do{
				// pocisiona no primeiro elemento
				
				Email email = new Email();
				emails.add(email);
				
				email.setCodigo(c.getInt(c.getColumnIndex(Email.CODIGO)));
				email.setEmail(c.getString(c.getColumnIndex(Email.EMAIL)));
				//email.setPessoa(repoPessoa.busca(c.getInt(c.getColumnIndex(Email.PESSOA))));
				//repoPessoa.fechar();
			} while(c.moveToNext());
			c.close();
			

		}
		return emails;
	}

	// retorna cursor com todos os Emailes
	public Cursor getCursor() {
		try {
			// select * frm carros
			return db.query(NOME_TABELA, Email.COLUNAS, null, null, null,
					null, null);
		} catch (SQLException e) {
			Log.e("Email", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<Email> listar() {
		Cursor c = getCursor();
		List<Email> emails = new ArrayList<Email>();
		RepositorioPessoa repoPessoa = new RepositorioPessoa(context);
		if (c.moveToFirst()) {


			do {
				Email email = new Email();
				emails.add(email);

				email.setCodigo(c.getInt(c.getColumnIndex(Email.CODIGO)));
				email.setEmail(c.getString(c.getColumnIndex(Email.EMAIL)));
				repoPessoa.fechar();

			} while (c.moveToNext());
		}

		return emails;
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}
	
	
}
