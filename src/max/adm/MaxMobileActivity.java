package max.adm;

import java.util.List;

import max.adm.controle.CriaLoginControle;
import max.adm.controle.LoginControle;
import max.adm.controle.MenuActivity;
import max.adm.controle.PrimeiroPassoControle;
import max.adm.dominio.Usuario;
import max.adm.parametro.RepositorioParametro;
import max.adm.repositorio.RepositorioUsuario;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

public class MaxMobileActivity extends Activity {

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == ValorAtividade.criaLogin.get()) {
			if (verificaSeExisteUsuario()) {
				chamaMenu();
			} else {
				finish();
			}
		}

		if (requestCode == ValorAtividade.menu.get()) {
			finish();
		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);

		if (verificaPrimeiroAcesso()) {
			new Handler().postDelayed(new Runnable() {
				public void run() {
					chamaPrimeiroPasso();
					finish();

				}
			}, 8000);

		}

		else {
			RepositorioUsuario repoUser = new RepositorioUsuario(this);
			List<Usuario> users = repoUser.listar();
			repoUser.fechar();

			if (users.size() == 0) {
				new Handler().postDelayed(new Runnable() {
					public void run() {
						// e necessaio definir um usuario ao sistema.
						// Toast.makeText(MaxMobileActivity.this,
						// R.string.mensagem_necessario_criar_usuario,
						// Toast.LENGTH_LONG).show();

						Intent criaLogin = new Intent(MaxMobileActivity.this, CriaLoginControle.class);
						startActivityForResult(criaLogin, ValorAtividade.criaLogin.get());

					}
				}, 8000);

			} else {
				new Handler().postDelayed(new Runnable() {
					public void run() {
						Intent login = new Intent(MaxMobileActivity.this, LoginControle.class);
						startActivity(login);
						finish();
					}
				}, 1000);
			}

		}

	}

	public boolean verificaPrimeiroAcesso() {
		
		RepositorioParametro repo = new RepositorioParametro(this);
		// List<Parametro> params = repoParametro.listar();
		boolean contemParametro = repo.contemRegistro();
		repo.fechar();

		if (!contemParametro) {
			Toast.makeText(this, "Este é o seu primeiro acesso.", Toast.LENGTH_LONG).show();



			Toast.makeText(this, "Os próxmos passos definirão as configurações iniciais.", Toast.LENGTH_LONG).show();

			return true;

		} else {
			return false;
		}
	}

	public void chamaPrimeiroPasso() {
		Intent primeiroPasso = new Intent(MaxMobileActivity.this, PrimeiroPassoControle.class);
		startActivityForResult(primeiroPasso, ValorAtividade.primeiroPasso.get());
	
	}

	public void chamaMenu() {
		Intent menu = new Intent(MaxMobileActivity.this, MenuActivity.class);
		startActivityForResult(menu, ValorAtividade.menu.get());
	}

	private boolean verificaSeExisteUsuario() {
		RepositorioUsuario repoUser = new RepositorioUsuario(this);
		List<Usuario> users = repoUser.listar();
		repoUser.fechar();
		if (users.size() > 0) {
			return true;
		}
		return false;
	}

}