package max.adm.parametro;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.empresa.RepositorioEmpresa;
import max.adm.entidade.Parametro;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RepositorioParametro {

	private Context context;
	
	private static final String NOME_TABELA = Parametro.NOME_TABELA;

	protected SQLiteDatabase db;

	public RepositorioParametro(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public int salvar(Parametro parametro) {
		int id = parametro.getId();
		if (id != 0) {
			atualizar(parametro);
		} else {
			id = inserir(parametro);
		}

		return id;
	}

//	private ContentValues montaValues(Parametro parametro) {
//		ContentValues values = new ContentValues();
//
//		if (parametro.getId() > 0) {
//			values.put(Parametro.ID, parametro.getId());
//		}
//
//		values.put(Parametro.EMPRESA, parametro.getEmpresa().getCodigo());
//		values.put(Parametro.CODIGOREPRESENTANTE, parametro.getCodigoRepresentante());
//		values.put(Parametro.NOMEREPRESENTANTE, parametro.getNomeRepresentante());
//		if (parametro.isSyncAutomatica()) {
//			values.put(Parametro.SYNCAUTOMATICA, "S");
//		} else {
//			values.put(Parametro.SYNCAUTOMATICA, "N");
//		}
//
//		values.put(Parametro.URLWEBSERVICE, parametro.getUrlWebService());
//		values.put(Parametro.EMAIL, parametro.getEmail());
//		values.put(Parametro.SENHAEMAIL, parametro.getSenhaEmail());
//		values.put(Parametro.SMTP, parametro.getSmtp());
//		values.put(Parametro.ASSINATURA, parametro.getAssinatura());
//		values.put(Parametro.PORTAEMAIL, parametro.getPortaEmail());
//		values.put(Parametro.MAXIMODESCONTO, parametro.getMaximoDesconto());
//
//		if (parametro.isSsl()) {
//			values.put(Parametro.SSL, "S");
//		} else {
//			values.put(Parametro.SSL, "N");
//		}
//
//		values.put(Parametro.REPRESENTANTEFAIXA, parametro.getRepresentantefaixa());
//
//		if (parametro.isTrabalhaProRegiao()) {
//			values.put(Parametro.TRABALHAPROREGIAO, "S");
//		} else {
//			values.put(Parametro.TRABALHAPROREGIAO, "N");
//		}
//
//		values.put(Parametro.IMAGEM, parametro.getImagem());
//
//		if (parametro.isUsaDadosFinanceiros()) {
//			values.put(Parametro.USADADOSFINANCEIROS, "S");
//		} else {
//			values.put(Parametro.USADADOSFINANCEIROS, "N");
//		}
//
//		if (parametro.isPrimeiroAcesso()) {
//			values.put(Parametro.PRIMEIROACESSO, "S");
//		} else {
//			values.put(Parametro.PRIMEIROACESSO, "N");
//		}
//
//		if (parametro.isExigeEmailCadCliente()) {
//			values.put(Parametro.EXIGEEMAILCADCLIENTE, "S");
//		} else {
//			values.put(Parametro.EXIGEEMAILCADCLIENTE, "N");
//		}
//
//		if (parametro.isExigeTelefoneCadCleinte()) {
//			values.put(Parametro.EXIGETELEFONECADCLEINTE, "S");
//		} else {
//			values.put(Parametro.EXIGETELEFONECADCLEINTE, "N");
//		}
//
//		values.put(Parametro.NUMEROMAXPECASPEDIDO, parametro.getNumeroMaxPecasPedido());
//
//		if (parametro.isPermiteCnpjRepetido()) {
//			values.put(Parametro.PERMITECNPJREPETIDO, "S");
//		} else {
//			values.put(Parametro.PERMITECNPJREPETIDO, "N");
//		}
//
//		if (parametro.isDuplicaCadastroCliente()) {
//			values.put(Parametro.DUPLICACADASTROCLIENTE, "S");
//		} else {
//			values.put(Parametro.DUPLICACADASTROCLIENTE, "N");
//		}
//
//		if (parametro.isMultiTabelaPreco()) {
//			values.put(Parametro.MULTITABELAPRECO, "S");
//		} else {
//			values.put(Parametro.MULTITABELAPRECO, "N");
//		}
//
//		if (parametro.usaLote()) {
//			values.put(Parametro.USALOTE, "S");
//		} else {
//			values.put(Parametro.USALOTE, "N");
//		}
//
//		if (parametro.isMostraPrecoNoPedido()) {
//			values.put(Parametro.MOSTRAPRECONOPEDIDO, "S");
//		} else {
//			values.put(Parametro.MOSTRAPRECONOPEDIDO, "N");
//		}
//
//		values.put(Parametro.CHAVE, parametro.getChave());
//
//		return values;
//	}

	public int inserir(Parametro parametro) {
		ContentValues values = new ContentValues();
		values.put(Parametro.EMPRESA, parametro.getEmpresa().getNumeroLoja());
		values.put(Parametro.CODIGOREPRESENTANTE, parametro.getCodigoRepresentante());
		values.put(Parametro.NOMEREPRESENTANTE, parametro.getNomeRepresentante());
		if (parametro.isSyncAutomatica()) {
			values.put(Parametro.SYNCAUTOMATICA, "S");
		} else {
			values.put(Parametro.SYNCAUTOMATICA, "N");
		}

		values.put(Parametro.URLWEBSERVICE, parametro.getUrlWebService());
		values.put(Parametro.EMAIL, parametro.getEmail());
		values.put(Parametro.SENHAEMAIL, parametro.getSenhaEmail());
		values.put(Parametro.SMTP, parametro.getSmtp());
		values.put(Parametro.ASSINATURA, parametro.getAssinatura());
		values.put(Parametro.PORTAEMAIL, parametro.getPortaEmail());
		values.put(Parametro.MAXIMODESCONTO, parametro.getMaximoDesconto());

		if (parametro.isSsl()) {
			values.put(Parametro.SSL, "S");
		} else {
			values.put(Parametro.SSL, "N");
		}

		values.put(Parametro.REPRESENTANTEFAIXA, parametro.getRepresentantefaixa());

		if (parametro.isTrabalhaProRegiao()) {
			values.put(Parametro.TRABALHAPROREGIAO, "S");
		} else {
			values.put(Parametro.TRABALHAPROREGIAO, "N");
		}

		values.put(Parametro.IMAGEM, parametro.getImagem());

		if (parametro.isUsaDadosFinanceiros()) {
			values.put(Parametro.USADADOSFINANCEIROS, "S");
		} else {
			values.put(Parametro.USADADOSFINANCEIROS, "N");
		}

		if (parametro.isPrimeiroAcesso()) {
			values.put(Parametro.PRIMEIROACESSO, "S");
		} else {
			values.put(Parametro.PRIMEIROACESSO, "N");
		}

		if (parametro.isExigeEmailCadCliente()) {
			values.put(Parametro.EXIGEEMAILCADCLIENTE, "S");
		} else {
			values.put(Parametro.EXIGEEMAILCADCLIENTE, "N");
		}

		if (parametro.isExigeTelefoneCadCleinte()) {
			values.put(Parametro.EXIGETELEFONECADCLEINTE, "S");
		} else {
			values.put(Parametro.EXIGETELEFONECADCLEINTE, "N");
		}

		values.put(Parametro.NUMEROMAXPECASPEDIDO, parametro.getNumeroMaxPecasPedido());

		if (parametro.isPermiteCnpjRepetido()) {
			values.put(Parametro.PERMITECNPJREPETIDO, "S");
		} else {
			values.put(Parametro.PERMITECNPJREPETIDO, "N");
		}

		if (parametro.isDuplicaCadastroCliente()) {
			values.put(Parametro.DUPLICACADASTROCLIENTE, "S");
		} else {
			values.put(Parametro.DUPLICACADASTROCLIENTE, "N");
		}

		if (parametro.isMultiTabelaPreco()) {
			values.put(Parametro.MULTITABELAPRECO, "S");
		} else {
			values.put(Parametro.MULTITABELAPRECO, "N");
		}

		if (parametro.usaLote()) {
			values.put(Parametro.USALOTE, "S");
		} else {
			values.put(Parametro.USALOTE, "N");
		}

		if (parametro.isMostraPrecoNoPedido()) {
			values.put(Parametro.MOSTRAPRECONOPEDIDO, "S");
		} else {
			values.put(Parametro.MOSTRAPRECONOPEDIDO, "N");
		}

		values.put(Parametro.CHAVE, parametro.getChave());

		int id = inserir(values);

		return id;
	}

	public int inserir(ContentValues values) {
		int id = (int) db.insert(Parametro.NOME_TABELA, "", values);
		return id;
	}

	public int atualizar(Parametro parametro) {
		ContentValues values = new ContentValues();
		values.put(Parametro.EMPRESA, parametro.getEmpresa().getNumeroLoja());
		values.put(Parametro.CODIGOREPRESENTANTE, parametro.getCodigoRepresentante());
		values.put(Parametro.NOMEREPRESENTANTE, parametro.getNomeRepresentante());
		if (parametro.isSyncAutomatica()) {
			values.put(Parametro.SYNCAUTOMATICA, "S");
		} else {
			values.put(Parametro.SYNCAUTOMATICA, "N");
		}

		values.put(Parametro.URLWEBSERVICE, parametro.getUrlWebService());
		values.put(Parametro.EMAIL, parametro.getEmail());
		values.put(Parametro.SENHAEMAIL, parametro.getSenhaEmail());
		values.put(Parametro.SMTP, parametro.getSmtp());
		values.put(Parametro.ASSINATURA, parametro.getAssinatura());
		values.put(Parametro.PORTAEMAIL, parametro.getPortaEmail());
		values.put(Parametro.MAXIMODESCONTO, parametro.getMaximoDesconto());

		if (parametro.isSsl()) {
			values.put(Parametro.SSL, "S");
		} else {
			values.put(Parametro.SSL, "N");
		}

		values.put(Parametro.REPRESENTANTEFAIXA, parametro.getRepresentantefaixa());

		if (parametro.isTrabalhaProRegiao()) {
			values.put(Parametro.TRABALHAPROREGIAO, "S");
		} else {
			values.put(Parametro.TRABALHAPROREGIAO, "N");
		}

		values.put(Parametro.IMAGEM, parametro.getImagem());

		if (parametro.isUsaDadosFinanceiros()) {
			values.put(Parametro.USADADOSFINANCEIROS, "S");
		} else {
			values.put(Parametro.USADADOSFINANCEIROS, "N");
		}

		if (parametro.isPrimeiroAcesso()) {
			values.put(Parametro.PRIMEIROACESSO, "S");
		} else {
			values.put(Parametro.PRIMEIROACESSO, "N");
		}

		if (parametro.isExigeEmailCadCliente()) {
			values.put(Parametro.EXIGEEMAILCADCLIENTE, "S");
		} else {
			values.put(Parametro.EXIGEEMAILCADCLIENTE, "N");
		}

		if (parametro.isExigeTelefoneCadCleinte()) {
			values.put(Parametro.EXIGETELEFONECADCLEINTE, "S");
		} else {
			values.put(Parametro.EXIGETELEFONECADCLEINTE, "N");
		}

		values.put(Parametro.NUMEROMAXPECASPEDIDO, parametro.getNumeroMaxPecasPedido());

		if (parametro.isPermiteCnpjRepetido()) {
			values.put(Parametro.PERMITECNPJREPETIDO, "S");
		} else {
			values.put(Parametro.PERMITECNPJREPETIDO, "N");
		}

		if (parametro.isDuplicaCadastroCliente()) {
			values.put(Parametro.DUPLICACADASTROCLIENTE, "S");
		} else {
			values.put(Parametro.DUPLICACADASTROCLIENTE, "N");
		}

		if (parametro.isMultiTabelaPreco()) {
			values.put(Parametro.MULTITABELAPRECO, "S");
		} else {
			values.put(Parametro.MULTITABELAPRECO, "N");
		}

		if (parametro.usaLote()) {
			values.put(Parametro.USALOTE, "S");
		} else {
			values.put(Parametro.USALOTE, "N");
		}
		if (parametro.isMostraPrecoNoPedido()) {
			values.put(Parametro.MOSTRAPRECONOPEDIDO, "S");
		} else {
			values.put(Parametro.MOSTRAPRECONOPEDIDO, "N");
		}

		values.put(Parametro.CHAVE, parametro.getChave());

		String _id = String.valueOf(parametro.getId());
		String where = Parametro.ID + "=?";
		String[] whereArgs = new String[]{_id};
		int count = atualizar(values, where, whereArgs);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public int deletar(int id) {
		String where = Parametro.ID + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[]{_id};
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(NOME_TABELA, where, whereArgs);
		return count;
	}

	public Parametro busca(int id) {
		Cursor c = db.query(true, NOME_TABELA, Parametro.COLUNAS, Parametro.ID + "=" + id, null, null, null, null, null);

		if (c.getCount() > 0) {
			RepositorioEmpresa repoEmpresa = new RepositorioEmpresa(context);
			// pocisiona no primeiro elemento
			c.moveToFirst();
			Parametro parametro = new Parametro();
			parametro.setId(c.getInt(c.getColumnIndex(Parametro.ID)));
			parametro.setEmpresa(repoEmpresa.busca(c.getInt(c.getColumnIndex(Parametro.EMPRESA))));
			repoEmpresa.fechar();

			parametro.setCodigoRepresentante(c.getInt(c.getColumnIndex(Parametro.CODIGOREPRESENTANTE)));
			parametro.setNomeRepresentante(c.getString(c.getColumnIndex(Parametro.NOMEREPRESENTANTE)));
			if (c.getString(c.getColumnIndex(Parametro.SYNCAUTOMATICA)).equals("S")) {
				parametro.setSyncAutomatica(true);
			} else {
				parametro.setSyncAutomatica(false);
			}
			parametro.setUrlWebService(c.getString(c.getColumnIndex(Parametro.URLWEBSERVICE)));
			parametro.setEmail(c.getString(c.getColumnIndex(Parametro.EMAIL)));
			parametro.setSenhaEmail(c.getString(c.getColumnIndex(Parametro.SENHAEMAIL)));
			parametro.setSmtp(c.getString(c.getColumnIndex(Parametro.SMTP)));
			parametro.setAssinatura(c.getString(c.getColumnIndex(Parametro.ASSINATURA)));
			parametro.setPortaEmail(c.getInt(c.getColumnIndex(Parametro.PORTAEMAIL)));
			parametro.setMaximoDesconto(c.getDouble(c.getColumnIndex(Parametro.MAXIMODESCONTO)));
			parametro.setRepresentantefaixa(c.getInt(c.getColumnIndex(Parametro.REPRESENTANTEFAIXA)));

			if (c.getString(c.getColumnIndex(Parametro.SSL)).equals("S")) {
				parametro.setSsl(true);
			} else {
				parametro.setSsl(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.TRABALHAPROREGIAO)).equals("S")) {
				parametro.setTrabalhaProRegiao(true);
			} else {
				parametro.setTrabalhaProRegiao(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.USADADOSFINANCEIROS)).equals("S")) {
				parametro.setUsaDadosFinanceiros(true);
			} else {
				parametro.setUsaDadosFinanceiros(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.PRIMEIROACESSO)).equals("S")) {
				parametro.setPrimeiroAcesso(true);
			} else {
				parametro.setPrimeiroAcesso(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.EXIGEEMAILCADCLIENTE)).equals("S")) {
				parametro.setExigeEmailCadCliente(true);
			} else {
				parametro.setExigeEmailCadCliente(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.EXIGETELEFONECADCLEINTE)).equals("S")) {
				parametro.setExigeTelefoneCadCleinte(true);
			} else {
				parametro.setExigeTelefoneCadCleinte(false);
			}

			parametro.setNumeroMaxPecasPedido(c.getInt(c.getColumnIndex(Parametro.NUMEROMAXPECASPEDIDO)));

			parametro.setImagem(c.getBlob(c.getColumnIndex(Parametro.IMAGEM)));

			if (c.getString(c.getColumnIndex(Parametro.USADADOSFINANCEIROS)).equals("S")) {
				parametro.setUsaDadosFinanceiros(true);
			} else {
				parametro.setUsaDadosFinanceiros(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.PRIMEIROACESSO)).equals("S")) {
				parametro.setPrimeiroAcesso(true);
			} else {
				parametro.setPrimeiroAcesso(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.EXIGEEMAILCADCLIENTE)).equals("S")) {
				parametro.setExigeEmailCadCliente(true);
			} else {
				parametro.setExigeEmailCadCliente(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.EXIGETELEFONECADCLEINTE)).equals("S")) {
				parametro.setExigeTelefoneCadCleinte(true);
			} else {
				parametro.setExigeTelefoneCadCleinte(false);
			}

			parametro.setNumeroMaxPecasPedido(c.getInt(c.getColumnIndex(Parametro.NUMEROMAXPECASPEDIDO)));

			if (c.getString(c.getColumnIndex(Parametro.PERMITECNPJREPETIDO)).equals("S")) {
				parametro.setPermiteCnpjRepetido(true);
			} else {
				parametro.setPermiteCnpjRepetido(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.DUPLICACADASTROCLIENTE)).equals("S")) {
				parametro.setDuplicaCadastroCliente(true);
			} else {
				parametro.setDuplicaCadastroCliente(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.MULTITABELAPRECO)).equals("S")) {
				parametro.setMultiTabelaPreco(true);
			} else {
				parametro.setMultiTabelaPreco(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.USALOTE)).equals("S")) {
				parametro.setUsaLote(true);
			} else {
				parametro.setUsaLote(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.MOSTRAPRECONOPEDIDO)).equals("S")) {
				parametro.setMostraPrecoNoPedido(true);
			} else {
				parametro.setMostraPrecoNoPedido(false);
			}

			parametro.setChave(c.getString(c.getColumnIndex(Parametro.CHAVE)));

			c.close();
			return parametro;

		}
		c.close();
		return null;
	}

	public Parametro buscaPorLoja(int idDaLoja) {

		Cursor c = db.query(NOME_TABELA, Parametro.COLUNAS, Parametro.EMPRESA + "=?", new String[]{String.valueOf(idDaLoja)}, null, null, null);
		if (c.getCount() > 0) {
			
			// pocisiona no primeiro elemento
			c.moveToFirst();
			Parametro parametro = new Parametro();
			parametro.setId(c.getInt(c.getColumnIndex(Parametro.ID)));
			RepositorioEmpresa repoEmpresa = new RepositorioEmpresa(context);
			parametro.setEmpresa(repoEmpresa.busca(c.getInt(c.getColumnIndex(Parametro.EMPRESA))));
			repoEmpresa.fechar();

			parametro.setCodigoRepresentante(c.getInt(c.getColumnIndex(Parametro.CODIGOREPRESENTANTE)));
			parametro.setNomeRepresentante(c.getString(c.getColumnIndex(Parametro.NOMEREPRESENTANTE)));
			if (c.getString(c.getColumnIndex(Parametro.SYNCAUTOMATICA)).equals("S")) {
				parametro.setSyncAutomatica(true);
			} else {
				parametro.setSyncAutomatica(false);
			}

			parametro.setUrlWebService(c.getString(c.getColumnIndex(Parametro.URLWEBSERVICE)));
			parametro.setEmail(c.getString(c.getColumnIndex(Parametro.EMAIL)));
			parametro.setSenhaEmail(c.getString(c.getColumnIndex(Parametro.SENHAEMAIL)));
			parametro.setSmtp(c.getString(c.getColumnIndex(Parametro.SMTP)));
			parametro.setAssinatura(c.getString(c.getColumnIndex(Parametro.ASSINATURA)));
			parametro.setPortaEmail(c.getInt(c.getColumnIndex(Parametro.PORTAEMAIL)));
			parametro.setMaximoDesconto(c.getDouble(c.getColumnIndex(Parametro.MAXIMODESCONTO)));
			parametro.setRepresentantefaixa(c.getInt(c.getColumnIndex(Parametro.REPRESENTANTEFAIXA)));

			if (c.getString(c.getColumnIndex(Parametro.SSL)).equals("S")) {
				parametro.setSsl(true);
			} else {
				parametro.setSsl(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.TRABALHAPROREGIAO)).equals("S")) {
				parametro.setTrabalhaProRegiao(true);
			} else {
				parametro.setTrabalhaProRegiao(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.USADADOSFINANCEIROS)).equals("S")) {
				parametro.setUsaDadosFinanceiros(true);
			} else {
				parametro.setUsaDadosFinanceiros(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.PRIMEIROACESSO)).equals("S")) {
				parametro.setPrimeiroAcesso(true);
			} else {
				parametro.setPrimeiroAcesso(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.EXIGEEMAILCADCLIENTE)).equals("S")) {
				parametro.setExigeEmailCadCliente(true);
			} else {
				parametro.setExigeEmailCadCliente(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.EXIGETELEFONECADCLEINTE)).equals("S")) {
				parametro.setExigeTelefoneCadCleinte(true);
			} else {
				parametro.setExigeTelefoneCadCleinte(false);
			}

			parametro.setNumeroMaxPecasPedido(c.getInt(c.getColumnIndex(Parametro.NUMEROMAXPECASPEDIDO)));

			parametro.setImagem(c.getBlob(c.getColumnIndex(Parametro.IMAGEM)));

			if (c.getString(c.getColumnIndex(Parametro.USADADOSFINANCEIROS)).equals("S")) {
				parametro.setUsaDadosFinanceiros(true);
			} else {
				parametro.setUsaDadosFinanceiros(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.PRIMEIROACESSO)).equals("S")) {
				parametro.setPrimeiroAcesso(true);
			} else {
				parametro.setPrimeiroAcesso(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.EXIGEEMAILCADCLIENTE)).equals("S")) {
				parametro.setExigeEmailCadCliente(true);
			} else {
				parametro.setExigeEmailCadCliente(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.EXIGETELEFONECADCLEINTE)).equals("S")) {
				parametro.setExigeTelefoneCadCleinte(true);
			} else {
				parametro.setExigeTelefoneCadCleinte(false);
			}

			parametro.setNumeroMaxPecasPedido(c.getInt(c.getColumnIndex(Parametro.NUMEROMAXPECASPEDIDO)));

			if (c.getString(c.getColumnIndex(Parametro.PERMITECNPJREPETIDO)).equals("S")) {
				parametro.setPermiteCnpjRepetido(true);
			} else {
				parametro.setPermiteCnpjRepetido(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.DUPLICACADASTROCLIENTE)).equals("S")) {
				parametro.setDuplicaCadastroCliente(true);
			} else {
				parametro.setDuplicaCadastroCliente(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.MULTITABELAPRECO)).equals("S")) {
				parametro.setMultiTabelaPreco(true);
			} else {
				parametro.setMultiTabelaPreco(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.USALOTE)).equals("S")) {
				parametro.setUsaLote(true);
			} else {
				parametro.setUsaLote(false);
			}

			if (c.getString(c.getColumnIndex(Parametro.MOSTRAPRECONOPEDIDO)).equals("S")) {
				parametro.setMostraPrecoNoPedido(true);
			} else {
				parametro.setMostraPrecoNoPedido(false);
			}

			parametro.setChave(c.getString(c.getColumnIndex(Parametro.CHAVE)));

			c.close();
			return parametro;

		}
		c.close();
		return null;

	}

	// retorna cursor com todos os Parametroes
	public Cursor getCursor() {
		try {
			// select * frm carros
			return db.query(NOME_TABELA, Parametro.COLUNAS, null, null, null, null, null);
		} catch (SQLException e) {
			Log.e("Parametro", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public boolean contemRegistro() {
		boolean contemRegistro = false;

		Cursor c = getCursor();

		if (c.getCount() > 0) {
			contemRegistro = true;
		}
		c.close();

		return contemRegistro;
	}

	public List<Parametro> listar() {
		Cursor c = getCursor();
		List<Parametro> parametros = new ArrayList<Parametro>();

		if (c.moveToFirst()) {

			// loop at� o final

			do {
				RepositorioEmpresa repoEmpresa = new RepositorioEmpresa(context);
				Parametro parametro = new Parametro();
				parametros.add(parametro);

				parametro.setId(c.getInt(c.getColumnIndex(Parametro.ID)));
				parametro.setEmpresa(repoEmpresa.busca(c.getInt(c.getColumnIndex(Parametro.EMPRESA))));
				repoEmpresa.fechar();

				parametro.setCodigoRepresentante(c.getInt(c.getColumnIndex(Parametro.CODIGOREPRESENTANTE)));
				parametro.setNomeRepresentante(c.getString(c.getColumnIndex(Parametro.NOMEREPRESENTANTE)));
				if (c.getString(c.getColumnIndex(Parametro.SYNCAUTOMATICA)).equals("S")) {
					parametro.setSyncAutomatica(true);
				} else {
					parametro.setSyncAutomatica(false);
				}

				parametro.setUrlWebService(c.getString(c.getColumnIndex(Parametro.URLWEBSERVICE)));
				parametro.setEmail(c.getString(c.getColumnIndex(Parametro.EMAIL)));
				parametro.setSenhaEmail(c.getString(c.getColumnIndex(Parametro.SENHAEMAIL)));
				parametro.setSmtp(c.getString(c.getColumnIndex(Parametro.SMTP)));
				parametro.setAssinatura(c.getString(c.getColumnIndex(Parametro.ASSINATURA)));
				parametro.setPortaEmail(c.getInt(c.getColumnIndex(Parametro.PORTAEMAIL)));
				parametro.setMaximoDesconto(c.getDouble(c.getColumnIndex(Parametro.MAXIMODESCONTO)));
				parametro.setRepresentantefaixa(c.getInt(c.getColumnIndex(Parametro.REPRESENTANTEFAIXA)));

				if (c.getString(c.getColumnIndex(Parametro.SSL)).equals("S")) {
					parametro.setSsl(true);
				} else {
					parametro.setSsl(false);
				}

				if (c.getString(c.getColumnIndex(Parametro.TRABALHAPROREGIAO)).equals("S")) {
					parametro.setTrabalhaProRegiao(true);
				} else {
					parametro.setTrabalhaProRegiao(false);
				}

				if (c.getString(c.getColumnIndex(Parametro.USADADOSFINANCEIROS)).equals("S")) {
					parametro.setUsaDadosFinanceiros(true);
				} else {
					parametro.setUsaDadosFinanceiros(false);
				}

				if (c.getString(c.getColumnIndex(Parametro.PRIMEIROACESSO)).equals("S")) {
					parametro.setPrimeiroAcesso(true);
				} else {
					parametro.setPrimeiroAcesso(false);
				}

				if (c.getString(c.getColumnIndex(Parametro.EXIGEEMAILCADCLIENTE)).equals("S")) {
					parametro.setExigeEmailCadCliente(true);
				} else {
					parametro.setExigeEmailCadCliente(false);
				}

				if (c.getString(c.getColumnIndex(Parametro.EXIGETELEFONECADCLEINTE)).equals("S")) {
					parametro.setExigeTelefoneCadCleinte(true);
				} else {
					parametro.setExigeTelefoneCadCleinte(false);
				}

				parametro.setNumeroMaxPecasPedido(c.getInt(c.getColumnIndex(Parametro.NUMEROMAXPECASPEDIDO)));

				parametro.setImagem(c.getBlob(c.getColumnIndex(Parametro.IMAGEM)));

				if (c.getString(c.getColumnIndex(Parametro.USADADOSFINANCEIROS)).equals("S")) {
					parametro.setUsaDadosFinanceiros(true);
				} else {
					parametro.setUsaDadosFinanceiros(false);
				}

				if (c.getString(c.getColumnIndex(Parametro.PRIMEIROACESSO)).equals("S")) {
					parametro.setPrimeiroAcesso(true);
				} else {
					parametro.setPrimeiroAcesso(false);
				}

				if (c.getString(c.getColumnIndex(Parametro.EXIGEEMAILCADCLIENTE)).equals("S")) {
					parametro.setExigeEmailCadCliente(true);
				} else {
					parametro.setExigeEmailCadCliente(false);
				}

				if (c.getString(c.getColumnIndex(Parametro.EXIGETELEFONECADCLEINTE)).equals("S")) {
					parametro.setExigeTelefoneCadCleinte(true);
				} else {
					parametro.setExigeTelefoneCadCleinte(false);
				}

				parametro.setNumeroMaxPecasPedido(c.getInt(c.getColumnIndex(Parametro.NUMEROMAXPECASPEDIDO)));

				if (c.getString(c.getColumnIndex(Parametro.PERMITECNPJREPETIDO)).equals("S")) {
					parametro.setPermiteCnpjRepetido(true);
				} else {
					parametro.setPermiteCnpjRepetido(false);
				}

				if (c.getString(c.getColumnIndex(Parametro.DUPLICACADASTROCLIENTE)).equals("S")) {
					parametro.setDuplicaCadastroCliente(true);
				} else {
					parametro.setDuplicaCadastroCliente(false);
				}

				if (c.getString(c.getColumnIndex(Parametro.MULTITABELAPRECO)).equals("S")) {
					parametro.setMultiTabelaPreco(true);
				} else {
					parametro.setMultiTabelaPreco(false);
				}

				if (c.getString(c.getColumnIndex(Parametro.USALOTE)).equals("S")) {
					parametro.setUsaLote(true);
				} else {
					parametro.setUsaLote(false);
				}

				if (c.getString(c.getColumnIndex(Parametro.MOSTRAPRECONOPEDIDO)).equals("S")) {
					parametro.setMostraPrecoNoPedido(true);
				} else {
					parametro.setMostraPrecoNoPedido(false);
				}

				parametro.setChave(c.getString(c.getColumnIndex(Parametro.CHAVE)));

			} while (c.moveToNext());
		}
		c.close();
		return parametros;
	}

	/**
	 * 
	 * @param idEmpresa
	 *            Int idEmpresa
	 * @return dataSincronismo Date data da realiza��o do �ltimo sinconismo de
	 *         RECEBIMENTO e onde as foi trazido as contas a receber do cliente
	 */
	public Date verificaUltmoSincrnismo(int idEmpresa) {
		Date dataSincronismo = null;
		String sql = "SELECT MAX(" + Parametro.SYNC_DATA + ") FROM " + Parametro.TABELA_SYNC + " WHERE " + Parametro.SYNC_EMPRESA + " = " + idEmpresa + " AND SYNC_TIPO = 'R' AND "
				+ Parametro.SYNC_CONTARECEBER + " = 'S'";

		Cursor c = db.rawQuery(sql, null);

		if (c.getCount() > 0) {
			c.moveToFirst();
			dataSincronismo = new Date(c.getLong(c.getColumnIndex("MAX(" + Parametro.SYNC_DATA + ")")));

		}
		c.close();
		return dataSincronismo;
	}

	public boolean replicaCadastro(int idEmpresa) {
		boolean replica = false;

		String sql = "SELECT " + Parametro.DUPLICACADASTROCLIENTE + " FROM " + Parametro.NOME_TABELA + " WHERE " + Parametro.EMPRESA + " = " + idEmpresa;

		Cursor c = db.rawQuery(sql, null);

		if (c.getCount() > 0) {
			c.moveToFirst();

			if (c.getString(c.getColumnIndex(Parametro.DUPLICACADASTROCLIENTE)).equals("S")) {
				replica = true;
			}
		}
		c.close();
		return replica;

	}

	public void fechar() {

		if (db != null) {
			db.close();
		}
	}

//	private ContentValues montaValuesSincronismo(Sincronismo s) {
//		ContentValues v = new ContentValues();
//		v.put(Parametro.SYNC_CEP, s.getCep());
//		v.put(Parametro.SYNC_CLIENTE, s.getCliente());
//		v.put(Parametro.SYNC_CONDICAOPGTO, s.getCondicaoPgto());
//		v.put(Parametro.SYNC_CONTARECEBER, s.getContaReceber());
//		v.put(Parametro.SYNC_DATA, s.getData().getTime());
//		v.put(Parametro.SYNC_EMPRESA, s.getEmpresa().getId());
//		v.put(Parametro.SYNC_FAIXACODIGO, s.getFaixaCodigo());
//		v.put(Parametro.SYNC_PEDIDO, s.getPedido());
//		v.put(Parametro.SYNC_TABPRECO, s.getTabelaPreco());
//		v.put(Parametro.SYNC_TIPO, s.getTipo());
//
//		return v;
//	}

//	public long salvarSincronismo(Sincronismo sincronismo) {
//		ContentValues v = montaValuesSincronismo(sincronismo);
//		return db.insert(Parametro.TABELA_SYNC, "", v);
//
//	}
}
