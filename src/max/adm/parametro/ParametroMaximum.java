package max.adm.parametro;

import java.io.Serializable;
import java.math.BigDecimal;


public class ParametroMaximum implements Serializable {

	private static final long serialVersionUID = 1L;
	public Long id;
	public BigDecimal maximodesconto;
	public String parAvisamensagem;
	public String parComisportabeladepreco;
	public String parDadosFin;
	public String parDuplicacadastrocliente;
	public String parExigeEmailCadCliente;
	public String parExigeFoneCadCiente;
	public byte[] parImagem;
	public BigDecimal parLjaCodigo;
	public String parMultitabelapreco;
	public String parNomeparametro;
	public String parPermitecnpjrepetido;
	public BigDecimal parQtdmensagem;
	public String parRegiao;
	public String parSyncAutomatica;
	public String parUsaLotes;

	
}
