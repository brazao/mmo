package max.adm.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
	
	public static final String nomeBanco = "maxmobile.db";
	public static final int versaoBanco = 1;
	
	public DBHelper(Context context) {
		super(context, nomeBanco, null, versaoBanco);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.i("banco ", "criando banco");
		
//		new RepositorioCepLogradouroScript(db).create();
//		new RepositorioCidadeScript(db).create();
//		new RepositorioClassificacaoPessoaScript(db).create();
//		new RepositorioCondicaoPagamentoScript(db).create();
//		new RepositorioContaReceberScript(db).create();
//		new RepositorioEmailScript(db).create();
//		new RepositorioEmpresaScript(db).create();
//		new RepositorioEnderecoScript(db).create();
//		new RepositorioEstadoScrpt(db).create();
//		new RepositorioGrupoDeProdutosScript(db).create();
//		new RepositorioItemNotaFiscalScript(db).create();
//		new RepositorioLoteScript(db).create();
//		new RepositorioMensagemScript(db).create();
//		new RepositorioNotaFiscalCapaScript(db).create();
//		new RepositorioPadroScript(db).create();
//		new RepositorioPaisScript(db).create();
//		new RepositorioParametroScript(db).create();
//		new RepositorioPedidoCapaScript(db).create();
//		new RepositorioPedidoItensScript(db).create();
//		new RepositorioPessoaScript(db).create();
//		new RepositorioProdutoReduzidoScript(db).create();
//		new RepositorioProdutoScript(db).create();
//		new RepositorioProImagemScript(db).create();
//		new RepositorioTipoLogradouroScript(db).create();
//		new RepositorioUsuarioScript(db).create();
//		new RepositorioTipoCobrancaScript(db).create();
//		new RepositorioTamanhoScript(db).create();
//		new RepositorioUnidadedeMedidaScript(db).create();
//		new RepostorioTabelaDePrecoScript(db).create();
//		new RepositorioTelefoneScript(db).create();
		
		
		Log.i("banco ", "banco criado com sucesso");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int versaoAntiga, int novaVersao) {
		Log.w("banco", "atualizando versao -> de "+versaoAntiga+" para " + novaVersao + ". Todos registros serao deletados");
	}
}
