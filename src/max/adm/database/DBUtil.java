package max.adm.database;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;

public class DBUtil {

	public static boolean gravaBancoNaPasta(Context context, byte[] banco) throws IOException {
		File file = new File(context.getDatabasePath(DBHelper.nomeBanco).getPath());

		FileOutputStream fileOutputStream = new FileOutputStream(file);
		fileOutputStream.write(banco);
		fileOutputStream.close();
		
		return file.exists();
	}

}
