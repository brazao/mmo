package max.adm.telefone;

import java.util.ArrayList;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.empresa.RepositorioEmpresa;
import max.adm.entidade.Empresa;
import max.adm.entidade.Telefone;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RepositorioTelefone {

	private Context context;

	protected SQLiteDatabase db;

	public RepositorioTelefone(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public long salvar(Telefone telefone) {
		long id = telefone.getCodigo();
		if (id != 0) {
			atualizar(telefone);
		} else {
			id = inserir(telefone);
		}

		return id;
	}

	public int inserir(Telefone telefone) {
		ContentValues values = new ContentValues();
		values.put(Telefone.NUMEROTELEFONE, telefone.getNumeroTelefone());
		values.put(Telefone.TIPOTELEFONE, telefone.getTipoTelefone().getDescricao());
		values.put(Telefone.PESSOA, telefone.getPessoa().getCodigo());

		int id = inserir(values);

		return id;
	}

	public int inserir(ContentValues values) {
		int id = (int) db.insert(Telefone.NOME_TABELA, "", values);
		return id;
	}

	public int atualizar(Telefone telefone) {
		ContentValues values = new ContentValues();
		values.put(Telefone.NUMEROTELEFONE, telefone.getNumeroTelefone());
		values.put(Telefone.TIPOTELEFONE, telefone.getTipoTelefone().getDescricao());
		values.put(Telefone.PESSOA, telefone.getPessoa().getCodigo());

		String _id = String.valueOf(telefone.getCodigo());
		String where = Telefone.CODIGO + "=?";
		String[] whereArgs = new String[] { _id };
		int count = atualizar(values, where, whereArgs);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(Telefone.NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public int deletar(int id) {
		String where = Telefone.CODIGO + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[] { _id };
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(Telefone.NOME_TABELA, where, whereArgs);
		return count;
	}
	
	public List<Telefone> buscaPorPessoa(int pessoaId, int empresaId) {
		Cursor c = db.query(true, Telefone.NOME_TABELA, Telefone.COLUNAS,
				Telefone.PESSOA + "=" + pessoaId, null, null, null, null, null);
		List<Telefone> telefones = new ArrayList<Telefone>();
		RepositorioEmpresa repositorioEmpresa = new RepositorioEmpresa(context);
		while (c.moveToNext()) {
				telefones.add(Telefone.builder(
						c.getInt(c.getColumnIndex(Telefone.CODIGO)),
						repositorioEmpresa.busca(empresaId),
						tipoTelefone(c.getString(c.getColumnIndex(Telefone.TIPOTELEFONE))))
						.withNumero(c.getString(c.getColumnIndex(Telefone.NUMEROTELEFONE)))
					.build());

		}
		repositorioEmpresa.fechar();
		c.close();
		return telefones;
	}

	private TipoTelefone tipoTelefone(String tipo) {
		if (tipo != null) {
			if (tipo.equals(TipoTelefone.F.getDescricao())) {
				return TipoTelefone.F;
			}
			if (tipo.equals(TipoTelefone.C.getDescricao())) {
				return TipoTelefone.C;
			}
		}
		return TipoTelefone.T;
	}

	// retorna cursor com todos os Telefonees
	public Cursor getCursor() {
		try {
			return db.query(Telefone.NOME_TABELA, Telefone.COLUNAS, null, null, null, null, null);
		} catch (SQLException e) {
			Log.e("Telefone", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<Telefone> listar() {
		Cursor c = getCursor();
		List<Telefone> telefones = new ArrayList<Telefone>();
		RepositorioEmpresa repoEmpresa = new RepositorioEmpresa(context);
		if (c.moveToFirst()) {

			do {
				Empresa empresa = repoEmpresa.buscaEmpresa(c.getInt(c.getColumnIndex(Telefone.EMPRESA)));
				telefones.add(Telefone
						.builder(
								c.getInt(c.getColumnIndex(Telefone.CODIGO)),
								empresa,
								tipoTelefone(c.getString(c.getColumnIndex(Telefone.TIPOTELEFONE))))
						.withNumero(c.getString(c.getColumnIndex(Telefone.NUMEROTELEFONE)))
						.build());

			} while (c.moveToNext());
		}
		repoEmpresa.fechar();

		return telefones;
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

}
