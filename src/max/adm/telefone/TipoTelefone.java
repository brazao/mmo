package max.adm.telefone;

public enum TipoTelefone {
	
	T("Telefone"),C("Celular"),F("Fax");
	
	private TipoTelefone(String descricao){
		this.descricao = descricao;
	}
	
	public String getDescricao(){
		return this.descricao;
	}
	
	private String descricao;

}
