package max.adm.controle;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import max.adm.R;
import max.adm.ValorAtividade;
import max.adm.endereco.RepositorioEndereco;
import max.adm.endereco.TipoEndereco;
import max.adm.entidade.Cidade;
import max.adm.entidade.Empresa;
import max.adm.entidade.Endereco;
import max.adm.utilidades.ValidaString;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.Lists;

public class EnderecoControle extends Activity {

	private final int PRI = 0;
	private final int ENT = 1;
	private final int COBR = 2;
	private int ATUAL;

	private RadioGroup radioGroupTipoEndereco;
	private RadioButton radioPrincipal;
	private RadioButton radioEntrega;
	private RadioButton radioCobranca;

	private EditText fieldCep;
	private EditText fieldLogradouro;
	private EditText fieldNumero;
	private EditText fieldComplemento;
	private EditText fieldBairro;

	private Spinner spinnerTipoLogradouro;

	private ImageView btEditaCidade;
	private Button btConfirma;
	private Button btCancela;

	private ImageView imgChekCep;
	private ImageView imgChekLogradouro;
	private ImageView imgChekNumero;

	private TextView textCidade;
	private TextView textEstado;

	private List<Endereco> enderecos = Lists.newLinkedList();
	private Endereco enderecoPrincipal;
	private Endereco enderecoEntrega;
	private Endereco enderecoCobranca;
	private Cidade cidade;
	private List<String> tipoLogradouros = new LinkedList<String>();

	private Empresa empresa;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.endereco);
		empresa = (Empresa) getIntent().getExtras().getSerializable("EMPRESA");
		
		ATUAL = PRI;
		montaTela();
		definefincoes();
		if (getIntent().getExtras() != null) {
			enderecos = (List<Endereco>) getIntent().getExtras().getSerializable("ENDERECOS");
		}
		if (enderecos != null) {
			for (Endereco e : enderecos) {
				if (e.getTipoEndereco().equals(TipoEndereco.PR)) {
					enderecoPrincipal = e;
				} else if (e.getTipoEndereco().equals(TipoEndereco.EN)) {
					enderecoEntrega = e;
				} else if (e.getTipoEndereco().equals(TipoEndereco.CO)) {
					enderecoCobranca = e;
				}
			}
			atualizaTela(enderecoPrincipal);
		} else {
			enderecos = Lists.newLinkedList();
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data != null && requestCode == ValorAtividade.CidadeList.get()) {
			cidade = (Cidade) data.getExtras().getSerializable("CIDADE");
			if (cidade != null) {
				textCidade.setText(cidade.getNome());
				textEstado.setText(cidade.getEstado().getSigla());
				switch (radioGroupTipoEndereco.getCheckedRadioButtonId()) {
				case R.id.radio_endereco_principal:
					if (enderecoPrincipal != null) {
						enderecoPrincipal.setCidade(cidade);
					}
					break;
				case R.id.radio_endereco_entrega:
					if (enderecoEntrega != null) {
						enderecoEntrega.setCidade(cidade);
					}
					break;
				case R.id.radio_endereco_cobranca:
					if (enderecoCobranca != null) {
						enderecoCobranca.setCidade(cidade);
					}
					break;
				default:
					break;
				}
			}

		}

	}

	@Override
	public void onBackPressed() {
		AlertDialog.Builder alerta = new AlertDialog.Builder(this);
		alerta.setTitle(R.string.titulo_atencao);
		alerta.setMessage(R.string.mensagem_tem_certeza_deseja_sair_sem_confirmar);
		alerta.setPositiveButton(R.string.bt_sim, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				finish();

			}
		});
		alerta.setNegativeButton(R.string.bt_nao, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();

			}
		});
		alerta.show();
	}

	private void montaTela() {
		radioGroupTipoEndereco = (RadioGroup) findViewById(R.id.radio_tipo_endereco);
		radioPrincipal = (RadioButton) findViewById(R.id.radio_endereco_principal);
		radioEntrega = (RadioButton) findViewById(R.id.radio_endereco_entrega);
		radioCobranca = (RadioButton) findViewById(R.id.radio_endereco_cobranca);

		fieldCep = (EditText) findViewById(R.id.text_cep);
		fieldLogradouro = (EditText) findViewById(R.id.text_logradouro);
		fieldNumero = (EditText) findViewById(R.id.text_numero_enderec);
		fieldComplemento = (EditText) findViewById(R.id.text_complemento_endereco);
		fieldBairro = (EditText) findViewById(R.id.text_bairro);

		spinnerTipoLogradouro = (Spinner) findViewById(R.id.spinner_tipo_logradouro);

		btEditaCidade = (ImageView) findViewById(R.id.bt_altera_cidade);
		btConfirma = (Button) findViewById(R.id.bt_confirma_edereco);
		btCancela = (Button) findViewById(R.id.bt_cancela_edereco);

		textCidade = (TextView) findViewById(R.id.text_cidade);
		textEstado = (TextView) findViewById(R.id.text_estado);

		imgChekCep = (ImageView) findViewById(R.id.img_chek_cep);
		imgChekLogradouro = (ImageView) findViewById(R.id.img_chek_logradouro);
		imgChekNumero = (ImageView) findViewById(R.id.img_chek_numero);
	}

	private void definefincoes() {
		RepositorioEndereco repositorioEndereco = new RepositorioEndereco(this);
		tipoLogradouros = repositorioEndereco.buscaTipoLogradouros();
		repositorioEndereco.fechar();

		ArrayAdapter<String> adapterTiposlogradouro = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, tipoLogradouros);
		spinnerTipoLogradouro.setAdapter(adapterTiposlogradouro);

		radioGroupTipoEndereco.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(RadioGroup group, int id) {
				imgChekCep.setImageResource(0);
				imgChekLogradouro.setImageResource(0);
				imgChekNumero.setImageResource(0);
				atualizaEndereco();

			}
		});

		fieldCep.setOnFocusChangeListener(new OnFocusChangeListener() {

			public void onFocusChange(View arg0, boolean hasFocus) {
				if (!hasFocus) {
					if (ValidaString.cep(fieldCep.getText().toString())) {
						imgChekCep.setImageResource(R.drawable.img_chek);
						fieldCep.setText(ValidaString.poeMascaraCep(fieldCep.getText().toString()));

						// buscaCep(buscarCep);

					} else {
						imgChekCep.setImageResource(R.drawable.img_error);
					}
				}
			}
		});

		fieldLogradouro.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					if (!fieldLogradouro.getText().toString().equals("")) {
						imgChekLogradouro.setImageResource(R.drawable.img_chek);
					} else {
						imgChekLogradouro.setImageResource(R.drawable.img_error);
					}
				}

			}
		});

		fieldNumero.setOnFocusChangeListener(new OnFocusChangeListener() {

			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {

					if (fieldNumero.getText().toString().equals("0")
							|| fieldNumero.getText().toString().length() > 10) {
						imgChekNumero.setImageResource(R.drawable.img_error);
					} else if (fieldNumero.getText().toString().equals("")
							|| fieldNumero.getText().toString() == null) {
						fieldNumero.setText("S/N");
						imgChekNumero.setImageResource(R.drawable.img_chek);

					} else {
						imgChekNumero.setImageResource(R.drawable.img_chek);
					}
				}
			}
		});

		btEditaCidade.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				Intent cidade = new Intent(EnderecoControle.this, CidadeListControle.class);
				startActivityForResult(cidade, ValorAtividade.CidadeList.get());

			}
		});

		btConfirma.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				if (confirmaEndereco()) {
					finalizar();
				}

			}
		});

		btCancela.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				onBackPressed();

			}
		});

	}

	private void atualizaEndereco() {
		switch (ATUAL) {
		case PRI:
			atulizaEnderecoPrincipal();
			break;

		case ENT:
			atulizaenderecoEntrega();
			break;

		case COBR:
			atulizaenderecoCobranca();
			break;

		default:
			break;
		}
		switch (radioGroupTipoEndereco.getCheckedRadioButtonId()) {
		case R.id.radio_endereco_principal:
			ATUAL = PRI;
			atualizaTela(enderecoPrincipal);
			break;
		case R.id.radio_endereco_entrega:
			ATUAL = ENT;
			atualizaTela(enderecoEntrega);
			break;
		case R.id.radio_endereco_cobranca:
			ATUAL = COBR;
			atualizaTela(enderecoCobranca);
			break;
		default:
			break;
		}
	}

	private void atualizaTela(Endereco endereco) {
		if (endereco != null) {
			fieldCep.setText(ValidaString.poeMascaraCep(endereco.getCep()));
			fieldComplemento.setText(endereco.getComplemento());
			fieldLogradouro.setText(endereco.getLogradouro());
			fieldBairro.setText(endereco.getBairro());
			fieldNumero.setText(endereco.getNumero());
			if (endereco.getCidade() != null) {
				textCidade.setText(endereco.getCidade().getNome());
				textEstado.setText(endereco.getCidade().getEstado().getSigla());
				textCidade.setTextColor(getResources().getColor(R.color.branco));
				cidade = endereco.getCidade();
			} else {
				textCidade.setText("");
			}
		} else {
			fieldCep.setText("");
			fieldComplemento.setText("");
			fieldLogradouro.setText("");
			fieldBairro.setText("");
			fieldNumero.setText("");
			textCidade.setText("");
		}

	}

	private boolean validaCampos(Endereco e) {
		if (e == null) {
			return false;
		}
		boolean tudoOk = true;

		if (e.getCep() != null) {
			if (!ValidaString.cep(e.getCep())) {
				tudoOk = false;
				imgChekCep.setImageResource(R.drawable.img_error);
			}
		} else {
			imgChekCep.setImageResource(R.drawable.img_chek);
		}

		if (e.getLogradouro().equals("")) {
			tudoOk = false;
			imgChekLogradouro.setImageResource(R.drawable.img_error);
		} else {
			imgChekLogradouro.setImageResource(R.drawable.img_chek);
		}
		if ((e.getNumero().equals("") || e.getNumero().equals("0")) && (!e.getNumero().equals("S/N"))) {
			tudoOk = false;
			imgChekNumero.setImageResource(R.drawable.img_error);
		} else {
			imgChekNumero.setImageResource(R.drawable.img_chek);
		}

		if (e.getCidade() == null) {
			tudoOk = false;
			textCidade.setText(R.string.mensagem_cidade_nao_preenchida);
			textCidade.setTextColor(getResources().getColor(R.color.vermelho));
		} else {
			textCidade.setTextColor(getResources().getColor(R.color.branco));
		}
		return tudoOk;
	}

	private boolean confirmaEndereco() {
		atualizaEndereco();
		if (enderecoPrincipal == null) {
			Toast.makeText(this, "Endereco Principal não foi preenchido", Toast.LENGTH_LONG).show();
			return false;
		}

		if (!validaCampos(enderecoPrincipal)) {
			radioPrincipal.setChecked(true);
			return false;
		} else if (enderecoEntrega != null && !validaCampos(enderecoEntrega)) {
			radioEntrega.setChecked(true);
			return false;
		} else if (enderecoCobranca != null && !validaCampos(enderecoCobranca)) {
			radioCobranca.setChecked(true);
			return false;
		} else {
			enderecos.clear();
			enderecos.add(enderecoPrincipal);
			if (enderecoEntrega != null)
				enderecos.add(enderecoEntrega);
			if (enderecoCobranca != null)
				enderecos.add(enderecoCobranca);
			return true;
		}

	}

	private void finalizar() {
		Intent i = new Intent();
		Bundle b = new Bundle();
		b.putSerializable("ENDERECOS", (Serializable) enderecos);
		i.putExtras(b);
		setResult(ValorAtividade.Endereco.get(), i);
		finish();

	}

//	private int getPositionTpLog(String tipoLogradouro) {
//		int i = 0;
//		for (i = 0; i < tipoLogradouros.size(); i++) {
//			if (tipoLogradouro.equals(tipoLogradouros.get(i))) {
//				break;
//			}
//		}
//		return i;
//	}

	private void atulizaEnderecoPrincipal() {
		if (enderecoPrincipal == null) {
			enderecoPrincipal = new Endereco();
		}
		enderecoPrincipal.setBairro(fieldBairro.getText().toString());
		enderecoPrincipal.setCep(ValidaString.tiraMascara(fieldCep.getText().toString()));
		enderecoPrincipal.setCidade(cidade);
		enderecoPrincipal.setComplemento(fieldComplemento.getText().toString());
		enderecoPrincipal.setNumero(fieldNumero.getText().toString());
		enderecoPrincipal.setTipoEndereco(TipoEndereco.PR);
		enderecoPrincipal.setEmpresa(empresa);
		enderecoPrincipal.setTipoLogradouro(String.valueOf(spinnerTipoLogradouro.getSelectedItem()));
		enderecoPrincipal.setLogradouro(fieldLogradouro.getText().toString());

	}

	private void atulizaenderecoEntrega() {
		if (enderecoEntrega == null) {
			enderecoEntrega = new Endereco();
		}
		enderecoEntrega.setBairro(fieldBairro.getText().toString());
		enderecoEntrega.setCep(ValidaString.tiraMascara(fieldCep.getText().toString()));
		enderecoEntrega.setCidade(cidade);
		enderecoEntrega.setComplemento(fieldComplemento.getText().toString());
		enderecoEntrega.setLogradouro(fieldLogradouro.getText().toString());
		enderecoEntrega.setNumero(fieldNumero.getText().toString());
		enderecoEntrega.setTipoEndereco(TipoEndereco.EN);
		enderecoEntrega.setEmpresa(empresa);
		enderecoEntrega.setTipoLogradouro(String.valueOf(spinnerTipoLogradouro.getSelectedItem()));
	}

	private void atulizaenderecoCobranca() {
		if (enderecoCobranca == null) {
			enderecoCobranca = new Endereco();
		}
		enderecoCobranca.setBairro(fieldBairro.getText().toString());
		enderecoCobranca.setCep(ValidaString.tiraMascara(fieldCep.getText().toString()));
		enderecoCobranca.setCidade(cidade);
		enderecoCobranca.setComplemento(fieldComplemento.getText().toString());
		enderecoCobranca.setNumero(fieldNumero.getText().toString());
		enderecoCobranca.setTipoEndereco(TipoEndereco.CO);
		enderecoCobranca.setEmpresa(empresa);
		enderecoCobranca.setTipoLogradouro(String.valueOf(spinnerTipoLogradouro.getSelectedItem()));
		enderecoCobranca.setLogradouro(fieldLogradouro.getText().toString());
	}

}
