package max.adm.controle;

import java.util.ArrayList;
import java.util.List;

import max.adm.R;
import max.adm.entidade.ImagemProduto;
import max.adm.entidade.Produto;
import max.adm.entidade.TabelaDePreco;
import max.adm.utilidades.adapters.ProdutoItemParaListar;
import max.adm.utilidades.adapters.ProdutoListAdapter;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

public class ProdutoListControle extends ListActivity {
	public static final int progress_bar_type = 0; // usado para a barra de
													// progresso
	private ProgressDialog pDialog; // usado na barra de progresso.

	AutoCompleteTextView fieldBuscaProduto;

	List<Produto> produtos;
	List<ProdutoItemParaListar> produtosLista;

	TabelaDePreco tabPreco;

	Button btBuscar;

	private ProdutoListAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.produto_list);

		montaTela();

		listaProdutos();

		defineFunces();

	}

	private void montaTela() {
		fieldBuscaProduto = (AutoCompleteTextView) findViewById(R.id.field_busca_produto);
		btBuscar = (Button) findViewById(R.id.bt_busca_produto);

	}

	private void listaProdutos() {

		if (getIntent().getExtras() == null) {
			Toast.makeText(this, "Não identifcamos uma tabela de preço definida. Faça o Login novamente!", Toast.LENGTH_LONG).show();

		} else {
			tabPreco = (TabelaDePreco) getIntent().getExtras().getSerializable("TABELAPRECO");

			// ////////////// carregar os produtos .. essa pesquisa pode demorar
			// /////////////////

			produtos = Produto.listarParaTela(this, tabPreco.getCodigo());

			produtosLista = new ArrayList<ProdutoItemParaListar>();

			// for (Produto p : produtos) {
			for (int i = 0; i < produtos.size(); i++) {
				Produto p = produtos.get(i);
				ProdutoItemParaListar prod = new ProdutoItemParaListar();
				prod.setProduto(p);
				produtosLista.add(prod);
			}

			adapter = new ProdutoListAdapter(ProdutoListControle.this, produtosLista);
			setListAdapter(adapter);

			// for (ProdutoItemParaListar p : produtosLista) {
			//
			// p.setImagem(ProImagem.buscaPorProduto(ProdutoListControle.this, p
			// .getProduto().getId()));
			// }

			new CarregaProduto().execute("");

			// busca os padroes, tamanhos e precos
			// for (int k = 0; k < produtosLista.size(); k++) {
			// ProdutoItemParaListar p = produtosLista.get(k);
			//
			// p.setReduzidos(ProdutoReduzido.listarParaTela(
			// ProdutoListControle.this, p.getProduto().getId()));
			// }

			// adapter = new ProdutoListAdapter(this, produtosLista);
			// setListAdapter(adapter);
			// //////////////////////////////////////////////////////////////////////////////////////////

		}

	}

	private void defineFunces() {

		ArrayAdapter<Produto> adapterProd = new ArrayAdapter<Produto>(this, R.layout.modelo_list_string, produtos);

		fieldBuscaProduto.setAdapter(adapterProd);

		fieldBuscaProduto.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> lista, View arg1, int position, long arg3) {
				Produto prod;
				String nomeProduto;
				String buscar = fieldBuscaProduto.getText().toString();
				for (int i = 0; i < produtos.size(); i++) {
					prod = produtos.get(i);
					nomeProduto = prod.toString();
					nomeProduto = nomeProduto.toLowerCase();
					buscar = buscar.toLowerCase();
					if (nomeProduto.contains(buscar)) {
						getListView().setSelection(i);
						break;
					}
				}
				InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				mgr.hideSoftInputFromWindow(fieldBuscaProduto.getWindowToken(), 0);

			}
		});

		btBuscar.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				Produto prod;
				String nomeProduto;
				String buscar = fieldBuscaProduto.getText().toString();
				for (int i = 0; i < produtos.size(); i++) {
					prod = produtos.get(i);
					nomeProduto = prod.toString();
					nomeProduto = nomeProduto.toLowerCase();
					buscar = buscar.toLowerCase();
					if (nomeProduto.contains(buscar)) {
						getListView().setSelection(i);
						break;
					}
				}
				InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				mgr.hideSoftInputFromWindow(fieldBuscaProduto.getWindowToken(), 0);

			}
		});
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();

		finish();

	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type: // we set this to 0
			pDialog = new ProgressDialog(this);
			pDialog.setMessage("Carregando produtos");
			pDialog.setIndeterminate(false);
			pDialog.setMax(100);
			pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pDialog.setCancelable(false);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}

	}

	class CarregaProduto extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {

			// try{
			// pDialog.setMessage("Carregando Produtos...");
			for (int i = 0; i < produtosLista.size(); i++) {
				ProdutoItemParaListar p = produtosLista.get(i);
				// publishProgress(""+(int)((i*50)/produtosLista.size()));
				p.setImagem(ImagemProduto.buscaPorProduto(ProdutoListControle.this, p.getProduto().getCodigo()).getImagem());
			}

			for (int k = 0; k < produtosLista.size(); k++) {
				ProdutoItemParaListar p = produtosLista.get(k);
				// publishProgress(""+(int) (((k*50)/produtosLista.size())+50)
				// );
//				p.setReduzidos(ProdutoReduzido.listarParaTela(ProdutoListControle.this, p.getProduto().getId(), tabPreco.getId()));
			}

			// catch (Exception e) {
			// // TODO: handle exception
			// }
			return null;
		}

		// @Override
		// protected void onPreExecute() {
		// super.onPreExecute();
		// //showDialog(progress_bar_type);
		// }

		// @Override
		// protected void onProgressUpdate(String... progress) {
		// //pDialog.setProgress(Integer.parseInt(progress[0]));
		// }

		@Override
		protected void onPostExecute(String result) {
			// dismissDialog(progress_bar_type);
			// adapter = new ProdutoListAdapter(ProdutoListControle.this,
			// produtosLista);
			// setListAdapter(adapter);
			adapter.notifyDataSetChanged();
		}

	}

}
