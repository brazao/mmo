package max.adm.controle;

import java.util.List;

import max.adm.R;
import max.adm.ValorAtividade;
import max.adm.entidade.Parametro;
import max.adm.entidade.Produto;
import max.adm.entidade.TabelaDePreco;
import max.adm.pedido.ListaDeReduzidos;
import max.adm.utilidades.adapters.ProdutoItemPedidoParaListar;
import max.adm.utilidades.adapters.ProdutoListPedidoAdapter;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.google.common.collect.Lists;

public class PedidoPesquisaProdutoControle extends ListActivity {

	List<Produto> produtos;
	Parametro param;
	List<TabelaDePreco> tabelas;
	List<ProdutoItemPedidoParaListar> listaMostrada = Lists.newLinkedList();
	ProdutoListPedidoAdapter adapter;

	AutoCompleteTextView fieldBusca;
	private Button btPesquisa;

	boolean mostraImagem = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.produto_list_para_pedido);
		// byte[] bytes = (byte[])
		// getIntent().getExtras().getSerializable("PRODUTOS");
		// listaMostrada = Comprimir.byteArrayToList(bytes,
		// ProdutoItemPedidoParaListar.class, new
		// TypeToken<List<ProdutoItemPedidoParaListar>>() {
		// }.getType());
		listaMostrada = ListaDeReduzidos.getInstance().getReduzidos();
		Log.d(PedidoPesquisaProdutoControle.class.getName(), "instanciando");
		montaTela();
		preencheTela();
		defineFuncoes();
	}

	private void montaTela() {
		fieldBusca = (AutoCompleteTextView) findViewById(R.id.field_busca_produto_para_pedido);
		btPesquisa = (Button) findViewById(R.id.bt_busca_produto_para_pedido);
	}

	private void preencheTela() {
		adapter = new ProdutoListPedidoAdapter(this, listaMostrada);
		setListAdapter(adapter);
		fieldBusca.setAdapter(adapter);
	}

	private void defineFuncoes() {

		getListView().setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> lista, View arg1, int position, long arg3) {
				ProdutoItemPedidoParaListar produto = (ProdutoItemPedidoParaListar) lista.getItemAtPosition(position);
				Bundle b = new Bundle();
				b.putSerializable("PRODUTO", produto);
				Intent i = new Intent();
				i.putExtras(b);
				setResult(ValorAtividade.PedidoPesquisaProduto.get(), i);
				finish();
			}
		});
	}

}
