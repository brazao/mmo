package max.adm.controle;

import java.util.List;

import com.google.common.collect.Lists;

import max.adm.R;
import max.adm.ValorAtividade;
import max.adm.cidade.RepositorioCidade;
import max.adm.entidade.Cidade;
import max.adm.estado.RepositorioEstado;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;

public class CidadeListControle extends ListActivity {

	private AutoCompleteTextView fieldbuscaCidade;
	private List<Cidade> cidades = Lists.newLinkedList();
	private Spinner spnSiglaEstado;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cidade_list);

		montaTela();

		defineFuncoes();

	}

	private void montaTela() {
		fieldbuscaCidade = (AutoCompleteTextView) findViewById(R.id.field_busca_cidade);
		spnSiglaEstado = (Spinner) findViewById(R.id.spnSiglaEstado);
	}

	private void defineFuncoes() {
		RepositorioEstado repositorioEstado = new RepositorioEstado(this);
		List<String> siglasEstados = repositorioEstado.buscaSiglas();
		repositorioEstado.fechar();

		ArrayAdapter<String> siglaEstadoAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_dropdown_item_1line, siglasEstados);
		spnSiglaEstado.setAdapter(siglaEstadoAdapter);
		spnSiglaEstado.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				buscaCidades();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}

		});

		fieldbuscaCidade.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> lista, View arg1, int position, long arg3) {
				Cidade c = (Cidade) lista.getItemAtPosition(position);
				int id = c.getCodigo();

				c = Cidade.busca(CidadeListControle.this, id);

				// retornar a cidade encontrada
				Intent i = new Intent();
				Bundle b = new Bundle();
				b.putSerializable("CIDADE", c);
				i.putExtras(b);

				setResult(ValorAtividade.CidadeList.get(), i);
				finish();

			}
		});

		getListView().setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> lista, View arg1, int position, long arg3) {

				Cidade c = (Cidade) lista.getItemAtPosition(position);
				int id = c.getCodigo();

				c = Cidade.busca(CidadeListControle.this, id);

				// retornar a cidade encontrada
				Intent i = new Intent();
				Bundle b = new Bundle();
				b.putSerializable("CIDADE", c);
				i.putExtras(b);

				setResult(ValorAtividade.CidadeList.get(), i);
				finish();
			}
		});

	}

	private void buscaCidades() {
		RepositorioCidade repositorioCidade = new RepositorioCidade(CidadeListControle.this);
		cidades = repositorioCidade.buscaPorEstado((String) spnSiglaEstado.getSelectedItem());
		repositorioCidade.fechar();

		ArrayAdapter<Cidade> adapterCidade = new ArrayAdapter<Cidade>(this,
				R.layout.modelo_list_string_nomes, cidades);
		setListAdapter(adapterCidade);

		fieldbuscaCidade.setAdapter(adapterCidade);
	}
}
