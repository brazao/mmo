package max.adm.controle;

import java.util.List;

import max.adm.R;
import max.adm.dominio.Usuario;
import max.adm.entidade.Empresa;
import max.adm.entidade.Parametro;
import max.adm.entidade.TabelaDePreco;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

public class LoginControle extends Activity {

	Button btConfirma;
	Button btCancela;

	EditText fieldUser;
	EditText fieldSenha;

	Spinner spinnerLoja;
	Spinner spinnerTabelaPreco;

	LinearLayout layoutTabelaPreco;

	Usuario user;

	Empresa empresa;
	TabelaDePreco tabPreco;
	Parametro param;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.login);

		montaTela();

		defineFuncoes();

	}

	private void montaTela() {
		btConfirma = (Button) findViewById(R.id.bt_ok_login);
		btCancela = (Button) findViewById(R.id.bt_cancel_login);

		fieldUser = (EditText) findViewById(R.id.field_usuario);
		fieldSenha = (EditText) findViewById(R.id.field_senha);

		spinnerLoja = (Spinner) findViewById(R.id.spinner_loja_login);
		spinnerTabelaPreco = (Spinner) findViewById(R.id.spinner_tabela_preco_login);

		layoutTabelaPreco = (LinearLayout) findViewById(R.id.layout_tab_preco_login);
	}

	private void defineFuncoes() {

		// preenche spinner Loja

		List<Empresa> empresas = Empresa.listar(this);

		ArrayAdapter<Empresa> adapterEmpresa = new ArrayAdapter<Empresa>(this,
				android.R.layout.simple_spinner_item, empresas);
		spinnerLoja.setAdapter(adapterEmpresa);

		// ao selecionar uma loja, carregar o spinner de tabela de Preco com as
		// tabelas relacionadas com esta loja
		spinnerLoja
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					public void onItemSelected(AdapterView<?> lista, View arg1,
							int position, long arg3) {
						Empresa loja = (Empresa) lista
								.getItemAtPosition(position);

						empresa = loja;

						List<TabelaDePreco> tabelasDePrecos = TabelaDePreco
								.listaPorEmpresa(loja, LoginControle.this);

						ArrayAdapter<TabelaDePreco> adapterTabPreco = new ArrayAdapter<TabelaDePreco>(
								LoginControle.this,
								android.R.layout.simple_spinner_item,
								tabelasDePrecos);
						spinnerTabelaPreco.setAdapter(adapterTabPreco);

						// caso o parametro seja multiTabela, nao e necessario
						// mostra o spinner Tabela de Preco
						carreParmetro();

						if (param.isMultiTabelaPreco()) {
							layoutTabelaPreco.setVisibility(View.INVISIBLE);
						} else {
							layoutTabelaPreco.setVisibility(View.VISIBLE);
						}

					}

					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}
				});

		btCancela.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				onBackPressed();

			}
		});

		btConfirma.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				String login = fieldUser.getText().toString();
				String senha = fieldSenha.getText().toString();
				boolean autenticado = false;

				List<Usuario> usuarios = Usuario.listar(LoginControle.this);

				for (Usuario u : usuarios) {
					if (u.getLogin().equals(login)
							&& u.getSenha().equals(senha)) {
						autenticado = true;
						empresa = (Empresa) spinnerLoja.getSelectedItem();
						tabPreco = (TabelaDePreco) spinnerTabelaPreco
								.getSelectedItem();
						break;
					}
				}

				if (autenticado) {
					chamaMenu();
				} else {
					AlertDialog.Builder alerta = new AlertDialog.Builder(
							LoginControle.this);
					alerta.setMessage(R.string.mensagem_login_incorreto);
					alerta.setPositiveButton(R.string.bt_ok,
							new DialogInterface.OnClickListener() {

								public void onClick(DialogInterface dialog,
										int which) {
									dialog.cancel();

								}
							});
					alerta.show();
				}

			}
		});

	}

	private void chamaMenu() {
		Intent menu = new Intent(LoginControle.this, MenuActivity.class);
		Bundle b = new Bundle();
		b.putSerializable("EMPRESA", empresa);
		b.putSerializable("TABELAPRECO", tabPreco);
		menu.putExtras(b);
		startActivity(menu);
		finish();
	}

	public void carreParmetro() {
		param = Parametro.buscaPorLoja(this, empresa.getNumeroLoja());

	}

}
