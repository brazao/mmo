package max.adm.controle;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Map;

import max.adm.Dinheiro;
import max.adm.R;
import max.adm.ValorAtividade;
import max.adm.entidade.Empresa;
import max.adm.entidade.PerfilCliente;
import max.adm.entidade.Pessoa;
import max.adm.perfilcliente.RepositorioPerfilCliente;
import max.adm.utilidades.MascaraDecimal;
import max.adm.utilidades.PieChart;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.Maps;

public class DadosFinanceirosControle extends Activity {

	SimpleDateFormat formatData = new SimpleDateFormat("dd/MM/yyyy");

	ImageView bt_pesquisa_pessoa;

	ScrollView scroll;

	TextView textDataSicronizado;

	TextView textNomeCliente;

	TextView textDataPrimeracompra;
	TextView textQtdePecasPrimeiraCompra;
	TextView textvalorPrimeiraCompra;

	TextView textDataUltimacompra;
	TextView textQtdePecasUltimaCompra;
	TextView textvalorUltimaCompra;

	TextView textTotalVencido;
	TextView textTotalVencer;
	TextView textTotalDeCompras;
	TextView textTotalEmAberto;
	TextView textvalorMedioPorPedido;
	TextView textPercentualEmAbertoSobreTotal;
	LinearLayout chartTotVencido;
	LinearLayout chartTotValorEmAberto;

	Pessoa pessoa;
	Empresa empresa;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dados_financeiros);

		montaTela();

		defineFuncoes();

		if (getIntent() != null) {
			if (getIntent().getExtras() != null)
				;
			String chamadoPor = getIntent().getStringExtra("CHAMADOPOR");
			if (!chamadoPor.equals("MENU")) { 

				bt_pesquisa_pessoa.setVisibility(View.INVISIBLE);

				pessoa = (Pessoa) getIntent().getExtras().getSerializable("PESSOA");
				empresa = pessoa.getEmpresa();
				preencheTela();

			} else {
				empresa = (Empresa) getIntent().getExtras().getSerializable("EMPRESA");
			}

		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (data != null) {
			// se retornar da tela de lista de pessoas
			if (requestCode == ValorAtividade.PessoaList.get()) {
				pessoa = (Pessoa) data.getExtras().getSerializable("PESSOA");
				preencheTela();
			}
		}

	}

	public void montaTela() {

		textDataSicronizado = (TextView) findViewById(R.id.text_data_sincronizado);

		// nome do cliente.
		textNomeCliente = (TextView) findViewById(R.id.text_nome_cliente_dados_financeiros);

		// dados primera compra
		textDataPrimeracompra = (TextView) findViewById(R.id.text_data_primera_compra);
		textQtdePecasPrimeiraCompra = (TextView) findViewById(R.id.text_pecas_primera_compra);
		textvalorPrimeiraCompra = (TextView) findViewById(R.id.text_valor_primera_compra);

		// dados da ultima compra
		textDataUltimacompra = (TextView) findViewById(R.id.text_data_ultima_compra);
		textQtdePecasUltimaCompra = (TextView) findViewById(R.id.text_pecas_ultima_compra);
		textvalorUltimaCompra = (TextView) findViewById(R.id.text_valor_ultima_compra);

		// dados sobre valores
		textTotalVencido = (TextView) findViewById(R.id.text_total_vencido);
		textTotalVencer = (TextView) findViewById(R.id.text_total_vencer);
		textTotalDeCompras = (TextView) findViewById(R.id.text_total_de_compras);
		textTotalEmAberto = (TextView) findViewById(R.id.text_total_aberto);
		textvalorMedioPorPedido = (TextView) findViewById(R.id.text_valor_medio_por_pedido);
		textPercentualEmAbertoSobreTotal = (TextView) findViewById(R.id.text_percent_em_abert_sobre_total);

		scroll = (ScrollView) findViewById(R.id.scrool_dados_financeiros);

		// inicialização dos compoentes da tela
		bt_pesquisa_pessoa = (ImageView) findViewById(R.id.bt_pesquisa_pessoa_dados_financeiros);
		// bt_sair = (ImageView) findViewById(R.id.bt_sair_dados_financeiros);
		textNomeCliente = (TextView) findViewById(R.id.text_nome_cliente_dados_financeiros);
		chartTotVencido = (LinearLayout) findViewById(R.id.chart_tot_vencido);
		chartTotValorEmAberto = (LinearLayout) findViewById(R.id.chart_tot_aberto);

	}

	private void defineFuncoes() {

		bt_pesquisa_pessoa.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				Bundle b = new Bundle();
				b.putSerializable("EMPRESA", empresa);
				Intent listaPessoa = new Intent(DadosFinanceirosControle.this, PessoaListControle.class);
				listaPessoa.putExtras(b);
				startActivityForResult(listaPessoa, ValorAtividade.PessoaList.get());

			}
		});
		
	}

	private void preencheTela() {
		limpaTela();
		PerfilCliente perfil = new RepositorioPerfilCliente(this).buscaPerfil(pessoa);

		textNomeCliente.setText(pessoa.getRazaoSocial());

		if (perfil != null) {
			System.out.println(perfil);
			//textDataSicronizado.setText(formatData.format(perfil.getDataSincronizacao()));
			textDataSicronizado.setText("EM DESENVOLVIMENTO");
			
			if (perfil.getPrimeiraCompra() != null) {
				textDataPrimeracompra.setText(formatData.format(perfil.getPrimeiraCompra()));
				textQtdePecasPrimeiraCompra.setText(String.valueOf(perfil.getQuantidadePecasPrimeiraCompra()));
				textvalorPrimeiraCompra.setText(Dinheiro.newInstance(perfil.getValorPrimeiraCompra()).formatToScreen());
			}

			if (perfil.getUltimaCompra() != null) {
				textDataUltimacompra.setText(formatData.format(perfil.getUltimaCompra()));
				textQtdePecasUltimaCompra.setText(String.valueOf(perfil.getQuantidadePecasUltimaCompra()));
				textvalorUltimaCompra.setText(Dinheiro.newInstance(perfil.getValorUltimaCompra()).formatToScreen());
			}

			textTotalDeCompras.setText(Dinheiro.newInstance(perfil.getTotalEmCompras()).formatToScreen());
			textTotalEmAberto.setText(Dinheiro.newInstance(perfil.getTotalEmAberto()).formatToScreen());
			textTotalVencido.setText(Dinheiro.newInstance(perfil.getTotalVencido()).formatToScreen());
			textTotalVencer.setText(Dinheiro.newInstance(perfil.getTotalVencer()).formatToScreen());
			textvalorMedioPorPedido.setText(Dinheiro.newInstance(perfil.getValorMedioPorPedido()).formatToScreen());
			textPercentualEmAbertoSobreTotal.setText(MascaraDecimal.mascaradecimal(perfil.getPercentualEmAberto().doubleValue(), MascaraDecimal.DECIMAL_DUAS_CASAS)+" %");
			
			montaGraficoVencimento(perfil.getTotalVencer(), perfil.getTotalVencido());
			montaGraficoValorEmAberto(perfil.getTotalEmCompras(), perfil.getTotalEmAberto());
		} else {
			Toast.makeText(this, "Sem dados para exibição!", Toast.LENGTH_LONG).show();
		}
	} 

	private void montaGraficoVencimento(BigDecimal vencer, BigDecimal vencido) {
		Map<String, BigDecimal> values = Maps.newHashMap();
		values.put("A vencer", vencer);
		values.put("Vencido", vencido);
		PieChart chart = new PieChart(this, values, "Análise de débitos");
		LinearLayout view = chart.getView();
		chartTotVencido.addView(view);
	}
	
	private void montaGraficoValorEmAberto(BigDecimal total, BigDecimal emAberto) {
		Map<String, BigDecimal> values = Maps.newHashMap();
		BigDecimal pago = total.subtract(emAberto);
		values.put("Pago", pago);
		values.put("Em aberto", emAberto);
		PieChart chart = new PieChart(this, values, "Análise de débitos");
		LinearLayout view = chart.getView();
		chartTotValorEmAberto.addView(view);
	}

	private void limpaTela() {
		textDataPrimeracompra.setText("");
		textDataUltimacompra.setText("");
		textPercentualEmAbertoSobreTotal.setText("");
		textQtdePecasPrimeiraCompra.setText("");
		textQtdePecasPrimeiraCompra.setText("");
		textQtdePecasUltimaCompra.setText("");
		textTotalDeCompras.setText("");
		textTotalEmAberto.setText("");
		textTotalVencer.setText("");
		textTotalVencido.setText("");
		textvalorMedioPorPedido.setText("");
		textvalorPrimeiraCompra.setText("");
		textvalorUltimaCompra.setText("");
		chartTotVencido.removeAllViews();
		chartTotValorEmAberto.removeAllViews();
	}
	
	

	


}
