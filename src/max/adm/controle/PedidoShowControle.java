package max.adm.controle;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;

import max.adm.R;
import max.adm.entidade.PedidoCapa;
import max.adm.entidade.PedidoItens;
import max.adm.utilidades.MascaraDecimal;
import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PedidoShowControle extends Activity {
	
	SimpleDateFormat formatData = new SimpleDateFormat("dd/MM/yyyy");
	
	TextView textNumPedido;
	TextView textColecao;
	TextView textDataPedido;
	TextView textDataEntrega;
	TextView textValorBruto;
	TextView textPercDesconto;
	TextView textValorLiquido;
	TextView textObservacao;
	LinearLayout layout;
	
	PedidoCapa pedido;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.pedido_show);
		
		montaTela();
		
		if(getIntent().getExtras() != null){
			pedido = (PedidoCapa) getIntent().getExtras().getSerializable("PEDIDO");
		}
		
		if (pedido != null){
			preencheTela();
		}
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 1, 0, "Gerar PDF");
		
		
		
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 1:
			gerarPdf();
			
			break;

		default:
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	
	private void montaTela(){
		
		textNumPedido = (TextView) findViewById(R.id.text_num_pedido_show);
		textColecao = (TextView) findViewById(R.id.text_cod_colecao_pedido_show);
		textDataPedido = (TextView) findViewById(R.id.text_data_pedido_show);
		textDataEntrega = (TextView) findViewById(R.id.text_data_entrega_pedido_show);
		textValorBruto = (TextView) findViewById(R.id.text_vlr_bruto_pedido_show);
		textPercDesconto = (TextView) findViewById(R.id.text_perc_desc_pedido_show);
		textValorLiquido = (TextView) findViewById(R.id.text_vlr_liquido_pedido_show);
		textObservacao = (TextView) findViewById(R.id.text_obs_pedido_show);
		
		layout = (LinearLayout)findViewById(R.id.layout_itens_pedido_show);
		
	}
	
	private void preencheTela(){
		
		
		textNumPedido.setText(String.valueOf(pedido.getNumeroPedido()));
		textColecao.setText(String.valueOf(pedido.getCodigoColecao()));
		textDataPedido.setText(formatData.format(pedido.getDataEmissao()));
		textDataEntrega.setText(formatData.format(pedido.getDataPrevisao()));
		textValorBruto .setText("R$ "+MascaraDecimal.mascaradecimal(pedido.getValor(), MascaraDecimal.DECIMAL_DUAS_CASAS));
		textPercDesconto.setText(MascaraDecimal.mascaradecimal(pedido.getPercentualDesconto(), MascaraDecimal.DECIMAL_DUAS_CASAS)+"%");
		textValorLiquido.setText("R$ "+MascaraDecimal.mascaradecimal(pedido.getValorLiquido(), MascaraDecimal.DECIMAL_DUAS_CASAS));
		textObservacao .setText(pedido.getObservacao());
		
		
		for (int i = 0; i < pedido.getItens().size() ; i++){
			PedidoItens it = pedido.getItens().get(i);
			
			LinearLayout linha = new LinearLayout(this);
			
			TextView referencia = new TextView(this);
			TextView descricao = new TextView(this);
			TextView padrao = new TextView(this);
			TextView tam = new TextView(this);
			TextView qtde = new TextView(this);
			TextView precoUnit = new TextView(this);
			TextView total = new TextView(this);
			
			layout.addView(linha);
			
			linha.addView(referencia);
			linha.addView(descricao);
			linha.addView(padrao);
			linha.addView(tam);
			linha.addView(qtde);
			linha.addView(precoUnit);
			linha.addView(total);
			
			
			referencia.setText(it.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto().getMascaraCodigo());
			descricao.setText(it.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto().getDescricao());
			padrao.setText(it.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getPadrao().getDescricao());
			tam.setText(it.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getTamanho().getCodigo());
			qtde.setText(MascaraDecimal.mascaradecimal(it.getQuantidade() , MascaraDecimal.DECIMAL_DUAS_CASAS));
			precoUnit.setText(it.getPreco().formatToScreen());
			total.setText("R$ "+MascaraDecimal.mascaradecimal(it.getValorTotal(), MascaraDecimal.DECIMAL_DUAS_CASAS));
			
			if (i % 2 == 0){
				linha.setBackgroundResource(R.color.branco);
			}
			else{
				linha.setBackgroundResource(R.color.cinza_claro);
			}
			
			referencia.setTextColor(getResources().getColor(R.color.preto));
			referencia.setGravity(Gravity.CENTER_VERTICAL);
			referencia.getLayoutParams().width = 200;
			referencia.getLayoutParams().height = 50;
			descricao.setSingleLine();
			
			descricao.setTextColor(getResources().getColor(R.color.preto));
			descricao.setGravity(Gravity.CENTER_VERTICAL);
			descricao.getLayoutParams().width = 400;
			descricao.getLayoutParams().height = 50;
			descricao.setSingleLine();
			
			padrao.setTextColor(getResources().getColor(R.color.preto));
			padrao.setGravity(Gravity.CENTER_VERTICAL);
			padrao.getLayoutParams().width = 150;
			padrao.getLayoutParams().height = 50;
			descricao.setSingleLine();
			
			tam.setTextColor(getResources().getColor(R.color.preto));
			tam.setGravity(Gravity.CENTER_VERTICAL);
			tam.getLayoutParams().width = 100;
			tam.getLayoutParams().height = 50;
			descricao.setSingleLine();
			
			qtde.setTextColor(getResources().getColor(R.color.preto));
			qtde.setGravity(Gravity.CENTER_VERTICAL);
			qtde.getLayoutParams().width = 100;
			qtde.getLayoutParams().height = 50;
			descricao.setSingleLine();
			
			precoUnit.setTextColor(getResources().getColor(R.color.preto));
			precoUnit.setGravity(Gravity.CENTER_VERTICAL);
			precoUnit.getLayoutParams().width = 150;
			precoUnit.getLayoutParams().height = 50;
			descricao.setSingleLine();
			
			total.setTextColor(getResources().getColor(R.color.preto));
			total.setGravity(Gravity.CENTER_VERTICAL);
			total.getLayoutParams().width = 150;
			total.getLayoutParams().height = 50;
			descricao.setSingleLine();
		}
	}
	
	private void gerarPdf(){
		Document documento = new Document(); 
		String file = "";
		try {
			
			
			file = Environment.getExternalStorageDirectory().getPath()+"/pedido_"+String.valueOf(pedido.getNumeroPedido())+".pdf";
			
			PdfWriter.getInstance(documento, new FileOutputStream(file));
			
			Font fonteNegrito = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
			
			documento.open();
			
			documento.add(new Paragraph(" Cópia de pedido", fonteNegrito));
			documento.add(new Paragraph("         Pedido "+String.valueOf(pedido.getNumeroPedido()), fonteNegrito)); 
			documento.add(new Paragraph(" "));
			documento.add(new Paragraph(" "));
			documento.add(new Paragraph("Coleção "+String.valueOf(pedido.getCodigoColecao())));
			documento.add(new Paragraph("Data de emissão: "+formatData.format(pedido.getDataEmissao())));
			documento.add(new Paragraph("Previsão de entrega: "+formatData.format(pedido.getDataPrevisao())));
			documento.add(new Paragraph("Valor Bruto: "+"R$ "+MascaraDecimal.mascaradecimal(pedido.getValor(), MascaraDecimal.DECIMAL_DUAS_CASAS)));
			documento.add(new Paragraph("Desconto: "+MascaraDecimal.mascaradecimal(pedido.getPercentualDesconto(), MascaraDecimal.DECIMAL_DUAS_CASAS)+"%"));
			documento.add(new Paragraph("Valor líquido: "+"R$ "+MascaraDecimal.mascaradecimal(pedido.getValorLiquido(), MascaraDecimal.DECIMAL_DUAS_CASAS)));
			
			documento.add(new Paragraph(" "));
			documento.add(new Paragraph(" "));
			documento.add(new Paragraph(" "));
			
			documento.add(new Paragraph("Itens",fonteNegrito));
			documento.add(new Paragraph(" "));
			
			PdfPTable table = new PdfPTable(7);
			
			
			PdfPCell c1 = new PdfPCell(new Phrase("Referencia"));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase("Descricao"));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase("Padrao"));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1);
			
			
			c1 = new PdfPCell(new Phrase("Tamanho"));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1);
			
			
			c1 = new PdfPCell(new Phrase("Quantidade"));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1);
			
			
			c1 = new PdfPCell(new Phrase("$ Unitario"));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1);
			
			
			c1 = new PdfPCell(new Phrase("$ Total"));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1);
			table.setHeaderRows(1);
			
			
			for (int i = 0; i < pedido.getItens().size() ; i++){
				PedidoItens it = pedido.getItens().get(i);
				
				table.addCell(it.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto().getMascaraCodigo());
				table.addCell(it.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto().getDescricao());
				table.addCell(it.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getPadrao().getDescricao());
				table.addCell(it.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getTamanho().getCodigo());
				table.addCell(MascaraDecimal.mascaradecimal(it.getQuantidade() , MascaraDecimal.DECIMAL_DUAS_CASAS));
				table.addCell(it.getPreco().formatToScreen());
				table.addCell("R$ "+MascaraDecimal.mascaradecimal(it.getValorTotal(), MascaraDecimal.DECIMAL_DUAS_CASAS));
			
			}
			
			documento.add(table);
             
            Log.d("arquivo", "arquivo gerado"+file);
            
            
            Toast.makeText(this, "PDF salvo com sucesso em seu cartão de memória", Toast.LENGTH_LONG).show();
            
            documento.close(); 
			
		} catch (FileNotFoundException e) { 
            Log.d("arquivo", "arquivo erro "+file); 
            Toast.makeText(this, "Erro ao gerar PDF. Verifique se o cartão de memória está inserido", Toast.LENGTH_LONG).show();
            e.printStackTrace(); 
		} catch (DocumentException e) { 
			Toast.makeText(this, "Erro ao gerar PDF. Verifique se o cartão de memória está inserido", Toast.LENGTH_LONG).show();
			
            e.printStackTrace(); 
		}
		
	}
	
	
	
}
