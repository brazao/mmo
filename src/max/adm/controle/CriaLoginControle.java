package max.adm.controle;

import max.adm.R;
import max.adm.dominio.Usuario;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CriaLoginControle extends Activity {
	
	
	Button btConfirma;
	Button btCancela;
	
	EditText user;
	EditText senha;
	EditText repeteSenha;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cria_login);
		
		montaTela();
		
		defineFuncoes();
		
	}
	
	private void montaTela(){
		
		btConfirma = (Button)findViewById(R.id.bt_confirma_cria_ogin);
		btCancela = (Button) findViewById(R.id.bt_cancela_cria_login);
		
		user = (EditText) findViewById(R.id.field_usuario_cria_login);
		senha = (EditText) findViewById(R.id.field_senha_cria_login); 
		repeteSenha = (EditText) findViewById(R.id.field_repete_senha_cria_login);
	}
	
	private void defineFuncoes(){
		btCancela.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				onBackPressed();
				
			}
		});
		
		btConfirma.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				
				//verifica se o campo USUARIO esta vazio
				if (user.getText().toString().equals("")){
					AlertDialog.Builder alerta = new AlertDialog.Builder(CriaLoginControle.this);
					alerta.setMessage(R.string.mensagem_campo_usuario_vazio);
					alerta.setPositiveButton(R.string.bt_ok, new DialogInterface.OnClickListener() {
						
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
							
						}
					});
					alerta.show();
							
				}
				
				
				else{
					//verifcar se os campos senha e repete senha estao vazio
					if (senha.getText().toString().equals("") || repeteSenha.getText().toString().equals("")){
						AlertDialog.Builder alerta = new AlertDialog.Builder(CriaLoginControle.this);
						alerta.setMessage(R.string.mensagem_todos_campos_devem_ser_preenchidos);
						alerta.setPositiveButton(R.string.bt_ok, new DialogInterface.OnClickListener() {
							
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
								
							}
						});
						alerta.show();
					}
					else{
						//verifica se a confirma��o de senha est� diferente da senha
						if (!senha.getText().toString().equals(repeteSenha.getText().toString())){
							AlertDialog.Builder alerta = new AlertDialog.Builder(CriaLoginControle.this);
							alerta.setMessage(R.string.mensagem_senha_nao_confere);
							alerta.setPositiveButton(R.string.bt_ok, new DialogInterface.OnClickListener() {
								
								public void onClick(DialogInterface dialog, int which) {
									dialog.cancel();
									
								}
							});
							alerta.show();
							senha.getText().clear();
							repeteSenha.getText().clear();
							
						}
						else{
							//chama a fun��o criausuario()
							criaUsuario();
							
							Toast.makeText(CriaLoginControle.this, R.string.mensagem_usuario_criado_com_sucesso, Toast.LENGTH_SHORT).show();
							
							
							chamaLogin();
							
						}
					}
				}
				
			}
		});
		
	}
	
	private void criaUsuario(){
		Usuario usuario = new Usuario();
		usuario.setNome(user.getText().toString());
		usuario.setLogin(user.getText().toString());
		usuario.setSenha(senha.getText().toString());
		usuario.setCodigoRepresentante("nada");
		
		usuario.salvar(this);
		
	}
	
	private void chamaLogin(){
		Intent i = new Intent(this, LoginControle.class);
		startActivity(i);
		
	}
	

}
