package max.adm.controle;

import java.util.Calendar;
import java.util.Date;

import max.adm.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

public class DateActivity extends Activity {

	private int year;
	private int month;
	private int day;
	private Button btOk;
	private Button btCancel;
	private DatePicker datePicker;
	private int CODE_RESULT;
	public final static String CODE_RESULT_KEY = "CODE_RESULT";
	public final static String DATA_KEY = "DATA";

	private Date date = new Date();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.date);
		CODE_RESULT = getIntent().getIntExtra(CODE_RESULT_KEY, 0);
		montaTela();
		defineFuncoes();
		setCurrentDateOnView();
	}

	private void montaTela() {
		datePicker = (DatePicker) findViewById(R.id.datePicker);
		btOk = (Button) findViewById(R.id.bt_confirma);
		btCancel = (Button) findViewById(R.id.bt_cancel);
	}

	private void setCurrentDateOnView() {
		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);
		datePicker.init(year, month, day, null);
	}

	private void defineFuncoes() {
		btOk.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				year = datePicker.getYear();
				month = datePicker.getMonth();
				day = datePicker.getDayOfMonth();
				date = new Date(year - 1900, month, day);
				confirma();

			}
		});

		btCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				onBackPressed();

			}
		});
	}

	private void confirma() {
		Intent i = new Intent();
		Bundle b = new Bundle();
		b.putSerializable(DATA_KEY, date);
		b.putInt(CODE_RESULT_KEY, CODE_RESULT);
		i.putExtras(b);
		setResult(RESULT_OK, i);
		finish();
	}

}
