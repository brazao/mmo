package max.adm.controle;

import java.text.SimpleDateFormat;
import java.util.Date;

import max.adm.R;
import max.adm.entidade.Mensagem;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MensagemRespostaControle extends Activity{
	SimpleDateFormat formatData = new SimpleDateFormat("dd/MM/yy");
	
	Mensagem mensagem;
	ImageView btEnviar;
	ImageView btExcluir;
	ImageView btConfirma;
	ImageView btCancela;
	
	TextView textMensagem;
	TextView textRemetente;
	TextView textDataRecebimento;
		
	
	EditText fieldResposta;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mensagem_responder);
		
		montaTela();
		
		if (getIntent().getExtras() != null){
			mensagem = (Mensagem) getIntent().getExtras().getSerializable("MENSAGEM");
		}
		
		preencheTela();
		
		defineFuncoes();
		
	}
	
	private void montaTela(){
		
		btEnviar = (ImageView)findViewById(R.id.bt_envia_resposta_mensagem);
		btExcluir = (ImageView)findViewById(R.id.bt_excluir_mensagem_resposta);
		btConfirma = (ImageView)findViewById(R.id.bt_confirma_resposta_mensagem);
		btCancela = (ImageView)findViewById(R.id.bt_cancela_resposta_mensagem);
		
		textMensagem = (TextView) findViewById(R.id.text_mensagem_resposta);
		fieldResposta = (EditText)findViewById(R.id.field_resposta_mensagem);
		
		textRemetente = (TextView) findViewById(R.id.text_remetente_resposta);
		textDataRecebimento = (TextView) findViewById(R.id.text_data_recebimento);
	}
	
	
	
	
	private void defineFuncoes(){
		
		btExcluir.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				AlertDialog.Builder alerta = new AlertDialog.Builder(MensagemRespostaControle.this);
				alerta.setTitle(R.string.titulo_atencao);
				alerta.setMessage(R.string.mensagem_tem_certeza_excluir_mensagem);
				alerta.setPositiveButton(R.string.bt_sim, new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int which) {
						excluiMensagem();
						
					}
					
				});
				alerta.setNegativeButton(R.string.bt_sim, new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
				alerta.show();
				
			}
		});
		
		btCancela.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				onBackPressed();
				
			}
		});
		
		btConfirma.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				
				mensagem.setResposta(fieldResposta.getText().toString());
				
				mensagem.salvar(MensagemRespostaControle.this);
				Toast.makeText(MensagemRespostaControle.this, R.string.mensagem_mensagem_salva , Toast.LENGTH_LONG).show();
				finish();
			}
		});
		
	}
	
	private void preencheTela(){
		if (mensagem != null){
			textMensagem.setText(mensagem.getMensagem());
			fieldResposta.setText(mensagem.getResposta());
			textRemetente.setText(mensagem.getRemetente());
			textDataRecebimento.setText(formatData.format(mensagem.getDataEnvio()));
			mensagem.setDataLeitura(new Date());
			
		}
	}
	
	private void excluiMensagem(){
		if (mensagem != null){
			Mensagem.excluir(this, mensagem.getId());
			Toast.makeText(this, R.string.mensagem_mensagem_excluida, Toast.LENGTH_LONG).show();
			finish();
		}
	}
}
