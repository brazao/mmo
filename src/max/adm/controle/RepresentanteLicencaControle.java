package max.adm.controle;

import max.adm.R;
import max.adm.service.ImportaBancoService;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RepresentanteLicencaControle extends Activity {

	ProgressDialog aviso;

	EditText fieldChaveRepres;
	Button btConfirma;
	Button btCancela;

	private static final int DIALOG_AGUARDE = 1;

	String url = "";
	String chave;
	String action = ""; // define se essa activity foi chamado pelo tela de
						// primeiro passo ou pela notifca��o.

	Notification notf;

	/**
	 * true se o codigo foi autenticado do webService
	 */
	boolean codigoValido = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.representante_licenca);

		Bundle dados = getIntent().getExtras();
		if (dados != null) {
			url = dados.getString("URL");
			action = dados.getString("ACTION");
		}

		montaTela();

		definiFuncoes();

		if (action.equals("notificação")) {
			AlertDialog.Builder alerta = new AlertDialog.Builder(this);
			alerta.setMessage("Não conseguimos encontrar configurações definidas para a licença informada. Tente novamente ou entre em contato com a fábrica.");
			alerta.setPositiveButton(R.string.bt_ok,
					new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();

						}
					});
			alerta.show();
		}
	}

	private void montaTela() {

		fieldChaveRepres = (EditText) findViewById(R.id.field_chave_representante_tela_licenca);
		btConfirma = (Button) findViewById(R.id.bt_confirma_cod_representante);
		btCancela = (Button) findViewById(R.id.bt_cancela_codigo_representante);

	}

	private void definiFuncoes() {
		btCancela.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				onBackPressed();

			}
		});

		btConfirma.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				AlertDialog.Builder alerta = new AlertDialog.Builder(
						RepresentanteLicencaControle.this);
				alerta.setTitle(R.string.titulo_informacao);
				alerta.setMessage("Estamos importando os dados da fábrica. Assim que terminarmos você será avisado na barra de notificações.");
				alerta.setPositiveButton(R.string.bt_ok,
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {
								String chave = fieldChaveRepres.getText()
										.toString();
								iniciaImportacao(chave);

								//Aqui está voltando para o home
//								Intent i = new Intent();
//								i.setAction(Intent.ACTION_MAIN);
//								i.addCategory(Intent.CATEGORY_HOME);
//								startActivity(i);
							}
						});
				alerta.show();
			}
		});
	}

	private void iniciaImportacao(String chave) {

		Log.d("Servico", "gerando servico");
		this.chave = chave;
		new IniciaImportacao().execute("");

	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_AGUARDE:
			aviso = new ProgressDialog(this);
			aviso.setMessage("Importando dados...");
			aviso.setIndeterminate(false);
			aviso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			aviso.setCancelable(false);
			aviso.show();
			return aviso;

		default:
			return super.onCreateDialog(id);

		}
	}

	class IniciaImportacao extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			showDialog(DIALOG_AGUARDE);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
//			Intent i = new Intent();
//
//			Bundle b = new Bundle();
//			i.putExtras(b);
//			i.putExtra("CHAVE", chave);
//			i.putExtra("URL", url);
//
//			startService(i);

			ImportaBancoService importaBancoService = new ImportaBancoService(
					RepresentanteLicencaControle.this, url, chave);
			importaBancoService.importar();

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			dismissDialog(DIALOG_AGUARDE);
			super.onPostExecute(result);
		}

	}

}
