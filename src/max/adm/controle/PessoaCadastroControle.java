/*esta classe manipula a entidade pessoa
 *esta classe maniula o layout pessoa_cadastro.xml*/

package max.adm.controle;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import max.adm.R;
import max.adm.ValorAtividade;
import max.adm.entidade.ClassifcacaoPessoa;
import max.adm.entidade.Email;
import max.adm.entidade.Empresa;
import max.adm.entidade.Endereco;
import max.adm.entidade.FaixaCodigoCliente;
import max.adm.entidade.Parametro;
import max.adm.entidade.Pessoa;
import max.adm.entidade.PessoaClassifcacaoPessoa;
import max.adm.entidade.Telefone;
import max.adm.pessoa.repositorio.RepositorioPessoa;
import max.adm.repositorio.RepositorioPessoaClassificacaoPessoa;
import max.adm.telefone.TipoTelefone;
import max.adm.utilidades.ValidaString;
import max.adm.utilidades.ValidacaoCpfCnpj;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.Lists;

public class PessoaCadastroControle extends Activity {

	EditText fieldRazao;
	EditText fieldNomeFantasia;
	EditText fieldCnpj;
	EditText fieldInscricaoEstadual;
	EditText fieldTelefone;
	EditText fieldFax;
	EditText fieldCelular;
	EditText fieldContato;
	EditText fieldEmail;
	EditText fieldObs;

	ImageView imgChekedRazao;
	ImageView imgChekedNomeFantasia;
	ImageView imgChekedCnpj;
	ImageView imgChekedInscEstadual;
	ImageView imgChekedTelefone;
	ImageView imgChekedCelular;
	ImageView imgChekedfax;
	ImageView imgChekedEmail;

	ImageView btPesquisaPessoa;
	Button btConfirma;
	Button btCancela;
	ImageView btExcluir;
	ImageView btEditarEndereco;

	TextView textEndereco;
	TextView textCodigoPessoa;

	Spinner spinnerClassificacao;

	LinearLayout layoutTela;

	Pessoa pessoa;
	Empresa empresa = new Empresa();
	Parametro param;

	boolean novoCadastro = true;

	List<ClassifcacaoPessoa> classificacoes;
	List<Endereco> enderecos = Lists.newLinkedList();


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pessoa_cadastro);
		empresa = (Empresa) getIntent().getExtras().getSerializable("EMPRESA");
		montaTela();
		defineFuncoes();
		leCodigoDisponivel(empresa.getNumeroLoja());

	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (data != null && requestCode == ValorAtividade.PessoaList.get()) {
			pessoa = (Pessoa) data.getExtras().getSerializable("PESSOA");
			if (pessoa != null) {
				if (pessoa.getEnderecos() != null) {
					enderecos = pessoa.getEnderecos();
				}
				atualizaTela();
			}

		} else if (data != null && requestCode == ValorAtividade.Endereco.get()) {
			enderecos = (List<Endereco>) data.getExtras().getSerializable("ENDERECOS");
			if (enderecos != null && !enderecos.isEmpty()) {
				if (pessoa != null) {
					pessoa.setEnderecos(enderecos);
				}
				textEndereco.setText(enderecos.get(0).toString());
			}
		}
	}

	private void montaTela() {

		layoutTela = (LinearLayout) findViewById(R.id.layoutCadastroPessoa);

		fieldRazao = (EditText) findViewById(R.id.field_razao);
		fieldNomeFantasia = (EditText) findViewById(R.id.field_fantasia);
		fieldCnpj = (EditText) findViewById(R.id.field_cnpj);
		fieldInscricaoEstadual = (EditText) findViewById(R.id.field_inscricao_estadual);
		fieldTelefone = (EditText) findViewById(R.id.field_telefone);
		fieldFax = (EditText) findViewById(R.id.field_fax);
		fieldCelular = (EditText) findViewById(R.id.field_celular);
		fieldContato = (EditText) findViewById(R.id.field_contato);
		fieldEmail = (EditText) findViewById(R.id.field_email);
		fieldObs = (EditText) findViewById(R.id.field_obs_pessoa);

		imgChekedRazao = (ImageView) findViewById(R.id.cheked_razao);
		imgChekedNomeFantasia = (ImageView) findViewById(R.id.cheked_nome_fantasia);
		imgChekedCnpj = (ImageView) findViewById(R.id.cheked_cnpj);
		imgChekedInscEstadual = (ImageView) findViewById(R.id.cheked_insc_estadual);
		imgChekedTelefone = (ImageView) findViewById(R.id.cheked_telefone);
		imgChekedCelular = (ImageView) findViewById(R.id.cheked_celular);
		imgChekedfax = (ImageView) findViewById(R.id.cheked_fax);
		imgChekedEmail = (ImageView) findViewById(R.id.cheked_email);

		btPesquisaPessoa = (ImageView) findViewById(R.id.bt_pesquisa_pessoa);
		btEditarEndereco = (ImageView) findViewById(R.id.bt_editar_endereco);
		btConfirma = (Button) findViewById(R.id.bt_confirma_cadastro_pessoa);
		btCancela = (Button) findViewById(R.id.bt_cancela_cadastro_pessoa);
		btExcluir = (ImageView) findViewById(R.id.bt_excluir_cadastro_pessoa);

		textEndereco = (TextView) findViewById(R.id.text_logradouro);
		textCodigoPessoa = (TextView) findViewById(R.id.textCodigoPessa);

		spinnerClassificacao = (Spinner) findViewById(R.id.spinner_classificacao);

	}

	private void defineFuncoes() {

		btPesquisaPessoa.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				chamaListaDePessoas();

			}
		});

		fieldCnpj.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					if (fieldCnpj.getText().toString().isEmpty()) {
						imgChekedCnpj.setImageResource(R.drawable.img_error);
					} else if (!ValidacaoCpfCnpj.CNPJ(fieldCnpj.getText().toString()) && !ValidacaoCpfCnpj.CPF(fieldCnpj.getText().toString())) {
						imgChekedCnpj.setImageResource(R.drawable.img_error);
					} else {
						imgChekedCnpj.setImageResource(R.drawable.img_chek);
						String str = ValidacaoCpfCnpj.poeMascaraCpfCnpj(fieldCnpj.getText().toString());
						fieldCnpj.setText(str);
					}
				}
			}
		});

		// verifica campo razao social
		fieldRazao.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					if (!(fieldRazao.getText().toString()).equals("")) {
						imgChekedRazao.setImageResource(R.drawable.img_chek);
					} else {
						imgChekedRazao.setImageResource(R.drawable.img_error);
					}
				}

			}
		});

		// validaFone
		fieldTelefone.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			public void onFocusChange(View arg0, boolean hasFocus) {
				if (!hasFocus) {
					if (!ValidaString.tiraMascara(fieldTelefone.getText().toString()).equals("")) {
						if (ValidaString.Telefone(fieldTelefone.getText().toString())) {
							imgChekedTelefone.setImageResource(R.drawable.img_chek);
						} else {
							imgChekedTelefone.setImageResource(R.drawable.img_error);
						}

					} else {
						imgChekedTelefone.setImageResource(0);
					}
				}

			}
		});

		// valida celular
		fieldCelular.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			public void onFocusChange(View arg0, boolean hasFocus) {
				if (!hasFocus) {
					if (!ValidaString.tiraMascara(fieldCelular.getText().toString()).equals("")) {
						if (ValidaString.Telefone(fieldCelular.getText().toString())) {
							imgChekedCelular.setImageResource(R.drawable.img_chek);
						} else {
							imgChekedCelular.setImageResource(R.drawable.img_error);
						}

					} else {
						imgChekedCelular.setImageResource(0);
					}
				}

			}
		});

		// valida fax
		fieldFax.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			public void onFocusChange(View arg0, boolean hasFocus) {
				if (!hasFocus) {
					if (!ValidaString.tiraMascara(fieldFax.getText().toString()).equals("")) {
						if (ValidaString.Telefone(fieldFax.getText().toString())) {
							imgChekedfax.setImageResource(R.drawable.img_chek);
						} else {
							imgChekedfax.setImageResource(R.drawable.img_error);
						}

					} else {
						imgChekedfax.setImageResource(0);
					}
				}

			}
		});

		// valida email *** tem que ser email v�lido ou em branco.
		fieldEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			public void onFocusChange(View arg0, boolean hasFocus) {
				if (!hasFocus) {
					if (!ValidaString.tiraMascara(fieldEmail.getText().toString()).equals("")) {
						if (ValidaString.Email(fieldEmail.getText().toString())) {
							imgChekedEmail.setImageResource(R.drawable.img_chek);
						} else {
							imgChekedEmail.setImageResource(R.drawable.img_error);
						}
					}
				}

			}
		});

		btEditarEndereco.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				chamaTelaEndereco();

			}
		});

		classificacoes = ClassifcacaoPessoa.listar(this);

		ArrayAdapter<ClassifcacaoPessoa> adapterClassificacao = new ArrayAdapter<ClassifcacaoPessoa>(this, R.layout.simple_spinner_item, classificacoes);
		spinnerClassificacao.setAdapter(adapterClassificacao);

		btConfirma.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				confirmar();
			}
		});

		btCancela.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				onBackPressed();

			}
		});

		btExcluir.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				excluir();

			}
		});

	} // fim defineFuncoes()

	private void atualizaTela() {
		fieldRazao.setText(pessoa.getRazaoSocial());
		fieldNomeFantasia.setText(pessoa.getNomeFantasia());
		fieldCnpj.setText(ValidacaoCpfCnpj.poeMascaraCpfCnpj(pessoa.getCpfCgc()));
		fieldInscricaoEstadual.setText(pessoa.getRgInscricao());
		fieldContato.setText(pessoa.getContato());
		if (pessoa.getEmails() != null && !pessoa.getEmails().isEmpty()) {
			fieldEmail.setText(pessoa.getEmails().get(0).getEmail());
		}
		if (pessoa.getTelefones() != null && !pessoa.getTelefones().isEmpty()) {
			for (Telefone t : pessoa.getTelefones()) {
				if (t.getTipoTelefone().equals(TipoTelefone.T)) {
					fieldTelefone.setText(t.getNumeroTelefone());
				} else if (t.getTipoTelefone().equals(TipoTelefone.F)) {
					fieldFax.setText(t.getNumeroTelefone());
				} else {
					fieldCelular.setText(t.getNumeroTelefone());
				}
			}
		}
		spinnerClassificacao.setSelection(getPositionClassificacao());
		fieldObs.setText(pessoa.getObservacao());
		if (pessoa.getEnderecos() != null && !pessoa.getEnderecos().isEmpty()) {
			textEndereco.setText(pessoa.getEnderecos().get(0).toString());
		}

	}

	public void onBackPressed() {
		AlertDialog.Builder alerta = new AlertDialog.Builder(this);
		alerta.setMessage(R.string.mensagem_tem_certeza_deseja_sair_sem_confirmar);
		alerta.setPositiveButton(R.string.bt_sim, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});

		alerta.setNegativeButton(R.string.bt_nao, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		alerta.show();

		return;

	}

	private void chamaTelaEndereco() {
		Intent intentEndereco = new Intent(PessoaCadastroControle.this, EnderecoControle.class);
		Bundle b = new Bundle();
		b.putSerializable("EMPRESA", empresa);
		if (pessoa != null)
			b.putSerializable("ENDERECOS", (Serializable) pessoa.getEnderecos());
		intentEndereco.putExtras(b);
		startActivityForResult(intentEndereco, ValorAtividade.Endereco.get());
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		pessoa = (Pessoa) savedInstanceState.getSerializable("PESSOA");
		empresa = (Empresa) savedInstanceState.getSerializable("EMPRESA");

	}

	private void chamaListaDePessoas() {
		Bundle b = new Bundle();
		b.putSerializable("EMPRESA", empresa);
		Intent listaDePessoas = new Intent(PessoaCadastroControle.this, PessoaListControle.class);
		listaDePessoas.putExtras(b);
		startActivityForResult(listaDePessoas, ValorAtividade.PessoaList.get());
	}

	private void leCodigoDisponivel(int idEmpresa) {

	}

	private void excluir() {

	}

	private void confirmar() {
		try {
			if (validaCampos()) {
				Pessoa.Builder builder;
				if (pessoa == null) {
					RepositorioPessoa repo = new RepositorioPessoa(this);
					FaixaCodigoCliente codigo = repo.leCodigoDisponivel(empresa);
					repo.fechar();
					builder = Pessoa.builder(codigo);

				} else {
					builder = Pessoa.builder(pessoa.getCodigo());
				}
				builder.comCgcCpf(ValidaString.tiraMascara(fieldCnpj.getText().toString()));
				builder.comContato(fieldContato.getText().toString());
				builder.comDataCadastro(new Date());
				List<Email> emails = Lists.newLinkedList();
				Email e = new Email();
				e.setEmail(fieldEmail.getText().toString());
				emails.add(e);
				builder.comEmails(emails);
				builder.comEmpresa(empresa);
				builder.comEnderecos(enderecos);
				builder.comInscRg(fieldInscricaoEstadual.getText().toString());
				builder.comNomeFantasia(fieldNomeFantasia.getText().toString());
				builder.comObservacao(fieldObs.getText().toString());
				builder.comRazaoSocial(fieldRazao.getText().toString());
				
				List<Telefone> fones = Lists.newLinkedList();
				if (!ValidaString.tiraMascara(fieldTelefone.getText().toString()).isEmpty() && ValidaString.Telefone(fieldTelefone.getText().toString())) {
					Telefone.Builder builderTel = Telefone.builder(empresa, TipoTelefone.T);
					builderTel.withNumero(ValidaString.tiraMascara(fieldTelefone.getText().toString()));
					fones.add(builderTel.build());
				}
				if (!ValidaString.tiraMascara(fieldFax.getText().toString()).isEmpty() && ValidaString.Telefone(fieldFax.getText().toString())) {
					max.adm.entidade.Telefone.Builder builderTel = Telefone.builder(empresa, TipoTelefone.F);
					builderTel.withNumero(ValidaString.tiraMascara(fieldFax.getText().toString()));
					fones.add(builderTel.build());
				}
				if (!ValidaString.tiraMascara(fieldCelular.getText().toString()).isEmpty() && ValidaString.Telefone(fieldCelular.getText().toString())) {
					Telefone.Builder builderTel = Telefone.builder(empresa, TipoTelefone.C);
					builderTel.withNumero(ValidaString.tiraMascara(fieldCelular.getText().toString()));
					fones.add(builderTel.build());
				}
				builder.comTelefones(fones);

				pessoa = builder.build();
				
				ClassifcacaoPessoa classifcacaoPessoa = (ClassifcacaoPessoa) spinnerClassificacao.getSelectedItem();
				PessoaClassifcacaoPessoa pessoaClassifcacaoPessoa = new PessoaClassifcacaoPessoa();
				pessoaClassifcacaoPessoa.setClassifcacaoPessoa(classifcacaoPessoa);
				pessoaClassifcacaoPessoa.setEmpresa(pessoa.getEmpresa());
				pessoaClassifcacaoPessoa.setPessoa(pessoa);
				
				RepositorioPessoaClassificacaoPessoa repositorioPessoaClassificacaoPessoa = new RepositorioPessoaClassificacaoPessoa(this);
				repositorioPessoaClassificacaoPessoa.salvar(pessoaClassifcacaoPessoa);
				repositorioPessoaClassificacaoPessoa.fechar();

				RepositorioPessoa repo = new RepositorioPessoa(this);
				repo.salvar(pessoa);
				Toast.makeText(this, "Cadastro gravado com sucesso!", Toast.LENGTH_LONG).show();
				repo.fechar();
				finish();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(this, "Não faixa de código disponível", Toast.LENGTH_LONG).show();
		}
	}

	private boolean validaCampos() {
		boolean ok = true;
		if (fieldRazao.getText().toString().isEmpty()) {
			imgChekedRazao.setImageResource(R.drawable.img_error);
			ok = false;
		}
		if (fieldNomeFantasia.getText().toString().isEmpty()) {
			imgChekedNomeFantasia.setImageResource(R.drawable.img_error);
			ok = false;
		}
		if (!ValidacaoCpfCnpj.CNPJ(fieldCnpj.getText().toString()) && !ValidacaoCpfCnpj.CPF(fieldCnpj.getText().toString())) {
			imgChekedCnpj.setImageResource(R.drawable.img_error);
			ok = false;
		}
		if (!ValidaString.Telefone(fieldTelefone.getText().toString())) {
			imgChekedTelefone.setImageResource(R.drawable.img_error);
			ok = false;
		}
		if (enderecos == null || enderecos.isEmpty()) {
			Toast.makeText(this, "Preencha o endereço.", Toast.LENGTH_LONG).show();
			ok = false;
		}
		return ok;

	}

	private int getPositionClassificacao() {
		int i = 0;
		RepositorioPessoaClassificacaoPessoa repo = new RepositorioPessoaClassificacaoPessoa(this);
		List<PessoaClassifcacaoPessoa> pessoaClassifcacaoPessoas = repo.busca(pessoa.getCodigo(), pessoa.getEmpresa().getNumeroLoja());
		
		for (i = 0; i < classificacoes.size(); i++) {
			for (PessoaClassifcacaoPessoa peClassifcacaoPessoa : pessoaClassifcacaoPessoas) {
				if (classificacoes.get(i).equals(peClassifcacaoPessoa.getClassifcacaoPessoa())) {
					return i;
				}
			}
		}
		return i;
	}

}
