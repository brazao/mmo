package max.adm.controle;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import max.adm.R;
import max.adm.entidade.PadraoProduto;
import max.adm.entidade.PedidoItens;
import max.adm.entidade.TabelaPrecoProdutoReduzido;
import max.adm.entidade.TamanhoProduto;
import max.adm.utilidades.adapters.ProdutoItemPedidoParaListar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class EscolhaPadraoTamanhoControle extends Activity {

	Spinner spinnerPadroes;
	ImageView btLixeira;
	ImageView btConfirma;
	ImageView btCancela;

	TextView textReferencia;

	LinearLayout layoutTamanhos;
	LinearLayout layoutLabelsPadrao;

	List<PadraoProduto> padroes = Lists.newLinkedList();
	List<TamanhoProduto> tamanhos = Lists.newLinkedList();
	List<PedidoItens> itensPedido = Lists.newLinkedList();
	Map<String, Integer> idDosCampos = Maps.newHashMap();

	private ProdutoItemPedidoParaListar produtoComPadraoTamanhos;

	boolean gerouCampos = false;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.escolha_de_padrao_e_tamanho);
		montaTela();
		System.out.println("entrou");
		if (getIntent().getExtras().containsKey("ITENSPEDIDO")) {
			System.out.println("contem itens");
			itensPedido = (List<PedidoItens>) getIntent().getExtras().getSerializable("ITENSPEDIDO");
		}
		produtoComPadraoTamanhos = (ProdutoItemPedidoParaListar) getIntent().getExtras().getSerializable("PRODUTO");
		textReferencia.setText(produtoComPadraoTamanhos.getProduto().getMascaraCodigo() + " " + produtoComPadraoTamanhos.getProduto().getDescricao());
		preencheTela();
		defineFuncoes();
	}

	private void montaTela() {
		btConfirma = (ImageView) findViewById(R.id.bt_confirma_padrao_tamanho);
		btCancela = (ImageView) findViewById(R.id.bt_cancela_padrao_tamanho);
		textReferencia = (TextView) findViewById(R.id.text_referenca_tela_padroes_tamanhos);
		layoutLabelsPadrao = (LinearLayout) findViewById(R.id.layout_label_padrao);
		layoutTamanhos = (LinearLayout) findViewById(R.id.layout_label_tamanho);

	}

	private void preencheTela() {
		geraLabelsComPadrao();
		geraLabelsComTamanho();
	}

	private void defineFuncoes() {

		btConfirma.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				confirmar();
			}
		});

		btCancela.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onBackPressed();
			}
		});

	}

	private void confirmar() {
		itensPedido.clear();
		for (TabelaPrecoProdutoReduzido red : produtoComPadraoTamanhos.getReduzidos()) {
			Log.i("idDosCampos 1", String.valueOf(idDosCampos.get(red.getProdutoReduzido().getPadrao().getCodigo() + red.getProdutoReduzido().getPadrao().getDescricao() + red.getProdutoReduzido().getTamanho().getCodigo())));
			EditText campoQtde = (EditText) findViewById(
					idDosCampos.get(red.getProdutoReduzido().getPadrao().getCodigo() + red.getProdutoReduzido().getPadrao().getDescricao() + red.getProdutoReduzido().getTamanho().getCodigo()));
					//idDosCampos.put(red2.getProdutoReduzido().getPadrao().getCodigo() + red2.getProdutoReduzido().getPadrao().getDescricao() + red2.getProdutoReduzido().getTamanho().getCodigo(), idCampo);
			String qtde = campoQtde.getText().toString();
			int quantidade = 0;
			if (qtde != null && !qtde.isEmpty()) {
				quantidade = Integer.parseInt(qtde);
			}
			
//			TabelaPrecoProdutoReduzido tabelaPrecoProdutoReduzido = 
//					new RepositorioTabelaDePrecoProdutoReduzido(this).busca(
//							red.getEmpresa().getNumeroLoja(), 
//							red.getTabelaPrecoProdutoReduzido().getTabelaDePreco().getCodigo(), 
//							Integer.parseInt(String.valueOf(red.getRed_produto_key())));
			
			itensPedido.add(PedidoItens.newItem(quantidade, red.getPreco(), 0, red));
		}
		Intent i = new Intent();
		Bundle b = new Bundle();
		b.putSerializable("ITENSPEDIDO", (Serializable) itensPedido);
		i.putExtras(b);
		setResult(RESULT_OK, i);
		finish();
	}

	private void geraLabelsComPadrao() {
		int idCampo = 1;
		for (TabelaPrecoProdutoReduzido red : produtoComPadraoTamanhos.getReduzidos()) {
			if (!padroes.contains(red.getProdutoReduzido().getPadrao())) {
				LinearLayout linha = new LinearLayout(this);
				linha.setOrientation(LinearLayout.HORIZONTAL);
				//linha.setId(red.getProdutoReduzido().getPadrao().hashCode());
				TextView textPadrao = new TextView(this);
				textPadrao.setText(String.valueOf(red.getProdutoReduzido().getPadrao().getCodigo()) + " " + red.getProdutoReduzido().getPadrao().getDescricao());

				linha.addView(textPadrao);

				layoutLabelsPadrao.addView(linha);
				textPadrao.setWidth(180);
				padroes.add(red.getProdutoReduzido().getPadrao());

				for (TabelaPrecoProdutoReduzido red2 : produtoComPadraoTamanhos.getReduzidos()) {
					if (red2.getProdutoReduzido().getPadrao().equals(red.getProdutoReduzido().getPadrao())) {
						EditText editText = new EditText(this);
						editText.setId(idCampo);
						Log.d("id", String.valueOf(idCampo));
						
						idDosCampos.put(red2.getProdutoReduzido().getPadrao().getCodigo() + red2.getProdutoReduzido().getPadrao().getDescricao() + red2.getProdutoReduzido().getTamanho().getCodigo(), idCampo);
						Log.i("idDosCampos 2", String.valueOf(idDosCampos.get(red2.getProdutoReduzido().getPadrao().getCodigo() + red2.getProdutoReduzido().getPadrao().getDescricao() + red2.getProdutoReduzido().getTamanho().getCodigo())));
						idCampo++;
						linha.addView(editText);
						editText.setPadding(0, 2, 2, 2);
						editText.setInputType(InputType.TYPE_CLASS_NUMBER);
						editText.setBackgroundResource(R.color.cinza_escuro);

						TableRow.LayoutParams params = new TableRow.LayoutParams(50, LayoutParams.WRAP_CONTENT);
						params.setMargins(10, 10, 10, 10);
						editText.setLayoutParams(params);
						handleEditText(editText);
						preencheCampo(editText, red2);
					}
				}
			}
		}
	}

	private void preencheCampo(EditText editText, TabelaPrecoProdutoReduzido red2) {
		for (PedidoItens item : itensPedido) {
			if (red2.equals(item.getTabelaPrecoProdutoReduzido())) {
				editText.setText(String.valueOf(item.getQuantidade()));
				break;
			}
		}
	}
	
	private void ordenar(List<TabelaPrecoProdutoReduzido> reds) {
		Comparator<TabelaPrecoProdutoReduzido> comparator = new Comparator<TabelaPrecoProdutoReduzido>() {

			@Override
			public int compare(TabelaPrecoProdutoReduzido a, TabelaPrecoProdutoReduzido b) {
				return a.getProdutoReduzido().getTamanho().compareTo(b.getProdutoReduzido().getTamanho());
			}
		};
		Collections.sort(reds, comparator);		
	}

	private void geraLabelsComTamanho() {
		TextView text = new TextView(this);
		text.setText("");
		layoutTamanhos.addView(text);
		text.setWidth(180);
		List<TabelaPrecoProdutoReduzido> reduzidos = produtoComPadraoTamanhos.getReduzidos();
		ordenar(reduzidos);
		for (TabelaPrecoProdutoReduzido red : produtoComPadraoTamanhos.getReduzidos()) {
			if (!tamanhos.contains(red.getProdutoReduzido().getTamanho())) {
				TextView textTam = new TextView(this);
				textTam.setText(red.getProdutoReduzido().getTamanho().getCodigo());
				layoutTamanhos.addView(textTam);
				textTam.setWidth(50);
				textTam.setPadding(0, 2, 2, 2);
				LinearLayout.LayoutParams l = new LinearLayout.LayoutParams(50, LinearLayout.LayoutParams.WRAP_CONTENT);
				l.setMargins(10, 10, 10, 10);
				textTam.setLayoutParams(l);
				tamanhos.add(red.getProdutoReduzido().getTamanho());
			}
		}
	}

	private void handleEditText(final EditText campo) {

		campo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int quantidade = 0;
				String str = campo.getText().toString();
				if (!"".equals(str)) {
					quantidade = Integer.parseInt(str);
				}
				quantidade++;
				campo.setText(String.valueOf(quantidade));
			}
		});

	}

}
