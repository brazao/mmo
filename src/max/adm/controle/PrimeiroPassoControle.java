package max.adm.controle;

import java.util.List;

import max.adm.R;
import max.adm.ValorAtividade;
import max.adm.dominio.Usuario;
import max.adm.repositorio.RepositorioUsuario;
import max.adm.sincronizacao.VerificaConexao;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class PrimeiroPassoControle extends Activity {
	String msg;
	private Button btconfirma;
	private Button btCancela;
	private EditText fieldUrl;
	private String url;
	
	final static int DIALOG_AGUARDE = 1;
	final static int DIALOG_ALERTA_TIME_OUT = 2;
	final static int DIALOG_SUCESSO_CONEXAO = 3;
	
	ProgressDialog aviso;
	

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		url = fieldUrl.getText().toString();
		outState.putString("URL", url);

	}

	@Override
	protected void onRestoreInstanceState(Bundle state) {
		super.onRestoreInstanceState(state);
		this.url = state.getString("URL");
		fieldUrl.setText(url);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// super.onActivityResult(requestCode, resultCode, data);

		// verificar se retornou do menu, se sim, encerrar a aplica��o
		if (requestCode == ValorAtividade.menu.get()) {
			finish();
		}

		if (requestCode == ValorAtividade.criaLogin.get()) {
			RepositorioUsuario repoUsu = new RepositorioUsuario(this);
			List<Usuario> usuarios = repoUsu.listar();

			if (usuarios.size() > 0) {
				Intent menu = new Intent(PrimeiroPassoControle.this,
						MenuActivity.class);
				startActivityForResult(menu, ValorAtividade.menu.get());
			} else {
				finish();
			}
		}

		// verifcar se retornou dos parametros
		if (requestCode == ValorAtividade.AutenticaLicecaRepres.get()) {
			
			if (data != null){
				Intent criaLogin = new Intent(PrimeiroPassoControle.this, CriaLoginControle.class);
				startActivityForResult(criaLogin, ValorAtividade.criaLogin.get());
			}
			else {
				finish();
			}

		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tela_url_primeiro_acesso);

		montaTela();

		defineFuncoes();

	}

	private void montaTela() {
		this.btconfirma = (Button) findViewById(R.id.bt_ok_url);
		this.btCancela = (Button) findViewById(R.id.bt_cancel_url);
		this.fieldUrl = (EditText) findViewById(R.id.field_url_primeiro_acesso);
		this.fieldUrl.setText("http://");
	}

	private void defineFuncoes() {
		btconfirma.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				if (urlValida()) {
					// verificar se ha conexao com internet
					if (temConexaoInternet(PrimeiroPassoControle.this)) {
						url = fieldUrl.getText().toString();
						new Conectar().execute("");
					} 
					else { // caso em que esta sem internet
						AlertDialog.Builder alerta = new AlertDialog.Builder(
								PrimeiroPassoControle.this);
						alerta.setTitle(R.string.sem_conexao);
						alerta.setMessage(R.string.mensagem_sem_conexao);
						alerta.setPositiveButton(R.string.bt_ok,
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int arg1) {
										dialog.cancel();

									}
								});
						alerta.show();
					}

				}
				else {
					AlertDialog.Builder alerta = new AlertDialog.Builder(
							PrimeiroPassoControle.this);
					alerta.setTitle(R.string.titulo_url_invalida);
					alerta.setMessage(R.string.mensagem_url_invalida);
					alerta.setPositiveButton(R.string.bt_ok,
							new DialogInterface.OnClickListener() {

								public void onClick(DialogInterface dialog,
										int arg1) {
									dialog.cancel();

								}
							});
					alerta.show();
				}

			}
		});

		btCancela.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				onBackPressed();

			}
		});

	}

	private boolean urlValida() {
		if (fieldUrl.getText().toString().equals("")
				|| fieldUrl.getText().toString().contains(" ")) {
			return false;
		}
		return true;
	}

	private boolean temConexaoInternet(Context context) {
		ConnectivityManager conn = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo net = conn.getActiveNetworkInfo();
		return (net != null && net.isConnected());

	}
	

	
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
        case DIALOG_AGUARDE: // we set this to 0
        	aviso = new ProgressDialog(this);
        	aviso.setMessage("Procurando Servidor...");
        	aviso.setIndeterminate(true);
        	aviso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        	aviso.setCancelable(true);
        	aviso.show();
            return aviso;
        case DIALOG_SUCESSO_CONEXAO:
        	return new AlertDialog.Builder(this)
        	.setMessage(msg)
        	.setPositiveButton(R.string.bt_ok, new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			})
        	.create();
        	
        default:
            return null;
        }
		
	}
	class Conectar extends AsyncTask<String, String, String>{
		boolean ok;
		@Override
		protected void onPreExecute() {
			showDialog(DIALOG_AGUARDE);

			super.onPreExecute();
		}
		
		@Override
		protected String doInBackground(String... params) {
			
			if (VerificaConexao.verificar(url + "/hello/teste")) {
				dismissDialog(DIALOG_AGUARDE);
				Intent config = new Intent(PrimeiroPassoControle.this, RepresentanteLicencaControle.class);
				config.putExtra("ACTION", "primeiroPasso");
				config.putExtra("URL", url);
				startActivityForResult(config, ValorAtividade.AutenticaLicecaRepres.get());
				msg = "conexão efetuada com sucesso";
				ok = true;
			}
			else { // caso em que nao tem sucesso de conexao com o servidor
				dismissDialog(DIALOG_AGUARDE);
				msg = "Não foi possível conectar-se a esta URL, por favor verifique se está correto, ou entre em contado com o administrador da rede da fábrica.";
				ok = false;
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			if (aviso.isShowing()){
				dismissDialog(DIALOG_AGUARDE);
				finish();
			}
			if (!ok){
				showDialog(DIALOG_SUCESSO_CONEXAO);
			}
			
			super.onPostExecute(result);
		}
		
	}

}
