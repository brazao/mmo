package max.adm.controle;

import max.adm.R;
import max.adm.dominio.Usuario;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UserControle extends Activity {

	Button btConfirma;
	Button btCancela;

	EditText user;
	EditText senha;
	EditText repeteSenha;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cria_login);
		montaTela();
		defineFincoes();
	}

	private void montaTela() {
		btConfirma = (Button) findViewById(R.id.bt_confirma_cria_ogin);
		btCancela = (Button) findViewById(R.id.bt_cancela_cria_login);

		user = (EditText) findViewById(R.id.field_usuario_cria_login);
		senha = (EditText) findViewById(R.id.field_senha_cria_login);
		repeteSenha = (EditText) findViewById(R.id.field_repete_senha_cria_login);
	}

	private void defineFincoes() {
		btCancela.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				onBackPressed();

			}
		});

		btConfirma.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (valida()) {
					Usuario usuario = new Usuario();
					usuario.setNome(user.getText().toString());
					usuario.setLogin(user.getText().toString());
					usuario.setSenha(senha.getText().toString());
					usuario.setCodigoRepresentante("nada");
					
					usuario.salvar(UserControle.this);
					finish();
				}
				else{
					Toast.makeText(UserControle.this, "verifique os campos", Toast.LENGTH_LONG).show();
				}

			}
		});

	}
	
	private boolean valida(){
		if(user.getText().toString().isEmpty()){
			return false;
		}
		if(senha.getText().toString().isEmpty()){
			return false;
		}
		if(!senha.getText().toString().equals(repeteSenha.getText().toString())){
			return false;
		}
		else{
			return true;
		}
	}
}
