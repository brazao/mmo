package max.adm.controle.configuracao;

import java.util.List;

import max.adm.R;
import max.adm.entidade.Parametro;
import max.adm.utilidades.ValidaString;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

public class ConfiguracoesEmailControle extends Activity {

	
	EditText fieldEmail;
	EditText fieldSenha;
	EditText fieldSmtp;
	EditText fieldPorta;
	EditText fieldAssinatura;
	
	Button btconfirma;
	Button btCancela;
	
	CheckBox habilitaSsl;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.configuracoes_email);
		
		montaTela();
		
		defineFuncoes();
		
		preencheTela();
	}
	
	private void montaTela(){
		

		fieldEmail= (EditText)findViewById(R.id.field_email_config_email);
		fieldSenha = (EditText)findViewById(R.id.field_senha_config_email);
		fieldSmtp = (EditText)findViewById(R.id.field_smtp_config_email);
		fieldPorta = (EditText)findViewById(R.id.field_porta_config_email);
		fieldAssinatura = (EditText)findViewById(R.id.field_assinatura_config_email);
		
		btconfirma = (Button)findViewById(R.id.bt_confirma_config_email);
		btCancela = (Button) findViewById(R.id.bt_cancela_config_email);
		
		habilitaSsl = (CheckBox) findViewById(R.id.check_habilita_ssl_email);
		
		
	}
	
	private void defineFuncoes(){
		
		btCancela.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				onBackPressed();
				
			}
		});
		
		
		btconfirma.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				
				//gravar a mesma cnfigura��o de email para todos os parametros
				List<Parametro> parametros = Parametro.listar(ConfiguracoesEmailControle.this);
				
				if (validaCampos()){
					
				
						for(Parametro p : parametros){
							p.setEmail(fieldEmail.getText().toString());
							p.setSenhaEmail(fieldSenha.getText().toString());
							p.setSmtp(fieldSmtp.getText().toString());
							if(!fieldPorta.getText().toString().equals("")){
								p.setPortaEmail(Integer.parseInt(fieldPorta.getText().toString()));
							}
							
							p.setSsl(habilitaSsl.isChecked());
							p.setAssinatura(fieldAssinatura.getText().toString());
							
							p.salvar(ConfiguracoesEmailControle.this);
							
						} // fim do for
						finish();
				}
				else{
					AlertDialog.Builder alerta = new AlertDialog.Builder(ConfiguracoesEmailControle.this);
					alerta.setTitle(R.string.titulo_atencao);
					alerta.setMessage("Existem campos preenchidos incorretamente, por favor verifique.");
					alerta.setPositiveButton(R.string.bt_ok, new DialogInterface.OnClickListener() {
						
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
							
						}
					});
					alerta.show();
				}
			}
		});
		
	}
	
	public void preencheTela(){
		List<Parametro> params = Parametro.listar(this);
		
		if (params != null){
			if (params.size() > 0){
				
				fieldEmail.setText(params.get(0).getEmail());
				fieldSenha.setText(params.get(0).getSenhaEmail());
				fieldSmtp.setText(params.get(0).getSmtp());
				fieldPorta.setText(String.valueOf(params.get(0).getPortaEmail()));
				fieldAssinatura.setText(params.get(0).getAssinatura());
				
			}
		}
		
		
		
	}
	
	public boolean validaCampos(){
		if (!ValidaString.Email(fieldEmail.getText().toString()) || fieldEmail.getText().toString().equals("")){
			return false;
		}
		if (ValidaString.CampoVazio(fieldSenha.getText().toString())){
			return false;
		}
		if (fieldSmtp.getText().toString().equals("") || fieldSmtp.getText().toString().contains(" ") || fieldSmtp.getText().toString().substring(fieldSmtp.getText().toString().length()-1, fieldSmtp.getText().toString().length()).equals(".")){
			return false;
		}
		if (fieldPorta.getText().toString().equals("")){
			return false;
		}
		
		
		
		return true;
	}
	

}
