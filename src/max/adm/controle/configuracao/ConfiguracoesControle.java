package max.adm.controle.configuracao;

import java.util.List;

import max.adm.R;
import max.adm.entidade.Empresa;
import max.adm.entidade.Parametro;
import max.adm.parametro.RepositorioParametro;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ConfiguracoesControle extends Activity {

	private Button btConfirma;
	private Button btCancela;

	private EditText fieldChaveLicenca;
	private EditText fieldUrlServidor;

	private Parametro parametroAtual;
	private boolean mudandoDeloja = false;

	private Empresa empresa;
	List<Parametro> parametros;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.configuracoes);

		montaTela();
		defineFuncoes();
		carregaCampos();

	}

	private void montaTela() {

		btConfirma = (Button) findViewById(R.id.bt_confirma_config_gerais);
		btCancela = (Button) findViewById(R.id.bt_cancela_config_gerais);

		fieldChaveLicenca = (EditText) findViewById(R.id.field_chave_licenca_cnfiguracao);
		fieldUrlServidor = (EditText) findViewById(R.id.field_url_servidor_configuracao);
		// checkSyncAutomatic = (CheckBox)
		// findViewById(R.id.check_sincroniza_automatico);

	}

	private void carregaCampos() {
		empresa = (Empresa) getIntent().getExtras().getSerializable("EMPRESA");
		RepositorioParametro repo = new RepositorioParametro(this);
		Parametro parametro = repo.buscaPorLoja(empresa.getNumeroLoja());
		repo.fechar();
		fieldChaveLicenca.setText(parametro.getChave());
		fieldUrlServidor.setText(parametro.getUrlWebService());
	}

	private void defineFuncoes() {

		btConfirma.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				RepositorioParametro repo = new RepositorioParametro(
						ConfiguracoesControle.this);
				parametros = repo.listar();
				repo.fechar();
				for (Parametro p : parametros) {
					p.setChave(fieldChaveLicenca.getText().toString());
					p.setUrlWebService(fieldUrlServidor.getText().toString());
					p.salvar(ConfiguracoesControle.this);
				}
				onBackPressed();
			}
		});

		btCancela.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				onBackPressed();

			}
		});

	}

}
