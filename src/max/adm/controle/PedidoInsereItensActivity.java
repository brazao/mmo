package max.adm.controle;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import max.adm.Dinheiro;
import max.adm.R;
import max.adm.ValorAtividade;
import max.adm.entidade.Empresa;
import max.adm.entidade.Parametro;
import max.adm.entidade.PedidoCapa;
import max.adm.entidade.PedidoItens;
import max.adm.entidade.Produto;
import max.adm.entidade.ProdutoReduzido;
import max.adm.entidade.TabelaDePreco;
import max.adm.entidade.TabelaPrecoProdutoReduzido;
import max.adm.pedido.EditarItemPedidoAcitivity;
import max.adm.pedido.GrupoDeItensDoPedido;
import max.adm.pedido.ListaDeReduzidos;
import max.adm.pedido.PedidoInsereDadosCapa;
import max.adm.repositorio.RepositorioProduto;
import max.adm.utilidades.PieChartBuilder;
import max.adm.utilidades.adapters.ItemDoPedidoAdapter;
import max.adm.utilidades.adapters.ProdutoItemPedidoParaListar;
import max.adm.utilidades.adapters.ProdutoListPedidoAdapter;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class PedidoInsereItensActivity extends ListActivity {

	private ProgressDialog pd;
	static final int DIALOG_CARREGA_PRODS = 999;

	final int DIALOG_MAIS_DE_UMA_TABELA = 0;

	final int MENU_CANCELA = 1;
	final int MENU_CONFIRMA = 2;
	final int MENU_GRAFICO = 3;

	private TextView textTotalPecas;
	private TextView textValorTotal;

	private ImageView btConfirma;
	private ImageView btCancela;
	private ImageView btGrafico;

	private AutoCompleteTextView fieldBusca;
	private ImageView btPesquisar;

	private ItemDoPedidoAdapter adapter;
	private List<TabelaDePreco> tabelasDePreco;
	private List<ProdutoItemPedidoParaListar> produtosAutoComplete = Lists
			.newLinkedList();
	private Empresa empresa;
	private Parametro parametro;
	private Map<ProdutoReduzido, PedidoItens> itensDoPedido = Maps.newHashMap();
	private List<GrupoDeItensDoPedido> agrupados = Lists.newLinkedList();
	private List<PedidoCapa.Builder> builders = Lists.newLinkedList();

	@SuppressWarnings("unchecked")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data != null
				&& requestCode == ValorAtividade.EscolhaPadraoTamanho.get()) {
			List<PedidoItens> itens = (List<PedidoItens>) data.getExtras()
					.getSerializable("ITENSPEDIDO");
			insereItensNaLista(itens);
		}
		if (data != null
				&& requestCode == ValorAtividade.EditarItemPedidoAcitivity
						.get()) {
			List<PedidoItens> itens = (List<PedidoItens>) data.getExtras()
					.getSerializable("ITENSPEDIDO");
			insereItensNaLista(itens);
		}
		if (requestCode == ValorAtividade.PedidoInsereDadosCapa.get()
				&& data != null) {
			String sair = data.getStringExtra("SAIR");
			if (sair.equals("SIM")) {
				finish();
			} else {
				for (PedidoCapa.Builder builder : builders) {
					builder.limparItens();
				}
			}
		}
		if (data != null
				&& requestCode == ValorAtividade.PedidoPesquisaProduto.get()) {
			ProdutoItemPedidoParaListar prod = (ProdutoItemPedidoParaListar) data
					.getExtras().getSerializable("PRODUTO");
			escolhePadroesTamanhos(prod);
			fieldBusca.getText().clear();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pedido_insere_itens);
		tabelasDePreco = (List<TabelaDePreco>) getIntent().getExtras()
				.getSerializable("TABELAS");
		empresa = (Empresa) getIntent().getExtras().getSerializable("EMPRESA");
		builders = (List<PedidoCapa.Builder>) getIntent().getExtras()
				.getSerializable("BUILDERS");
		parametro = (Parametro) getIntent().getExtras().getSerializable(
				"PARAMETRO");
		new CarregaProdutos().execute("");

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_pedido_insere_itens, menu);

		//
		// menu.add(0, MENU_CONFIRMA, 0, "Confirma").setIcon(R.drawable.bt_ok);
		// menu.add(0, MENU_CANCELA, 1,
		// "Cancela").setIcon(R.drawable.bt_cancel);
		// menu.add(0, MENU_GRAFICO, 2,
		// "Gráfico").setIcon(R.drawable.bt_grafico);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_CANCELA:
			onBackPressed();
			break;
		case MENU_CONFIRMA:
			confirma();
			break;

		case MENU_GRAFICO:
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void montaTela() {
		textTotalPecas = (TextView) findViewById(R.id.text_total_de_pecas_no_pedido);
		textValorTotal = (TextView) findViewById(R.id.text_preco_total_itens_pedido);
		fieldBusca = (AutoCompleteTextView) findViewById(R.id.auto_complete_produto_para_pedido);
		btPesquisar = (ImageView) findViewById(R.id.bt_pesquisa_produto_novo_pedido);
		fieldBusca.setAdapter(new ProdutoListPedidoAdapter(this,
				produtosAutoComplete));
		btConfirma = (ImageView) findViewById(R.id.bt_confirma);
		btCancela = (ImageView) findViewById(R.id.bt_cancel);
		btGrafico = (ImageView) findViewById(R.id.bt_grafico);
	}

	private void defineFuncoes() {
		btPesquisar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				chamaTelaPesquisa();
			}
		});

		fieldBusca.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> lista, View arg1,
					int position, long id) {
				ProdutoItemPedidoParaListar prod = (ProdutoItemPedidoParaListar) lista
						.getItemAtPosition(position);
				escolhePadroesTamanhos(prod);
				fieldBusca.getText().clear();
			}
		});

		getListView().setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> lista, View arg1,
					int position, long arg3) {
				List<PedidoItens> itens = (List<PedidoItens>) lista.getItemAtPosition(position);
				mostraItemDialog(itens);
			}
		});

		btConfirma.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				confirma();
			}
		});

		btCancela.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		btGrafico.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				List<PedidoItens> itens = Lists.newLinkedList();
				for (Map.Entry<ProdutoReduzido, PedidoItens> it : itensDoPedido
						.entrySet()) {
					itens.add(it.getValue());
				}
				Intent i = new Intent(PedidoInsereItensActivity.this,
						PieChartBuilder.class);
				Bundle b = new Bundle();
				b.putSerializable("ITENSPEDIDO", (Serializable) itens);
				i.putExtras(b);
				startActivity(i);
			}
		});

	}

	private void chamaTelaPesquisa() {
		Intent i = new Intent(this, PedidoPesquisaProdutoControle.class);
		ListaDeReduzidos.newInstance(produtosAutoComplete);
		startActivityForResult(i, ValorAtividade.PedidoPesquisaProduto.get());
	}

	public void buscaProdutos() {
		RepositorioProduto repo = new RepositorioProduto(this);
		produtosAutoComplete = repo.listarPorTabelas(tabelasDePreco, empresa);
		repo.fechar();
	}

	public void escolhePadroesTamanhos(ProdutoItemPedidoParaListar produto) {
		Intent i = new Intent(this, EscolhaPadraoTamanhoControle.class);
		Bundle b = new Bundle();
		b.putSerializable("PRODUTO", produto);
		i.putExtras(b);
		startActivityForResult(i, ValorAtividade.EscolhaPadraoTamanho.get());
	}

	private void insereItensNaLista(List<PedidoItens> itens) {
		for (PedidoItens item : itens) {
			itensDoPedido.put(item.getTabelaPrecoProdutoReduzido().getProdutoReduzido(), item);
		}
		removeItensComQuantidadeZero();
		agruparItens();
		insereNaGrid();
		atualizaTotais();
	}

	private void agruparItens() {
		agrupados.clear();
		for (Map.Entry<ProdutoReduzido, PedidoItens> item : itensDoPedido
				.entrySet()) {
			GrupoDeItensDoPedido grupo = GrupoDeItensDoPedido.newInstance(item
					.getKey().getProduto(), item.getKey().getPadrao());
			if (!agrupados.contains(grupo)) {
				agrupados.add(grupo);
				grupo.addItem(item.getValue());
			} else {
				for (GrupoDeItensDoPedido grp : agrupados) {
					if (grp.equals(grupo)) {
						grp.addItem(item.getValue());
					}
				}
			}
		}

	}

	private void insereNaGrid() {
		adapter = new ItemDoPedidoAdapter(this, agrupados);
		setListAdapter(adapter);
	}

	private void atualizaTotais() {
		double total = 0;
		int quantidade = 0;
		for (GrupoDeItensDoPedido item : agrupados) {
			total += item.getValor().getValor().doubleValue();
			quantidade += item.getQuantidade();
		}
		textTotalPecas.setText(String.valueOf(quantidade));
		textValorTotal.setText(Dinheiro.newInstance(new BigDecimal(total))
				.formatToScreen());
	}

	private void mostraItemDialog(final List<PedidoItens> itens) {
		AlertDialog.Builder alerta = new AlertDialog.Builder(this);
		StringBuilder sb = new StringBuilder();
		alerta.setTitle(itens.get(0).getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto()
				.getDescricao()
				+ "\n");
		for (PedidoItens it : itens) {
			sb.append(it.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getTamanho().getCodigo())
					.append("   ").append(it.getQuantidade())
					.append(" unidade(s)\n");
		}
		alerta.setMessage(sb.toString());
		alerta.setPositiveButton(R.string.bt_editar,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						editar(itens);
					}
				});
		alerta.setNegativeButton(R.string.bt_excluir,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						excluir(itens);
					}
				});
		alerta.setNeutralButton(R.string.bt_ok,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		alerta.show();
	}

	private void removeItensComQuantidadeZero() {
		for (Iterator<Map.Entry<ProdutoReduzido, PedidoItens>> i = itensDoPedido
				.entrySet().iterator(); i.hasNext();) {
			Map.Entry<ProdutoReduzido, PedidoItens> entry = i.next();
			if (entry.getValue().getQuantidade() == 0) {
				i.remove();
			}
		}
	}

	private void editar(List<PedidoItens> itens) {
		List<TabelaPrecoProdutoReduzido> tabelaPrecoReduzidos = Lists.newLinkedList();
		for (PedidoItens item : itens) {
			tabelaPrecoReduzidos.add(item.getTabelaPrecoProdutoReduzido());
		}
		Produto produto = itens.get(0).getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto();
		Intent i = new Intent(this, EditarItemPedidoAcitivity.class);
		ProdutoItemPedidoParaListar p = ProdutoItemPedidoParaListar
				.newInstance(produto, tabelaPrecoReduzidos);
		Bundle b = new Bundle();
		b.putSerializable("PRODUTO", p);
		b.putSerializable("ITENSPEDIDO", (Serializable) itens);
		i.putExtras(b);
		startActivityForResult(i,
				ValorAtividade.EditarItemPedidoAcitivity.get());
	}

	private void excluir(List<PedidoItens> itens) {
		for (PedidoItens it : itens) {
			for (Iterator<Map.Entry<ProdutoReduzido, PedidoItens>> i = itensDoPedido
					.entrySet().iterator(); i.hasNext();) {
				Map.Entry<ProdutoReduzido, PedidoItens> entry = i.next();
				if (entry.getValue().equals(it)) {
					i.remove();
				}
			}
		}
		agruparItens();
		insereNaGrid();
		atualizaTotais();
	}

	@Override
	public void onBackPressed() {
		AlertDialog.Builder alerta = new AlertDialog.Builder(this);
		alerta.setTitle("Ateção");
		alerta.setMessage("Tem certeza que deseja cancelar o edição de pedidos?");
		alerta.setPositiveButton(R.string.bt_sim,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				});
		alerta.setNegativeButton(R.string.bt_nao,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		alerta.show();
	}

	private void confirma() {
		if (!itensDoPedido.isEmpty()) {
			for (PedidoCapa.Builder builder : builders) {
				for (Map.Entry<ProdutoReduzido, PedidoItens> item : itensDoPedido
						.entrySet()) {
					if (item.getValue().getTabelaPrecoProdutoReduzido().getTabelaDePreco()
							.equals(builder.getTabelaDePreco()))
						;
					builder.addItem(item.getValue());
				}
			}
			Intent i = new Intent(this, PedidoInsereDadosCapa.class);
			Bundle b = new Bundle();
			b.putSerializable("BUILDERS", (Serializable) builders);
			b.putSerializable("EMPRESA", empresa);
			b.putSerializable("PARAMETRO", parametro);
			i.putExtras(b);
			startActivityForResult(i,
					ValorAtividade.PedidoInsereDadosCapa.get());

		} else {
			Toast.makeText(this, "Insira itens no pedido", Toast.LENGTH_LONG)
					.show();
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == DIALOG_CARREGA_PRODS) {
			pd = new ProgressDialog(this);
			pd.setTitle("Carregando");
			pd.setMessage("Aguarde enquanto carregamos os produtos");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();
			return pd;
		}
		return super.onCreateDialog(id);
	}

	class CarregaProdutos extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			showDialog(DIALOG_CARREGA_PRODS);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			buscaProdutos();
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			montaTela();
			defineFuncoes();
			dismissDialog(DIALOG_CARREGA_PRODS);
			super.onPostExecute(result);
		}

	}

}
