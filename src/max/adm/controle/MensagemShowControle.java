package max.adm.controle;

import java.text.SimpleDateFormat;
import java.util.Date;

import max.adm.R;
import max.adm.entidade.Mensagem;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MensagemShowControle extends Activity {
SimpleDateFormat formatData = new SimpleDateFormat("dd/MM/yy");
	
	Mensagem mensagem;
	ImageView btExcluir;
	ImageView btConfirma;
	
	TextView textMensagem;
	TextView textRemetente;
	TextView textDataRecebimento;
		
	
	TextView textResposta;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mensagem_show);
		
		montaTela();
		
		if (getIntent().getExtras() != null){
			mensagem = (Mensagem) getIntent().getExtras().getSerializable("MENSAGEM");
		}
		
		preencheTela();
		
		defineFuncoes();
		
	}
	
	private void montaTela(){
		
		btExcluir = (ImageView)findViewById(R.id.bt_excluir_mensagem);
		btConfirma = (ImageView)findViewById(R.id.bt_confirma_show_mensagem);
		
		textMensagem = (TextView) findViewById(R.id.text_mensagem_show);
		textResposta = (TextView) findViewById(R.id.text_resposta_mensagem_show);
		
		textRemetente = (TextView) findViewById(R.id.text_remetente_show);
		textDataRecebimento = (TextView) findViewById(R.id.text_data_show);
	}
	
	
	
	
	private void defineFuncoes(){
		
		btExcluir.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				AlertDialog.Builder alerta = new AlertDialog.Builder(MensagemShowControle.this);
				alerta.setTitle(R.string.titulo_atencao);
				alerta.setMessage(R.string.mensagem_tem_certeza_excluir_mensagem);
				alerta.setPositiveButton(R.string.bt_sim, new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int which) {
						excluiMensagem();
						
					}
					
				});
				alerta.setNegativeButton(R.string.bt_sim, new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
				alerta.show();
				
			}
		});
		
		
		
		btConfirma.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				
				finish();
			}
		});
		
	}
	
	private void preencheTela(){
		if (mensagem != null){
			textMensagem.setText(mensagem.getMensagem());
			textResposta.setText(mensagem.getResposta());
			textRemetente.setText(mensagem.getRemetente());
			textDataRecebimento.setText(formatData.format(mensagem.getDataEnvio()));
			
		}
	}
	
	private void excluiMensagem(){
		if (mensagem != null){
			Mensagem.excluir(this, mensagem.getId());
			Toast.makeText(this, R.string.mensagem_mensagem_excluida, Toast.LENGTH_LONG).show();
			finish();
		}
	}
}
