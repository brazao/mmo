package max.adm.controle;

import java.util.ArrayList;
import java.util.List;

import max.adm.R;
import max.adm.ValorAtividade;
import max.adm.entidade.Mensagem;
import max.adm.utilidades.adapters.MensagemAdapter;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class MensagemControle extends ListActivity {

	RadioGroup radioFiltro;
	RadioButton radioNaoEnviadas;
	ImageView btExcluir;
	List<Mensagem> mensagens = new ArrayList<Mensagem>();
	List<Mensagem> listaMostrada = new ArrayList<Mensagem>();
	

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		radioNaoEnviadas.setChecked(true);
		atualizarLista();
		listarNaoRespondidas();
		
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mensagens);

		montaTela();

		preencheLista();

		defineFuncoes();

	}

	private void montaTela() {
		radioFiltro = (RadioGroup) findViewById(R.id.radioGroup_mensagens);
		btExcluir = (ImageView) findViewById(R.id.bt_lixeira_mensagens);
		radioNaoEnviadas = (RadioButton)findViewById(R.id.radio_nao_envidas);

	}

	private void preencheLista() {
		mensagens = Mensagem.listar(this);
		listarNaoRespondidas();

	}

	private void defineFuncoes() {

		// funcao do radio group
		radioFiltro.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(RadioGroup group, int checkedId) {

				switch (radioFiltro.getCheckedRadioButtonId()) {
				case R.id.radio_nao_envidas:
					listarNaoRespondidas();
					break;
				case R.id.radio_envadas:
					listarRespondidas();
					break;

				case R.id.radio_todas:
					listarTodas();
					break;

				default:
					break;
				}

			}
		});

		// botao excluir
		btExcluir.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				switch (radioFiltro.getCheckedRadioButtonId()) {
				case R.id.radio_nao_envidas:
					excluirNaoRespondidas();
					break;
				case R.id.radio_envadas:
					excluirRespondidas();
					break;

				case R.id.radio_todas:
					excluirTodas();
					break;

				default:
					break;
				}
			}
		});
		
		
		// ao clicar em um item da lista
		getListView().setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> lista, View arg1, int position,
					long arg3) {
				
				Mensagem m = (Mensagem) lista.getItemAtPosition(position);
				if (m.getDataResposta() == null){
					Intent i = new Intent(MensagemControle.this, MensagemRespostaControle.class);
					Bundle b = new Bundle();
					b.putSerializable("MENSAGEM", m);
					i.putExtras(b);
					startActivityForResult(i, ValorAtividade.MensagemResposta.get());
				}
				else{
					Intent i = new Intent(MensagemControle.this, MensagemShowControle.class);
					Bundle b = new Bundle();
					b.putSerializable("MENSAGEM", m);
					i.putExtras(b);
					startActivityForResult(i, ValorAtividade.MensagemShow.get());
				}
				
			}
		});

	}
	
	private void atualizarLista(){
		mensagens.clear();
		mensagens = Mensagem.listar(this);
	}

	
	private void listarNaoRespondidas() {
		listaMostrada.clear();
		for (Mensagem m : mensagens) {
			if (m.getDataResposta() == null) {
				listaMostrada.add(m);
			}
		}
		setListAdapter(new MensagemAdapter(this, listaMostrada));
	}
	

	private void listarRespondidas() {

		listaMostrada.clear();
		for (Mensagem m : mensagens) {
			if (m.getDataResposta() != null) {
				listaMostrada.add(m);
			}
		}
		setListAdapter(new MensagemAdapter(this, listaMostrada));
	}

	
	
	private void listarTodas() {
		setListAdapter(new MensagemAdapter(this, mensagens));
	}

	private void excluirTodas() {
		Mensagem.deletarVarias(this, mensagens);
		mensagens = Mensagem.listar(this);
		listarTodas();
				
	}

	private void excluirRespondidas() {
		Mensagem.deletarVarias(this, listaMostrada);
		mensagens = Mensagem.listar(this);
		listarRespondidas();
	}

	private void excluirNaoRespondidas() {
		Mensagem.deletarVarias(this, listaMostrada);
		mensagens = Mensagem.listar(this);
		listarNaoRespondidas();
	}

}
