package max.adm.controle;

import max.adm.R;
import max.adm.controle.configuracao.ConfiguracoesControle;
import max.adm.entidade.Empresa;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MenuConfiguracoesControle extends Activity {

	Button btConfigGerais;
	Button btConfigUsuario;
	ImageView btVoltar;
	
	Empresa empresa;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_configuracoes);
		empresa = (Empresa) getIntent().getExtras().getSerializable("EMPRESA");
		montaTela();
		defineFuncoes();
		
	}
	
	private void montaTela(){
		
		btConfigGerais = (Button) findViewById(R.id.bt_config_gerais);
		btConfigUsuario = (Button) findViewById(R.id.bt_config_usuario);
		btVoltar = (ImageView) findViewById(R.id.flecha_sair_menu_config);
		
	}
	
	private void defineFuncoes(){
		btConfigGerais.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				Intent confgGerais = new Intent(MenuConfiguracoesControle.this, ConfiguracoesControle.class);
				Bundle b = new Bundle();
				b.putSerializable("EMPRESA", empresa);
				confgGerais.putExtras(b);
				startActivity(confgGerais);
				
			}
		});
		
		btConfigUsuario.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				Intent configMensagem = new Intent(MenuConfiguracoesControle.this, UserControle.class);
				startActivity(configMensagem);
				
			}
		});
		
		btVoltar.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				finish();
				
			}
		});
	}
	
	
}
