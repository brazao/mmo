package max.adm.controle.sincronizacao;

import max.adm.R;
import max.adm.entidade.Empresa;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MenuSincronizaControle extends Activity {
	
	ImageView btEnvia;
	ImageView btRecebe;
	ImageView btVoltar;
	Empresa empresa;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_sincroniza);
		
		empresa = (Empresa) getIntent().getExtras().getSerializable("EMPRESA");
		
		montaTela();
		defineFuncoes();
		
	}
	
	private void montaTela(){
		
		btEnvia = (ImageView) findViewById(R.id.bt_sincroniza_envio);
		btRecebe = (ImageView) findViewById(R.id.bt_sincroniza_receber);
		btVoltar = (ImageView) findViewById(R.id.flecha_voltar_ao_menu);
	
	}
	
	private void defineFuncoes(){
		
		btEnvia.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				chamaTelaSincronismoEnvio();
				
			}
		});
		
		btRecebe.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				chamaTelaSincronismoReceber();
			}
		});
		
		btVoltar.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				onBackPressed();
				
			}
		});
		
	}
	
	private void chamaTelaSincronismoEnvio(){
		Intent i = new Intent(this, SincronismoEnvioControle.class);
		Bundle b = new Bundle();
		b.putSerializable("EMPRESA", empresa);
		i.putExtras(b);
		startActivity(i);
	}
	
	private void chamaTelaSincronismoReceber(){
//		Intent i = new Intent(this, SincronismoReceberControle.class);
//		Bundle b = new Bundle();
//		b.putSerializable("EMPRESA", empresa);
//		i.putExtras(b);
//		startActivity(i);
		
		new RecebeBancoAsyncTask(this, empresa).execute();
		
	}
}
