package max.adm.controle.sincronizacao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import max.adm.R;
import max.adm.entidade.Empresa;
import max.adm.entidade.PedidoCapa;
import max.adm.entidade.Pessoa;
import max.adm.pedido.PedidoDoc;
import max.adm.pessoa.repositorio.RepositorioPessoa;
import max.adm.repositorio.RepositorioPedidoCapa;
import max.adm.service.ConexaoHttp;
import max.adm.service.Enviar;
import max.adm.utilidades.adapters.ItemSincronismoPedidoEnvio;
import max.adm.utilidades.adapters.ItemSincronismoPedidoEnvioAdapter;
import max.adm.utilidades.adapters.ItemSincronismoPessoaEnvio;
import max.adm.utilidades.adapters.ItemSincronismoPessoaEnvioAdapter;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

import com.google.common.collect.Lists;

public class SincronismoEnvioControle extends ListActivity {

	static final int DIALOG_ENVIA_PESSOA = 0;
	static final int DIALOG_ENVIA_PEDIDO = 1;
	static final int DIALOG_ALERTA_FALHA_ENVIO = 2;
	static final int DIALOG_CARREGANDO_DADOS = 3;

	boolean envioPessoaOk;
	boolean envioPedidoOk;
	boolean todosMarcados;

	ProgressDialog pDialog;
	AlertDialog.Builder alerta;

	RadioGroup radioGroup;
	ImageView btEnvia;
	ImageView btMarcaTodos;
	ImageView btInverteSelecao;

	Empresa loja;

	List<Pessoa> pessoas = Lists.newLinkedList();
	List<Pessoa> pessoasEnviar = Lists.newLinkedList();
	List<ItemSincronismoPessoaEnvio> pessoasTela = Lists.newLinkedList();

	List<PedidoDoc> pedidos = Lists.newLinkedList();
	List<PedidoDoc> pedidosEnviar = Lists.newLinkedList();
	List<ItemSincronismoPedidoEnvio> pedidosTela = new ArrayList<ItemSincronismoPedidoEnvio>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sincroniza_envio);
		loja = (Empresa) getIntent().getExtras().getSerializable("EMPRESA");
		new CarregaDados().execute("");
	}

	private void preencheTela() {
		pedidosTela = ItemSincronismoPedidoEnvio.preencheLista(pedidos);
		pessoasTela = ItemSincronismoPessoaEnvio.preencheLista(pessoas);

		verificaRadioCheck(radioGroup.getCheckedRadioButtonId());
	}

	private void montaTela() {
		radioGroup = (RadioGroup) findViewById(R.id.radioSincronizaEnvio);
		btEnvia = (ImageView) findViewById(R.id.bt_sincronismo_enviar);
		btMarcaTodos = (ImageView) findViewById(R.id.bt_marca_desmarca_todos_sincro_envio);
		btInverteSelecao = (ImageView) findViewById(R.id.bt_inverte_selecao_sincroniza_envio);
	}

	private void defineFuncoes() {

		radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(RadioGroup group, int checkedId) {
				verificaRadioCheck(checkedId);
			}
		});

		btEnvia.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				int idRadioButton = radioGroup.getCheckedRadioButtonId();

				if (ConexaoHttp.isConnected(SincronismoEnvioControle.this)) {
					enviaDados(idRadioButton);
				} else {
					Toast.makeText(SincronismoEnvioControle.this, "Conecte-se à internet.", Toast.LENGTH_LONG)
							.show();
				}

			}
		});

		btMarcaTodos.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				marcarTodos();

			}
		});

		btInverteSelecao.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				inverteSelecao();

			}
		});
	}

	private void verificaRadioCheck(int checkedId) {
		switch (checkedId) {
		case R.id.radio_sincroniza_cliente:
			mostrarClientes();
			break;

		case R.id.radio_sincroniza_pedido:
			mostrarPedidos();
			break;

		default:
			break;
		}
	}

	private void mostrarClientes() {
		setListAdapter(new ItemSincronismoPessoaEnvioAdapter(this, pessoasTela));

	}

	private void mostrarPedidos() {
		setListAdapter(new ItemSincronismoPedidoEnvioAdapter(this, pedidosTela));
	}

	private void enviaDados(int idRadioButon) {
		switch (idRadioButon) {
		case R.id.radio_sincroniza_cliente:
			new EnviaPessoas().execute("");
			break;

		case R.id.radio_sincroniza_pedido:
			new EnviaPedidos().execute("");
			break;

		default:
			break;
		}
	}

	private void marcarTodos() {
		switch (radioGroup.getCheckedRadioButtonId()) {
		case R.id.radio_sincroniza_cliente:
			marcaTodosClientes();
			break;

		case R.id.radio_sincroniza_pedido:
			marcaTodosPedidos();
			break;

		default:
			break;
		}
	}

	private void marcaTodosClientes() {
		if (!todosMarcados) {
			for (ItemSincronismoPessoaEnvio pessoa : pessoasTela) {
				pessoa.marcado = true;

			}
		} else {
			for (ItemSincronismoPessoaEnvio pessoa : pessoasTela) {
				pessoa.marcado = false;
			}
		}
		todosMarcados = !todosMarcados;
		setListAdapter(new ItemSincronismoPessoaEnvioAdapter(this, pessoasTela));
	}

	private void marcaTodosPedidos() {
		if (!todosMarcados) {
			for (ItemSincronismoPedidoEnvio pedido : pedidosTela) {
				pedido.marcado = true;
			}
		} else {
			for (ItemSincronismoPedidoEnvio pedido : pedidosTela) {
				pedido.marcado = false;
			}
		}
		todosMarcados = !todosMarcados;
		setListAdapter(new ItemSincronismoPedidoEnvioAdapter(this, pedidosTela));
	}

	private void inverteSelecao() {
		switch (radioGroup.getCheckedRadioButtonId()) {
		case R.id.radio_sincroniza_cliente:
			inverteSelecaoPessoa();
			break;

		case R.id.radio_sincroniza_pedido:
			inverteSelecaoPedido();
			break;

		default:
			break;
		}
	}

	private void inverteSelecaoPessoa() {
		for (ItemSincronismoPessoaEnvio p : pessoasTela) {
			p.marcado = !p.marcado;
		}
		setListAdapter(new ItemSincronismoPessoaEnvioAdapter(this, pessoasTela));
	}

	private void inverteSelecaoPedido() {
		for (ItemSincronismoPedidoEnvio p : pedidosTela) {
			p.marcado = !p.marcado;
		}
		setListAdapter(new ItemSincronismoPedidoEnvioAdapter(this, pedidosTela));
	}

	private void mostraMensagemPessoa() {
		if (envioPessoaOk) {
			Toast.makeText(this, "Pessoas enviadas com sucesso", Toast.LENGTH_LONG).show();
		} else {
			Toast.makeText(this, "Falha ao enviar pessoas", Toast.LENGTH_LONG).show();
		}
	}

	private void mostraMensagemPedido() {
		if (envioPedidoOk) {
			Toast.makeText(this, "Pedidos enviados com sucesso", Toast.LENGTH_LONG).show();
		} else {
			Toast.makeText(this, "Falha ao enviar pedidos", Toast.LENGTH_LONG).show();
		}
	}

	class EnviaPessoas extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			showDialog(DIALOG_ENVIA_PESSOA);

		}

		@Override
		protected String doInBackground(String... params) {
			pessoasEnviar.clear();
			for (ItemSincronismoPessoaEnvio p : pessoasTela) {
				if (p.marcado) {
					pessoasEnviar.add(p.pessoa);
				}
			}

			boolean ok = Enviar.enviaClientes(SincronismoEnvioControle.this, pessoasEnviar, loja);
			if (ok) {
				Iterator<ItemSincronismoPessoaEnvio> itr = pessoasTela.iterator();
				while (itr.hasNext()) {
					ItemSincronismoPessoaEnvio p = itr.next();
					if (p.marcado) {
						itr.remove();
					}
				}
				dismissDialog(DIALOG_ENVIA_PESSOA);
				envioPessoaOk = true;
			} else {
				dismissDialog(DIALOG_ENVIA_PESSOA);
				envioPessoaOk = false;
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			if (!envioPessoaOk) {
				showDialog(DIALOG_ALERTA_FALHA_ENVIO);
			} else {
				setListAdapter(new ItemSincronismoPessoaEnvioAdapter(SincronismoEnvioControle.this,
						pessoasTela));
				for (Pessoa p : pessoasEnviar) {
					p.setSync(true);
				}

				RepositorioPessoa repo = new RepositorioPessoa(SincronismoEnvioControle.this);
				repo.updateLista(pessoasEnviar);
				repo.fechar();
				pessoasEnviar.clear();
				mostraMensagemPessoa();
			}
		}

	}

	private class EnviaPedidos extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			showDialog(DIALOG_ENVIA_PEDIDO);
			for (ItemSincronismoPedidoEnvio itemTela : pedidosTela) {
				Log.i("bbbbb", String.valueOf(itemTela.pedido.getCPE_DATA_EMISSAO()));
				if (itemTela.marcado) {
					pedidosEnviar.add(itemTela.pedido);
				}
			}

		}

		@Override
		protected String doInBackground(String... params) {
			// Parametro param =
			// Parametro.buscaPorLoja(SincronismoEnvioControle.this,
			// loja.getNumeroLoja());
			// CheckerLicense checker =
			// CheckerLicense.newInstance(SincronismoEnvioControle.this,
			// "http://ts.produtec.com.br:8282/pdo/rest/licenca/check",
			// param.getChave());
			// if(!checker.check()) {
			// dismissDialog(DIALOG_ENVIA_PEDIDO);
			// envioPedidoOk = false;
			// return null;
			// }

			boolean ok = Enviar.enviaPedidos(SincronismoEnvioControle.this, pedidosEnviar, loja);
			if (ok) {
				Iterator<ItemSincronismoPedidoEnvio> itr = pedidosTela.iterator();
				while (itr.hasNext()) {
					ItemSincronismoPedidoEnvio p = itr.next();
					if (p.marcado) {
						itr.remove();
					}
				}
				dismissDialog(DIALOG_ENVIA_PEDIDO);
				envioPedidoOk = true;
			} else {
				dismissDialog(DIALOG_ENVIA_PEDIDO);
				envioPedidoOk = false;
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			if (!envioPedidoOk) {
				showDialog(DIALOG_ALERTA_FALHA_ENVIO);
			} else {
				setListAdapter(new ItemSincronismoPedidoEnvioAdapter(SincronismoEnvioControle.this,
						pedidosTela));
				for (PedidoDoc p : pedidosEnviar) {
					p.setCPE_SYNC("S");
				}
				PedidoCapa.updateLista(SincronismoEnvioControle.this, pedidosEnviar);
			}
			pedidosEnviar.clear();
			mostraMensagemPedido();
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_ENVIA_PESSOA:
			pDialog = new ProgressDialog(this);
			pDialog.setMessage("Enviando Pessoas...");
			pDialog.setIndeterminate(true);
			pDialog.setCancelable(false);
			pDialog.show();
			return pDialog;

		case DIALOG_ENVIA_PEDIDO:
			pDialog = new ProgressDialog(this);
			pDialog.setMessage("Enviando Pedidos...");
			pDialog.setIndeterminate(true);
			pDialog.setCancelable(false);
			pDialog.show();
			return pDialog;

		case DIALOG_ALERTA_FALHA_ENVIO:
			alerta = new AlertDialog.Builder(SincronismoEnvioControle.this);
			alerta.setTitle(R.string.titulo_atencao);
			alerta.setMessage("Nao foi possovel enviar dados. Entre em contato com a fábrica!");
			alerta.setPositiveButton(R.string.bt_ok, new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			});
			return alerta.create();

		case DIALOG_CARREGANDO_DADOS:
			pDialog = new ProgressDialog(this);
			pDialog.setMessage("Carregando dados...");
			pDialog.setIndeterminate(true);
			pDialog.setCancelable(false);
			pDialog.show();
			return pDialog;

		default:
			break;
		}
		return super.onCreateDialog(id);
	}

	private class CarregaDados extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			showDialog(DIALOG_CARREGANDO_DADOS);
		}

		@Override
		protected String doInBackground(String... params) {
			RepositorioPedidoCapa repoPedido = new RepositorioPedidoCapa(SincronismoEnvioControle.this);
			pedidos = repoPedido.buscaPedidosDocs(loja);
			repoPedido.fechar();

			RepositorioPessoa repoPessoa = new RepositorioPessoa(SincronismoEnvioControle.this);
			pessoas = repoPessoa.listarNaoSincronizados(loja.getNumeroLoja());
			repoPessoa.fechar();

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			dismissDialog(DIALOG_CARREGANDO_DADOS);
			montaTela();
			defineFuncoes();
			preencheTela();
		}
	}
}
