package max.adm.controle.sincronizacao;

import max.adm.R;
import max.adm.entidade.Empresa;
import max.adm.entidade.Parametro;
import max.adm.licenca.CheckerLicense;
import max.adm.parametro.RepositorioParametro;
import max.adm.service.ConexaoHttp;
import max.adm.service.SincronismoReceberService;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

public class SincronismoReceberControle extends Activity {

	CheckBox checkTabPrecoProduto;
	CheckBox checkFaixaCodigo;
	CheckBox checkCEPs;
	CheckBox checkClientes;
	CheckBox checkCondPgto;
	CheckBox checkPedidosContaReceber;
	CheckBox checkLotes;
	CheckBox checkImagens;
	CheckBox checkParametros;
	CheckBox checkTipoCobranca;

	Button btDowload;
	ImageView btMarcaTodos;
	ImageView btInvertSelecao;

	private boolean todosMarcados;

	private Empresa empresa = new Empresa();
	Parametro param;

	private ProgressDialog pDialog;

	private static final int DIALOG_CARREGANDO_DADOS = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sincroniza_recebe);
		montaTela();
		defineFuncoes();
		empresa = (Empresa) getIntent().getExtras().getSerializable("EMPRESA");
		todosMarcados = false;
		RepositorioParametro repo = new RepositorioParametro(this);
		param = repo.buscaPorLoja(empresa.getNumeroLoja());
		repo.fechar();

	}

	private void montaTela() {
		checkTabPrecoProduto = (CheckBox) findViewById(R.id.check_tab_preco_produto);
		checkFaixaCodigo = (CheckBox) findViewById(R.id.check_faixa_codigo);
		checkCEPs = (CheckBox) findViewById(R.id.check_cep);
		checkClientes = (CheckBox) findViewById(R.id.check_clientes);
		checkCondPgto = (CheckBox) findViewById(R.id.check_condicao_pgto);
		checkPedidosContaReceber = (CheckBox) findViewById(R.id.check_faturamento);
		checkLotes = (CheckBox) findViewById(R.id.check_lotes);
		checkImagens = (CheckBox) findViewById(R.id.check_imagens_produtos);
		checkParametros = (CheckBox) findViewById(R.id.check_parametro);
		checkTipoCobranca = (CheckBox) findViewById(R.id.check_tpCobranca);

		btDowload = (Button) findViewById(R.id.bt_sincroniza_receber);
		btMarcaTodos = (ImageView) findViewById(R.id.bt_marca_todos_sincronismo_receber);
		btInvertSelecao = (ImageView) findViewById(R.id.bt_inverter_sincronismo_receber);

	}

	private void defineFuncoes() {

		btMarcaTodos.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				marcaDesmarcaTodos();
			}
		});

		btInvertSelecao.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				inverteSelecaoCheckBoxs();

			}
		});

		btDowload.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				
				if (ConexaoHttp.isConnected(SincronismoReceberControle.this)) {
					new ExecuteDownload().execute("");
					Toast.makeText(SincronismoReceberControle.this, "Fazendo download dos dados solicitados", Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(SincronismoReceberControle.this, "Conecte à interet.", Toast.LENGTH_LONG).show();
				}

			}
		});
	}

	private void marcaDesmarcaTodos() {
		if (todosMarcados) {
			checkTabPrecoProduto.setChecked(false);
			checkFaixaCodigo.setChecked(false);
			checkCEPs.setChecked(false);
			checkClientes.setChecked(false);
			checkCondPgto.setChecked(false);
			checkPedidosContaReceber.setChecked(false);
			checkLotes.setChecked(false);
			checkImagens.setChecked(false);
			checkParametros.setChecked(false);
			checkTipoCobranca.setChecked(false);
			todosMarcados = false;
		} else {
			checkTabPrecoProduto.setChecked(true);
			checkFaixaCodigo.setChecked(true);
			checkCEPs.setChecked(true);
			checkClientes.setChecked(true);
			checkCondPgto.setChecked(true);
			checkPedidosContaReceber.setChecked(true);
			checkLotes.setChecked(true);
			checkImagens.setChecked(true);
			checkParametros.setChecked(true);
			checkTipoCobranca.setChecked(true);
			todosMarcados = true;
		}
	}

	private void inverteSelecaoCheckBoxs() {
		checkTabPrecoProduto.setChecked(!checkTabPrecoProduto.isChecked());
		checkFaixaCodigo.setChecked(!checkFaixaCodigo.isChecked());
		checkCEPs.setChecked(!checkCEPs.isChecked());
		checkClientes.setChecked(!checkClientes.isChecked());
		checkCondPgto.setChecked(!checkCondPgto.isChecked());
		checkPedidosContaReceber.setChecked(!checkPedidosContaReceber.isChecked());
		checkLotes.setChecked(!checkLotes.isChecked());
		checkImagens.setChecked(!checkImagens.isChecked());
		checkParametros.setChecked(!checkParametros.isChecked());
		checkTipoCobranca.setChecked(!checkTipoCobranca.isChecked());
	}

	private void iniciaDownload() {
		param = Parametro.buscaPorLoja(this, empresa.getNumeroLoja());

		Intent i = new Intent();
		Bundle b = new Bundle();
		b.putSerializable("PARAMETRO", this.param);
		b.putSerializable("EMPRESA", empresa);

		b.putBoolean("importaTabelaPreco", checkTabPrecoProduto.isChecked());
		b.putBoolean("importaFaixaDeCodigos", checkFaixaCodigo.isChecked());
		b.putBoolean("importaCeps", checkCEPs.isChecked());
		b.putBoolean("importaClientes", checkClientes.isChecked());
		b.putBoolean("importaCondicoesPagamentos", checkCondPgto.isChecked());
		b.putBoolean("importaContasReceber", checkPedidosContaReceber.isChecked());
		b.putBoolean("importaLotes", checkLotes.isChecked());
		b.putBoolean("importaImagens", checkImagens.isChecked());
		b.putBoolean("importaParametros", checkParametros.isChecked());
		b.putBoolean("importaTipoCobranca", checkTipoCobranca.isChecked());

		i.putExtras(b);

		SincronismoReceberService.newSincronismoReceberService(this).execute(i);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
			case DIALOG_CARREGANDO_DADOS :
				pDialog = new ProgressDialog(this);
				pDialog.setMessage("Importando dados...");
				pDialog.setIndeterminate(true);
				pDialog.setCancelable(false);

				pDialog.show();
				return pDialog;

			default :
				return super.onCreateDialog(id);
		}

	}

	private class ExecuteDownload extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			showDialog(DIALOG_CARREGANDO_DADOS);

		}

		@Override
		protected String doInBackground(String... params) {
			Parametro param = Parametro.buscaPorLoja(SincronismoReceberControle.this, empresa.getNumeroLoja());
			CheckerLicense checker = CheckerLicense.newInstance(SincronismoReceberControle.this, "http://ts.produtec.com.br:8282/pdo/rest/licenca/check", param.getChave());
			if(checker.check()) {
				iniciaDownload();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			dismissDialog(DIALOG_CARREGANDO_DADOS);
		}
	}
}
