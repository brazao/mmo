package max.adm.controle.sincronizacao;

import java.io.IOException;

import max.adm.Servico;
import max.adm.database.DBUtil;
import max.adm.entidade.Empresa;
import max.adm.entidade.Parametro;
import max.adm.service.ConexaoHttp;
import max.adm.utilidades.Comprimir;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.entity.ByteArrayEntity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;

public class RecebeBancoAsyncTask extends AsyncTask<String, Integer, Integer> {
	
	private static final int ERRO_CONEXAO = 0;
	private static final int ERRO_TAMANHO_ARQUIVO = 1;
	private static final int ERRO_GRAVAR_ARQUIVO = 2;
	private static final int SUCESSO = 3;

	private Context context;
	private ProgressDialog progressDialog;
	private Empresa empresa;
	
	public RecebeBancoAsyncTask(Context context, Empresa empresa) {
		this.context = context;
		this.empresa = empresa;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		progressDialog = new ProgressDialog(context);
		progressDialog.setMessage("Atualizando dados...");
		progressDialog.setIndeterminate(true);
		progressDialog.setCancelable(false);
		progressDialog.show();
		
	}
	
	@Override
	protected Integer doInBackground(String... params) {
		
		Parametro parametro = Parametro.buscaPorLoja(context, empresa.getNumeroLoja());
		String url = parametro.getUrlWebService();
		
		Integer representanteId = parametro.getCodigoRepresentante();
		
		byte[] entityByte = Comprimir.transformToByteArray(representanteId);
		
		ConexaoHttp conexaoHttp = ConexaoHttp.newInstance(url+Servico.buscaBancoDeDados.get());
		try {
			byte[] bancoByte = conexaoHttp.recebeDados(new ByteArrayEntity(entityByte));
			if (bancoByte.length == 0) {
				return ERRO_TAMANHO_ARQUIVO;
			} else {
				try {
					if (!DBUtil.gravaBancoNaPasta(context, bancoByte)) {
						return ERRO_GRAVAR_ARQUIVO;
					} else {
						return SUCESSO;
					}
				} catch (IOException e) {
					e.printStackTrace();
					return ERRO_GRAVAR_ARQUIVO;
				}
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			return ERRO_CONEXAO;
		} catch (IOException e) {
			e.printStackTrace();
			return ERRO_CONEXAO;
		}
	}
	
	@Override
	protected void onPostExecute(Integer result) {
		super.onPostExecute(result);
		progressDialog.dismiss();
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setNeutralButton("OK", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		String mensagem;
		
		switch (result) {
		case SUCESSO:
			mensagem = "Dados carregados com sucesso!";
			break;
			
		case ERRO_CONEXAO:
			mensagem = "Ocorreu um problema de conexão.";
			break;
			
		case ERRO_GRAVAR_ARQUIVO:
			mensagem = "Problema ao salvar os dados.";
			break;
			
		case ERRO_TAMANHO_ARQUIVO:
			mensagem = "Arquivo do banco não encontrado / corrompido.";
			break;

		default:
			mensagem = "Problema não identificado";
			break;
		}
		builder.setMessage(mensagem);
		builder.show();
	}

}
