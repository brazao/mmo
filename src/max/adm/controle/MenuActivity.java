package max.adm.controle;

import java.util.List;

import max.adm.R;
import max.adm.ValorAtividade;
import max.adm.controle.sincronizacao.MenuSincronizaControle;
import max.adm.controle.sincronizacao.SincronismoReceberControle;
import max.adm.entidade.Cidade;
import max.adm.entidade.Empresa;
import max.adm.entidade.Parametro;
import max.adm.entidade.TabelaDePreco;
import max.adm.parametro.RepositorioParametro;
import max.adm.pedido.list.PedidoListActivity;
import max.adm.produto.catalogo.CatalogoActivity;
import max.adm.produtoreduzido.repositorio.RepositorioProdutoReduzido;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MenuActivity extends Activity {

	private Empresa empresa;
	private TabelaDePreco tabPreco;
	private Parametro parametro;

	private TextView textLojalogada;
	private ImageView btnPessoa;

	final int DIALOG_LOJA = 1;
	final int DIALOG_TABELA = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_principal);
		montaTela();
		verificaLojaEtabela();
		verificaSeHaDadosSincronizados();
		definefuncoes();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// menu.add(0, 1, 0,
		// "Alterar Tabela").setIcon(R.drawable.bt_tabela_preco);
		menu.add(0, 2, 0, "Alterar Loja").setIcon(android.R.drawable.ic_menu_share);
		menu.add(0, 3, 0, "Sair").setIcon(android.R.drawable.ic_menu_close_clear_cancel);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 2: // altera loja
			escolherLoja();
			break;

		case 3: // sair
			finish();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void montaTela() {

		btnPessoa = (ImageView) findViewById(R.id.bt_pessoa);
		textLojalogada = (TextView) findViewById(R.id.text_loja_logada);

	}

	private void definefuncoes() {

		// botao sincronizar
		ImageView bt_sincronizar = (ImageView) findViewById(R.id.bt_sincronizar);
		bt_sincronizar.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				Intent i = new Intent(MenuActivity.this, MenuSincronizaControle.class);
				Bundle b = new Bundle();
				b.putSerializable("EMPRESA", empresa);
				i.putExtras(b);
				startActivity(i);
			}
		});

		// botao produto
		ImageView bt_produto = (ImageView) findViewById(R.id.bt_produto);
		bt_produto.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				chamaProduto();
			}
		});

		// botao pessoa
		btnPessoa.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				if (baseDeCepOk()) {
					chamaCadastroPessoa();
				} else {
					// AlertDialog.Builder alerta = new
					// AlertDialog.Builder(MenuActivity.this);
					// alerta.setTitle(R.string.titulo_atencao);
					// alerta.setMessage(R.string.mensagem_base_cep_vazia);
					// alerta.setPositiveButton(R.string.bt_sim, new
					// DialogInterface.OnClickListener() {
					//
					// public void onClick(DialogInterface dialog, int which) {
					// chamaSincroniszaReceber();
					//
					// }
					// });
					// alerta.setNegativeButton(R.string.bt_nao, new
					// DialogInterface.OnClickListener() {
					//
					// public void onClick(DialogInterface dialog, int which) {
					// dialog.cancel();
					//
					// }
					// });
					// alerta.show();
					
					Toast.makeText(MenuActivity.this, "Sem CEP", Toast.LENGTH_SHORT).show();
				}

			}
		});

		ImageView bt_chama_daddos_financeiros = (ImageView) findViewById(R.id.bt_dados_financeiros);
		bt_chama_daddos_financeiros.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				chamaDadosFinanceiros();

			}
		});

		ImageView bt_chama_pedido = (ImageView) findViewById(R.id.bt_pedido);
		bt_chama_pedido.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				chamaPedido();

			}
		});

		ImageView bt_chama_menu_config = (ImageView) findViewById(R.id.bt_confg);
		bt_chama_menu_config.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				chamaMenuConfiguracoes();

			}
		});

	}

	private void chamaProduto() {
		Bundle b = new Bundle();
		b.putSerializable("TABELAPRECO", tabPreco);
		b.putSerializable("EMPRESA", empresa);
		b.putSerializable("PARAMETRO", parametro);
		// Intent i = new Intent(MenuActivity.this,
		// GalleryProdutoActivty.class);
		Intent i = new Intent(this, CatalogoActivity.class);
		i.putExtras(b);
		startActivity(i);

	}

	private void verificaSeHaDadosSincronizados() {
		RepositorioProdutoReduzido repo = new RepositorioProdutoReduzido(this);
		boolean tabelaVazia = repo.tabelaVazia();
		repo.fechar();
		if (tabelaVazia) {
			final LinearLayout layoutMenu = (LinearLayout) findViewById(R.id.layout_menu);
			layoutMenu.setEnabled(false);

			new Handler().postDelayed(new Runnable() {
				public void run() {

					AlertDialog.Builder alerta = new AlertDialog.Builder(MenuActivity.this);
					alerta.setTitle(R.string.titulo_sugestao);
					alerta.setMessage(R.string.mensagem_nao_ha_produtos_sincronizados);
					alerta.setPositiveButton(R.string.bt_sim, new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							chamaSincroniszaReceber();
						}
					});
					alerta.setNegativeButton(R.string.bt_nao, new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();

						}
					});

					alerta.show();
					layoutMenu.setEnabled(true);

				}
			}, 2000);

		}

	}

	private void chamaSincroniszaReceber() {
		Intent sincronizaReceber = new Intent(MenuActivity.this, SincronismoReceberControle.class);
		startActivityForResult(sincronizaReceber, ValorAtividade.SincronismoReceber.get());
	}

	private void chamaCadastroPessoa() {
		Bundle b = new Bundle();
		b.putSerializable("EMPRESA", empresa);
		Intent cadastroPessoa = new Intent(MenuActivity.this, PessoaCadastroControle.class);
		cadastroPessoa.putExtras(b);
		cadastroPessoa.putExtra("NOVOCADASTRO", true);
		startActivityForResult(cadastroPessoa, ValorAtividade.PessoaCadastro.get());

	}

	private void chamaDadosFinanceiros() {
		Bundle b = new Bundle();
		b.putSerializable("EMPRESA", empresa);
		Intent dadosFianceiros = new Intent(MenuActivity.this, DadosFinanceirosControle.class);
		dadosFianceiros.putExtras(b);
		dadosFianceiros.putExtra("CHAMADOPOR", "MENU");
		startActivity(dadosFianceiros);
	}

	private void chamaPedido() {
		Bundle b = new Bundle();
		b.putSerializable("EMPRESA", empresa);
		b.putSerializable("PARAMETRO", parametro);
		Intent pedidos = new Intent(MenuActivity.this, PedidoListActivity.class);
		pedidos.putExtras(b);
		startActivity(pedidos);

	}

	private void chamaMenuConfiguracoes() {
		Intent menuConfig = new Intent(MenuActivity.this, MenuConfiguracoesControle.class);
		Bundle b = new Bundle();
		b.putSerializable("EMPRESA", empresa);
		menuConfig.putExtras(b);
		startActivity(menuConfig);
	}

	private void chamaMensagens() {
		Intent mensagens = new Intent(MenuActivity.this, MensagemControle.class);
		startActivity(mensagens);
	}

	private void verificaLojaEtabela() {
		if (getIntent().getExtras() != null) {
			empresa = (Empresa) getIntent().getExtras().getSerializable("EMPRESA");
			tabPreco = (TabelaDePreco) getIntent().getExtras().getSerializable("TABELAPRECO");

		}
		RepositorioParametro repo = new RepositorioParametro(this);
		parametro = repo.buscaPorLoja(empresa.getNumeroLoja());
		repo.fechar();
		System.err.println(empresa);
		System.err.println(parametro);
		textLojalogada.setText("Loja " + String.valueOf(empresa.getNumeroLoja()));

	}

	private boolean baseDeCepOk() {
		return Cidade.tabelaVazia(this);
	}

	private void escolherLoja() {
		showDialog(DIALOG_LOJA);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_LOJA:

			// Primeiro precisamos criar um inflater que adapte o conteudo
			// do
			// xml para o AlertDialog
			LayoutInflater factory = LayoutInflater.from(this);
			final View view = factory.inflate(R.layout.spinner, null); // passamos
																		// o
																		// XML
																		// criado
			final Spinner spinnerLoja = (Spinner) view.findViewById(R.id.combo_spinner_loja);
			final Spinner spinnerTab = (Spinner) view.findViewById(R.id.combo_spinner_tabela_preco);

			List<Empresa> lojas = Empresa.listar(MenuActivity.this);
			ArrayAdapter<Empresa> adapter = new ArrayAdapter<Empresa>(MenuActivity.this,
					android.R.layout.simple_spinner_item, lojas);
			spinnerLoja.setAdapter(adapter);

			spinnerLoja.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

				public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
					empresa = (Empresa) spinnerLoja.getSelectedItem();

					List<TabelaDePreco> tabelas = TabelaDePreco.listaPorEmpresa(empresa, MenuActivity.this);
					ArrayAdapter<TabelaDePreco> adapterTab = new ArrayAdapter<TabelaDePreco>(
							MenuActivity.this, android.R.layout.simple_spinner_item, tabelas);
					spinnerTab.setAdapter(adapterTab);

				}

				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}
			});

			return new AlertDialog.Builder(MenuActivity.this).setTitle("Loja e Tabela").setView(view)
					.setPositiveButton(R.string.bt_ok, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {

							MenuActivity.this.empresa = (Empresa) spinnerLoja.getSelectedItem();
							MenuActivity.this.tabPreco = (TabelaDePreco) spinnerTab.getSelectedItem();

							textLojalogada.setText("Loja " + String.valueOf(empresa.getNumeroLoja()));
						}
					}).create();

		}

		return null;
	}
}
