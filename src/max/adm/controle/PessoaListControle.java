package max.adm.controle;

import java.util.List;

import max.adm.R;
import max.adm.ValorAtividade;
import max.adm.entidade.Empresa;
import max.adm.entidade.Pessoa;
import max.adm.pessoa.PessoaListAdapter;
import max.adm.pessoa.PessoaParaLista;
import max.adm.pessoa.repositorio.RepositorioPessoa;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;

import com.google.common.collect.Lists;

public class PessoaListControle extends ListActivity {

	private EditText edtFiltroPessoa;

	private List<PessoaParaLista> pessoas = Lists.newLinkedList();
	private PessoaListAdapter adapter;
	private Empresa empresa;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pessoa_list);
		montaTela();
		defineFuncoes();
		empresa = (Empresa) getIntent().getExtras().getSerializable("EMPRESA");
		listar();
//		listarPorRazao();
		adapter = new PessoaListAdapter(pessoas, this);
		setListAdapter(adapter);
		
//		pesquisaPessoa.setAdapter(adapter);

	}

	private void listar() {
		RepositorioPessoa repo = new RepositorioPessoa(this);
		pessoas = repo.listarParaTela(empresa.getNumeroLoja());
		repo.fechar();
	}

	private void montaTela() {
		edtFiltroPessoa = (EditText) findViewById(R.id.field_pesquisa_pessoa);

	}

	private void defineFuncoes() {

//		radioGroupTipoPesquisa.setOnCheckedChangeListener(new OnCheckedChangeListener() {
//
//			public void onCheckedChanged(RadioGroup group, int checkedId) {
//				switch (radioGroupTipoPesquisa.getCheckedRadioButtonId()) {
//					case R.id.radio_razao :
//						listarPorRazao();
//						break;
//
//					case R.id.radio_fantasia :
//						listarPorFantasia();
//						break;
//
//					case R.id.radio_cnpj :
//						listarPorCnpj();
//						break;
//
//					default :
//						break;
//				}
//
//			}
//		});

//		pesquisaPessoa.setOnItemClickListener(new OnItemClickListener() {
//
//			public void onItemClick(AdapterView<?> lista, View arg1, int position, long id) {
//				PessoaParaLista pessoaParaLista = pessoas.get(position);
//				RepositorioPessoa repo = new RepositorioPessoa(PessoaListControle.this);
//				Pessoa pessoa = repo.busca(pessoaParaLista.getId(), empresa.getNumeroLoja());
//				repo.fechar();
//				confirma(pessoa);
//			}
//		});

		getListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long id) {
				PessoaParaLista pessoaParaLista = (PessoaParaLista) adapterView.getAdapter().getItem(position);
				RepositorioPessoa repo = new RepositorioPessoa(PessoaListControle.this);
				Pessoa pessoa = repo.busca(pessoaParaLista.getId(), empresa.getNumeroLoja());
				repo.fechar();
				confirma(pessoa);
			}
		});
		
		edtFiltroPessoa.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				adapter.getFilter().filter(s);
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			
			@Override
			public void afterTextChanged(Editable s) {}
		});

	}

//	private void listarPorRazao() {
//		Collections.sort(pessoas, new Comparator<PessoaParaLista>() {
//			public int compare(PessoaParaLista lhs, PessoaParaLista rhs) {
//				if (lhs.getRazao().compareTo(rhs.getRazao()) < 0) {
//					return -1;
//				} else if (lhs.getRazao().compareTo(rhs.getRazao()) == 0) {
//					return 0;
//				} else {
//					return 1;
//				}
//			}
//		});
//		adapter = new PessoaListAdapter(pessoas, this);
//		setListAdapter(adapter);
////		pesquisaPessoa.setAdapter(adapter);
//	}

//	private void listarPorFantasia() {
//		Collections.sort(pessoas, new Comparator<PessoaParaLista>() {
//
//			public int compare(PessoaParaLista lhs, PessoaParaLista rhs) {
//				if (lhs.getFantasia().compareTo(rhs.getFantasia()) < 0) {
//					return -1;
//				} else if (lhs.getFantasia().compareTo(rhs.getFantasia()) == 0) {
//					return 0;
//				} else {
//					return 1;
//				}
//			}
//		});
//		adapter = new PessoaListAdapter(pessoas, this);
//		setListAdapter(adapter);
////		pesquisaPessoa.setAdapter(adapter);
//	}

//	private void listarPorCnpj() {
//		Collections.sort(pessoas, new Comparator<PessoaParaLista>() {
//			public int compare(PessoaParaLista lhs, PessoaParaLista rhs) {
//				if (lhs.getCpf().compareTo(rhs.getCpf()) < 0) {
//					return -1;
//				} else if (lhs.getCpf().compareTo(rhs.getCpf()) == 0) {
//					return 0;
//				} else {
//					return 1;
//				}
//			}
//		});
//
//		adapter = new PessoaListAdapter(pessoas, this);
//		setListAdapter(adapter);
////		pesquisaPessoa.setAdapter(adapter);
//	}

	private void confirma(Pessoa pessoa) {
		Intent i = new Intent();
		Bundle b = new Bundle();
		b.putSerializable("PESSOA", pessoa);
		i.putExtras(b);
		setResult(ValorAtividade.PessoaList.get(), i);
		finish();
	}

}
