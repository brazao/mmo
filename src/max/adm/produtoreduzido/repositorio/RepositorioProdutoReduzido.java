package max.adm.produtoreduzido.repositorio;

import java.util.ArrayList;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.empresa.RepositorioEmpresa;
import max.adm.entidade.PadraoProduto;
import max.adm.entidade.Produto;
import max.adm.entidade.ProdutoReduzido;
import max.adm.entidade.ProdutoReduzido.Builder;
import max.adm.entidade.TabelaDePreco;
import max.adm.entidade.TamanhoProduto;
import max.adm.repositorio.RepositorioPadrao;
import max.adm.repositorio.RepositorioProduto;
import max.adm.repositorio.RepositorioTabelaDePrecoProdutoReduzido;
import max.adm.repositorio.RepositorioTamanho;
import max.adm.tabelaDePreco.RepositorioTabelaDePreco;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.common.collect.Lists;

public class RepositorioProdutoReduzido {

	private Context context;

	protected SQLiteDatabase db;

	public RepositorioProdutoReduzido(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public Integer salvar(ProdutoReduzido produtoReduzido) {
		Integer id = produtoReduzido.getRed_produto_key();
		if (id != null && id != 0) {
			atualizar(produtoReduzido);
		} else {
			id = inserir(produtoReduzido);
		}
		System.out.println("salvando id " + id);
		return id;
	}

	public static Integer salvar(ProdutoReduzido produtoReduzido, SQLiteDatabase db) {
		Integer id = produtoReduzido.getRed_produto_key();
		if (id != null && id != 0) {
			atualizar(produtoReduzido, db);
		} else {
			id = inserir(produtoReduzido, db);
		}
		System.out.println("salvando id " + id);
		return id;
	}

	public Integer inserir(ProdutoReduzido produtoReduzido) {
		ContentValues values = new ContentValues();
		values.put(ProdutoReduzido.RED_PRODUTO_KEY, produtoReduzido.getRed_produto_key());

		values.put(ProdutoReduzido.PRODUTO, produtoReduzido.getProduto().getCodigo());
		values.put(ProdutoReduzido.PADRAO, produtoReduzido.getPadrao().getCodigo());
		values.put(ProdutoReduzido.TAMANHO, produtoReduzido.getTamanho().getCodigo());
		values.put(ProdutoReduzido.CODIGOBARRAS, produtoReduzido.getCodigoDeBarras());

		Integer id = inserir(values);

		return id;
	}

	public static Integer inserir(ProdutoReduzido produtoReduzido, SQLiteDatabase db) {
		ContentValues values = new ContentValues();
		values.put(ProdutoReduzido.RED_PRODUTO_KEY, produtoReduzido.getRed_produto_key());

		values.put(ProdutoReduzido.PRODUTO, produtoReduzido.getProduto().getCodigo());
		values.put(ProdutoReduzido.PADRAO, produtoReduzido.getPadrao().getCodigo());
		values.put(ProdutoReduzido.TAMANHO, produtoReduzido.getTamanho().getCodigo());
		values.put(ProdutoReduzido.CODIGOBARRAS, produtoReduzido.getCodigoDeBarras());

		Integer id = inserir(values, db);

		return id;
	}

	public int inserir(ContentValues values) {
		int id = (int) db.insert(ProdutoReduzido.NOME_TABELA, "", values);
		return id;
	}

	public static int inserir(ContentValues values, SQLiteDatabase db) {
		int id = (int) db.insert(ProdutoReduzido.NOME_TABELA, "", values);
		return id;
	}

	public int atualizar(ProdutoReduzido produtoReduzido) {
		ContentValues values = new ContentValues();
		values.put(ProdutoReduzido.RED_PRODUTO_KEY, produtoReduzido.getRed_produto_key());
		values.put(ProdutoReduzido.PRODUTO, produtoReduzido.getProduto().getCodigo());
		values.put(ProdutoReduzido.PADRAO, produtoReduzido.getPadrao().getCodigo());
		values.put(ProdutoReduzido.TAMANHO, produtoReduzido.getTamanho().getCodigo());
		values.put(ProdutoReduzido.CODIGOBARRAS, produtoReduzido.getCodigoDeBarras());

		String _id = String.valueOf(produtoReduzido.getRed_produto_key());
		String where = ProdutoReduzido.RED_PRODUTO_KEY + "=?";
		String[] whereArgs = new String[] { _id };
		int count = atualizar(values, where, whereArgs);

		return count;
	}

	public static int atualizar(ProdutoReduzido produtoReduzido, SQLiteDatabase db) {
		ContentValues values = new ContentValues();
		values.put(ProdutoReduzido.RED_PRODUTO_KEY, produtoReduzido.getRed_produto_key());
		values.put(ProdutoReduzido.PRODUTO, produtoReduzido.getProduto().getCodigo());
		values.put(ProdutoReduzido.PADRAO, produtoReduzido.getPadrao().getCodigo());
		values.put(ProdutoReduzido.TAMANHO, produtoReduzido.getTamanho().getCodigo());
		values.put(ProdutoReduzido.CODIGOBARRAS, produtoReduzido.getCodigoDeBarras());

		String _id = String.valueOf(produtoReduzido.getRed_produto_key());
		String where = ProdutoReduzido.RED_PRODUTO_KEY + "=?";
		String[] whereArgs = new String[] { _id };
		int count = atualizar(values, where, whereArgs, db);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(ProdutoReduzido.NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public static int atualizar(ContentValues valores, String where, String[] whereArgs, SQLiteDatabase db) {
		int count = db.update(ProdutoReduzido.NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public int deletar(int id) {
		String where = ProdutoReduzido.RED_PRODUTO_KEY + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[] { _id };
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(ProdutoReduzido.NOME_TABELA, where, whereArgs);
		return count;
	}

	public ProdutoReduzido buscaPorRedProdKey(int key) {
		Cursor c = db.query(true, ProdutoReduzido.NOME_TABELA, ProdutoReduzido.COLUNAS,
				ProdutoReduzido.RED_PRODUTO_KEY + "=" + key, null, null, null, null, null);
		RepositorioProduto repoProduto = new RepositorioProduto(context);
		RepositorioPadrao repoPadrao = new RepositorioPadrao(context);
		RepositorioTamanho repoTamanho = new RepositorioTamanho(context);
		RepositorioEmpresa repoEmpresa = new RepositorioEmpresa(context);
		RepositorioTabelaDePreco repoTabPreco = new RepositorioTabelaDePreco(context);

		if (c.getCount() > 0) {
			c.moveToFirst();
			Builder builder = ProdutoReduzido.builder(c.getInt(c.getColumnIndex(ProdutoReduzido.RED_PRODUTO_KEY)));
			builder.comPadrao(repoPadrao.busca(c.getInt(c.getColumnIndex(ProdutoReduzido.PADRAO))));
			builder.comProduto(repoProduto.busca(c.getInt(c.getColumnIndex(ProdutoReduzido.PRODUTO))));
			builder.comRedProdutoKey(c.getInt(c.getColumnIndex(ProdutoReduzido.RED_PRODUTO_KEY)));
			builder.comTamanho(repoTamanho.busca(c.getString(c.getColumnIndex(ProdutoReduzido.TAMANHO))));
			builder.comCodigoDeBarras(c.getString(c.getColumnIndex(ProdutoReduzido.CODIGOBARRAS)));

			repoProduto.fechar();
			repoPadrao.fechar();
			repoTamanho.fechar();
			repoEmpresa.fechar();
			repoTabPreco.fechar();

			c.close();
			return builder.build();
		}
		c.close();
		return null;
	}

	public ProdutoReduzido busca(int id) {
		Cursor c = db.query(
				true, 
				ProdutoReduzido.NOME_TABELA, 
				ProdutoReduzido.COLUNAS,
				ProdutoReduzido.RED_PRODUTO_KEY + "=?", 
				new String[] { String.valueOf(id) }, 
				null, null, null, null);
		
		Log.i("busca", "id "+ id);
		if (c.moveToFirst()) {
			Log.i("tamanho", c.getString(c.getColumnIndex(ProdutoReduzido.TAMANHO)));
			
			RepositorioProduto repoProduto = new RepositorioProduto(context);
			RepositorioPadrao repoPadrao = new RepositorioPadrao(context);
			RepositorioTamanho repoTamanho = new RepositorioTamanho(context);
			RepositorioEmpresa repoEmpresa = new RepositorioEmpresa(context);
			
			Builder builder = ProdutoReduzido.builder(c.getInt(c.getColumnIndex(ProdutoReduzido.RED_PRODUTO_KEY)));
			builder.comPadrao(repoPadrao.busca(c.getInt(c.getColumnIndex(ProdutoReduzido.PADRAO))));
			builder.comProduto(repoProduto.busca(c.getInt(c.getColumnIndex(ProdutoReduzido.PRODUTO))));
			builder.comRedProdutoKey(c.getInt(c.getColumnIndex(ProdutoReduzido.RED_PRODUTO_KEY)));
			builder.comTamanho(repoTamanho.busca(c.getString(c.getColumnIndex(ProdutoReduzido.TAMANHO))));
			builder.comCodigoDeBarras(c.getString(c.getColumnIndex(ProdutoReduzido.CODIGOBARRAS)));
			builder.comEmpresa(repoEmpresa.busca(c.getInt(c.getColumnIndex(ProdutoReduzido.EMPRESA))));
			
			repoProduto.fechar();
			repoPadrao.fechar();
			repoTamanho.fechar();
			repoEmpresa.fechar();

			c.close();
			return builder.build();
		}
		c.close();
		return null;
	}

	public boolean tabelaVazia() {

		String sql = "SELECT max(" + ProdutoReduzido.RED_PRODUTO_KEY + ") FROM "
				+ ProdutoReduzido.NOME_TABELA;
		Cursor c = db.rawQuery(sql, null);
		if (c.moveToFirst()) {
			c.close();
			return false;
		}
		c.close();
		return true;
	}

	public Cursor getCursor() {
		try {
			return db.query(ProdutoReduzido.NOME_TABELA, ProdutoReduzido.COLUNAS, null, null, null, null,
					null);
		} catch (SQLException e) {
			Log.e("ProdutoReduzido", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<ProdutoReduzido> listar() {
		Cursor c = getCursor();
		List<ProdutoReduzido> produtoReduzidos = new ArrayList<ProdutoReduzido>();

		RepositorioProduto repoProduto = new RepositorioProduto(context);
		RepositorioPadrao repoPadrao = new RepositorioPadrao(context);
		RepositorioTamanho repoTamanho = new RepositorioTamanho(context);

		if (c.moveToFirst()) {

			do {
				Builder builder = ProdutoReduzido.builder(c.getInt(c
						.getColumnIndex(ProdutoReduzido.RED_PRODUTO_KEY)));
				builder.comPadrao(repoPadrao.busca(c.getInt(c.getColumnIndex(ProdutoReduzido.PADRAO))));
				builder.comProduto(repoProduto.busca(c.getInt(c.getColumnIndex(ProdutoReduzido.PRODUTO))));
				builder.comRedProdutoKey(c.getInt(c.getColumnIndex(ProdutoReduzido.RED_PRODUTO_KEY)));
				builder.comTamanho(repoTamanho.busca(c.getString(c.getColumnIndex(ProdutoReduzido.TAMANHO))));
				builder.comCodigoDeBarras(c.getString(c.getColumnIndex(ProdutoReduzido.CODIGOBARRAS)));
				produtoReduzidos.add(builder.build());

			} while (c.moveToNext());
		}
		c.close();
		return produtoReduzidos;
	}

	public List<ProdutoReduzido> listarParaTela(int codigoProduto, int idTabPreco) {
		return null;
	}

	public List<ProdutoReduzido> listarPorProdutoTabela(int codigoProduto, String idsTabela) {
		return null;
	}

//	public List<ProdutoReduzido> buscaPorProdutoTabela(Produto produto, TabelaDePreco tabela) {
//		StringBuilder sb = new StringBuilder();
//		sb.append(" select * from pro_reduzido_produtos ")
//				.append(" inner join produto on  ")
//				.append(" pro_reduzido_produtos.produto_produto_id = produto.produto_id ")
//				.append(" inner join tabela_preco on  ")
//				.append(" pro_reduzido_produtos.tabela_preco_tabelapreco_id = tabela_preco.tabelapreco_id ")
//				.append(" inner join pro_padrao on ")
//				.append(" pro_reduzido_produtos.pro_padrao_pad_id = pro_padrao.pad_id ")
//				.append(" inner join pro_ordem_grandeza on ")
//				.append(" pro_reduzido_produtos.pro_ordem_grandeza_tamanho_id = pro_ordem_grandeza.tamanho_id ")
//				.append(" where   ").append(" produto.produto_id =").append(produto.getCodigo())
//				.append(" and ").append(" tabela_preco.tabelapreco_id =").append(tabela.getCodigo())
//				.append(" order by pro_padrao.pad_descricao, pro_ordem_grandeza.tamanho_classificacao ");
//
//		Cursor c = db.rawQuery(sb.toString(), null);
//		List<ProdutoReduzido> reduzidos = new ArrayList<ProdutoReduzido>();
//
//		if (c.moveToFirst()) {
//
//			do {
//				Builder builder = ProdutoReduzido.builder(c.getInt(c.getColumnIndex(ProdutoReduzido.RED_PRODUTO_KEY)));
//				PadraoProduto padrao = PadraoProduto.newPadrao(
//						c.getInt(c.getColumnIndex(PadraoProduto.CODIGO)),
//						c.getString(c.getColumnIndex(PadraoProduto.DESCRICAO)),
//						c.getString(c.getColumnIndex(PadraoProduto.SIGLA)));
//
//				builder.comPadrao(padrao);
//				builder.comProduto(produto);
//				builder.comRedProdutoKey(c.getInt(c.getColumnIndex(ProdutoReduzido.RED_PRODUTO_KEY)));
//				//builder.comTabelaDePreco(tabela);
//				TamanhoProduto tamanho = TamanhoProduto.newTamanho(
//						c.getString(c.getColumnIndex(TamanhoProduto.CODIGO)),
//						c.getString(c.getColumnIndex(TamanhoProduto.DESCRICAO)),
//						c.getInt(c.getColumnIndex(TamanhoProduto.TAMANHOCLASSIFICACAO)));
//				builder.comTamanho(tamanho);
//				reduzidos.add(builder.build());
//
//			} while (c.moveToNext());
//		}
//		c.close();
//		return reduzidos;
//
//	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

	public int qtdeNaBase() {
		String sql = "select count(" + ProdutoReduzido.RED_PRODUTO_KEY + ") as qtde from " + ProdutoReduzido.NOME_TABELA;
		int quantidade = 0;
		Cursor c = db.rawQuery(sql, null);

		if (c.getCount() > 0) {
			c.moveToFirst();
			quantidade = c.getInt(c.getColumnIndex("qtde"));
		}
		c.close();
		return quantidade;
	}

	private List<Integer> listarIds() {
		Cursor c = db.query(ProdutoReduzido.NOME_TABELA, new String[] { ProdutoReduzido.RED_PRODUTO_KEY }, null, null, null, null, null);
		List<Integer> ids = new ArrayList<Integer>();
		if (c.moveToFirst()) {
			do {
				ids.add(c.getInt(c.getColumnIndex(ProdutoReduzido.RED_PRODUTO_KEY)));
			} while (c.moveToNext());
		}
		c.close();
		return ids;

	}

	public void deletaReduzidosSemRelacionamentos() {
		List<Integer> reduzidos = this.listarIds();
		for (Integer red : reduzidos) {

			try {
				Log.d("Reduzido", "deletando id" + String.valueOf(red));
				this.deletar(red);
			} catch (Exception e) {
				Log.d("Reduzido", "Reduzido com relacionamento, impossivel excluir");
			}
		}

	}

	public List<ProdutoReduzido> buscaPorCodigoDeBarras(String codigoDeBarras) {
		Cursor c = db.query(ProdutoReduzido.NOME_TABELA, 
				new String[] { ProdutoReduzido.PRODUTO }, 
				ProdutoReduzido.CODIGOBARRAS + "='" + codigoDeBarras, 
				null, null, null, null);
		List<ProdutoReduzido> reduzidos = Lists.newLinkedList();
		if (c.getCount() > 0) {
			c.moveToFirst();
			int produto = c.getInt(c.getColumnIndex(ProdutoReduzido.PRODUTO));
			c.close();
			Cursor cReds = db.query(ProdutoReduzido.NOME_TABELA, 
					ProdutoReduzido.COLUNAS, 
					ProdutoReduzido.PRODUTO + "=" + produto, 
					null, null, null, null);
			if (cReds.moveToFirst()) {
				do {
					reduzidos.add(build(cReds));
					System.err.println("montando reduzidos " + reduzidos);
				} while (cReds.moveToNext());
			}
			cReds.close();

		}
		return reduzidos;
	}

	private ProdutoReduzido build(Cursor c) {

		Builder builder = ProdutoReduzido.builder(c.getInt(c.getColumnIndex(ProdutoReduzido.RED_PRODUTO_KEY)));

		RepositorioPadrao repoPadrao = new RepositorioPadrao(context);
		builder.comPadrao(repoPadrao.busca(c.getInt(c.getColumnIndex(ProdutoReduzido.PADRAO))));
		repoPadrao.fechar();

		RepositorioProduto repoProduto = new RepositorioProduto(context);
		builder.comProduto(repoProduto.busca(c.getInt(c.getColumnIndex(ProdutoReduzido.PRODUTO))));
		repoProduto.fechar();

		RepositorioTamanho repoTamanho = new RepositorioTamanho(context);
		builder.comTamanho(repoTamanho.busca(c.getString(c.getColumnIndex(ProdutoReduzido.TAMANHO))));
		repoTamanho.fechar();

		builder.comRedProdutoKey(c.getInt(c.getColumnIndex(ProdutoReduzido.RED_PRODUTO_KEY)));
		builder.comCodigoDeBarras(c.getString(c.getColumnIndex(ProdutoReduzido.CODIGOBARRAS)));
		return builder.build();
	}

}
