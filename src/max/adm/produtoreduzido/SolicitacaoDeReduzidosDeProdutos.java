package max.adm.produtoreduzido;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

/**
 * Classe utlizada para envio de requisição ao servidor para busca de produtos reduzidos
 * @author sutil
 *
 */
public class SolicitacaoDeReduzidosDeProdutos implements Serializable{
	private static final long serialVersionUID = 1L;

	private String chave;
	private int inicio;

	private SolicitacaoDeReduzidosDeProdutos(String chave, int inicio){
		this.chave = chave;
		this.inicio = inicio;
	}

	public static SolicitacaoDeReduzidosDeProdutos newIntance(String chave, int inicio){
		checkNotNull(chave, "chave de licenca nao pode ser nula");
		checkArgument(inicio >=0, "inicio da solicitacao de reduzidos nao pode ser negativo.");
		return new SolicitacaoDeReduzidosDeProdutos(chave, inicio);
	}

	public String getChave() {
		return chave;
	}

	public int getInicio() {
		return inicio;
	}
	
	public void somarInicio(int somar){
		this.inicio += somar;
	}
}
