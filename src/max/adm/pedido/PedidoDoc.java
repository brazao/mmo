package max.adm.pedido;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;

public class PedidoDoc implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long CPE_ID;
	private Long CPE_NUMERO_PEDIDO;
	private Long COL_CODIGO;
	private Date CPE_DATA_EMISSAO;
	private Date CPE_DATA_PREVISAO_ENT;
	private BigDecimal CPE_PERC_DESCONTO;
	private String CPE_STATUS;
	private String CPE_OBS;
	private String CPE_SYNC;
	private Long EMPRESANUMERO;
	private Long TIPOCOBRANCACODIGO;
	private String TIPOCOBRANCADESCRICAO;
	private Long TIPOCOBRANCACODIGO2;
	private String TIPOCOBRANCADESCRICAO2;
	private Long PESSOACODIGO;
	private String PESSOANOME;
	private Long TABELADEPRECONUMERO;
	private String TABELADEPRECODESCRICAO;
	private Long CONDICAODEPAGAMENTOCODIGO;
	private String CONDICAODEPAGAMENTODESCRICAO;
	private List<PedidoDocItens> itens = Lists.newLinkedList();

	private PedidoDoc(Builder b) {
		this.CPE_ID = b.CPE_ID;
		this.CPE_NUMERO_PEDIDO = b.CPE_NUMERO_PEDIDO;
		this.COL_CODIGO = b.COL_CODIGO;
		this.CPE_DATA_EMISSAO = b.CPE_DATA_EMISSAO;
		this.CPE_DATA_PREVISAO_ENT = b.CPE_DATA_PREVISAO_ENT;
		this.CPE_PERC_DESCONTO = b.CPE_PERC_DESCONTO;
		this.CPE_STATUS = b.CPE_STATUS;
		this.CPE_OBS = b.CPE_OBS;
		this.CPE_SYNC = b.CPE_SYNC;
		this.EMPRESANUMERO = b.EMPRESANUMERO;
		this.TIPOCOBRANCACODIGO = b.TIPOCOBRANCACODIGO;
		this.TIPOCOBRANCADESCRICAO = b.TIPOCOBRANCADESCRICAO;
		this.TIPOCOBRANCACODIGO2 = b.TIPOCOBRANCACODIGO2;
		this.TIPOCOBRANCADESCRICAO2 = b.TIPOCOBRANCADESCRICAO2;
		this.PESSOACODIGO = b.PESSOACODIGO;
		this.PESSOANOME = b.PESSOANOME;
		this.TABELADEPRECONUMERO = b.TABELADEPRECONUMERO;
		this.TABELADEPRECODESCRICAO = b.TABELADEPRECODESCRICAO;
		this.CONDICAODEPAGAMENTOCODIGO = b.CONDICAODEPAGAMENTOCODIGO;
		this.CONDICAODEPAGAMENTODESCRICAO = b.CONDICAODEPAGAMENTODESCRICAO;
	}

	public static Builder builder() {
		return new Builder();
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof PedidoDoc) {
			PedidoDoc other = (PedidoDoc) o;
			return Objects.equal(this.CPE_NUMERO_PEDIDO, other.CPE_NUMERO_PEDIDO) && Objects.equal(this.EMPRESANUMERO, other.EMPRESANUMERO);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(CPE_NUMERO_PEDIDO, EMPRESANUMERO);
	}

	public void addItens(PedidoDocItens item) {
		itens.add(item);
	}

	public BigDecimal getValor() {
		BigDecimal valor = BigDecimal.ZERO;
		for (PedidoDocItens it : itens) {
			valor = valor.add(it.getValor());
		}
		return valor;
	}

	public BigDecimal getValorLiquido() {
		BigDecimal valor = BigDecimal.ZERO;
		BigDecimal valorDesconto = BigDecimal.ZERO;
		for (PedidoDocItens it : itens) {
			valor = valor.add(it.getValor());
		}
		valorDesconto = valor.multiply(this.CPE_PERC_DESCONTO.divide(new BigDecimal(100)));
		return valor.subtract(valorDesconto);
	}

	public Long getCPE_ID() {
		return CPE_ID;
	}

	public void setCPE_ID(Long cPE_ID) {
		CPE_ID = cPE_ID;
	}

	public Long getCPE_NUMERO_PEDIDO() {
		return CPE_NUMERO_PEDIDO;
	}
	public Long getCOL_CODIGO() {
		return COL_CODIGO;
	}
	public Date getCPE_DATA_EMISSAO() {
		return CPE_DATA_EMISSAO;
	}
	public Date getCPE_DATA_PREVISAO_ENT() {
		return CPE_DATA_PREVISAO_ENT;
	}
	public BigDecimal getCPE_PERC_DESCONTO() {
		return CPE_PERC_DESCONTO;
	}
	public String getCPE_STATUS() {
		return CPE_STATUS;
	}
	public String getCPE_OBS() {
		return CPE_OBS;
	}
	public String getCPE_SYNC() {
		return CPE_SYNC;
	}

	public void setCPE_SYNC(String cPE_SYNC) {
		CPE_SYNC = cPE_SYNC;
	}

	public Long getEMPRESANUMERO() {
		return EMPRESANUMERO;
	}
	public Long getTIPOCOBRANCACODIGO() {
		return TIPOCOBRANCACODIGO;
	}
	public String getTIPOCOBRANCADESCRICAO() {
		return TIPOCOBRANCADESCRICAO;
	}
	public Long getTIPOCOBRANCACODIGO2() {
		return TIPOCOBRANCACODIGO2;
	}
	public String getTIPOCOBRANCADESCRICAO2() {
		return TIPOCOBRANCADESCRICAO2;
	}
	public Long getPESSOACODIGO() {
		return PESSOACODIGO;
	}
	public String getPESSOANOME() {
		return PESSOANOME;
	}
	public Long getTABELADEPRECONUMERO() {
		return TABELADEPRECONUMERO;
	}
	public String getTABELADEPRECODESCRICAO() {
		return TABELADEPRECODESCRICAO;
	}
	public Long getCONDICAODEPAGAMENTOCODIGO() {
		return CONDICAODEPAGAMENTOCODIGO;
	}
	public String getCONDICAODEPAGAMENTODESCRICAO() {
		return CONDICAODEPAGAMENTODESCRICAO;
	}

	public static class Builder {
		private Long CPE_ID;
		private Long CPE_NUMERO_PEDIDO;
		private Long COL_CODIGO;
		private Date CPE_DATA_EMISSAO;
		private Date CPE_DATA_PREVISAO_ENT;
		private BigDecimal CPE_PERC_DESCONTO;
		private String CPE_STATUS;
		private String CPE_OBS;
		private String CPE_SYNC;
		private Long EMPRESANUMERO;
		private Long TIPOCOBRANCACODIGO;
		private String TIPOCOBRANCADESCRICAO;
		private Long TIPOCOBRANCACODIGO2;
		private String TIPOCOBRANCADESCRICAO2;
		private Long PESSOACODIGO;
		private String PESSOANOME;
		private Long TABELADEPRECONUMERO;
		private String TABELADEPRECODESCRICAO;
		private Long CONDICAODEPAGAMENTOCODIGO;
		private String CONDICAODEPAGAMENTODESCRICAO;

		public Builder setCPE_ID(Long cPE_ID) {
			checkNotNull(cPE_ID);
			CPE_ID = cPE_ID;
			return this;
		}

		public Builder setCPE_NUMERO_PEDIDO(Long cPE_NUMERO_PEDIDO) {
			checkNotNull(cPE_NUMERO_PEDIDO);
			checkArgument(cPE_NUMERO_PEDIDO > 0);
			CPE_NUMERO_PEDIDO = cPE_NUMERO_PEDIDO;
			return this;
		}
		public Builder setCOL_CODIGO(Long cOL_CODIGO) {
			COL_CODIGO = cOL_CODIGO;
			return this;
		}
		public Builder setCPE_DATA_EMISSAO(Date cPE_DATA_EMISSAO) {
			checkNotNull(cPE_DATA_EMISSAO);
			CPE_DATA_EMISSAO = cPE_DATA_EMISSAO;
			return this;
		}
		public Builder setCPE_DATA_PREVISAO_ENT(Date cPE_DATA_PREVISAO_ENT) {
			checkNotNull(cPE_DATA_PREVISAO_ENT);
			CPE_DATA_PREVISAO_ENT = cPE_DATA_PREVISAO_ENT;
			return this;
		}
		public Builder setCPE_PERC_DESCONTO(BigDecimal cPE_PERC_DESCONTO) {
			checkNotNull(cPE_PERC_DESCONTO);
			CPE_PERC_DESCONTO = cPE_PERC_DESCONTO;
			return this;
		}
		public Builder setCPE_STATUS(String cPE_STATUS) {
			CPE_STATUS = cPE_STATUS;
			return this;
		}
		public Builder setCPE_OBS(String cPE_OBS) {
			CPE_OBS = cPE_OBS;
			return this;
		}
		public Builder setCPE_SYNC(String cPE_SYNC) {
			checkNotNull(cPE_SYNC);
			CPE_SYNC = cPE_SYNC;
			return this;
		}
		public Builder setEMPRESANUMERO(Long eMPRESANUMERO) {
			checkNotNull(eMPRESANUMERO);
			EMPRESANUMERO = eMPRESANUMERO;
			return this;
		}
		public Builder setTIPOCOBRANCACODIGO(Long tIPOCOBRANCACODIGO) {
			checkNotNull(tIPOCOBRANCACODIGO);
			TIPOCOBRANCACODIGO = tIPOCOBRANCACODIGO;
			return this;
		}
		public Builder setTIPOCOBRANCADESCRICAO(String tIPOCOBRANCADESCRICAO) {
			checkNotNull(tIPOCOBRANCADESCRICAO);
			TIPOCOBRANCADESCRICAO = tIPOCOBRANCADESCRICAO;
			return this;
		}
		public Builder setTIPOCOBRANCACODIGO2(Long tIPOCOBRANCACODIGO2) {
			TIPOCOBRANCACODIGO2 = tIPOCOBRANCACODIGO2;
			return this;
		}
		public Builder setTIPOCOBRANCADESCRICAO2(String tIPOCOBRANCADESCRICAO2) {
			TIPOCOBRANCADESCRICAO2 = tIPOCOBRANCADESCRICAO2;
			return this;
		}
		public Builder setPESSOACODIGO(Long pESSOACODIGO) {
			checkNotNull(pESSOACODIGO);
			PESSOACODIGO = pESSOACODIGO;
			return this;
		}
		public Builder setPESSOANOME(String pESSOANOME) {
			checkNotNull(pESSOANOME);
			PESSOANOME = pESSOANOME;
			return this;
		}
		public Builder setTABELADEPRECONUMERO(Long tABELADEPRECONUMERO) {
			checkNotNull(tABELADEPRECONUMERO);
			TABELADEPRECONUMERO = tABELADEPRECONUMERO;
			return this;
		}
		public Builder setTABELADEPRECODESCRICAO(String tABELADEPRECODESCRICAO) {
			checkNotNull(tABELADEPRECODESCRICAO);
			TABELADEPRECODESCRICAO = tABELADEPRECODESCRICAO;
			return this;
		}
		public Builder setCONDICAODEPAGAMENTOCODIGO(Long cONDICAODEPAGAMENTOCODIGO) {
			checkNotNull(cONDICAODEPAGAMENTOCODIGO);
			CONDICAODEPAGAMENTOCODIGO = cONDICAODEPAGAMENTOCODIGO;
			return this;
		}
		public Builder setCONDICAODEPAGAMENTODESCRICAO(String cONDICAODEPAGAMENTODESCRICAO) {
			checkNotNull(cONDICAODEPAGAMENTODESCRICAO);
			CONDICAODEPAGAMENTODESCRICAO = cONDICAODEPAGAMENTODESCRICAO;
			return this;
		}

		public PedidoDoc build() {
			checkState(CPE_ID != null);
			checkState(CPE_NUMERO_PEDIDO != null);
			checkState(COL_CODIGO != null);
			checkState(CPE_DATA_EMISSAO != null);
			checkState(CPE_DATA_PREVISAO_ENT != null);
			checkState(CPE_PERC_DESCONTO != null);
			checkState(CPE_STATUS != null);
			checkState(CPE_OBS != null);
			checkState(CPE_SYNC != null);
			checkState(EMPRESANUMERO != null);
			checkState(TIPOCOBRANCACODIGO != null);
			checkState(TIPOCOBRANCADESCRICAO != null);
			checkState(PESSOACODIGO != null);
			checkState(PESSOANOME != null);
			checkState(TABELADEPRECONUMERO != null);
			checkState(TABELADEPRECODESCRICAO != null);
			checkState(CONDICAODEPAGAMENTOCODIGO != null);
			checkState(CONDICAODEPAGAMENTODESCRICAO != null);
			return new PedidoDoc(this);
		}

	}

}
