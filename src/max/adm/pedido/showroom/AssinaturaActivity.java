package max.adm.pedido.showroom;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;

import max.adm.Dinheiro;
import max.adm.R;
import max.adm.entidade.PedidoCapa;
import max.adm.repositorio.RepositorioPedidoCapa;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.gesture.GestureOverlayView;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.Lists;

public class AssinaturaActivity extends Activity {

	TextView numeroPedido;
	TextView dataEmissao;
	TextView cliente;
	TextView condicaoPagamento;
	TextView tipoCobranca;
	TextView tipoCobranca2;
	TextView quantiaPecas;
	TextView valorPedido;
	TextView labelTipoCobranca;
	Button btConfirmar;
	Button btCancelar;
	GestureOverlayView gestureView;

	List<PedidoCapa> pedidos = Lists.newLinkedList();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.assinar_pedido);
		super.onCreate(savedInstanceState);
		pedidos = (List<PedidoCapa>) getIntent().getExtras().getSerializable("PEDIDOS");
		initComponentes();
		preencheTela();
		handleComponents();
	}

	private void initComponentes() {
		numeroPedido = (TextView) findViewById(R.id.numeroPedido);
		dataEmissao = (TextView) findViewById(R.id.dataEmissao);
		cliente = (TextView) findViewById(R.id.cliente);
		condicaoPagamento = (TextView) findViewById(R.id.condicao);
		tipoCobranca = (TextView) findViewById(R.id.tipoCobranca);
		tipoCobranca2 = (TextView) findViewById(R.id.tipoCobranca2);
		quantiaPecas = (TextView) findViewById(R.id.quantiaPecas);
		valorPedido = (TextView) findViewById(R.id.valorPedido);
		labelTipoCobranca = (TextView) findViewById(R.id.labelCobranca2);
		btCancelar = (Button) findViewById(R.id.btCancelar);
		btConfirmar = (Button) findViewById(R.id.btConfirmar);
		gestureView = (GestureOverlayView) findViewById(R.id.assinatura);
	}

	private void preencheTela() {
		if (pedidos != null && !pedidos.isEmpty()) {

			int quantidadePecas = 0;
			double valor = 0;
			PedidoCapa p = pedidos.get(0);
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			for (PedidoCapa pedido : pedidos) {
				quantidadePecas += pedido.getQuantidadePecas();
				valor += pedido.getValorLiquido();
			}

			numeroPedido.setText(String.valueOf(p.getNumeroPedido()));
			dataEmissao.setText(format.format(p.getDataEmissao()));
			cliente.setText(p.getPessoa().getRazaoSocial());
			condicaoPagamento.setText(p.getCondicaoDePagamento().getDescricao());
			tipoCobranca.setText(p.getTipoCobranca().getDescricao());
			if (p.getTipoCobranca2() != null) {
				tipoCobranca2.setText(p.getTipoCobranca2().getDescricao());
			} else {
				tipoCobranca2.setText("");
				labelTipoCobranca.setText("");
			}
			quantiaPecas.setText(String.valueOf(quantidadePecas));
			valorPedido.setText(Dinheiro.newInstance(new BigDecimal(valor)).formatToScreen());

		}
	}

	private void handleComponents() {
		btCancelar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				cancelar();

			}
		});

		btConfirmar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				confirmar();
			}
		});

	}

	@Override
	public void onBackPressed() {
		cancelar();
	}

	private void cancelar() {
		AlertDialog.Builder alerta = new AlertDialog.Builder(this);
		alerta.setTitle("Cancelar");
		alerta.setMessage("Voltar aos dados do pedido?");
		alerta.setPositiveButton("Sim", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent i = new Intent();
				i.putExtra("SAIR", "NÃO");
				setResult(RESULT_OK, i);
				finish();
			}
		});
		alerta.setNeutralButton("Não", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();

			}
		});
		alerta.setNegativeButton("Cancelar pedido", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent i = new Intent();
				i.putExtra("SAIR", "SIM");
				setResult(RESULT_OK, i);
				finish();
			}
		});
		alerta.show();
	}

	private void confirmar() {
		if (pedidos != null && !pedidos.isEmpty()) {
			for (PedidoCapa pedido : pedidos) {
				pedido.setAssinatura(getAssinatura());

				RepositorioPedidoCapa repo = new RepositorioPedidoCapa(this);
				repo.salvar(pedido);
				repo.fechar();
			}
			Toast.makeText(this, "Pedido gravado com sucesso!", Toast.LENGTH_LONG).show();
		}

		Intent i = new Intent();
		i.putExtra("SAIR", "SIM");
		setResult(RESULT_OK, i);
		finish();
	}
	private byte[] getAssinatura() {
		gestureView.setDrawingCacheEnabled(true);
		Bitmap b = Bitmap.createBitmap(gestureView.getDrawingCache());
		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		b.compress(CompressFormat.PNG, 100, bos);
		System.out.println(bos.toByteArray());
		return bos.toByteArray();

	}

}