package max.adm.pedido.showroom;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import max.adm.Dinheiro;
import max.adm.R;
import max.adm.ValorAtividade;
import max.adm.controle.EscolhaPadraoTamanhoControle;
import max.adm.controle.PedidoPesquisaProdutoControle;
import max.adm.entidade.Empresa;
import max.adm.entidade.Parametro;
import max.adm.entidade.PedidoCapa;
import max.adm.entidade.PedidoItens;
import max.adm.entidade.TabelaDePreco;
import max.adm.entidade.TabelaPrecoProdutoReduzido;
import max.adm.pedido.ListaDeReduzidos;
import max.adm.pedido.PedidoInsereDadosCapa;
import max.adm.pedido.showroom.adapter.AdapterItemPedidoShowroom;
import max.adm.pedido.showroom.popup.EditarItemPedidoShowroom;
import max.adm.repositorio.RepositorioProduto;
import max.adm.tabelaDePreco.TabelaPrecoMultActivity;
import max.adm.tabelaDePreco.TabelaPrecoSingleActivity;
import max.adm.utilidades.PieChartBuilder;
import max.adm.utilidades.adapters.ProdutoItemPedidoParaListar;
import max.adm.utilidades.adapters.ProdutoListPedidoAdapter;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

public class ScannerItemReduzidoShowroomActivity extends ListActivity {

	private static int CARREGA_PRODS = 999;
	ProgressDialog progressDialog;

	protected List<TabelaDePreco> tabelasPreco = Lists.newLinkedList();
	protected Parametro parametro;
	protected List<TabelaPrecoProdutoReduzido> tabelaPrecoReduzidos = Lists.newLinkedList();
	protected Empresa empresa;
	protected Button btScanner;
	private LinkedList<PedidoItens> itens = Lists.newLinkedList();

	private TextView totalPecas;
	private TextView totalPedido;

	private Button btCancela;
	private Button btConfirma;
	private Button btGrafico;
	private ImageView btPesquisa;

	private AdapterItemPedidoShowroom adapter;

	protected List<ProdutoItemPedidoParaListar> produtosAutoComplete = Lists.newLinkedList();
	protected AutoCompleteTextView autocomplete;
	protected ProdutoListPedidoAdapter adapterAutoComplete;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.showroom);
		parametro = (Parametro) getIntent().getExtras().getSerializable("PARAMETRO");
		empresa = (Empresa) getIntent().getExtras().getSerializable("EMPRESA");
		initComponentes();
		handleComponents();
		Intent i = new Intent();
		Bundle b = new Bundle();
		b.putSerializable("EMPRESA", empresa);
		b.putSerializable("TABELAS", (Serializable) tabelasPreco);
		b.putSerializable("PARAMETRO", parametro);
		i.putExtras(b);

		if (parametro.isMultiTabelaPreco()) {
			i.setClass(this, TabelaPrecoMultActivity.class);
			startActivityForResult(i, ValorAtividade.TabelaPrecoMultActivity.get());
		} else {
			i.setClass(this, TabelaPrecoSingleActivity.class);
			startActivityForResult(i, ValorAtividade.TabelaPrecoSingleActivity.get());
		}

		adapter = new AdapterItemPedidoShowroom(this, itens);
		setListAdapter(adapter);

	}

	private void initComponentes() {
		btScanner = (Button) findViewById(R.id.scanner);
		autocomplete = (AutoCompleteTextView) findViewById(R.id.autoCompleteProdutos);
		totalPecas = (TextView) findViewById(R.id.quantiaDePecas);
		totalPedido = (TextView) findViewById(R.id.totalPedido);
		btCancela = (Button) findViewById(R.id.btCancelar);
		btConfirma = (Button) findViewById(R.id.btConfirmar);
		btGrafico = (Button) findViewById(R.id.btGrafico);
		btPesquisa = (ImageView) findViewById(R.id.btPesquisa);
	}

	private void handleComponents() {
		btScanner.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				chamaLeitor();
			}
		});
		autocomplete.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> lista, View view, int position, long id) {
				ProdutoItemPedidoParaListar produto = (ProdutoItemPedidoParaListar) lista
						.getItemAtPosition(position);
				tabelaPrecoReduzidos = produto.getReduzidos();
				escolherPadroesTamanhos();
				autocomplete.getText().clear();
			}
		});

		getListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> lista, View v, int position, long id) {
				PedidoItens item = (PedidoItens) lista.getItemAtPosition(position);
				editarItem(item);
			}
		});
		btCancela.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				cancelarPedido();
			}
		});
		btConfirma.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				confirmarPedido();
			}
		});
		btGrafico.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				chamaTelaGrafico();
			}
		});
		btPesquisa.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				chamaTelaPesquisa();
			}
		});
	}

	private void chamaLeitor() {
		try {
			Intent intent = new Intent("com.google.zxing.client.android.SCAN");
			intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
			startActivityForResult(intent, ValorAtividade.CodigoBarra.get());
		} catch (Exception e) {
			Toast.makeText(ScannerItemReduzidoShowroomActivity.this,
					"Leitor de código de barras não encontrado.", Toast.LENGTH_LONG).show();
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == ValorAtividade.CodigoBarra.get() && data != null && resultCode == RESULT_OK) {
			String codigoBarra = data.getStringExtra("SCAN_RESULT");
			buscaProduto(codigoBarra);
			escolherPadroesTamanhos();
			tabelaPrecoReduzidos = null;

		}
		if ((requestCode == ValorAtividade.TabelaPrecoMultActivity.get() || requestCode == ValorAtividade.TabelaPrecoSingleActivity
				.get()) && data != null) {
			tabelasPreco = (List<TabelaDePreco>) data.getExtras().getSerializable("TABELAS");
			if (tabelasPreco == null || tabelasPreco.isEmpty()) {
				finish();
			} else {
				montaAutoComplete();
			}
		}
		if (requestCode == ValorAtividade.EscolhaPadraoTamanho.get() && data != null) {
			List<PedidoItens> itensEscolhidos = (List<PedidoItens>) data.getExtras().getSerializable(
					"ITENSPEDIDO");
			addItens(itensEscolhidos);
			atualizarTotais();
		}
		if (requestCode == ValorAtividade.EditarItemPedidoShowroom.get() && data != null) {
			PedidoItens item = (PedidoItens) data.getExtras().getSerializable("ITEM");
			atualizarLista(item);
			atualizarTotais();
		}
		if (requestCode == ValorAtividade.PedidoInsereDadosCapa.get() && data != null) {
			String sair = data.getStringExtra("SAIR");
			if ("SIM".equals(sair)) {
				finish();
			}
		}
		if (data != null && requestCode == ValorAtividade.PedidoPesquisaProduto.get()) {
			ProdutoItemPedidoParaListar prod = (ProdutoItemPedidoParaListar) data.getExtras()
					.getSerializable("PRODUTO");
			tabelaPrecoReduzidos = prod.getReduzidos();
			escolherPadroesTamanhos();
			autocomplete.getText().clear();
		}
	}

	private void buscaProduto(String codigoBarra) {
		boolean achou = false;
		int i = 0;
		for (ProdutoItemPedidoParaListar p : produtosAutoComplete) {
			for (TabelaPrecoProdutoReduzido red : p.getReduzidos()) {
				i++;
				Log.d("debug", codigoBarra + " -> " + red.getProdutoReduzido().getCodigoDeBarras() + " " + String.valueOf(i));

				if (codigoBarra.equals(red.getProdutoReduzido().getCodigoDeBarras())) {
					tabelaPrecoReduzidos = ImmutableList.copyOf(p.getReduzidos());
					achou = true;
					break;
				}
			}
			if (achou) {
				break;
			}
		}

	}

	private void montaAutoComplete() {
		new CargaDeProdutos().execute("");
	}

	private void escolherPadroesTamanhos() {
		if (tabelaPrecoReduzidos != null && !tabelaPrecoReduzidos.isEmpty()) {
			ProdutoItemPedidoParaListar reduzidosEscolhidos = ProdutoItemPedidoParaListar.newInstance(
					tabelaPrecoReduzidos.get(0).getProdutoReduzido().getProduto(), tabelaPrecoReduzidos);
			Intent i = new Intent(this, EscolhaPadraoTamanhoControle.class);
			Bundle b = new Bundle();
			b.putSerializable("PRODUTO", reduzidosEscolhidos);
			i.putExtras(b);
			startActivityForResult(i, ValorAtividade.EscolhaPadraoTamanho.get());
		} else {
			Toast.makeText(this, "Produto não encontrado", Toast.LENGTH_LONG).show();
		}
	}

	private void addItens(List<PedidoItens> itensEscolhidos) {
//		if (tabelasPreco.size() > 1) {
//			for (PedidoItens itemPedido : itens) {
//				for (PedidoItens itemAserInserido : itensEscolhidos) {
//					if (itemAserInserido.equals(itemPedido)
//							&& !itemAserInserido
//									.getProdutoReduzido()
//									.getTabelaPrecoProdutoReduzido()
//									.getTabelaDePreco()
//									.equals(itemPedido.getProdutoReduzido().getTabelaPrecoProdutoReduzido()
//											.getTabelaDePreco())) {
//						String msg = "Este item já hávia sido inserido com a tabela "
//								+ itemPedido.getProdutoReduzido().getTabelaPrecoProdutoReduzido()
//										.getTabelaDePreco().getCodigo() + " e foi usada a mesma tabela.";
//						Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
//					}
//				}
//			}
//		}

		for (PedidoItens it : itensEscolhidos) {
			if (it.getQuantidade() > 0) {
				if (itens.contains(it)) {
					for (PedidoItens itemDoPedido : itens) {
						if (itemDoPedido.equals(it)) {
							itemDoPedido.setQuantidade(itemDoPedido.getQuantidade() + it.getQuantidade());
						}
					}
				} else {
					itens.add(it);
				}
			}
		}
		adapter = new AdapterItemPedidoShowroom(this, itens);
		setListAdapter(adapter);
	}

	public void editarItem(PedidoItens item) {
		Intent i = new Intent(this, EditarItemPedidoShowroom.class);
		Bundle b = new Bundle();
		b.putSerializable("ITEM", item);
		i.putExtras(b);
		startActivityForResult(i, ValorAtividade.EditarItemPedidoShowroom.get());
	}

	private void atualizarLista(PedidoItens item) {
		if (item.getQuantidade() == 0) {
			itens.remove(item);
		} else {
			for (PedidoItens it : itens) {
				if (it.equals(item)) {
					it.setQuantidade(item.getQuantidade());
				}
			}
		}
		adapter = new AdapterItemPedidoShowroom(this, itens);
		setListAdapter(adapter);
	}

	private void atualizarTotais() {
		int quantidadePecas = 0;
		double totalPedido = 0;

		for (PedidoItens item : itens) {
			quantidadePecas += item.getQuantidade();
			totalPedido += item.getValorTotal();
		}

		this.totalPecas.setText(String.valueOf(quantidadePecas));
		this.totalPedido.setText(String.valueOf(Dinheiro.newInstance(new BigDecimal(totalPedido))
				.formatToScreen()));
	}

	private void cancelarPedido() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Cancelar");
		builder.setMessage("Cancelar pedido?");
		builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
		builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		builder.show();
	}

	private void confirmarPedido() {
		if (!itens.isEmpty()) {
			List<PedidoCapa.Builder> builders = Lists.newLinkedList();
			for (TabelaDePreco tabela : tabelasPreco) {
				builders.add(PedidoCapa.builder().comTabelaDePreco(tabela));
			}
			for (PedidoItens item : itens) {
				for (PedidoCapa.Builder builder : builders) {
					if (builder.getTabelaDePreco().equals(
							item.getTabelaPrecoProdutoReduzido().getTabelaDePreco())) {
						builder.addItem(item);
					}
				}
			}
			Bundle b = new Bundle();
			b.putSerializable("EMPRESA", empresa);
			b.putSerializable("PARAMETRO", parametro);
			b.putSerializable("BUILDERS", (Serializable) builders);
			Intent i = new Intent(this, PedidoInsereDadosCapa.class);
			i.putExtras(b);
			startActivityForResult(i, ValorAtividade.PedidoInsereDadosCapa.get());
		} else {
			Toast.makeText(this, "Insira itens no pedido", Toast.LENGTH_LONG).show();
		}
	}

	private void chamaTelaGrafico() {
		Intent i = new Intent(ScannerItemReduzidoShowroomActivity.this, PieChartBuilder.class);
		Bundle b = new Bundle();
		b.putSerializable("ITENSPEDIDO", itens);
		i.putExtras(b);
		startActivity(i);
	}

	private void chamaTelaPesquisa() {
		Intent i = new Intent(this, PedidoPesquisaProdutoControle.class);
		ListaDeReduzidos.newInstance(produtosAutoComplete);
		startActivityForResult(i, ValorAtividade.PedidoPesquisaProduto.get());
	}

	@Override
	public void onBackPressed() {
		cancelarPedido();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == CARREGA_PRODS) {
			progressDialog = new ProgressDialog(this);
			progressDialog.setTitle("Carregando");
			progressDialog.setMessage("Aguarde enquanto buscamos os produtos");
			progressDialog.setIndeterminate(true);
			progressDialog.setCancelable(false);
			progressDialog.show();
			return progressDialog;
		}
		return super.onCreateDialog(id);
	}

	class CargaDeProdutos extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			showDialog(CARREGA_PRODS);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			RepositorioProduto repo = new RepositorioProduto(ScannerItemReduzidoShowroomActivity.this);
			produtosAutoComplete = repo.listarPorTabelas(tabelasPreco, empresa);
			repo.fechar();

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			adapterAutoComplete = new ProdutoListPedidoAdapter(ScannerItemReduzidoShowroomActivity.this,
					produtosAutoComplete);
			autocomplete.setAdapter(adapterAutoComplete);
			dismissDialog(CARREGA_PRODS);
			super.onPostExecute(result);
		}

	}

}
