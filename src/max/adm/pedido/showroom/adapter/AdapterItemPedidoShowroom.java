package max.adm.pedido.showroom.adapter;

import java.math.BigDecimal;
import java.util.List;

import max.adm.Dinheiro;
import max.adm.R;
import max.adm.entidade.PedidoItens;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.common.collect.Lists;

public class AdapterItemPedidoShowroom extends BaseAdapter {

	List<PedidoItens> itens = Lists.newLinkedList();
	Context ctx;

	public AdapterItemPedidoShowroom(Context ctx, List<PedidoItens> itens) {
		if (itens != null) {
			this.itens = itens;
		}
		this.ctx = ctx;
	}

	@Override
	public int getCount() {
		return this.itens.size();
	}

	@Override
	public PedidoItens getItem(int position) {
		return itens.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		PedidoItens item = itens.get(position);
		Labels labels;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.modelo_item_pedido_showroom, null);
			labels = new Labels();
			labels.quantidade = (TextView) view.findViewById(R.id.quantidade);
			labels.referencia = (TextView) view.findViewById(R.id.referencia);
			labels.descricao = (TextView) view.findViewById(R.id.descricao);
			labels.padrao = (TextView) view.findViewById(R.id.padrao);
			labels.tamanho = (TextView) view.findViewById(R.id.tamanho);
			labels.vlUnit = (TextView) view.findViewById(R.id.valorUnitario);
			labels.vlTotal = (TextView) view.findViewById(R.id.valoTotal);
			view.setTag(labels);
		} else {
			labels = (Labels) view.getTag();
		}

		labels.quantidade.setText(String.valueOf(item.getQuantidade()));
		labels.referencia.setText(" " + item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto().getMascaraCodigo());
		labels.descricao.setText(" " + item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto().getDescricao());
		labels.padrao.setText(" " + item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getPadrao().getDescricao());
		labels.tamanho.setText(" " + item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getTamanho().getCodigo());
		labels.vlUnit.setText(" " + item.getPreco().formatToScreen());
		labels.vlTotal.setText(" " + Dinheiro.newInstance(new BigDecimal(item.getValorTotal())).formatToScreen());
		return view;
	}

	static class Labels {
		TextView quantidade;
		TextView referencia;
		TextView descricao;
		TextView padrao;
		TextView tamanho;
		TextView vlUnit;
		TextView vlTotal;
	}

}
