package max.adm.pedido.showroom.popup;

import max.adm.R;
import max.adm.entidade.PedidoItens;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class EditarItemPedidoShowroom extends Activity {

	PedidoItens item;

	TextView codigo;
	TextView descricao;
	TextView padrao;
	TextView tamanho;
	EditText quantidade;
	Button btOk;
	Button btExcluir;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.editar_item_showroom);
		item = (PedidoItens) getIntent().getExtras().getSerializable("ITEM");
		initComponents();
		handleComponents();
		atualizaTela();
		super.onCreate(savedInstanceState);
	}

	private void initComponents() {
		codigo = (TextView) findViewById(R.id.codigo);
		descricao = (TextView) findViewById(R.id.descricao);
		padrao = (TextView) findViewById(R.id.padrao);
		tamanho = (TextView) findViewById(R.id.tamanho);
		quantidade = (EditText) findViewById(R.id.quantidade);
		btOk = (Button) findViewById(R.id.btOk);
		btExcluir = (Button) findViewById(R.id.btExcluir);
	}

	private void handleComponents() {
		btExcluir.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (item != null) {
					item.setQuantidade(0);
				}
				confirma();
			}
		});

		btOk.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (item != null) {
					int quant = 0;
					String qtde = quantidade.getText().toString();
					if (!"".equals(qtde)) {
						quant = Integer.parseInt(qtde);
					}
					item.setQuantidade(quant);
				}
				confirma();
			}
		});
	}

	private void atualizaTela() {
		if (item != null) {
			codigo.setText(item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto().getMascaraCodigo());
			descricao.setText(item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto().getDescricao());
			padrao.setText(item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getPadrao().getDescricao());
			tamanho.setText(item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getTamanho().getCodigo());
			quantidade.setText(String.valueOf(item.getQuantidade()));
		}
	}

	private void confirma() {
		Bundle b = new Bundle();
		b.putSerializable("ITEM", item);

		Intent i = new Intent();
		i.putExtras(b);
		setResult(RESULT_OK, i);
		finish();
	}

}
