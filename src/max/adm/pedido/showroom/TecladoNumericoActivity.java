package max.adm.pedido.showroom;

import max.adm.R;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TecladoNumericoActivity extends ListActivity {

	protected Button bt1;
	protected Button bt2;
	protected Button bt3;
	protected Button bt4;
	protected Button bt5;
	protected Button bt6;
	protected Button bt7;
	protected Button bt8;
	protected Button bt9;
	protected Button bt0;
	protected Button btLimpar;
	protected Button btApagar;
	protected Button btOk;
	protected boolean apaga1 = true;

	protected TextView labelQuantidade;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initComponentes();
		handleComponents();
	}

	private void initComponentes() {
		bt1 = (Button) findViewById(R.id.bt1);
		bt2 = (Button) findViewById(R.id.bt2);
		bt3 = (Button) findViewById(R.id.bt3);
		bt4 = (Button) findViewById(R.id.bt4);
		bt5 = (Button) findViewById(R.id.bt5);
		bt6 = (Button) findViewById(R.id.bt6);
		bt7 = (Button) findViewById(R.id.bt7);
		bt8 = (Button) findViewById(R.id.bt8);
		bt9 = (Button) findViewById(R.id.bt9);
		bt0 = (Button) findViewById(R.id.bt0);
		btLimpar = (Button) findViewById(R.id.btLimpar);
		btApagar = (Button) findViewById(R.id.btApagar);
		labelQuantidade = (TextView) findViewById(R.id.labelQuantidadePecas);
		btOk = (Button) findViewById(R.id.btOk);
	}

	private void handleComponents() {
		bt1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				atualizaLabel("1");
			}
		});

		bt2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				atualizaLabel("2");

			}
		});

		bt3.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				atualizaLabel("3");
			}
		});

		bt4.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				atualizaLabel("4");
			}
		});

		bt5.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				atualizaLabel("5");
			}
		});

		bt6.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				atualizaLabel("6");
			}
		});

		bt7.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				atualizaLabel("7");
			}
		});

		bt8.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				atualizaLabel("8");
			}
		});

		bt9.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				atualizaLabel("9");
			}
		});

		bt0.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				atualizaLabel("0");
			}
		});

		btLimpar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				limpar();
			}
		});

		btApagar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String str = labelQuantidade.getText().toString();
				if (str.length() == 1) {
					labelQuantidade.setText("1");
					apaga1 = true;
				} else {
					String str2 = str.substring(0, str.length() - 1);
					labelQuantidade.setText(str2);
				}

			}
		});

	}

	protected void limpar() {
		labelQuantidade.setText("1");
		apaga1 = true;
	}

	protected void atualizaLabel(String numero) {
		String labelAtual = labelQuantidade.getText().toString();
		String labelAtualizado = "";
		if (labelAtual.length() <= 11) {
			if (labelAtual.equals("1") && apaga1) {
				labelAtualizado = numero;
				apaga1 = false;
			} else if (labelAtual.equals("1") && !apaga1) {
				labelAtualizado = labelAtual + numero;
			} else if (!labelAtual.equals("1")) {
				labelAtualizado = labelAtual + numero;
			}
			labelQuantidade.setText(labelAtualizado);
		}
	}

}
