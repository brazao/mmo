package max.adm.pedido;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import max.adm.R;
import max.adm.entidade.ImagemProduto;
import max.adm.entidade.PedidoItens;
import max.adm.entidade.Produto;
import max.adm.entidade.TabelaPrecoProdutoReduzido;
import max.adm.pedido.gridview.FieldGeneratingGrids;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.common.collect.Lists;

public class PedidoResumoItensAdapter extends BaseAdapter {
	LayoutInflater inflater;
	Context ctx;
	List<List<TabelaPrecoProdutoReduzido>> itens = Lists.newLinkedList();
	List<PedidoItens> itensDoPedido = Lists.newLinkedList();
	TextView quantidade;
	TextView subTotal;
	FieldGeneratingGrids generator;

	public PedidoResumoItensAdapter(Map<Produto, List<TabelaPrecoProdutoReduzido>> itens, Context ctx, List<PedidoItens> itensDoPedido, TextView quantidade, TextView subTotal) {
		for (Entry<Produto, List<TabelaPrecoProdutoReduzido>> entry : itens.entrySet()) {
			this.itens.add(entry.getValue());
		}
		this.ctx = ctx;
		this.itensDoPedido = itensDoPedido;
		inflater = LayoutInflater.from(ctx);
		this.quantidade = quantidade;
		this.subTotal = subTotal;
		generator = FieldGeneratingGrids.newInstance(ctx, quantidade, subTotal);
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		final List<TabelaPrecoProdutoReduzido> item = itens.get(position);

		Holder holder;
		if (view == null) {
			holder = new Holder();
			inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.pedido_resumo_itens_adapter, null);
			holder.mascara = (TextView) view.findViewById(R.id.mascara);
			holder.descricao = (TextView) view.findViewById(R.id.descricao);
			holder.layoutFields = (LinearLayout) view.findViewById(R.id.layoutFields);
			holder.btRemove = (ImageView) view.findViewById(R.id.btremove);
			holder.img = (ImageView) view.findViewById(R.id.img);
			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();
		}
		if (item.size() > 0) {
			Produto produto = item.get(0).getProdutoReduzido().getProduto();
			holder.mascara.setText(produto.getMascaraCodigo());
			holder.descricao.setText(produto.getDescricao());
			holder.layoutFields.removeAllViews();
			generator.generateView(item, holder.layoutFields, itensDoPedido);
			holder.img.setImageDrawable(ImagemProduto.getMiniDrawable(produto.getCodigo()));
			
			holder.btRemove.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					removeItens(item);
				}
			});
		}

		return view;
	}

	static class Holder {
		TextView mascara;
		TextView descricao;
		LinearLayout layoutFields;
		ImageView btRemove;
		ImageView img;
	}

	@Override
	public int getCount() {
		return itens.size();
	}

	@Override
	public Object getItem(int position) {
		return itens.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
	
	private void removeItens(List<TabelaPrecoProdutoReduzido> tabelaPrecoReduzidos){
		for(PedidoItens item : itensDoPedido) {
			for (TabelaPrecoProdutoReduzido tabelaPrecoReduzido : tabelaPrecoReduzidos) {
				if(item.equals(tabelaPrecoReduzido)) {
					item.setQuantidade(0);
					notifyDataSetChanged();
				}
			}
		}
	}

}
