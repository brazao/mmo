package max.adm.pedido;

import java.io.Serializable;
import java.util.List;

import max.adm.utilidades.adapters.ProdutoItemPedidoParaListar;

import com.google.common.collect.Lists;

public class ListaDeReduzidos implements Serializable {

	private static final long serialVersionUID = 1L;

	private static ListaDeReduzidos listaDeReduzidos;

	private List<ProdutoItemPedidoParaListar> reduzidos = Lists.newLinkedList();

	private ListaDeReduzidos() {
	}

	public static ListaDeReduzidos newInstance(List<ProdutoItemPedidoParaListar> reduzidos) {
		if (listaDeReduzidos == null) {
			listaDeReduzidos = new ListaDeReduzidos();
		}
		listaDeReduzidos.reduzidos = reduzidos;
		return listaDeReduzidos;
	}

	public static ListaDeReduzidos getInstance() {
		return listaDeReduzidos;
	}

	public List<ProdutoItemPedidoParaListar> getReduzidos() {
		return reduzidos;
	}

}
