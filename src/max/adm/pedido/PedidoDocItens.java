package max.adm.pedido;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.math.BigDecimal;

public class PedidoDocItens implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long PEDIDOITEM_ID;
	private Long PEDIDOCAPA_CPE_ID;
	private Long PEDITEM_QUANTIDADE;
	private BigDecimal PEDITEM_PRECO;
	private Long PEDITEM_NRCONJUNTO;
	private Long PRO_REDUZIDO_PRODUTOS_REDUZIDO_ID;
	private Long RED_PRODUTO_KEY;
	private String DESCRICAO;
	private String TAMANHODESCRICAO;
	private Long PADRAOCODIGO;
	private String PADRAODESCRICAO;

	public PedidoDocItens(Builder b) {
		this.PEDIDOITEM_ID = b.PEDIDOITEM_ID;
		this.PEDIDOCAPA_CPE_ID = b.PEDIDOCAPA_CPE_ID;
		this.PEDITEM_QUANTIDADE = b.PEDITEM_QUANTIDADE;
		this.PEDITEM_PRECO = b.PEDITEM_PRECO;
		this.PEDITEM_NRCONJUNTO = b.PEDITEM_NRCONJUNTO;
		this.PRO_REDUZIDO_PRODUTOS_REDUZIDO_ID = b.PRO_REDUZIDO_PRODUTOS_REDUZIDO_ID;
		this.RED_PRODUTO_KEY = b.RED_PRODUTO_KEY;
		this.DESCRICAO = b.DESCRICAO;
		this.TAMANHODESCRICAO = b.TAMANHODESCRICAO;
		this.PADRAOCODIGO = b.PADRAOCODIGO;
		this.PADRAODESCRICAO = b.PADRAODESCRICAO;
	}

	public static Builder builder() {
		return new Builder();
	}

	public BigDecimal getValor() {
		return PEDITEM_PRECO.multiply(new BigDecimal(PEDITEM_QUANTIDADE));
	}

	public Long getPEDIDOITEM_ID() {
		return PEDIDOITEM_ID;
	}

	public Long getPEDIDOCAPA_CPE_ID() {
		return PEDIDOCAPA_CPE_ID;
	}

	public Long getPEDITEM_QUANTIDADE() {
		return PEDITEM_QUANTIDADE;
	}

	public BigDecimal getPEDITEM_PRECO() {
		return PEDITEM_PRECO;
	}

	public Long getPEDITEM_NRCONJUNTO() {
		return PEDITEM_NRCONJUNTO;
	}

	public Long getPRO_REDUZIDO_PRODUTOS_REDUZIDO_ID() {
		return PRO_REDUZIDO_PRODUTOS_REDUZIDO_ID;
	}

	public Long getRED_PRODUTO_KEY() {
		return RED_PRODUTO_KEY;
	}

	public String getDESCRICAO() {
		return DESCRICAO;
	}

	public String getTAMANHODESCRICAO() {
		return TAMANHODESCRICAO;
	}

	public Long getPADRAOCODIGO() {
		return PADRAOCODIGO;
	}

	public String getPADRAODESCRICAO() {
		return PADRAODESCRICAO;
	}

	public static class Builder {
		private Long PEDIDOITEM_ID;
		private Long PEDIDOCAPA_CPE_ID;
		private Long PEDITEM_QUANTIDADE;
		private BigDecimal PEDITEM_PRECO;
		private Long PEDITEM_NRCONJUNTO;
		private Long PRO_REDUZIDO_PRODUTOS_REDUZIDO_ID;
		private Long RED_PRODUTO_KEY;
		private String DESCRICAO;
		private String TAMANHODESCRICAO;
		private Long PADRAOCODIGO;
		private String PADRAODESCRICAO;

		public Builder setPEDIDOITEM_ID(Long pEDIDOITEM_ID) {
			checkNotNull(pEDIDOITEM_ID);
			PEDIDOITEM_ID = pEDIDOITEM_ID;
			return this;
		}
		public Builder setPEDIDOCAPA_CPE_ID(Long pEDIDOCAPA_CPE_ID) {
			checkNotNull(pEDIDOCAPA_CPE_ID);
			PEDIDOCAPA_CPE_ID = pEDIDOCAPA_CPE_ID;
			return this;
		}
		public Builder setPEDITEM_QUANTIDADE(Long pEDITEM_QUANTIDADE) {
			checkNotNull(pEDITEM_QUANTIDADE);
			PEDITEM_QUANTIDADE = pEDITEM_QUANTIDADE;
			return this;
		}
		public Builder setPEDITEM_PRECO(BigDecimal pEDITEM_PRECO) {
			checkNotNull(pEDITEM_PRECO);
			PEDITEM_PRECO = pEDITEM_PRECO;
			return this;
		}
		public Builder setPEDITEM_NRCONJUNTO(Long pEDITEM_NRCONJUNTO) {
			PEDITEM_NRCONJUNTO = pEDITEM_NRCONJUNTO;
			return this;
		}
		public Builder setPRO_REDUZIDO_PRODUTOS_REDUZIDO_ID(Long pRO_REDUZIDO_PRODUTOS_REDUZIDO_ID) {
			PRO_REDUZIDO_PRODUTOS_REDUZIDO_ID = pRO_REDUZIDO_PRODUTOS_REDUZIDO_ID;
			return this;
		}

		public Builder setRED_PRODUTO_KEY(Long rED_PRODUTO_KEY) {
			checkNotNull(rED_PRODUTO_KEY);
			RED_PRODUTO_KEY = rED_PRODUTO_KEY;
			return this;
		}

		public Builder setDESCRICAO(String dESCRICAO) {
			checkNotNull(dESCRICAO);
			DESCRICAO = dESCRICAO;
			return this;
		}

		public Builder setTAMANHODESCRICAO(String tAMANHODESCRICAO) {
			checkNotNull(tAMANHODESCRICAO);
			TAMANHODESCRICAO = tAMANHODESCRICAO;
			return this;
		}
		public Builder setPADRAOCODIGO(Long pADRAOCODIGO) {
			checkNotNull(pADRAOCODIGO);
			PADRAOCODIGO = pADRAOCODIGO;
			return this;
		}
		public Builder setPADRAODESCRICAO(String pADRAODESCRICAO) {
			checkNotNull(pADRAODESCRICAO);
			PADRAODESCRICAO = pADRAODESCRICAO;
			return this;
		}

		public PedidoDocItens build() {
			return new PedidoDocItens(this);
		}

	}

}
