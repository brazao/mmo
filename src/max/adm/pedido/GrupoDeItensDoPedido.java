package max.adm.pedido;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import max.adm.Dinheiro;
import max.adm.entidade.PadraoProduto;
import max.adm.entidade.PedidoItens;
import max.adm.entidade.Produto;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;

public class GrupoDeItensDoPedido {
	
	private List<PedidoItens> itens = Lists.newLinkedList();
	private Produto produto;
	private PadraoProduto padrao;
	
	private GrupoDeItensDoPedido(Produto produto, PadraoProduto padrao) {
		this.produto = produto;
		this.padrao = padrao;
	}
	
	public static GrupoDeItensDoPedido newInstance(Produto produto, PadraoProduto padrao){
		checkNotNull(produto);
		checkNotNull(padrao);
		return new GrupoDeItensDoPedido(produto, padrao);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof GrupoDeItensDoPedido){
			GrupoDeItensDoPedido other = (GrupoDeItensDoPedido) o;
			return Objects.equal(this.produto, other.produto)&&
					Objects.equal(this.padrao, other.padrao);
		}
		return false;
	}
	
	public List<PedidoItens> getItens() {
		return itens;
	}
	public Produto getProduto() {
		return produto;
	}
	public PadraoProduto getPadrao() {
		return padrao;
	}
	
	public int getQuantidade(){
		int quantidade = 0;
		for(PedidoItens i: itens){
			quantidade += i.getQuantidade();
		}
		return quantidade;
	}
	
	public Dinheiro getValor(){
		double total = 0;
		for (PedidoItens it: itens){
			total += it.getValorTotal();
		}
		return Dinheiro.newInstance(new BigDecimal(total));
	}
	
	public GrupoDeItensDoPedido addItem(PedidoItens item){
		checkArgument(item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto().equals(this.produto));
		checkArgument(item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getPadrao().equals(this.padrao));
		if(item != null){
			if(!itens.contains(item) && item.getQuantidade() > 0){
				itens.add(item);
			}
			else{
				System.out.println("procurar para remover");
				Iterator<PedidoItens> itr = itens.iterator();
				PedidoItens it;
				while(itr.hasNext()){
					it = (PedidoItens) itr.next();
					if(item.equals(it)){
						itr.remove();
						System.out.println("encontrou e removeu");
						break;
					}
				}
				if(item.getQuantidade() > 0){
					itens.add(item);
				}
			}
		}
		return this;
	}

}
