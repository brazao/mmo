package max.adm.pedido;

import java.io.Serializable;
import java.util.List;

import max.adm.R;
import max.adm.ValorAtividade;
import max.adm.controle.PedidoInsereItensActivity;
import max.adm.entidade.Empresa;
import max.adm.entidade.FaixaCodigoPedido;
import max.adm.entidade.Parametro;
import max.adm.entidade.PedidoCapa;
import max.adm.entidade.TabelaDePreco;
import max.adm.tabelaDePreco.TabelaPrecoMultActivity;
import max.adm.tabelaDePreco.TabelaPrecoSingleActivity;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.Lists;

public class PrimeiroPassoPedidoActivity extends Activity {

	private Empresa empresa;
	private Parametro parametro;
	private FaixaCodigoPedido faixaCodigoPedido;

	private TextView textEmpresa;
	private TextView textNumPedido;
	private TextView textTabelaPreco;
	private EditText fieldNumBloco;
	private EditText fieldColecao;

	private Button btCancelar;
	private Button btAddItens;
	private List<PedidoCapa.Builder> builders = Lists.newLinkedList();

	private List<TabelaDePreco> tabelasSelecionadas = Lists.newLinkedList();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pedido_cadastro_primeiro_passo);
		montaTela();
		parametro = (Parametro) getIntent().getExtras().getSerializable("PARAMETRO");
		empresa = (Empresa) getIntent().getExtras().getSerializable("EMPRESA");
		textEmpresa.setText("Loja " + String.valueOf(empresa.getNumeroLoja()));
		try {
			faixaCodigoPedido = FaixaCodigoPedido.newInstance(this, empresa.getNumeroLoja());
			textNumPedido.setText(String.valueOf(faixaCodigoPedido.getPedidoId()));
		} catch (Exception e) {
			avisaCodigoIndisponivel();
		}

		defineFuncoes();
	}

	private void montaTela() {
		textEmpresa = (TextView) findViewById(R.id.text_loja_pedido_primeiro_passo);
		textNumPedido = (TextView) findViewById(R.id.text_numero_pedido_primeiro_passo);
		textTabelaPreco = (TextView) findViewById(R.id.text_tabela_preco_pedido_novo);
		fieldNumBloco = (EditText) findViewById(R.id.field_numero_bloco_pedido);
		fieldColecao = (EditText) findViewById(R.id.field_colecao_pedido);
		btCancelar = (Button) findViewById(R.id.bt_cancela_pedido_primeiro_passo);
		btAddItens = (Button) findViewById(R.id.bt_add_itens_pedido);
	}

	private void defineFuncoes() {
		textTabelaPreco.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent();
				Bundle b = new Bundle();
				b.putSerializable("EMPRESA", empresa);
				b.putSerializable("TABELAS", (Serializable) tabelasSelecionadas);
				b.putSerializable("PARAMETRO", parametro);
				i.putExtras(b);
				if (parametro.isMultiTabelaPreco()) {
					i.setClass(PrimeiroPassoPedidoActivity.this, TabelaPrecoMultActivity.class);
					startActivityForResult(i, ValorAtividade.TabelaPrecoMultActivity.get());
				} else {
					i.setClass(PrimeiroPassoPedidoActivity.this, TabelaPrecoSingleActivity.class);
					startActivityForResult(i, ValorAtividade.TabelaPrecoSingleActivity.get());
				}
			}
		});

		btAddItens.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (validaCampos()) {
					confirma();
				}
			}
		});

		btCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	private void avisaCodigoIndisponivel() {
		AlertDialog.Builder alerta = new AlertDialog.Builder(this);
		alerta.setTitle("Codigo Indisponivel");
		alerta.setMessage("Não há código de pedido disponivel. Sincronize os dados para importar novos codigos.");
		alerta.setPositiveButton(R.string.bt_ok, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
		alerta.show();
	}

	@Override
	public void onBackPressed() {
		AlertDialog.Builder alerta = new AlertDialog.Builder(this);
		alerta.setTitle("Cancelar operação");
		alerta.setMessage("Tem certeza que deseja sair da inserção de pedido?");
		alerta.setPositiveButton(R.string.bt_sim, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
		alerta.setNegativeButton(R.string.bt_nao, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		alerta.show();
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == ValorAtividade.TabelaPrecoMultActivity.get() || requestCode == ValorAtividade.TabelaPrecoSingleActivity.get()) {
			if (data != null) {
				tabelasSelecionadas = (List<TabelaDePreco>) data.getExtras().getSerializable("TABELAS");
				textTabelaPreco.setText(onChangeTabelaPreco());
			}
		}
		if (requestCode == ValorAtividade.PedidoInsereItes.get()) {
			finish();
		}
	}

	private boolean validaCampos() {
		if (tabelasSelecionadas.isEmpty()) {
			Toast.makeText(this, "Escolha a tabela de preços.", Toast.LENGTH_LONG).show();
			return false;
		}
		if (fieldColecao.getText().toString().equals("")) {
			fieldColecao.setText("0");
		}
		if (fieldNumBloco.getText().toString().equals("")) {
			fieldNumBloco.setText("0");
		}
		return true;
	}

	private String onChangeTabelaPreco() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < tabelasSelecionadas.size(); i++) {
			TabelaDePreco t = tabelasSelecionadas.get(i);
			if (i == 0)
				sb.append(t.getCodigo());
			else
				sb.append(", " + t.getCodigo());
		}
		return sb.toString();
	}

	private void confirma() {
		for (TabelaDePreco t : tabelasSelecionadas) {
			PedidoCapa.Builder builderPedido = new PedidoCapa.Builder();
			builderPedido.comCodigoColecao(Integer.parseInt(fieldColecao.getText().toString()));
			builderPedido.comNumeroPedido(faixaCodigoPedido.getPedidoId());
			builderPedido.comNumeroBloco(Integer.parseInt(fieldNumBloco.getText().toString()));
			builderPedido.comTabelaDePreco(t);
			builderPedido.comEmpresa(empresa);
			builders.add(builderPedido);
		}
		Intent i = new Intent(this, PedidoInsereItensActivity.class);
		Bundle b = new Bundle();
		b.putSerializable("EMPRESA", empresa);
		b.putSerializable("BUILDERS", (Serializable) builders);
		b.putSerializable("TABELAS", (Serializable) tabelasSelecionadas);
		b.putSerializable("PARAMETRO", parametro);
		i.putExtras(b);
		startActivityForResult(i, ValorAtividade.PedidoInsereItes.get());
	}

}
