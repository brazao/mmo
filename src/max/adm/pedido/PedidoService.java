package max.adm.pedido;

import static com.google.common.base.Preconditions.checkNotNull;
import max.adm.Servico;
import max.adm.service.ConexaoHttp;
import max.adm.service.SolicitacaoDeGravacaoDePedido;
import max.adm.utilidades.Comprimir;
import max.adm.utilidades.Notificacao;
import max.adm.utilidades.NotificacaoFactory;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class PedidoService {

	private Context ctx;
	private String url;
	private SolicitacaoDeGravacaoDePedido solicitacao;

	private Notificacao notificacao;

	private PedidoService(Context ctx, String url, SolicitacaoDeGravacaoDePedido solicitacao) {
		this.ctx = ctx;
		this.url = url;
		this.solicitacao = solicitacao;

	}

	public static PedidoService newInstance(Context ctx, String url, SolicitacaoDeGravacaoDePedido solicitacao) {
		checkNotNull(ctx);
		checkNotNull(url);
		checkNotNull(solicitacao);
		return new PedidoService(ctx, url, solicitacao);
	}

	public boolean execute() {
		if (ConexaoHttp.isConnected(ctx)) {
			Toast.makeText(ctx, "Conecte a internet.", Toast.LENGTH_LONG).show();
			return false;
		}
		notifica();
		try {
			ConexaoHttp conexao = ConexaoHttp.newInstance(url + Servico.enviaPedidos.get());
			String resposta = conexao.executaRequisicaoObj(Comprimir.transformToByteArray(solicitacao), String.class);
			if (resposta.equals("ok")) {
				notificaSucesso();
				return true;
			}
			notificaFalha();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private void notifica() {
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoInfo(this.ctx, "Pedidos", "Enviando Pedidos", new Intent());
	}

	private void notificaFalha() {
		notificacao.cancelaNotificacao();
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoFalha(this.ctx, "Pedidos", "Falha ao enviar", new Intent());
	}

	private void notificaSucesso() {
		notificacao.cancelaNotificacao();
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoSucesso(this.ctx, "Pedidos", "Envio concluído", new Intent());
	}

}
