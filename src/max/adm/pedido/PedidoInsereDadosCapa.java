package max.adm.pedido;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import max.adm.Dinheiro;
import max.adm.R;
import max.adm.ValorAtividade;
import max.adm.controle.DateActivity;
import max.adm.controle.PessoaListControle;
import max.adm.entidade.CondicaoPagamento;
import max.adm.entidade.Empresa;
import max.adm.entidade.Parametro;
import max.adm.entidade.PedidoCapa;
import max.adm.entidade.PedidoItens;
import max.adm.entidade.Pessoa;
import max.adm.entidade.TipoCobranca;
import max.adm.pedido.showroom.AssinaturaActivity;
import max.adm.repositorio.RepositorioCondicaoPagamento;
import max.adm.repositorio.RepositorioPedidoCapa;
import max.adm.tipoCobranca.RepositorioTipoCobranca;
import max.adm.utilidades.MascaraDecimal;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.Lists;

public class PedidoInsereDadosCapa extends Activity {

	SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

	private List<PedidoCapa.Builder> builders = Lists.newLinkedList();
	private List<TipoCobranca> tiposDeCobranca = Lists.newLinkedList();
	private List<CondicaoPagamento> condicoesPagamento = Lists.newLinkedList();
	private Empresa empresa;
	private Parametro parametro;

	private TextView textLoja;
	private TextView textNumPedido;
	private TextView textValorBruto;
	private TextView textValorLiquido;
	private TextView textPercentualDescanco;
	private TextView textCliente;
	private TextView textDataEmissao;
	private TextView textDataPrevisao;

	private EditText fieldDesconto;
	private EditText fieldObs;

	private Spinner spinnerTpCobranca1;
	private Spinner spinnerTpCobranca2;
	private Spinner spinnerCondicaoPgto;

	private ImageView btPesquisaPessoa;

	private RadioGroup radioGroupDesconto;

	private Button btConfirmar;
	private Button btCancelar;
	private Button btItens;

	private static int CODE_DATA_EMISSAO = 1;
	private static int CODE_DATA_PREVISAO = 2;
	private int CODE_RESULT;

	private Pessoa pessoa;
	private Dinheiro valor;
	private double percentualDesconto = 0;
	private Date dataEmissao = new Date();
	private Date dataPrevisao = new Date();
	private TipoCobranca tipoDeCobranca1;
	private TipoCobranca tipoDeCobranca2;
	private CondicaoPagamento condicaoPagamento;
	private double valorMaximoDesconto;

	private boolean usarTipoCobranca2 = false;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pedido_insere_dados_capa);
		empresa = (Empresa) getIntent().getExtras().getSerializable("EMPRESA");
		builders = (List<PedidoCapa.Builder>) getIntent().getExtras().getSerializable("BUILDERS");
		parametro = (Parametro) getIntent().getExtras().getSerializable("PARAMETRO");
		dataPrevisao.setDate(dataEmissao.getDate() + 30);
		montaTela();
		defineFuncoes();
		preencheTela();
		valorMaximoDesconto = valorTotalDoPedido().getValor().doubleValue() * (parametro.getMaximoDesconto() / 100);
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(fieldDesconto.getWindowToken(), 0);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data != null && requestCode == ValorAtividade.DateActivity.get()) {
			CODE_RESULT = data.getExtras().getInt(DateActivity.CODE_RESULT_KEY, 0);
			Date date = (Date) data.getExtras().getSerializable(DateActivity.DATA_KEY);
			preencheCampoData(date);
		}
		if (data != null && requestCode == ValorAtividade.PessoaList.get()) {
			pessoa = (Pessoa) data.getExtras().getSerializable("PESSOA");
			textCliente.setText(pessoa.getCodigo() + " " + pessoa.getRazaoSocial());
		}
		if (data != null && requestCode == ValorAtividade.AssinaturaActivity.get()) {
			String sair = data.getStringExtra("SAIR");
			if ("SIM".equals(sair)) {
				Intent i = new Intent();
				i.putExtra("SAIR", sair);
				setResult(RESULT_OK, i);
				finish();
			}
		}
		if(data != null && requestCode == ValorAtividade.PedidoResumoItens.get()) {
			builders = (List<PedidoCapa.Builder>) data.getExtras().getSerializable(PedidoResumoItens.KEY_BUILDERS);
			preencheTela();
		}
	}

	private void montaTela() {
		textLoja = (TextView) findViewById(R.id.text_loja);
		textNumPedido = (TextView) findViewById(R.id.text_numero_pedido);
		textValorBruto = (TextView) findViewById(R.id.text_valor_bruto_pedido);
		textValorLiquido = (TextView) findViewById(R.id.text_valor_liquido_pedido);
		textPercentualDescanco = (TextView) findViewById(R.id.text_percentual_desconto_pedido);
		textCliente = (TextView) findViewById(R.id.text_cliente);
		textDataEmissao = (TextView) findViewById(R.id.text_data_emissao);
		textDataPrevisao = (TextView) findViewById(R.id.text_data_previsao);
		fieldDesconto = (EditText) findViewById(R.id.field_desconto);
		fieldObs = (EditText) findViewById(R.id.field_obs);
		spinnerTpCobranca1 = (Spinner) findViewById(R.id.spinner_tipo_cabranca1);
		spinnerTpCobranca2 = (Spinner) findViewById(R.id.spinner_tipo_cobranca2);
		spinnerCondicaoPgto = (Spinner) findViewById(R.id.spinner_condicao_pagamento);
		btPesquisaPessoa = (ImageView) findViewById(R.id.bt_pesquisa_pessoa);
		radioGroupDesconto = (RadioGroup) findViewById(R.id.radioGroupDesconto);
		btConfirmar = (Button) findViewById(R.id.bt_confirmar);
		btCancelar = (Button) findViewById(R.id.bt_cancelar);
		btItens = (Button) findViewById(R.id.bt_itens);
	}

	private void preencheTela() {
		textLoja.setText("Loja " + empresa.getNumeroLoja());
		textNumPedido.setText(String.valueOf(builders.get(0).getNumeroPedido()));
		textValorBruto.setText(valorTotalDoPedido().formatToScreen());
		textValorLiquido.setText(valorTotalDoPedido().formatToScreen());
		textPercentualDescanco.setText("0");
		textDataEmissao.setText(format.format(dataEmissao));
		textDataPrevisao.setText(format.format(dataPrevisao));
		buscaTiposCobrancas();
		buscaCondicoesDePagamentos();
		verificaTabelaComDesconto();
	}
	
	private void defineFuncoes() {
		textDataEmissao.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialogData(CODE_DATA_EMISSAO);
			}
		});

		textDataPrevisao.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialogData(CODE_DATA_PREVISAO);
			}
		});

		textCliente.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showViewCliente();
			}
		});
		btPesquisaPessoa.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showViewCliente();
			}
		});
		radioGroupDesconto.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				alteraTipoDesconto(checkedId);

			}
		});
		fieldDesconto.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					alteraTipoDesconto(radioGroupDesconto.getCheckedRadioButtonId());
				}
			}
		});

		btCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		btConfirmar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				confirmar();
			}
		});

		spinnerTpCobranca1.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> lista, View arg1, int position, long id) {
				tipoDeCobranca1 = (TipoCobranca) lista.getItemAtPosition(position);
				Log.d("teste", "t cob 1");

			}
			@Override
			public void onNothingSelected(AdapterView<?> lista) {

			}
		});

		spinnerTpCobranca2.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> lista, View arg1, int position, long id) {
				tipoDeCobranca2 = (TipoCobranca) lista.getItemAtPosition(position);
				Log.d("teste", "t cob 2");
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		spinnerCondicaoPgto.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> lista, View arg1, int position, long id) {
				condicaoPagamento = (CondicaoPagamento) lista.getItemAtPosition(position);
				Log.d("teste", "cond pag");
				System.out.println("cond pagto spinner " + condicaoPagamento);
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		
		btItens.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				List<PedidoItens> itens = getItens();
				Intent i = new Intent(PedidoInsereDadosCapa.this, PedidoResumoItens.class);
				Bundle b = new Bundle();
				b.putSerializable(PedidoResumoItens.KEY_ITENSPEDIDO, (Serializable) itens);
				i.putExtras(b);
				startActivityForResult(i, ValorAtividade.PedidoResumoItens.get());
			}
		});

	}
	
	private List<PedidoItens> getItens() {
		List<PedidoItens> itens = Lists.newLinkedList();
		for (PedidoCapa.Builder builder : builders) {
			itens.addAll(builder.getItens());
		}
		return itens;
		
	}

	private Dinheiro valorTotalDoPedido() {
		double total = 0;
		for (PedidoCapa.Builder builder : builders) {
			for (PedidoItens item : builder.getItens()) {
				total += item.getValorTotal();
			}
		}
		return Dinheiro.newInstance(new BigDecimal(total));
	}

	private void buscaTiposCobrancas() {
		RepositorioTipoCobranca repo = new RepositorioTipoCobranca(this);
		tiposDeCobranca = repo.listar();
		repo.fechar();
		ArrayAdapter<TipoCobranca> adapter = new ArrayAdapter<TipoCobranca>(this, android.R.layout.simple_spinner_item, tiposDeCobranca);
		spinnerTpCobranca1.setAdapter(adapter);
		spinnerTpCobranca2.setAdapter(adapter);
	}

	private void buscaCondicoesDePagamentos() {
		RepositorioCondicaoPagamento repo = new RepositorioCondicaoPagamento(this);
		condicoesPagamento = repo.listar();
		repo.fechar();
		ArrayAdapter<CondicaoPagamento> adapter = new ArrayAdapter<CondicaoPagamento>(this, android.R.layout.simple_spinner_item, condicoesPagamento);
		spinnerCondicaoPgto.setAdapter(adapter);
	}

	private void showDialogData(int codeResult) {
		Intent i = new Intent(this, DateActivity.class);
		i.putExtra("CODE_RESULT", codeResult);
		startActivityForResult(i, ValorAtividade.DateActivity.get());
	}

	private void preencheCampoData(Date date) {
		int dia = date.getDate();
		int mes = date.getMonth() + 1;
		int ano = date.getYear();

		System.err.println("new date " + new Date());
		System.err.println("data escolhida " + date);

		StringBuilder sb = new StringBuilder();
		sb.append(dia).append("/").append(mes).append("/").append(ano);
		Date hoje = new Date();
		Date dataAtual = new Date(hoje.getYear(), hoje.getMonth(), hoje.getDate());
		System.err.println("dataAtual " + dataAtual);
		if (CODE_RESULT == CODE_DATA_EMISSAO) {
			if (date.before(dataAtual)) {
				dataEmissao = dataAtual;
				Toast.makeText(this, "Data de emissão não pode ser menor que a data atual.", Toast.LENGTH_LONG).show();
			} else {
				dataEmissao = date;
				textDataEmissao.setText(sb.toString());
			}

		} else if (CODE_RESULT == CODE_DATA_PREVISAO) {
			if (!date.before(dataEmissao)) {
				textDataPrevisao.setText(sb.toString());
				dataPrevisao = date;
			} else {
				dataPrevisao = dataEmissao;
				Toast.makeText(this, "Data de previsão não pode ser menor que a data do pedido.", Toast.LENGTH_LONG).show();
			}

		}
	}

	private void showViewCliente() {
		Intent i = new Intent(this, PessoaListControle.class);
		Bundle b = new Bundle();
		b.putSerializable("EMPRESA", empresa);
		i.putExtras(b);
		startActivityForResult(i, ValorAtividade.PessoaList.get());
	}

	private void alteraTipoDesconto(int id) {
		switch (id) {
			case R.id.radio_percentual :
				alteraDescontoParaPercentual();
				break;

			case R.id.radio_valor :
				alteraDescontoParaValor();
				break;

			default :
				break;
		}

	}

	private void alteraDescontoParaPercentual() {
		String desc = fieldDesconto.getText().toString();
		if (!desc.isEmpty()) {
			percentualDesconto = Double.parseDouble(desc);
			double valorDesc = valorTotalDoPedido().getValor().doubleValue() * (percentualDesconto / 100);
			if (valorDesc <= valorMaximoDesconto) {
				double liquido = valorTotalDoPedido().getValor().doubleValue() - valorDesc;
				valor = Dinheiro.newInstance(new BigDecimal(liquido));
				textValorLiquido.setText(valor.formatToScreen());
				textPercentualDescanco.setText(MascaraDecimal.comDuasCasaDecimais(new BigDecimal(percentualDesconto)) + "%");
			} else {
				fieldDesconto.setText("0");
				Toast.makeText(this, "Desconto acima do permitido", Toast.LENGTH_LONG).show();
				alteraDescontoParaPercentual();
			}
		} else {
			fieldDesconto.setText("0");
			alteraDescontoParaPercentual();
		}
	}

	private void alteraDescontoParaValor() {
		String desc = fieldDesconto.getText().toString();
		if (!desc.isEmpty()) {
			double valorDesc = Double.parseDouble(desc);
			if (valorDesc <= valorMaximoDesconto) {
				double liquido = valorTotalDoPedido().getValor().doubleValue() - valorDesc;
				percentualDesconto = (valorDesc * 100) / valorTotalDoPedido().getValor().doubleValue();
				valor = Dinheiro.newInstance(new BigDecimal(liquido));
				textValorLiquido.setText(valor.formatToScreen());
				textPercentualDescanco.setText(MascaraDecimal.comDuasCasaDecimais(new BigDecimal(percentualDesconto)) + "%");
			} else {
				fieldDesconto.setText("0");
				Toast.makeText(this, "Desconto dever ser menor que 100%", Toast.LENGTH_LONG).show();
				alteraDescontoParaValor();
			}
		} else {
			fieldDesconto.setText("0");
			alteraDescontoParaValor();
		}
	}

	private void confirmar() {
		if (validaCampos()) {
			List<PedidoCapa> pedidos = Lists.newLinkedList();

			for (PedidoCapa.Builder builder : builders) {
				RepositorioPedidoCapa repo = new RepositorioPedidoCapa(this);

				builder.comEmpresa(empresa);
				builder.comNumeroPedido(repo.leCodigoDisponivel(empresa.getNumeroLoja()));
				repo.fechar();

				builder.comPercentualDesconto(percentualDesconto);
				builder.comPessoa(pessoa);
				builder.comDataEmissao(dataEmissao);
				builder.comDataPrevisao(dataPrevisao);
				builder.comTipoCobranca(tipoDeCobranca1);
				if (usarTipoCobranca2) {
					builder.comTipoCobranca2(tipoDeCobranca2);
				}
				System.out.println("cond pgto builder " + condicaoPagamento);
				builder.comCondicaoDePagamento(condicaoPagamento);
				builder.comObservacao(fieldObs.getText().toString());
				PedidoCapa pedido = builder.build();

				pedidos.add(pedido);
			}
			Intent i = new Intent(this, AssinaturaActivity.class);

			Bundle bundle = new Bundle();
			bundle.putSerializable("PEDIDOS", (Serializable) pedidos);

			i.putExtras(bundle);

			startActivityForResult(i, ValorAtividade.AssinaturaActivity.get());
		}
	}
	@Override
	public void onBackPressed() {
		AlertDialog.Builder alerta = new AlertDialog.Builder(this);
		alerta.setTitle("Atenção");
		alerta.setMessage("Deseja voltar aos itens?");
		alerta.setPositiveButton(R.string.bt_sim, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent i = new Intent();
				i.putExtra("SAIR", "NÃO");
				setResult(RESULT_OK, i);
				finish();
			}
		});
		alerta.setNeutralButton(R.string.bt_nao, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		alerta.setNegativeButton("Cancelar Pedido", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent i = new Intent();
				i.putExtra("SAIR", "SIM");
				setResult(RESULT_OK, i);
				finish();

			}
		});
		alerta.show();
	}

	private void verificaTabelaComDesconto() {
		for (PedidoCapa.Builder builder : builders) {
			if (builder.getTabelaDePreco().getPercentualDesconto() > 0) {
				usarTipoCobranca2 = true;
			}
		}
		if (usarTipoCobranca2) {
			spinnerTpCobranca2.setVisibility(View.VISIBLE);
		} else {
			spinnerTpCobranca2.setVisibility(View.INVISIBLE);
		}
	}

	private boolean validaCampos() {
		boolean ok = true;
		if (pessoa == null) {
			Toast.makeText(this, "Insira um cliente.", Toast.LENGTH_LONG).show();
			ok = false;
		} else if (dataPrevisao.before(dataEmissao)) {
			Toast.makeText(this, "A data de entrega deve ser maior que a de emissão", Toast.LENGTH_LONG).show();
			ok = false;
		}
		return ok;
	}
}
