package max.adm.pedido.show;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;

import max.adm.Dinheiro;
import max.adm.entidade.PedidoCapa;
import max.adm.entidade.PedidoItens;
import android.os.Environment;

import com.google.common.base.Strings;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PdfPedido {
	private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
	private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
	private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
	private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
	private SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

	private PedidoCapa pedido;

	private PdfPedido(PedidoCapa pedido) {
		this.pedido = pedido;
	}

	public static PdfPedido newInstance(PedidoCapa pedido) {
		checkNotNull(pedido, "não é possivel criar pdf com pedido nulo.");
		return new PdfPedido(pedido);
	}

	public boolean gerarPdf() {
		if (pedido != null) {
			try {

				String path = Environment.getExternalStorageDirectory().getPath();
				Document dc = new Document();
				PdfWriter.getInstance(dc, new FileOutputStream(path + "/lj" + pedido.getEmpresa().getNumeroLoja() + "ped" + pedido.getNumeroPedido() + ".pdf"));
				dc.open();

				addMetaData(dc);
				addTitle(dc);
				addCapaPedido(dc);
				addItens(dc);
				addAssinatura(dc);

				dc.close();
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}

		}
		return false;

	}

	private void addMetaData(Document document) {
		document.addTitle("pedido_" + this.pedido.getNumeroPedido());
		document.addSubject("pedido");
		document.addKeywords("representante, PDF, pedido");
		document.addAuthor("Produtec");
		document.addCreator("Produtec");
	}

	private void addTitle(Document document) throws DocumentException {
		Paragraph title = new Paragraph(new Paragraph(pedido.getEmpresa().getRazaoSocial(), catFont));
		title.setAlignment(Element.ALIGN_CENTER);
		document.add(title);

		Paragraph numPedido = new Paragraph("Cópia do pedido " + this.pedido.getNumeroPedido(), catFont);
		numPedido.setAlignment(Element.ALIGN_CENTER);
		document.add(numPedido);

	}

	private void addCapaPedido(Document document) throws DocumentException {
		Paragraph line = new Paragraph("Cliente: " + this.pedido.getPessoa().getRazaoSocial(), subFont);
		document.add(line);

		line = new Paragraph("Emissão: " + df.format(this.pedido.getDataEmissao()));
		document.add(line);

		line = new Paragraph("Previsão de entrega: " + df.format(this.pedido.getDataPrevisao()));
		document.add(line);

		line = new Paragraph("Tabela de preço: " + this.pedido.getTabelaDePreco().getCodigo() + " - " + this.pedido.getTabelaDePreco().getDescricao());
		document.add(line);

		line = new Paragraph("Forma de pagamento: " + this.pedido.getTipoCobranca().getDescricao());
		document.add(line);

		if (this.pedido.getTipoCobranca2() != null) {
			line = new Paragraph("Forma de pagamento 2: " + this.pedido.getTipoCobranca2().getDescricao());
			document.add(line);
		}

		line = new Paragraph("Valor bruto: " + Dinheiro.newInstance(new BigDecimal(this.pedido.getValor())).formatToScreen());
		document.add(line);

		line = new Paragraph("Percentual de desconto: " + this.pedido.getPercentualDesconto() + "%");
		document.add(line);

		line = new Paragraph("Valor do desconto: " + Dinheiro.newInstance(new BigDecimal(this.pedido.getValorDesconto())).formatToScreen());
		document.add(line);

		line = new Paragraph("Valor líquido: " + Dinheiro.newInstance(new BigDecimal(this.pedido.getValorLiquido())).formatToScreen());
		document.add(line);

		if (!Strings.isNullOrEmpty(this.pedido.getObservacao())) {
			line = new Paragraph("Observações: " + this.pedido.getObservacao());
			document.add(line);
		}

	}

	private void addItens(Document document) throws DocumentException {
		Paragraph linha = new Paragraph();
		addEmptyLine(linha, 2);
		document.add(linha);

		document.add(new Paragraph("Itens", subFont));
		linha = new Paragraph();
		addEmptyLine(linha, 1);
		document.add(linha);

		float[] widths = {10, 50, 20, 15, 15, 10, 15};
		PdfPTable table = new PdfPTable(widths);
		table.setWidthPercentage(95);

		PdfPCell c1 = new PdfPCell(new Phrase("Ref."));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Descricão"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Cor"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Tamanho"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Preço"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Qtde"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Tot. Item"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		table.setHeaderRows(1);

		for (PedidoItens item : this.pedido.getItens()) {
			table.addCell(new Phrase(item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto().getMascaraCodigo(), smallBold));
			table.addCell(new Phrase(item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto().getDescricao(), smallBold));
			table.addCell(new Phrase(item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getPadrao().getDescricao(), smallBold));
			table.addCell(new Phrase(item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getTamanho().getCodigo(), smallBold));
			table.addCell(new Phrase(item.getPreco().formatToScreen(), smallBold));
			table.addCell(new Phrase(String.valueOf(item.getQuantidade()), smallBold));
			table.addCell(new Phrase(Dinheiro.newInstance(new BigDecimal(item.getValorTotal())).formatToScreen(), smallBold));

		}

		document.add(table);

	}

	private static void addEmptyLine(Paragraph paragraph, int number) {

		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}

	private void addAssinatura(Document document) throws DocumentException, MalformedURLException, IOException {

		Image i = Image.getInstance(pedido.getAssinatura());
		i.scaleAbsolute(225f, 50f);
		i.setAlignment(Element.ALIGN_CENTER);
		document.add(i);
		Paragraph linha = new Paragraph("Assinatura do Cliente");
		linha.setAlignment(Element.ALIGN_CENTER);
		document.add(linha);

	}

}
