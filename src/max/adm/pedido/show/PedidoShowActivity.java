package max.adm.pedido.show;

import java.io.File;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;

import max.adm.Dinheiro;
import max.adm.R;
import max.adm.entidade.PedidoCapa;
import max.adm.entidade.PedidoItens;
import max.adm.repositorio.RepositorioPedidoCapa;
import max.adm.utilidades.MascaraDecimal;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.base.Strings;

public class PedidoShowActivity extends Activity {

	private ImageView assinatura;
	private TextView empresa;
	private TextView numeroBloco;
	private TextView numeroPedido;
	private TextView dataEmissao;
	private TextView dataPrevisao;
	private TextView percentualDesconto;
	private TextView observacao;
	private TextView sync;
	private TextView tipoCobranca;
	private TextView tipoCobranca2;
	private TextView pessoa;
	private TextView tabelaDePreco;
	private TextView condicaoDePagamento;
	private TextView valorBruto;
	private TextView valorLiquido;
	private Button btEnviaEmail;
	private Button btVoltar;
	private Button btExcluir;
	private LinearLayout layoutItens;

	private PedidoCapa pedido;

	private SimpleDateFormat fm = new SimpleDateFormat("dd/MM/yyyy");

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.options_pedido);
		pedido = (PedidoCapa) getIntent().getExtras().getSerializable("PEDIDO");
		initComponents();
		preencheTela();
		handleComponents();
	}

	private void initComponents() {
		assinatura = (ImageView) findViewById(R.id.assinatura);
		empresa = (TextView) findViewById(R.id.empresa);
		numeroBloco = (TextView) findViewById(R.id.numeroBloco);
		numeroPedido = (TextView) findViewById(R.id.numeroPedido);
		dataEmissao = (TextView) findViewById(R.id.dataEmissao);
		dataPrevisao = (TextView) findViewById(R.id.dataPrevisao);
		percentualDesconto = (TextView) findViewById(R.id.desconto);
		observacao = (TextView) findViewById(R.id.obs);
		sync = (TextView) findViewById(R.id.sync);
		tipoCobranca = (TextView) findViewById(R.id.tipoCobranca);
		tipoCobranca2 = (TextView) findViewById(R.id.tipoCobranca2);
		pessoa = (TextView) findViewById(R.id.cliente);
		tabelaDePreco = (TextView) findViewById(R.id.tabelaPreco);
		condicaoDePagamento = (TextView) findViewById(R.id.condicaoPagamento);
		valorBruto = (TextView) findViewById(R.id.valorBruto);
		valorLiquido = (TextView) findViewById(R.id.valorLiquido);
		btEnviaEmail = (Button) findViewById(R.id.btEnviarEmail);
		btVoltar = (Button) findViewById(R.id.btVoltar);
		btExcluir = (Button) findViewById(R.id.btExcluir);
		layoutItens = (LinearLayout) findViewById(R.id.layoutItens);

	}

	private void preencheTela() {
		if (pedido != null) {
			if (pedido.getAssinatura() != null) {
				Bitmap bm = BitmapFactory.decodeByteArray(pedido.getAssinatura(), 0, pedido.getAssinatura().length);
				assinatura.setImageBitmap(bm);
			}
			empresa.setText("Loja " + String.valueOf(pedido.getEmpresa().getNumeroLoja()));
			numeroBloco.setText(String.valueOf(pedido.getNumeroBloco()));
			numeroPedido.setText(String.valueOf(pedido.getNumeroPedido()));
			dataEmissao.setText(fm.format(pedido.getDataEmissao()));
			dataPrevisao.setText(fm.format(pedido.getDataPrevisao()));
			percentualDesconto.setText(MascaraDecimal.comDuasCasaDecimais(new BigDecimal(pedido.getPercentualDesconto())) + "%");
			observacao.setText(!Strings.isNullOrEmpty(pedido.getObservacao()) ? pedido.getObservacao() : "-");
			sync.setText(pedido.isSync() ? "SIM" : "NÃO");
			tipoCobranca.setText(pedido.getTipoCobranca().getDescricao());
			tipoCobranca2.setText(pedido.getTipoCobranca2() != null ? pedido.getTipoCobranca2().getDescricao() : "-");
			pessoa.setText(String.valueOf(pedido.getPessoa().getCodigo()) + " " + pedido.getPessoa().getRazaoSocial());
			tabelaDePreco.setText(String.valueOf(pedido.getTabelaDePreco().getCodigo()) + " " + pedido.getTabelaDePreco().getDescricao());
			condicaoDePagamento.setText(pedido.getCondicaoDePagamento().getDescricao());
			valorBruto.setText(Dinheiro.newInstance(new BigDecimal(pedido.getValor())).formatToScreen());
			valorLiquido.setText(Dinheiro.newInstance(new BigDecimal(pedido.getValorLiquido())).formatToScreen());
			if (pedido.isSync()) {
				btExcluir.setVisibility(View.INVISIBLE);
			}

			for (PedidoItens item : pedido.getItens()) {
				TextView mascara = new TextView(this);
				mascara.setWidth(120);
				mascara.setText(item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto().getMascaraCodigo());

				TextView descricao = new TextView(this);
				descricao.setWidth(300);
				descricao.setText(item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto().getDescricao());

				TextView cor = new TextView(this);
				cor.setWidth(150);
				cor.setText(item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getPadrao().getDescricao());

				TextView tam = new TextView(this);
				tam.setWidth(100);
				tam.setText(item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getTamanho().getCodigo());

				TextView preco = new TextView(this);
				preco.setWidth(150);
				preco.setText(item.getPreco().formatToScreen());

				TextView qtde = new TextView(this);
				qtde.setWidth(100);
				qtde.setText(String.format("%d", item.getQuantidade()));

				TextView total = new TextView(this);
				total.setWidth(150);
				total.setText(Dinheiro.newInstance(new BigDecimal(item.getValorTotal())).formatToScreen());

				LinearLayout linha = new LinearLayout(this);
				linha.addView(mascara);
				linha.addView(descricao);
				linha.addView(cor);
				linha.addView(tam);
				linha.addView(preco);
				linha.addView(qtde);
				linha.addView(total);

				layoutItens.addView(linha);
			}

		}
	}

	private void handleComponents() {
		btEnviaEmail.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				geraPdf();
				enviaEmail();

			}
		});

		btVoltar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		btExcluir.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				exluirPedido();

			}
		});
	}

	private void geraPdf() {
		if (pedido != null) {
			PdfPedido.newInstance(this.pedido).gerarPdf();

		}
	}

	private void enviaEmail() {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_SUBJECT, "Seu pedido");
		intent.putExtra(Intent.EXTRA_TEXT, "A cópia do pedido está no anexo.");
		String path = Environment.getExternalStorageDirectory().getPath() + "/lj" + pedido.getEmpresa().getNumeroLoja() + "ped" + pedido.getNumeroPedido() + ".pdf";
		File file = new File(path);
		intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
		if (pedido.getPessoa().getEmails() != null && !pedido.getPessoa().getEmails().isEmpty()) {
			intent.putExtra(Intent.EXTRA_EMAIL, new String[]{pedido.getPessoa().getEmails().get(0).getEmail()});
		}
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	private void exluirPedido() {

		AlertDialog.Builder alerta = new AlertDialog.Builder(this);
		alerta.setTitle("Excluir Pedido");
		alerta.setTitle("Tem certeza que deseja excluir o pedido");
		alerta.setCancelable(false);
		alerta.setPositiveButton("SIM", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				RepositorioPedidoCapa repo = new RepositorioPedidoCapa(PedidoShowActivity.this);
				int count = repo.deletar(pedido);
				repo.fechar();
				if (count > 0) {
					Toast.makeText(PedidoShowActivity.this, "Pedido excluído com sucesso", Toast.LENGTH_LONG).show();
					finish();
				} else {
					Toast.makeText(PedidoShowActivity.this, "Não foi possível excluir pedido", Toast.LENGTH_LONG).show();
				}

			}
		});
		alerta.setNegativeButton("NÃO", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();

			}
		});
		alerta.show();

	}

}
