package max.adm.pedido;

import java.io.Serializable;
import java.util.List;

import max.adm.R;
import max.adm.entidade.PadraoProduto;
import max.adm.entidade.PedidoItens;
import max.adm.entidade.TabelaPrecoProdutoReduzido;
import max.adm.entidade.TamanhoProduto;
import max.adm.utilidades.adapters.ProdutoItemPedidoParaListar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.common.collect.Lists;

public class EditarItemPedidoAcitivity extends Activity{
	
	Spinner spinnerPadroes;
	ImageView btLixeira;
	ImageView btConfirma;
	ImageView btCancela;

	TextView textReferencia;

	LinearLayout layoutTamanhos;
	LinearLayout layoutLabelsPadrao;

	List<PadraoProduto> padroes = Lists.newLinkedList();
	List<TamanhoProduto> tamanhos = Lists.newLinkedList();
	List<PedidoItens> itensPedido = Lists.newLinkedList();

	private ProdutoItemPedidoParaListar produtoComPadraoTamanhos;

	boolean gerouCampos = false; 
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.editar_item_pedido);
		montaTela();
		itensPedido = (List<PedidoItens>) getIntent().getExtras().getSerializable("ITENSPEDIDO");
		produtoComPadraoTamanhos = (ProdutoItemPedidoParaListar) getIntent().getExtras().getSerializable("PRODUTO");
		textReferencia.setText(produtoComPadraoTamanhos.getProduto().getMascaraCodigo() + " " + produtoComPadraoTamanhos.getProduto().getDescricao());
		preencheTela();
		defineFuncoes();
		if(itensPedido == null){
			itensPedido = Lists.newLinkedList();
		}
		else{
			preencheDados();
		}
	}
	
	private void montaTela() {
		btConfirma = (ImageView) findViewById(R.id.bt_confirma_padrao_tamanho);
		btCancela = (ImageView) findViewById(R.id.bt_cancela_padrao_tamanho);
		textReferencia = (TextView) findViewById(R.id.text_referenca_tela_padroes_tamanhos);
		layoutLabelsPadrao = (LinearLayout) findViewById(R.id.layout_label_padrao);
		layoutTamanhos = (LinearLayout) findViewById(R.id.layout_label_tamanho);

	}

	private void preencheTela() {
		geraLabelsComPadrao();
		geraLabelsComTamanho();
	}

	private void defineFuncoes() {

		btConfirma.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				confirmar();

			}
		});

		btCancela.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				onBackPressed();

			}
		});

	}

	

	private void confirmar() {
		itensPedido.clear();
		for(TabelaPrecoProdutoReduzido red : produtoComPadraoTamanhos.getReduzidos()){
			EditText campoQtde = (EditText) findViewById(red.getProdutoReduzido().getPadrao().hashCode() + red.getProdutoReduzido().getTamanho().hashCode());
			String qtde = campoQtde.getText().toString();
			int quantidade = 0;
			if(qtde != null && !qtde.isEmpty()){
				quantidade = Integer.parseInt(qtde);
			}
			
			itensPedido.add(PedidoItens.newItem(quantidade, red.getPreco(), 0, red));
		}
		Intent i = new Intent();
		Bundle b = new Bundle();
		b.putSerializable("ITENSPEDIDO", (Serializable) itensPedido);
		i.putExtras(b);
		setResult(RESULT_OK, i);
		finish();
	}

	private void geraLabelsComPadrao() {
		for (TabelaPrecoProdutoReduzido red : produtoComPadraoTamanhos.getReduzidos()) {
			if (!padroes.contains(red.getProdutoReduzido().getPadrao())) {
				LinearLayout linha = new LinearLayout(this);
				linha.setOrientation(LinearLayout.HORIZONTAL);
				linha.setId(red.getProdutoReduzido().getPadrao().hashCode());
				TextView textPadrao = new TextView(this);
				textPadrao.setText(red.getProdutoReduzido().getPadrao().getDescricao());

				linha.addView(textPadrao);

				layoutLabelsPadrao.addView(linha);
				textPadrao.setWidth(180);
				padroes.add(red.getProdutoReduzido().getPadrao());
				for (TabelaPrecoProdutoReduzido red2 : produtoComPadraoTamanhos.getReduzidos()) {
					if (red2.getProdutoReduzido().getPadrao().equals(red.getProdutoReduzido().getPadrao())) {
						EditText editText = new EditText(this);
						editText.setId(red.getProdutoReduzido().getPadrao().hashCode() + red2.getProdutoReduzido().getTamanho().hashCode());
						linha.addView(editText);
						editText.setWidth(50);
						editText.setPadding(0, 2, 2, 2);
						editText.setInputType(InputType.TYPE_NUMBER_FLAG_SIGNED);
						LinearLayout.LayoutParams l = new LinearLayout.LayoutParams(50, LinearLayout.LayoutParams.WRAP_CONTENT);
						l.setMargins(1, 1, 1, 1);
						editText.setLayoutParams(l);
						editText.setBackgroundResource(R.color.cinza_escuro);
					}
				}
			}
		}
	}

	private void geraLabelsComTamanho() {
		TextView text = new TextView(this);
		text.setText("");
		layoutTamanhos.addView(text);
		text.setWidth(180);
		for (TabelaPrecoProdutoReduzido red : produtoComPadraoTamanhos.getReduzidos()) {
			if (!tamanhos.contains(red.getProdutoReduzido().getTamanho())) {
				TextView textTam = new TextView(this);
				textTam.setText(red.getProdutoReduzido().getTamanho().getCodigo());
				layoutTamanhos.addView(textTam);
				textTam.setWidth(50);
				textTam.setPadding(0, 2, 2, 2);
				LinearLayout.LayoutParams l = new LinearLayout.LayoutParams(50, LinearLayout.LayoutParams.WRAP_CONTENT);
				l.setMargins(1, 1, 1, 1);
				textTam.setLayoutParams(l);
				tamanhos.add(red.getProdutoReduzido().getTamanho());
			}
		}
	}
	
	public void preencheDados(){
		for (PedidoItens item : itensPedido){
			int id = item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getPadrao().hashCode()+item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getTamanho().hashCode();
			EditText campo = (EditText) findViewById(id);
			campo.setText(String.valueOf(item.getQuantidade()));
		}
	}

}
