package max.adm.pedido.gridview;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import max.adm.Dinheiro;
import max.adm.R;
import max.adm.entidade.PadraoProduto;
import max.adm.entidade.PedidoItens;
import max.adm.entidade.ProdutoReduzido;
import max.adm.entidade.TabelaPrecoProdutoReduzido;
import max.adm.entidade.TamanhoProduto;
import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 
 * @author Sutil 
 * 
 * Use this class for generating grid of Product.
 * 
 * @return I.E.: if you have a produtec whith tree colors and two sizes we can
 *         get a view 
 */
public class FieldGeneratingGrids {

	private Context ctx;
	private int idField = 0;
	private Map<String, Integer> ids = Maps.newHashMap();
	private TextView subTotal;
	private TextView quantidade;

	private FieldGeneratingGrids(Context ctx, TextView quantidade, TextView subTotal) {
		this.ctx = ctx;
		this.subTotal = subTotal;
		this.quantidade = quantidade;
	}

	public static FieldGeneratingGrids newInstance(Context ctx, TextView quantidade, TextView subTotal) {
		checkNotNull(ctx);
		return new FieldGeneratingGrids(ctx, quantidade, subTotal);
	}

	/**
	 * insert a view in LinearLayout inserted in parameter.
	 * 
	 * @param tabelaPrecoReduzidos
	 * @param layout
	 */
	public void generateView(List<TabelaPrecoProdutoReduzido> tabelaPrecoReduzidos, LinearLayout layout, List<PedidoItens> itensDoPedido) {
		generateLabelsWithSizes(tabelaPrecoReduzidos, layout);
		List<PadraoProduto> padroes = getPadroes(tabelaPrecoReduzidos);
		for (PadraoProduto p : padroes) {
			LinearLayout linha = new LinearLayout(ctx);
			linha.setOrientation(LinearLayout.HORIZONTAL);
			linha.setId(p.hashCode());

			TextView textPadrao = new TextView(ctx);
			textPadrao.setWidth(180);
			textPadrao.setText(String.format("%d - %s", p.getCodigo(), p.getDescricao()));
			linha.addView(textPadrao);

			layout.addView(linha);
			for (TabelaPrecoProdutoReduzido tabelaPrecoReduzido : tabelaPrecoReduzidos) {
				if (tabelaPrecoReduzido.getProdutoReduzido().getPadrao().equals(p)) {
					EditText editText = new EditText(ctx);
					ids.put(String.format("%d%s%s", p.getCodigo(), p.getDescricao(), tabelaPrecoReduzido.getProdutoReduzido().getTamanho().getCodigo()), idField++);
					editText.setId(idField);
					editText.setPadding(0, 2, 2, 2);
					editText.setInputType(InputType.TYPE_CLASS_NUMBER);
					editText.setBackgroundResource(R.color.cinza_escuro);
					TableRow.LayoutParams params = new TableRow.LayoutParams(50, LayoutParams.WRAP_CONTENT);
					params.setMargins(10, 10, 10, 10);
					editText.setLayoutParams(params);
					handleEditText(editText, tabelaPrecoReduzido, itensDoPedido);
					fillField(editText, tabelaPrecoReduzido.getProdutoReduzido(), itensDoPedido);
					linha.addView(editText);
				}
			}
		}
	}

	private void generateLabelsWithSizes(List<TabelaPrecoProdutoReduzido> tabelaPrecoReduzidos, LinearLayout parentLayout) {
		TextView text = new TextView(ctx);
		text.setText("");

		LinearLayout layout = new LinearLayout(ctx);
		layout.addView(text);
		text.setWidth(180);

		List<TamanhoProduto> tamanhos = getTamanhos(tabelaPrecoReduzidos);
		for (TamanhoProduto t : tamanhos) {
			TextView textTam = new TextView(ctx);
			textTam.setText(t.getDescricao());
			layout.addView(textTam);
			textTam.setWidth(50);
			textTam.setPadding(0, 2, 2, 2);
			LinearLayout.LayoutParams l = new LinearLayout.LayoutParams(50, LinearLayout.LayoutParams.WRAP_CONTENT);
			l.setMargins(10, 10, 10, 10);
			textTam.setLayoutParams(l);
		}
		parentLayout.addView(layout);
	}

	private List<PadraoProduto> getPadroes(List<TabelaPrecoProdutoReduzido> tabelaPrecoProdutoReduzidos) {
		List<PadraoProduto> padroes = Lists.newLinkedList();
		for (TabelaPrecoProdutoReduzido tPrecoProdutoReduzido : tabelaPrecoProdutoReduzidos) {
			if (!padroes.contains(tPrecoProdutoReduzido.getProdutoReduzido().getPadrao())) {
				padroes.add(tPrecoProdutoReduzido.getProdutoReduzido().getPadrao());
			}
		}
		return padroes;
	}

	private List<TamanhoProduto> getTamanhos(List<TabelaPrecoProdutoReduzido> tabelaPrecoReduzidos) {
		List<TamanhoProduto> tamanhos = Lists.newLinkedList();
		for (TabelaPrecoProdutoReduzido reduzido : tabelaPrecoReduzidos) {
			if (!tamanhos.contains(reduzido.getProdutoReduzido().getTamanho())) {
				tamanhos.add(reduzido.getProdutoReduzido().getTamanho());
			}
		}
		return tamanhos;
	}

	private void handleEditText(final EditText campo, final TabelaPrecoProdutoReduzido tabelaPrecoReduzido, final List<PedidoItens> itens) {

		campo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int quantidade = 0;
				String str = campo.getText().toString();
				if (!"".equals(str)) {
					quantidade = Integer.parseInt(str);
				}
				quantidade++;
				campo.setText(String.valueOf(quantidade));
			}
		});

		TextWatcher watcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				int quantidade = 0;
				if(s.length() > 0) {
					quantidade = Integer.parseInt(s.toString());
				}
				setQuantidade(quantidade, tabelaPrecoReduzido, itens);
				FieldGeneratingGrids.this.quantidade.setText(String.valueOf(getQuantidade(itens)));
				FieldGeneratingGrids.this.subTotal.setText(getValor(itens).formatToScreen());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		};
		
		campo.addTextChangedListener(watcher);

	}

	private void fillField(EditText editText, ProdutoReduzido red, List<PedidoItens> itensDoPedido) {
		for (PedidoItens item : itensDoPedido) {
			if (red.equals(item.getTabelaPrecoProdutoReduzido().getProdutoReduzido())) {
				editText.setText(String.valueOf(item.getQuantidade()));
				break;
			}
		}
	}
	
	private void setQuantidade(int quantidade, TabelaPrecoProdutoReduzido tabelaPrecoReduzido, List<PedidoItens> itens) {
		boolean encontrou = false;
		for(PedidoItens item : itens) {
			if(item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().equals(tabelaPrecoReduzido)) {
				item.setQuantidade(quantidade);
				encontrou = true;
				break;
			}
		}
		if(!encontrou && quantidade > 0) {
			
			itens.add(PedidoItens.newItem(quantidade, tabelaPrecoReduzido.getPreco(), 0, tabelaPrecoReduzido));
		}
		removeQuantidadeZero(itens);
	}
	
	private void removeQuantidadeZero(List<PedidoItens> itens) {
		Iterator<PedidoItens> it = itens.iterator();
		while (it.hasNext()) {
			PedidoItens item = it.next();
			if(item.getQuantidade() == 0) {
				it.remove();
			}
		}
	}
	
	public int getIdField(ProdutoReduzido reduzido) {
		String key = String.format("%d%s%s", reduzido.getPadrao().getCodigo(), reduzido.getPadrao().getDescricao(), reduzido.getTamanho().getCodigo());
		if(ids.containsKey(key)) {
			return ids.get(key);
		}
		return 0;
		
	}
	
	private int getQuantidade(List<PedidoItens> itens) {
		int quantidade = 0;
		for(PedidoItens item : itens) {
			quantidade += item.getQuantidade();
		}
		return quantidade;
	}
	
	private Dinheiro getValor(List<PedidoItens> itens) {
		BigDecimal valor = new BigDecimal(0);
		for(PedidoItens item : itens) {
			valor = valor.add(new BigDecimal(item.getValorTotal()));
		}
		return Dinheiro.newInstance(valor);
	}
	
	

}
