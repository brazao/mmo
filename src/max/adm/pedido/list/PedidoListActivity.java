package max.adm.pedido.list;

import java.util.List;

import max.adm.R;
import max.adm.ValorAtividade;
import max.adm.entidade.Empresa;
import max.adm.entidade.Parametro;
import max.adm.entidade.PedidoCapa;
import max.adm.pedido.show.PedidoShowActivity;
import max.adm.pedido.showroom.ScannerItemReduzidoShowroomActivity;
import max.adm.relatorios.ItemRelatorioListaPedido;
import max.adm.repositorio.RepositorioPedidoCapa;
import max.adm.utilidades.adapters.PedidoListAdapter;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;

public class PedidoListActivity extends ListActivity {

	private ImageView btNovPedido;

	private List<ItemRelatorioListaPedido> pedidos;
	private Empresa empresa;
	private Parametro parametro;

	private ProgressDialog pDialog;

	private static final int DIALOG_CARREGANDO_PEDIDOS = 1;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		preencheTela();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pedido_list);

		if (getIntent().getExtras() != null) {
			empresa = (Empresa) getIntent().getExtras().getSerializable("EMPRESA");
			parametro = (Parametro) getIntent().getExtras().getSerializable("PARAMETRO");
		}

		montaTela();

		defineFuncoes();

		preencheTela();

	}

	private void montaTela() {
		btNovPedido = (ImageView) findViewById(R.id.bt_novo_pedido);
	}

	private void defineFuncoes() {
		btNovPedido.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				chamaPedidoNovo();

			}
		});

		getListView().setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> lista, View arg1, int position, long arg3) {
				ItemRelatorioListaPedido item = (ItemRelatorioListaPedido) lista.getItemAtPosition(position);

				chamaTelaDeExibicao(item.getId());

			}
		});
	}

	private void preencheTela() {
		if (empresa != null)
			new ListarPedidos().execute("");

	}

	private void chamaPedidoNovo() {
		Intent i = new Intent(this, ScannerItemReduzidoShowroomActivity.class);

		Bundle b = new Bundle();
		b.putSerializable("EMPRESA", empresa);
		b.putSerializable("PARAMETRO", parametro);
		i.putExtras(b);
		startActivityForResult(i, ValorAtividade.PedidoCadastro.get());
	}

	private void chamaTelaDeExibicao(int id) {
		RepositorioPedidoCapa repositorioPedidoCapa = new RepositorioPedidoCapa(this);
		PedidoCapa pedido = repositorioPedidoCapa.buscaPedidoCompleto(id);
		repositorioPedidoCapa.fechar();
		Intent i = new Intent(this, PedidoShowActivity.class);
		Bundle b = new Bundle();
		b.putSerializable("PEDIDO", pedido);
		i.putExtras(b);
		startActivityForResult(i, ValorAtividade.PedidoShowActivity.get());
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
			case DIALOG_CARREGANDO_PEDIDOS :
				pDialog = new ProgressDialog(this);
				pDialog.setMessage("Carregando pedidos...");
				pDialog.setIndeterminate(true);
				pDialog.setCancelable(false);
				pDialog.show();
				return pDialog;

			default :
				return super.onCreateDialog(id);
		}

	}

	private class ListarPedidos extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			showDialog(DIALOG_CARREGANDO_PEDIDOS);
		}

		@Override
		protected String doInBackground(String... params) {
			pedidos = PedidoCapa.listarParaTela(PedidoListActivity.this, empresa.getNumeroLoja());
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			setListAdapter(new PedidoListAdapter(PedidoListActivity.this, pedidos));
			dismissDialog(DIALOG_CARREGANDO_PEDIDOS);

		}
	}

}
