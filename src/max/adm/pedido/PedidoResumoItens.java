package max.adm.pedido;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import max.adm.R;
import max.adm.entidade.PedidoCapa;
import max.adm.entidade.PedidoItens;
import max.adm.entidade.Produto;
import max.adm.entidade.TabelaDePreco;
import max.adm.entidade.TabelaPrecoProdutoReduzido;
import max.adm.produtoreduzido.repositorio.RepositorioProdutoReduzido;
import max.adm.repositorio.RepositorioTabelaDePrecoProdutoReduzido;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class PedidoResumoItens extends ListActivity {

	public static final String KEY_ITENSPEDIDO = "ITENSPEDIDO";
	public static final String KEY_BUILDERS = "BUILDERS";

	protected List<PedidoItens> itensDoPedido = Lists.newLinkedList();
	protected Map<Produto, List<TabelaPrecoProdutoReduzido>> agrupados = Maps.newHashMap();
	protected PedidoResumoItensAdapter adapter;
	protected TextView quantidade;
	protected TextView subTotal;
	protected ImageView btOk;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pedido_resumo_itens);
		itensDoPedido = (List<PedidoItens>) getIntent().getExtras().getSerializable(KEY_ITENSPEDIDO);

		initCoimponents();
		handleComponents();

		agrupar(itensDoPedido);
		adapter = new PedidoResumoItensAdapter(agrupados, this, itensDoPedido, quantidade, subTotal);
		setListAdapter(adapter);
	}

	private void initCoimponents() {
		quantidade = (TextView) findViewById(R.id.quantidade);
		subTotal = (TextView) findViewById(R.id.subtotal);
		btOk = (ImageView) findViewById(R.id.btOk);
	}

	private void handleComponents() {
		btOk.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				removeZeros();
				if (itensDoPedido.size() > 0) {
					goBack();
				} else {
					Toast.makeText(PedidoResumoItens.this,
							"Não é permitido remover todos os itens do pedido", Toast.LENGTH_LONG).show();
				}

			}
		});
	}

	private void agrupar(List<PedidoItens> itens) {
		for (PedidoItens item : itens) {
			Produto produto = item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto();
			if (!agrupados.containsKey(produto)) {
				RepositorioProdutoReduzido repo = new RepositorioProdutoReduzido(this);
				RepositorioTabelaDePrecoProdutoReduzido repoTabelaDePrecoProdutoReduzido = new RepositorioTabelaDePrecoProdutoReduzido(this);
				
				List<TabelaPrecoProdutoReduzido> tabelaPrecoReduzidos = 
						repoTabelaDePrecoProdutoReduzido.buscaPorProdutoTabela(item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getEmpresa().getNumeroLoja(), 
								item.getTabelaPrecoProdutoReduzido().getTabelaDePreco().getCodigo(), 
								produto.getCodigo());
				repo.fechar();

				agrupados.put(produto, tabelaPrecoReduzidos);
			}
		}
	}

	protected void removeZeros() {
		Iterator<PedidoItens> it = itensDoPedido.iterator();
		while (it.hasNext()) {
			PedidoItens item = it.next();
			if (item.getQuantidade() == 0) {
				it.remove();
			}
		}
	}

	protected List<TabelaDePreco> getTabelasPreco() {
		List<TabelaDePreco> tabelas = Lists.newLinkedList();
		for (PedidoItens item : itensDoPedido) {
			if (!tabelas.contains(item.getTabelaPrecoProdutoReduzido().getTabelaDePreco())) {
				tabelas.add(item.getTabelaPrecoProdutoReduzido().getTabelaDePreco());
			}
		}
		return tabelas;
	}

	protected void goBack() {

		List<PedidoCapa.Builder> builders = Lists.newLinkedList();
		for (TabelaDePreco tabela : getTabelasPreco()) {
			builders.add(PedidoCapa.builder().comTabelaDePreco(tabela));
		}
		for (PedidoItens item : itensDoPedido) {
			for (PedidoCapa.Builder builder : builders) {
				if (builder.getTabelaDePreco().equals(
						item.getTabelaPrecoProdutoReduzido().getTabelaDePreco())) {
					builder.addItem(item);
				}
			}
		}
		Bundle b = new Bundle();
		b.putSerializable(KEY_BUILDERS, (Serializable) builders);
		Intent i = new Intent(this, PedidoInsereDadosCapa.class);
		i.putExtras(b);
		setResult(RESULT_OK, i);
		finish();
	}

}
