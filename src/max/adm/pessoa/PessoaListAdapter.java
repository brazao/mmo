package max.adm.pessoa;

import java.util.List;

import max.adm.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.google.common.collect.Lists;

public class PessoaListAdapter extends BaseAdapter implements Filterable {

	private List<PessoaParaLista> pessoas;
	private List<PessoaParaLista> originalPessoas = Lists.newLinkedList();
	private Context ctx;

	public PessoaListAdapter(List<PessoaParaLista> pessoas, Context ctx) {
		this.pessoas = pessoas;
		this.originalPessoas = Lists.newLinkedList(pessoas);
		this.ctx = ctx;
	}

	@Override
	public int getCount() {
		return pessoas.size();
	}

	@Override
	public PessoaParaLista getItem(int position) {
		return pessoas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return pessoas.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		PessoaParaLista pessoa = pessoas.get(position);
		ViewHolder holder;

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.modelo_item_pessoa, null);
			holder = new ViewHolder();

			holder.razao = (TextView) convertView.findViewById(R.id.text_razao_modelo_item_lista_pessoa);
			holder.fantasia = (TextView) convertView.findViewById(R.id.text_fantasia_modelo_item_list_pessoa);
			holder.cnpj = (TextView) convertView.findViewById(R.id.text_cnpj_modelo_item_list_pessoa);
			holder.inscricao = (TextView) convertView.findViewById(R.id.text_insc_modelo_item_list_pessoa);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.razao.setText(pessoa.getRazao());
		holder.fantasia.setText(pessoa.getFantasia());
		holder.cnpj.setText(pessoa.getCpf());
		holder.inscricao.setText(pessoa.getInsc());

		return convertView;
	}

	@Override
	public Filter getFilter() {
		Filter filter = new Filter() {

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults filterResults = new FilterResults();
				List<PessoaParaLista> results = Lists.newLinkedList();
				
				if (constraint != null) {
					String stringFiltro = constraint.toString().toUpperCase();
					try {
						for (PessoaParaLista p : originalPessoas) {

							if (inserir(p, stringFiltro)) {
								results.add(p);
							}
						}
						filterResults.values = results;
						filterResults.count = results.size();
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
				return filterResults;
			}
			
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				if (results != null && results.count > 0) {
					pessoas = (List<PessoaParaLista>) results.values;
					notifyDataSetChanged();
				} else {
					pessoas = Lists.newLinkedList(originalPessoas);
					notifyDataSetInvalidated();
				}

			}
			
			private boolean inserir(PessoaParaLista pessoaParaLista, String stringFiltro) {
				if ( (pessoaParaLista.getRazao() != null &&  pessoaParaLista.getRazao().toUpperCase().contains(stringFiltro)) || 
						(pessoaParaLista.getFantasia() != null && pessoaParaLista.getFantasia().toUpperCase().contains(stringFiltro)) || 
						(pessoaParaLista.getCpf() != null && pessoaParaLista.getCpf().contains(stringFiltro) ) ) {
					return true;
				} else {
					return false;
				}
			}
			
		};
		return filter;
	}

	static class ViewHolder {
		TextView razao;
		TextView fantasia;
		TextView cnpj;
		TextView inscricao;
	}

}
