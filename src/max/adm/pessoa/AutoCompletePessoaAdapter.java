package max.adm.pessoa;

import java.util.List;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.google.common.collect.Lists;

public class AutoCompletePessoaAdapter extends ArrayAdapter<PessoaParaLista> implements Filterable{
	
	protected List<PessoaParaLista> pessoas;
	
	public AutoCompletePessoaAdapter(Context context, int textViewResourceId, List<PessoaParaLista> pessoas) {
		super(context, textViewResourceId);
		this.pessoas = pessoas;
	}
	
	@Override
	public int getCount() {
		return pessoas.size();
	}
	
	@Override
	public PessoaParaLista getItem(int position) {
		return this.pessoas.get(position);
	}
	
	@Override
	public Filter getFilter() {
		Filter myFilter = new Filter() {
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults filterResults = new FilterResults();
				List<PessoaParaLista> results = Lists.newLinkedList();
				if(constraint != null){
					for(PessoaParaLista p: pessoas){
						if(p.getRazao().contains(constraint)){
							results.add(p);
						}
					}
					filterResults.values = results;
				}
				return filterResults;
			}
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				pessoas = (List<PessoaParaLista>) results.values;
				notifyDataSetChanged();
			}
		};
		return myFilter;
	}
	
	
	

}
