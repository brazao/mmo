package max.adm.pessoa.repositorio;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import max.adm.classificacaoPessoa.RepositorioClassificacaoPessoa;
import max.adm.database.DBHelper;
import max.adm.empresa.RepositorioEmpresa;
import max.adm.endereco.RepositorioEndereco;
import max.adm.entidade.Cidade;
import max.adm.entidade.ClassifcacaoPessoa;
import max.adm.entidade.Email;
import max.adm.entidade.Empresa;
import max.adm.entidade.Endereco;
import max.adm.entidade.Estado;
import max.adm.entidade.FaixaCodigoCliente;
import max.adm.entidade.Pessoa;
import max.adm.entidade.Telefone;
import max.adm.perfilcliente.SolicitacaoPerfil;
import max.adm.pessoa.PessoaParaLista;
import max.adm.repositorio.RepositorioEmail;
import max.adm.telefone.RepositorioTelefone;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class RepositorioPessoa {

	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private Map<Integer, Cidade> cidades = Maps.newHashMap();
	private Map<String, Estado> estados = Maps.newHashMap();
	private Map<Integer, Empresa> empresas = Maps.newHashMap();
	private Map<Integer, ClassifcacaoPessoa> classificacoes = Maps.newHashMap();

	private int idPais = 0;

	private Context context;

	private SQLiteDatabase db;

	public RepositorioPessoa(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public long salvar(Pessoa pessoa) {

		if (existeRegistro(pessoa)) {
			return atualizar(pessoa);
		} else {
			return inserir(pessoa);
		}
	}
	
	public boolean existeRegistro(Pessoa pessoa) {
		Cursor cursor = db.query(
				true, 
				Pessoa.NOME_TABELA, 
				Pessoa.COLUNAS, 
				Pessoa.CODIGO+"=? and "+ Pessoa.EMPRESA+"=?", 
				new String[] {String.valueOf(pessoa.getCodigo()), String.valueOf(pessoa.getEmpresa().getNumeroLoja())}, 
				null, null, null, null);
		
		return cursor.moveToFirst();
	}

	public List<Pessoa> preparaParaInsert(List<Pessoa> pessoas) {

		for (Pessoa pessoa : pessoas) {
			Map<Integer, ClassifcacaoPessoa> classificacoes = Maps.newHashMap();
			Map<Integer, Empresa> lojas = Maps.newHashMap();

			// busca id lojas
			if (lojas.containsKey(pessoa.getEmpresa().getNumeroLoja())) {
				// pessoa.getEmpresa().setId(lojas.get(pessoa.getEmpresa().getNumeroLoja()).getNumeroLoja());
			} else {
				if (pessoa.getEmpresa() != null) {

					Cursor cLoja = db.query(Empresa.NOME_TABELA, new String[] { Empresa.NUMEROLOJA },
							Empresa.NUMEROLOJA + "=?",
							new String[] { String.valueOf(pessoa.getEmpresa().getNumeroLoja()) }, null, null,
							null);

					if (cLoja.moveToFirst()) {
						pessoa.getEmpresa().setNumeroLoja(
								cLoja.getInt(cLoja.getColumnIndex(Empresa.NUMEROLOJA)));
					}
					lojas.put(pessoa.getEmpresa().getNumeroLoja(), pessoa.getEmpresa());
					cLoja.close();
				}
			}

		}

		return pessoas;
	}

	private ContentValues montaValues(Pessoa pessoa) {
		ContentValues values = new ContentValues();

		values.put(Pessoa.CODIGO, pessoa.getCodigo());
		values.put(Pessoa.RAZAOSOCIAL, pessoa.getRazaoSocial());
		values.put(Pessoa.NOMEFANTASIA, pessoa.getNomeFantasia());
		values.put(Pessoa.OBSERVACAO, pessoa.getObservacao());
		values.put(Pessoa.CGCCPF, pessoa.getCpfCgc());
		values.put(Pessoa.INSCRG, pessoa.getRgInscricao());

		if (pessoa.isSync()) {
			values.put(Pessoa.SYNC, "S");
		} else {
			values.put(Pessoa.SYNC, "N");
		}
		if (pessoa.getContato() != null) {
			values.put(Pessoa.CONTATO, pessoa.getContato().toUpperCase());
		}

		// if (pessoa.getEmpresa().getNumeroLoja() == 0) {
		// if (empresas.containsKey(pessoa.getEmpresa().getNumeroLoja())) {
		// pessoa.getEmpresa().setId(empresas.get(pessoa.getEmpresa().getNumeroLoja()).getNumeroLoja());
		// } else {
		// pessoa.getEmpresa().setId(buscaIdEmpresa(pessoa.getEmpresa()));
		// empresas.put(pessoa.getEmpresa().getNumeroLoja(),
		// pessoa.getEmpresa());
		// }
		// }
		values.put(Pessoa.EMPRESA, pessoa.getEmpresa().getNumeroLoja());
		return values;
	}

	public boolean salvarLista(List<Pessoa> pessoas) {

		for (Pessoa pessoa : pessoas) {
			db.beginTransaction();
			try {
				ContentValues values = montaValues(pessoa);
				inserir(values, pessoa);
				db.setTransactionSuccessful();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				db.endTransaction();
			}

		} // fim do for

		return true;
	}

	private long inserir(Pessoa pessoa) {

		Map<Integer, ClassifcacaoPessoa> classificacoes = new HashMap<Integer, ClassifcacaoPessoa>();
		Map<Integer, Empresa> lojas = new HashMap<Integer, Empresa>();

		ContentValues values = new ContentValues();

		values.put(Pessoa.CODIGO, pessoa.getCodigo());
		values.put(Pessoa.RAZAOSOCIAL, pessoa.getRazaoSocial().toUpperCase());
		values.put(Pessoa.NOMEFANTASIA, pessoa.getNomeFantasia().toUpperCase());
		values.put(Pessoa.OBSERVACAO, pessoa.getObservacao().toUpperCase());
		values.put(Pessoa.CGCCPF, pessoa.getCpfCgc());
		values.put(Pessoa.INSCRG, pessoa.getRgInscricao());

		if (pessoa.isSync()) {
			values.put(Pessoa.SYNC, "S");
		} else {
			values.put(Pessoa.SYNC, "N");
		}

		if (pessoa.getContato() != null) {
			values.put(Pessoa.CONTATO, pessoa.getContato().toUpperCase());
		}

		if (lojas.containsKey(pessoa.getEmpresa().getNumeroLoja())) {
			values.put(Pessoa.EMPRESA, lojas.get(pessoa.getEmpresa().getNumeroLoja()).getNumeroLoja());
		} else {
			if (pessoa.getEmpresa() != null) {

				Cursor cLoja = db.query(Empresa.NOME_TABELA, new String[] { Empresa.NUMEROLOJA },
						Empresa.NUMEROLOJA + "=?",
						new String[] { String.valueOf(pessoa.getEmpresa().getNumeroLoja()) }, null, null,
						null);

				if (cLoja.getCount() > 0) {
					cLoja.moveToFirst();
					int idLoja = cLoja.getInt(cLoja.getColumnIndex(Empresa.NUMEROLOJA));
					values.put(Pessoa.EMPRESA, idLoja);

				}
				cLoja.close();
			}
		}

		return inserir(values, pessoa);

	}

	private ContentValues montaValuesEmail(Email email, long idPessoa) {

		ContentValues values = new ContentValues();
		values.put(Email.CODIGO, 1); //esse codigo é a quantidade de mails que tem no banco
		values.put(Email.EMAIL, email.getEmail());
		values.put(Email.PESSOA, idPessoa);

		return values;
	}

	private ContentValues montaValuesTelefone(Telefone tel, long idPessoa) {
		ContentValues valoresTel = new ContentValues();

		int telefoneId = 0;
		Cursor cursor = db.rawQuery("select max("+Telefone.CODIGO+") "+Telefone.CODIGO+" from telefone", null);
		if (cursor.moveToNext()) {
			telefoneId = cursor.getInt(cursor.getColumnIndex(Telefone.CODIGO)) +1;
		}
		
		valoresTel.put(Telefone.CODIGO, telefoneId);
		valoresTel.put(Telefone.PESSOA, idPessoa);
		valoresTel.put(Telefone.NUMEROTELEFONE, tel.getNumeroTelefone());
		valoresTel.put(Telefone.TIPOTELEFONE, tel.getTipoTelefone().name());
		valoresTel.put(Telefone.EMPRESA, tel.getEmpresa().getNumeroLoja());
		return valoresTel;
	}

	private long inserir(ContentValues values, Pessoa pessoa) {
		long rowId = 0;
		try {
			rowId = db.insert(Pessoa.NOME_TABELA, "", values);

			if (pessoa.getEmails() != null) {
				for (Email e : pessoa.getEmails()) {
					if (e.getEmail() != null) {
						e.setPessoa(pessoa);
						ContentValues vlrEmail = this.montaValuesEmail(e, pessoa.getCodigo());
						db.insert(Email.NOME_TABELA, "", vlrEmail);
					}
				}
			}

			if (pessoa.getTelefones() != null) {
				for (Telefone tel : pessoa.getTelefones()) {
					ContentValues valoresTel = montaValuesTelefone(tel, pessoa.getCodigo());
					db.insert(Telefone.NOME_TABELA, null, valoresTel);
				}
			}

			if (pessoa.getEnderecos() != null) {
				for (Endereco end : pessoa.getEnderecos()) {
					end.setPessoa(pessoa);
					ContentValues valoresEnd = montaValuesEndereco(end, pessoa.getCodigo());

					db.insert(Endereco.NOME_TABELA, null, valoresEnd);
				}
			}

			ContentValues valFaixa = new ContentValues();

			valFaixa.put(FaixaCodigoCliente.UTILIZADO, "S");

			String where = FaixaCodigoCliente.CODIGO + "=? and " + FaixaCodigoCliente.EMPRESA + "=?";
			String[] whereArgs = { String.valueOf(pessoa.getCodigo()),
					String.valueOf(pessoa.getEmpresa().getNumeroLoja()) };
			System.out.println(where);
			System.out.println(String.valueOf(pessoa.getCodigo()));
			System.out.println(String.valueOf(pessoa.getEmpresa().getNumeroLoja()));

			db.update(FaixaCodigoCliente.TABELA_FAIXA_CODIGO, valFaixa, where, whereArgs);

		} catch (Exception e2) {
			Log.d("erro", "ERRO AO INSERIR PESSOA");
			e2.printStackTrace();
			return 0;
		}

		return rowId;
	}

	private ContentValues montaValuesEndereco(Endereco end, long idPessoa) {
		ContentValues valoresEnd = new ContentValues();
		
		int enderecoId = 0;
		Cursor cursor = db.rawQuery("select max("+Endereco.CODIGO+") "+Endereco.CODIGO+" from "+Endereco.NOME_TABELA, null);
		if (cursor.moveToNext()) {
			enderecoId = cursor.getInt(cursor.getColumnIndex(Telefone.CODIGO)) +1;
		}
		
		valoresEnd.put(Endereco.CODIGO, enderecoId);
		valoresEnd.put(Endereco.PESSOA, idPessoa);
		valoresEnd.put(Endereco.EMPRESA, end.getEmpresa().getNumeroLoja());
		valoresEnd.put(Endereco.BAIRRO, end.getBairro());
		valoresEnd.put(Endereco.CEP, end.getCep());
		valoresEnd.put(Endereco.CIDADE, end.getCidade().getCodigo());
		valoresEnd.put(Endereco.COMPLEMENTO, end.getComplemento());
		valoresEnd.put(Endereco.TIPO_LOGRADOURO, end.getLogradouro());
		valoresEnd.put(Endereco.NUMERO, end.getNumero());
		valoresEnd.put(Endereco.TIPOENDERECO, end.getTipoEndereco().name());

		return valoresEnd;
	}

	public int atualizar(Pessoa pessoa) {
		ContentValues values = new ContentValues();

		values.put(Pessoa.CODIGO, pessoa.getCodigo());
		values.put(Pessoa.RAZAOSOCIAL, pessoa.getRazaoSocial().toUpperCase());
		values.put(Pessoa.NOMEFANTASIA, pessoa.getNomeFantasia().toUpperCase());
		values.put(Pessoa.OBSERVACAO, pessoa.getObservacao().toUpperCase());
		values.put(Pessoa.CGCCPF, pessoa.getCpfCgc());
		values.put(Pessoa.INSCRG, pessoa.getRgInscricao().toUpperCase());

		if (pessoa.isSync()) {
			values.put(Pessoa.SYNC, "S");
		} else {
			values.put(Pessoa.SYNC, "N");
		}

		if (pessoa.getContato() != null) {
			values.put(Pessoa.CONTATO, pessoa.getContato().toUpperCase());
		}

		if (pessoa.getEmpresa() != null)
			values.put(Pessoa.EMPRESA, pessoa.getEmpresa().getNumeroLoja());

		String codigo = String.valueOf(pessoa.getCodigo());
		String empresaId = String.valueOf(pessoa.getEmpresa().getNumeroLoja());
		String where = Pessoa.CODIGO + "=? and " + Pessoa.EMPRESA + "=?";
		String[] whereArgs = new String[] { codigo, empresaId };
		int count = atualizar(values, where, whereArgs, pessoa);

		return count;
	}

	public int atualizar(ContentValues values, String where, String[] whereArgs, Pessoa pessoa) {

		int count;

		db.beginTransaction();
		try {
			count = db.update(Pessoa.NOME_TABELA, values, where, whereArgs);

			// id = (int) db.insert(Pessoa.NOME_TABELA, "", values);
			// pessoa.setId(id);
			db.delete(Email.NOME_TABELA, Email.PESSOA + "=" + pessoa.getCodigo(), null);
			for (Email e : pessoa.getEmails()) {
				e.setPessoa(pessoa);
				ContentValues vlrEmail = new ContentValues();
				vlrEmail.put(Email.EMAIL, e.getEmail().toLowerCase());
				vlrEmail.put(Email.PESSOA, e.getPessoa().getCodigo());
				db.insert(Email.NOME_TABELA, "", vlrEmail);

			}
			db.delete(Telefone.NOME_TABELA, Telefone.PESSOA + "=" + pessoa.getCodigo(), null);
			for (Telefone tel : pessoa.getTelefones()) {
				ContentValues valoresTel = new ContentValues();
				valoresTel.put(Telefone.PESSOA, pessoa.getCodigo());
				valoresTel.put(Telefone.NUMEROTELEFONE, tel.getNumeroTelefone());
				valoresTel.put(Telefone.TIPOTELEFONE, tel.getTipoTelefone().getDescricao());

				db.insert(Telefone.NOME_TABELA, "", valoresTel);
			}

			db.delete(Endereco.NOME_TABELA, Endereco.PESSOA + "=?",
					new String[] { String.valueOf(pessoa.getCodigo()) });

			for (Endereco end : pessoa.getEnderecos()) {
				end.setPessoa(pessoa);
				ContentValues valoresEnd = new ContentValues();
				valoresEnd.put(Endereco.PESSOA, end.getPessoa().getCodigo());
				valoresEnd.put(Endereco.BAIRRO, end.getBairro().toUpperCase());
				valoresEnd.put(Endereco.CEP, end.getCep());
				if (end.getCidade() != null)
					valoresEnd.put(Endereco.CIDADE, end.getCidade().getCodigo());

				valoresEnd.put(Endereco.COMPLEMENTO, end.getComplemento() != null ? end.getComplemento()
						.toUpperCase() : "");
				valoresEnd.put(Endereco.TIPO_LOGRADOURO, end.getLogradouro().toUpperCase());
				valoresEnd.put(Endereco.NUMERO, end.getNumero().toUpperCase());
				if (end.getTipoEndereco() != null)
					valoresEnd.put(Endereco.TIPOENDERECO, end.getTipoEndereco().getDescricao());
				valoresEnd.put(Endereco.TIPO_LOGRADOURO, end.getLogradouro());
				end.setPessoa(pessoa);

				// insere endereco
				db.insert(Endereco.NOME_TABELA, "", valoresEnd);

				// db.update(Endereco.NOME_TABELA, valoresEnd, Endereco.ID+"=?",
				// new String[]{String.valueOf(end.getId())});
			}

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();

		}

		return count;
	}

	public int deletar(int codigo, int empresaId) {
		String where = Pessoa.CODIGO + "=? and " + Pessoa.EMPRESA + "=?";
		String[] whereArgs = new String[] { String.valueOf(codigo), String.valueOf(empresaId) };
		int count = deletar(where, whereArgs, codigo);
		return count;
	}

	public int deleteAll() {
		return db.delete(Pessoa.NOME_TABELA, null, null);

	}

	public int deletar(String where, String[] whereArgs, int codigo) {
		int count = 0;
		db.beginTransaction();
		try {
			db.delete(Endereco.NOME_TABELA, Endereco.PESSOA + "=?", whereArgs);

			// deleta os emails
			db.delete(Email.NOME_TABELA, Email.PESSOA + "=?", whereArgs);

			// deletar os telefones
			db.delete(Telefone.NOME_TABELA, Telefone.PESSOA + "=?", whereArgs);

			// deletar pessoa
			count = db.delete(Pessoa.NOME_TABELA, where, whereArgs);

			db.setTransactionSuccessful();

		} finally {
			db.endTransaction();
		}

		return count;
	}

	public Pessoa busca(int codigo, int empresaId) {
		Pessoa.Builder builder = findoById(codigo, empresaId);
		return builder.build();
	}

	public Pessoa.Builder findoById(int codigo, int empresaId) {
		Cursor c = db.query(true, Pessoa.NOME_TABELA, Pessoa.COLUNAS, Pessoa.CODIGO + "=" + codigo+ " and "+Pessoa.EMPRESA+"="+empresaId, null,
				null, null, null, null);
		if (c.moveToFirst()) {
			Pessoa.Builder builder = montaBuilder(c);
			c.close();
			return builder;
		}
		c.close();
		return null;
	}

	public Pessoa.Builder montaBuilder(Cursor c) {
		RepositorioEmpresa repoEmp = new RepositorioEmpresa(context);
		RepositorioClassificacaoPessoa repoClassPess = new RepositorioClassificacaoPessoa(context);
		RepositorioEndereco repoEnd = new RepositorioEndereco(context);
		RepositorioEmail repoEmail = new RepositorioEmail(context);
		RepositorioTelefone repoTel = new RepositorioTelefone(context);

		Pessoa.Builder builder = Pessoa.builder(c.getInt(c.getColumnIndex(Pessoa.CODIGO)));
		builder.comRazaoSocial(c.getString(c.getColumnIndex(Pessoa.RAZAOSOCIAL)));
		builder.comNomeFantasia(c.getString(c.getColumnIndex(Pessoa.NOMEFANTASIA)));
		builder.comObservacao(c.getString(c.getColumnIndex(Pessoa.OBSERVACAO)));
		builder.comCgcCpf(c.getString(c.getColumnIndex(Pessoa.CGCCPF)));
		builder.comInscRg(c.getString(c.getColumnIndex(Pessoa.INSCRG)));
		builder.comDataCadastro(new Date(c.getLong(c.getColumnIndex(Pessoa.DATACADASTRO))));
		repoClassPess.fechar();

		if (c.getString(c.getColumnIndex(Pessoa.SYNC)).equals("S")) {
			builder.sincronozado(true);
		} else {
			builder.sincronozado(false);
		}
		builder.comContato(c.getString(c.getColumnIndex(Pessoa.CONTATO)));
		builder.comEmpresa(repoEmp.busca(c.getInt(c.getColumnIndex(Pessoa.EMPRESA))));
		repoEmp.fechar();
		builder.comEnderecos(repoEnd.buscaPorPessoa(c.getInt(c.getColumnIndex(Pessoa.CODIGO))));
		repoEnd.fechar();
		builder.comEmails(repoEmail.buscaPorPessoa(c.getInt(c.getColumnIndex(Pessoa.CODIGO))));
		repoEmail.fechar();
		builder.comTelefones(repoTel.buscaPorPessoa(c.getInt(c.getColumnIndex(Pessoa.CODIGO)), c.getInt(c.getColumnIndex(Pessoa.EMPRESA))));
		repoTel.fechar();

		return builder;
	}

	// retorna cursor com todos os Pessoaes
	public Cursor getCursor() {
		try {
			// select * frm carros
			return db.query(Pessoa.NOME_TABELA, Pessoa.COLUNAS, null, null, null, null, null);
		} catch (SQLException e) {
			Log.e("Pessoa", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public Cursor getCursorPorRazao(int idEmpresa) {
		try {
			return db.query(Pessoa.NOME_TABELA, Pessoa.COLUNAS, Pessoa.EMPRESA + "=?",
					new String[] { String.valueOf(idEmpresa) }, null, null, Pessoa.RAZAOSOCIAL);
		} catch (SQLException e) {
			Log.e("Pessoa", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public Cursor getCursorPorFantasia() {
		try {
			// select * frm carros
			return db.query(Pessoa.NOME_TABELA, Pessoa.COLUNAS, null, null, null, null, Pessoa.NOMEFANTASIA);
		} catch (SQLException e) {
			Log.e("Pessoa", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public Cursor getCursorPorCnpj() {
		try {
			// select * frm carros
			return db.query(Pessoa.NOME_TABELA, Pessoa.COLUNAS, null, null, null, null, Pessoa.CGCCPF);
		} catch (SQLException e) {
			Log.e("Pessoa", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<Pessoa> listar() {
		Cursor c = getCursor();
		List<Pessoa> pessoas = Lists.newLinkedList();

		if (c.moveToFirst()) {
			do {
				Pessoa.Builder builder = montaBuilder(c);
				pessoas.add(builder.build());
			} while (c.moveToNext());
		}
		c.close();
		return pessoas;
	}

	public List<Pessoa> listarPorRazao(int idEmpresa) {
		Cursor c = getCursorPorRazao(idEmpresa);
		List<Pessoa> pessoas = new ArrayList<Pessoa>();

		if (c.moveToFirst()) {
			do {
				Pessoa.Builder builder = montaBuilder(c);
				pessoas.add(builder.build());
			} while (c.moveToNext());
		}
		c.close();
		return pessoas;
	}

	public List<Pessoa> listarPorFantasia() {
		Cursor c = getCursorPorFantasia();
		List<Pessoa> pessoas = new ArrayList<Pessoa>();

		if (c.moveToFirst()) {
			do {
				Pessoa.Builder builder = montaBuilder(c);
				pessoas.add(builder.build());
			} while (c.moveToNext());
			c.close();
		}

		return pessoas;
	}

	public List<Pessoa> listarPorCnpj() {
		Cursor c = getCursorPorCnpj();
		List<Pessoa> pessoas = new ArrayList<Pessoa>();

		if (c.moveToFirst()) {
			do {
				Pessoa.Builder builder = montaBuilder(c);
				pessoas.add(builder.build());
			} while (c.moveToNext());
		}
		c.close();
		return pessoas;
	}

	public boolean cnpjExiste(int idLoja, String cnpj) {
		String sql = "SELECT " + Pessoa.CODIGO + " FROM " + Pessoa.NOME_TABELA + " WHERE " + Pessoa.EMPRESA
				+ " = " + idLoja + " and " + Pessoa.CGCCPF + " = '" + cnpj + "'";

		Cursor c = db.rawQuery(sql, null);

		if (c.getCount() > 0) {
			return true;
		}

		return false;
	}

	public FaixaCodigoCliente leCodigoDisponivel(Empresa empresa) {
		String sql = "SELECT min(" + FaixaCodigoCliente.CODIGO + ") as " + FaixaCodigoCliente.CODIGO
				+ " FROM " + FaixaCodigoCliente.TABELA_FAIXA_CODIGO + " where " + FaixaCodigoCliente.EMPRESA
				+ " = " + empresa.getNumeroLoja() + " and " + FaixaCodigoCliente.UTILIZADO + " = 'N'";

		Cursor c = db.rawQuery(sql, null);
		int codigo = 0;

		if (c.getCount() > 0) {
			c.moveToFirst();

			codigo = c.getInt(c.getColumnIndex(FaixaCodigoCliente.CODIGO));

		}
		c.close();
		return FaixaCodigoCliente.newInstance(empresa.getNumeroLoja(), 0, codigo, false);
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

	private boolean faixaJaInserida(int cod, int loja) {
		Cursor c = db.query(FaixaCodigoCliente.TABELA_FAIXA_CODIGO, FaixaCodigoCliente.COLUNAS_FAIXA,
				FaixaCodigoCliente.CODIGO + " = " + String.valueOf(cod) + " and "
						+ FaixaCodigoCliente.EMPRESA + " = " + String.valueOf(loja), null, null, null, null);

		if (c.getCount() > 0) {
			c.close();
			return true;
		}
		c.close();
		return false;
	}

	public boolean salvarFaixa(List<FaixaCodigoCliente> faixa) {

		db.beginTransaction();
		try {

			db.delete(FaixaCodigoCliente.TABELA_FAIXA_CODIGO, null, null);
			for (FaixaCodigoCliente cod : faixa) {
				try {
					if (!faixaJaInserida(cod.getCodigoCliente(), cod.getCodigoLoja())) {
						ContentValues values = new ContentValues();

						values.put(FaixaCodigoCliente.CODIGO, cod.getCodigoCliente());
						values.put(FaixaCodigoCliente.EMPRESA, cod.getCodigoLoja());
						values.put(FaixaCodigoCliente.TIPO, "C");
						values.put(FaixaCodigoCliente.UTILIZADO, "N");
						try {
							db.insert(FaixaCodigoCliente.TABELA_FAIXA_CODIGO, "", values);
						} catch (Exception e) {
							e.printStackTrace();
						}

					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}

		return true;
	}

	public List<Pessoa> listarNaoSincronizados(int idLoja) {
		Cursor c = db.query(Pessoa.NOME_TABELA, Pessoa.COLUNAS, Pessoa.SYNC + "= 'N' and " + Pessoa.EMPRESA
				+ " = " + String.valueOf(idLoja), null, null, null, Pessoa.RAZAOSOCIAL);

		RepositorioEmpresa repoEmp = new RepositorioEmpresa(context);
		Empresa empresa = repoEmp.busca(idLoja);
		repoEmp.fechar();
		List<Pessoa> pessoas = new ArrayList<Pessoa>();
		if (c.moveToFirst()) {

			do {
				Pessoa pessoa = montaBuilder(c).comEmpresa(empresa).build();
				pessoas.add(pessoa);
			} while (c.moveToNext());
		}
		c.close();

		return pessoas;
	}

	public void updateLista(List<Pessoa> pessoas) {
		db.beginTransaction();
		try {
			for (Pessoa p : pessoas) {
				ContentValues values = this.montaValues(p);
				db.update(Pessoa.NOME_TABELA, values, Pessoa.CODIGO + "= " + p.getCodigo(), null);
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	public void deletarPessoasSemRelacionamentos(int idLoja) {
		Cursor c = db.query(Pessoa.NOME_TABELA, new String[] { Pessoa.CODIGO, Pessoa.EMPRESA },
				Pessoa.EMPRESA + "=" + idLoja, null, null, null, null);

		if (c.moveToFirst()) {
			do {
				try {
					db.delete(Pessoa.NOME_TABELA,
							Pessoa.CODIGO + "=" + c.getInt(c.getColumnIndex(Pessoa.CODIGO)), null);
				} catch (Exception e) {
					Log.d("erro ao excluir", "cliente possui relacionamento com outros");
				}

			} while (c.moveToNext());
		}

	}

	public List<PessoaParaLista> listarParaTela(int empresaId) {

		Cursor c = db.query(Pessoa.NOME_TABELA, new String[] { Pessoa.CODIGO, Pessoa.RAZAOSOCIAL,
				Pessoa.NOMEFANTASIA, Pessoa.CGCCPF, Pessoa.INSCRG, Pessoa.EMPRESA }, Pessoa.EMPRESA + "="
				+ empresaId, null, null, null, Pessoa.RAZAOSOCIAL);
		Log.d("debug", "query retornando " + String.valueOf(c.getCount()) + " resultados para pessoas");
		List<PessoaParaLista> pessoas = Lists.newLinkedList();
		while (c.moveToNext()) {
			PessoaParaLista p = PessoaParaLista.newInstance(c.getInt(c.getColumnIndex(Pessoa.CODIGO)),
					c.getString(c.getColumnIndex(Pessoa.RAZAOSOCIAL)),
					c.getString(c.getColumnIndex(Pessoa.NOMEFANTASIA)),
					c.getString(c.getColumnIndex(Pessoa.CGCCPF)),
					c.getString(c.getColumnIndex(Pessoa.INSCRG)), c.getInt(c.getColumnIndex(Pessoa.EMPRESA)));
			pessoas.add(p);
		}
		c.close();
		Log.d("debug", String.valueOf(pessoas.size()) + " encontradas no banco.");
		return pessoas;
	}

	public void deletarFaixar() {
		db.delete(FaixaCodigoCliente.TABELA_FAIXA_CODIGO, null, null);

	}

	public List<SolicitacaoPerfil> buildSolicitacaoDePerfil() {
		String sql = "select CLF_CODIGO, EMP_NUMEROLOJA from FIA_PESSOA inner join FIA_EMPRESA on FIA_EMPRESA_EMP_ID = EMP_ID";
		List<SolicitacaoPerfil> solicitacoes = Lists.newLinkedList();

		Cursor c = db.rawQuery(sql, null);
		int codigo = c.getColumnIndex("CLF_CODIGO");
		int loja = c.getColumnIndex("EMP_NUMEROLOJA");
		if (c.moveToFirst()) {
			do {
				SolicitacaoPerfil solicitacao = SolicitacaoPerfil.newInstance(
						new BigDecimal(c.getInt(codigo)), c.getLong(loja));
				solicitacoes.add(solicitacao);
			} while (c.moveToNext());
		}
		c.close();
		return solicitacoes;
	}

}
