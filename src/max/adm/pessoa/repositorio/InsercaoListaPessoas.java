package max.adm.pessoa.repositorio;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Map;

import max.adm.classificacaoPessoa.RepositorioClassificacaoPessoa;
import max.adm.empresa.RepositorioEmpresa;
import max.adm.entidade.ClassifcacaoPessoa;
import max.adm.entidade.Empresa;
import max.adm.entidade.Pessoa;
import android.content.Context;

import com.google.common.collect.Maps;

public class InsercaoListaPessoas {
	
	private Context ctx;
	private List<Pessoa> pessoas;
	
	private Map<String, ClassifcacaoPessoa> classificacoes = Maps.newHashMap();
	private Map<Integer, Empresa> empresas = Maps.newHashMap();
	
	private InsercaoListaPessoas(Context ctx, List<Pessoa> pessoas ) {
		this.ctx = ctx;
		this.pessoas = pessoas;
		RepositorioClassificacaoPessoa repo = new RepositorioClassificacaoPessoa(ctx);
		List<ClassifcacaoPessoa> classifs = repo.listarTodas();
		repo.fechar();
		for(ClassifcacaoPessoa cla: classifs){ 
			classificacoes.put(cla.getCodigo(), cla); 
		}
		
		RepositorioEmpresa repoEmp = new RepositorioEmpresa(ctx);
		List<Empresa> empresas = repoEmp.listar();
		repoEmp.fechar();
		for(Empresa e : empresas) {
			this.empresas.put(e.getNumeroLoja(), e);
		}
	}
	
	public static InsercaoListaPessoas newInstance(Context ctx, List<Pessoa> pessoas){
		checkNotNull(ctx);
		return new InsercaoListaPessoas(ctx, pessoas);
	}
	
	public void salvar(){
		RepositorioPessoa repo = new RepositorioPessoa(ctx);
		repo.deleteAll();
		for (Pessoa p: pessoas){
			if(empresas.containsKey(p.getEmpresa().getNumeroLoja())) {
				p.setEmpresa(empresas.get(p.getEmpresa().getNumeroLoja()));
			}
		}
		repo.salvarLista(pessoas);
		repo.fechar();
	}
	
}
