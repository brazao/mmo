package max.adm.pessoa;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

/**
 * Objeto pessoa somente com id, razao, fantasia, cnpj, insc Estadual, loja
 * @author sutil
 *
 */
public class PessoaParaLista implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String razao;
	private String fantasia;
	private String cpf;
	private String insc;
	private int loja;
	
	private PessoaParaLista(int id, String razao, String fantasia, String cpf,
			String insc, int loja) {
		this.id = id;
		this.razao = razao;
		this.fantasia = fantasia;
		this.cpf = cpf;
		this.insc = insc;
		this.loja = loja;
	}
	
	public static PessoaParaLista newInstance(int id, String razao, String fantasia, String cpf,
			String insc, int loja){
		checkArgument(id > 0);
		checkNotNull(razao);
		checkNotNull(cpf);
		checkArgument(loja>0);
		return new PessoaParaLista(id, razao, fantasia, cpf, insc, loja);
	}

	public int getId() {
		return id;
	}

	public String getRazao() {
		return razao;
	}

	public String getFantasia() {
		return fantasia;
	}

	public String getCpf() {
		return cpf;
	}

	public String getInsc() {
		return insc;
	}

	public int getLoja() {
		return loja;
	}

	@Override
	public String toString() {
		return this.razao;
	}
	
	
}
