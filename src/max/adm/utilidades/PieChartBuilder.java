/**
 * Copyright (C) 2009, 2010 SC 4ViewSoft SRL
 *  
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package max.adm.utilidades;

import java.util.ArrayList;
import java.util.List;

import max.adm.R;
import max.adm.entidade.GrupoDeProdutos;
import max.adm.entidade.PedidoItens;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.SeriesSelection;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.Toast;

public class PieChartBuilder extends Activity {
	public static final String TYPE = "type";

	private static int[] COLORS = new int[]{Color.GREEN, Color.BLUE, Color.MAGENTA, Color.CYAN, Color.GRAY, Color.YELLOW};

	private CategorySeries mSeries = new CategorySeries("");

	private DefaultRenderer mRenderer = new DefaultRenderer();

	private String mDateFormat;

	private GraphicalView mChartView;

	List<PedidoItens> itens = new ArrayList<PedidoItens>();

	List<GrupoDeProdutos> grupos = new ArrayList<GrupoDeProdutos>();

	@Override
	protected void onRestoreInstanceState(Bundle savedState) {
		super.onRestoreInstanceState(savedState);
		mSeries = (CategorySeries) savedState.getSerializable("current_series");
		mRenderer = (DefaultRenderer) savedState.getSerializable("current_renderer");
		mDateFormat = savedState.getString("date_format");
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable("current_series", mSeries);
		outState.putSerializable("current_renderer", mRenderer);
		outState.putString("date_format", mDateFormat);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.xy_chart);
		// mX = (EditText) findViewById(R.id.xValue);
		mRenderer.setApplyBackgroundColor(true);
		mRenderer.setBackgroundColor(Color.argb(100, 50, 50, 50));
		mRenderer.setChartTitleTextSize(20);
		mRenderer.setLabelsTextSize(15);
		mRenderer.setLegendTextSize(15);
		mRenderer.setMargins(new int[]{20, 30, 15, 0});
		mRenderer.setZoomButtonsVisible(true);
		mRenderer.setStartAngle(90);

		if (getIntent().getExtras() != null) {
			itens = (List<PedidoItens>) getIntent().getExtras().getSerializable("ITENSPEDIDO");

		}

		GrupoDeProdutos grupo;
		boolean contemVazio = false;
		for (PedidoItens it : itens) {
			grupo = it.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto().getGrupoDeProduto();
			if (grupo != null) {
				if (!grupos.contains(grupo)) {
					grupos.add(grupo);
				}
			} else {
				if (!contemVazio) {
					grupo = new GrupoDeProdutos();
					grupo.setDescricao("Vazio");
					grupos.add(grupo);
					contemVazio = true;
				}
			}
		}

		Double[] qtdes = new Double[grupos.size()];
		for (int k = 0; k < qtdes.length; k++) {
			qtdes[k] = 0.0;

		}
		double totalDePecas = 0;
		for (int i = 0; i < grupos.size(); i++) {
			grupo = grupos.get(i);
			for (PedidoItens p : itens) {
				if (grupo.equals(p.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto().getGrupoDeProduto())) {
					qtdes[i] += p.getQuantidade();
					totalDePecas += p.getQuantidade();
				}
			}
		}

		double percentual = 0;
		for (int j = 0; j < grupos.size(); j++) {
			if (qtdes[j] > 0) {
				percentual = ((qtdes[j] * 100) / totalDePecas);
				mSeries.add(grupos.get(j).getDescricao() + " (" + MascaraDecimal.mascaradecimal(percentual, MascaraDecimal.DECIMAL_DUAS_CASAS) + "%)", qtdes[j]);
				SimpleSeriesRenderer renderer = new SimpleSeriesRenderer();
				renderer.setColor(COLORS[(mSeries.getItemCount() - 1) % COLORS.length]);
				mRenderer.addSeriesRenderer(renderer);
				if (mChartView != null) {
					mChartView.repaint();
				}
			}

		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mChartView == null) {
			LinearLayout layout = (LinearLayout) findViewById(R.id.chart);
			mChartView = ChartFactory.getPieChartView(this, mSeries, mRenderer);
			mRenderer.setClickEnabled(true);
			mRenderer.setSelectableBuffer(10);
			mChartView.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					SeriesSelection seriesSelection = mChartView.getCurrentSeriesAndPoint();
					if (seriesSelection != null) {
						Toast.makeText(PieChartBuilder.this, seriesSelection.getValue() + " peças", Toast.LENGTH_SHORT).show();
					}
				}
			});

			layout.addView(mChartView, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		} else {
			mChartView.repaint();
		}
	}
}
