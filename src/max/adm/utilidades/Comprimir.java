package max.adm.utilidades;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import com.google.common.io.ByteStreams;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class Comprimir {
	
	private static Gson getGson() {
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(Date.class, new JsonSerializer<Date>() {
			@Override
			public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
				return new JsonPrimitive(src.getTime());
			}
		});
		
		builder.registerTypeAdapter(Date.class , new JsonDeserializer<Date>() {
			@Override
			public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
				return new Date(json.getAsLong());
			}
		});
		
		
		return builder.create();
	}

	public static byte[] transformToByteArray(Object obj) {
		Gson gson = getGson();
		String json = gson.toJson(obj);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream(64000);
		GZIPOutputStream gzipOutputStream;
		try {
			gzipOutputStream = new GZIPOutputStream(baos);
			gzipOutputStream.write(json.getBytes("UTF-8"));
			gzipOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return baos.toByteArray();
	}

	public static <T> T byteArrayToObject(byte[] bytes, Class<T> tipo) {
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		GZIPInputStream gzipInputStream;
		byte[] byteArray = null;
		try {
			gzipInputStream = new GZIPInputStream(bais);
			byteArray = ByteStreams.toByteArray(gzipInputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Gson gson = getGson();
		T obj = gson.fromJson(new String(byteArray), tipo);
		return obj;
	}

	public static <T> List<T> byteArrayToList(byte[] bytes, Class<T> listOf, Type typeToken) {
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		GZIPInputStream gzipInputStream;
		byte[] byteArray = null;
		try {
			gzipInputStream = new GZIPInputStream(bais);
			byteArray = ByteStreams.toByteArray(gzipInputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Gson gson = getGson();
		String json = new String(byteArray);
		System.out.println(json);
		List<T> list = gson.fromJson(json, typeToken);
		return list;
	}

	public static <T> byte[] listToByteArray(List<T> list, Type typeToken) {
		Gson gson = getGson();
		String json = gson.toJson(list, typeToken);
		ByteArrayOutputStream baos = new ByteArrayOutputStream(64000);
		GZIPOutputStream gzipOutputStream;
		try {
			gzipOutputStream = new GZIPOutputStream(baos);
			gzipOutputStream.write(json.getBytes("UTF-8"));
			gzipOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return baos.toByteArray();
	}

}
