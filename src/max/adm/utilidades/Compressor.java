package max.adm.utilidades;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class Compressor {

	private static Compressor compressor;

	private Compressor() {

	}

	static Compressor getInstance() {
		if (compressor == null) {
			compressor = new Compressor();
		}
		return compressor;
	}

	public byte[] postBodyCompressor(String postBodyDecompressed) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ZipOutputStream zout = new ZipOutputStream(out);
		byte[] compressed = null;
		try {
			zout.putNextEntry(new ZipEntry("0"));
			zout.write(postBodyDecompressed.getBytes());
			zout.closeEntry();
			compressed = out.toByteArray();
			zout.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return compressed;
	}

	public String postBodyDecompressor(byte[] postBodyCompressed) {
		String postBodyDecompressed = null;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ByteArrayInputStream in = new ByteArrayInputStream(postBodyCompressed);
		ZipInputStream zin = new ZipInputStream(in);
		ZipEntry entry;
		byte[] buffer;
		try {
			entry = zin.getNextEntry();
			buffer = new byte[1024];
			int offset = -1;
			while ((offset = zin.read(buffer)) != -1) {
				out.write(buffer, 0, offset);
			}

			postBodyDecompressed = out.toString();
			out.close();
			zin.close();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		System.out.println("byte " + postBodyCompressed);
		System.out.println("str " + postBodyDecompressed);
		return postBodyDecompressed;
	}

}
