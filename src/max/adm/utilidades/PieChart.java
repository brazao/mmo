/**
 * Copyright (C) 2009, 2010 SC 4ViewSoft SRL
 *  
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package max.adm.utilidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.SeriesSelection;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;

import com.google.common.collect.Maps;

public class PieChart implements Serializable {

	private static final long serialVersionUID = 1L;
	

	private static int[] COLORS = new int[]{Cor.vermelho,  Cor.verde,  Cor.azul, Cor.roxo
		, Cor.amarelo,  Color.GREEN, Color.BLUE, Color.MAGENTA, Color.CYAN, Color.GRAY, Color.YELLOW};
	
	
	private CategorySeries mSeries = new CategorySeries("");
	private DefaultRenderer mRenderer = new DefaultRenderer();
	private GraphicalView mChartView;
	private Map<String, BigDecimal> values = Maps.newHashMap();
	Context ctx;
	

	public PieChart(Context ctx, Map<String, BigDecimal> values, String title) {
		this.values = values;
		this.ctx = ctx;
		mRenderer.setApplyBackgroundColor(true);
		mRenderer.setBackgroundColor(Color.argb(50, 200, 200, 200));
		mRenderer.setChartTitleTextSize(10);
		mRenderer.setLabelsTextSize(10);
		mRenderer.setLegendTextSize(10);
		mRenderer.setMargins(new int[]{3, 3, 3, 3});
		mRenderer.setZoomButtonsVisible(true);
		mRenderer.setStartAngle(90);
		mRenderer.setInScroll(true);
		
		BigDecimal percentual = BigDecimal.ZERO;
		BigDecimal total = getTotal();
		for(Map.Entry<String, BigDecimal> entry : values.entrySet()) {
			percentual = entry.getValue().multiply(new BigDecimal(100)).divide(total, RoundingMode.HALF_EVEN);
			System.out.println("valor do gráfico "+entry.getValue());
			System.out.println("percent "+percentual);
			mSeries.add(entry.getKey() + MascaraDecimal.mascaradecimal(percentual.doubleValue(), MascaraDecimal.DECIMAL_DUAS_CASAS)+" %", entry.getValue().doubleValue());
			SimpleSeriesRenderer renderer = new SimpleSeriesRenderer();
			renderer.setColor(COLORS[(mSeries.getItemCount() - 1) % COLORS.length]);
			mRenderer.addSeriesRenderer(renderer);
			if (mChartView != null) {
				mChartView.repaint();
			}
		}

	}
	
	private BigDecimal getTotal() {
		BigDecimal total = BigDecimal.ZERO;
		for(Map.Entry<String, BigDecimal> entry : values.entrySet()) {
			total = total.add(entry.getValue());
		}
		System.err.println("total "+total);
		return total.doubleValue() > 0 ? total : new BigDecimal(1);
	}
	
	

	public LinearLayout getView() {
		LinearLayout layout = new LinearLayout(ctx);
		if (mChartView == null) {
			mChartView = ChartFactory.getPieChartView(ctx, mSeries, mRenderer);
			mRenderer.setClickEnabled(true);
			mRenderer.setSelectableBuffer(10);
			mChartView.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					SeriesSelection seriesSelection = mChartView.getCurrentSeriesAndPoint();
					if (seriesSelection != null) {
//						Toast.makeText(PieChart.this, seriesSelection.getValue() , Toast.LENGTH_SHORT).show();
					}
				}
			});

			layout.addView(mChartView, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		} else {
			mChartView.repaint();
		}
		return layout;
	}
}
