package max.adm.utilidades;

public class Mascara {
	
	public static String poeMascaraCNPJ(String str){
		
		if (str.equals("") || str == null){
			return str;
		}
		
		
		str = tiraMascara(str);
		
		if (str.length() != 14){
			return str;
		}
		
		String part1 = str.substring(0, 2);
		String part2 = str.substring(2, 5);
		String part3 = str.substring(5, 8);
		String part4 = str.substring(8, 12);
		String part5 = str.substring(12, 14);
		str = part1 + "." + part2 + "." + part3 + "/" + part4 + "-" + part5;
		
		return str;
	}
	
	public static String poemascaraCPF(String str){
		
		if (str.equals("") || str == null){
			return str;
		}
		
		
		str = tiraMascara(str);
		
		if (str.length() != 11){
			return str;
		}
		
		String parte1 = str.substring(0, 3);
		String parte2 = str.substring(3, 6);
		String parte3 = str.substring(6, 9);
		String parte4 = str.substring(9, 11);
		str = parte1 + "." + parte2 + "." + parte3 + "-"
				+ parte4;
		
		return str;
	}
	
	public static String poeMascaraCep(String cep){
		if (cep.length() == 8){
			String part1 = "";
			String part2 = "";
			part1 = cep.substring(0, 5);
			part2 = cep.substring(5, 8);
			cep = part1+"-"+part2;
		}
		
		return cep;
	}
	
	
	public static String tiraMascara(String str){
		
		String strSemMascara = "";
		
		for (int i= 0; i<str.length(); i++){
	    	   if (!str.substring(i, i+1).equals(" ") && !str.substring(i, i+1).equals("-") && !str.substring(i, i+1).equals(".")&& !str.substring(i, i+1).equals("/"))
	    		   strSemMascara += str.substring(i, i+1);
	       }
		return strSemMascara;
	}

}
