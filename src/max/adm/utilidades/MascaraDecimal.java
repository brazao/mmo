package max.adm.utilidades;


import java.math.BigDecimal;
import java.text.DecimalFormat;


public class MascaraDecimal {
	
	public static final DecimalFormat DECIMAL_DUAS_CASAS = new DecimalFormat("########0.00");

	public static String comDuasCasaDecimais(BigDecimal valor){  
        return DECIMAL_DUAS_CASAS.format(valor);  
    }  
	
	public static String mascaradecimal(double valor, DecimalFormat formato){  
        return formato.format(valor);  
    }  
}
