package max.adm.utilidades.adapters;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import max.adm.dominio.Entidade;
import max.adm.entidade.Produto;
import max.adm.entidade.TabelaPrecoProdutoReduzido;

import com.google.common.base.Objects;

public class ProdutoItemPedidoParaListar extends Entidade {

	private static final long serialVersionUID = 1L;

	private Produto produto;
	private List<TabelaPrecoProdutoReduzido> reduzidos;

	private ProdutoItemPedidoParaListar(Produto produto, List<TabelaPrecoProdutoReduzido> reduzidos) {
		this.produto = produto;
		this.reduzidos = reduzidos;
	}

	public static ProdutoItemPedidoParaListar newInstance(Produto produto, List<TabelaPrecoProdutoReduzido> reduzidos) {
		checkNotNull(produto);
		checkNotNull(reduzidos);
		return new ProdutoItemPedidoParaListar(produto, reduzidos);
	}

	public Produto getProduto() {
		return produto;
	}

	public List<TabelaPrecoProdutoReduzido> getReduzidos() {
		return reduzidos;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.produto);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ProdutoItemPedidoParaListar) {
			ProdutoItemPedidoParaListar other = (ProdutoItemPedidoParaListar) obj;
			return Objects.equal(this.produto, other.produto);
		}
		return false;
	}

	@Override
	public String toString() {
		return produto.getMascaraCodigo() + " " + produto.getDescricao();
	}

}
