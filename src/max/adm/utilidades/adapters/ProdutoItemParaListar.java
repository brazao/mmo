package max.adm.utilidades.adapters;

import java.util.Arrays;
import java.util.List;

import max.adm.dominio.Entidade;
import max.adm.entidade.Produto;
import max.adm.entidade.ProdutoReduzido;

public class ProdutoItemParaListar extends Entidade{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private byte[] imagem;
	private Produto produto;
	private List<ProdutoReduzido> reduzidos;
	private boolean mostraPreco;
	
	

	public boolean isMostraPreco() {
		return mostraPreco;
	}

	public void setMostraPreco(boolean mostraPreco) {
		this.mostraPreco = mostraPreco;
	}

	public byte[] getImagem() {
		return imagem;
	}

	public void setImagem(byte[] imagem) {
		this.imagem = imagem;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public List<ProdutoReduzido> getReduzidos() {
		return reduzidos;
	}

	public void setReduzidos(List<ProdutoReduzido> reduzidos) {
		this.reduzidos = reduzidos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(imagem);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProdutoItemParaListar other = (ProdutoItemParaListar) obj;
		if (!Arrays.equals(imagem, other.imagem))
			return false;
		return true;
	}
	
	

}
