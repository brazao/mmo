package max.adm.utilidades.adapters;

import java.util.ArrayList;
import java.util.List;

import max.adm.entidade.Pessoa;

public class ItemSincronismoPessoaEnvio {
	public Pessoa pessoa;
	public boolean marcado;
	
	public static List<ItemSincronismoPessoaEnvio> preencheLista(List<Pessoa> pessoas){
		List<ItemSincronismoPessoaEnvio> lista = new ArrayList<ItemSincronismoPessoaEnvio>();
		for(Pessoa p : pessoas){
			ItemSincronismoPessoaEnvio item = new ItemSincronismoPessoaEnvio();
			item.pessoa = p;
			item.marcado = false;
			lista.add(item);
		}
		return lista;
	}
}
