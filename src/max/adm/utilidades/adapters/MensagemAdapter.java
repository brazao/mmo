package max.adm.utilidades.adapters;

import java.text.SimpleDateFormat;
import java.util.List;

import max.adm.R;
import max.adm.entidade.Mensagem;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MensagemAdapter extends BaseAdapter {

	SimpleDateFormat formatData = new SimpleDateFormat("dd/MM/yy");
	LayoutInflater inflater;
	Context context;
	List<Mensagem> mensagens;

	public MensagemAdapter(Context context, List<Mensagem> mensagens) {
		this.context = context;
		this.mensagens = mensagens;
		inflater = LayoutInflater.from(context);
	}

	public int getCount() {
		return mensagens.size();
	}

	public Object getItem(int position) {
		return mensagens.get(position);
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		System.out.println("entrou no adapter");
		final Mensagem mensagem = mensagens.get(position);

		

			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			convertView = inflater.inflate(R.layout.modelo_item_mensagem, null);

			TextView remetente = (TextView) convertView
					.findViewById(R.id.text_remetente_list_mensagem);
			TextView data = (TextView) convertView
					.findViewById(R.id.text_data_list_mensagem);
			TextView respondida = (TextView) convertView
					.findViewById(R.id.text_respondida_list_mensagem);

			remetente.setText(mensagem.getRemetente());
			data.setText(formatData.format(mensagem.getDataEnvio()));

			if (mensagem.getResposta() != null) {
				respondida.setText("Sim");
			} else {
				respondida.setText("Não");
			}

			return convertView;
		
		
		
	}

}
