package max.adm.utilidades.adapters;

import java.util.ArrayList;
import java.util.List;

import max.adm.pedido.PedidoDoc;

public class ItemSincronismoPedidoEnvio {
	public PedidoDoc pedido;
	public boolean marcado;

	public static List<ItemSincronismoPedidoEnvio> preencheLista(List<PedidoDoc> pedidos) {
		List<ItemSincronismoPedidoEnvio> lista = new ArrayList<ItemSincronismoPedidoEnvio>();
		for (PedidoDoc p : pedidos) {
			ItemSincronismoPedidoEnvio item = new ItemSincronismoPedidoEnvio();
			item.pedido = p;
			item.marcado = false;
			lista.add(item);
		}
		return lista;
	}
}
