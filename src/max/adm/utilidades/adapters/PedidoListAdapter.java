package max.adm.utilidades.adapters;

import java.text.SimpleDateFormat;
import java.util.List;

import max.adm.R;
import max.adm.entidade.PedidoCapa;
import max.adm.entidade.PedidoItens;
import max.adm.relatorios.ItemRelatorioListaPedido;
import max.adm.utilidades.MascaraDecimal;
import max.adm.utilidades.adapters.ProdutoListAdapter.ViewHolder;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PedidoListAdapter extends BaseAdapter {
	SimpleDateFormat formatData = new SimpleDateFormat("dd/MM/yyyy");

	private Context context;
	private List<ItemRelatorioListaPedido> pedidoList;
	

	public PedidoListAdapter(Context context, List<ItemRelatorioListaPedido> pedidoList) {
		this.context = context;
		this.pedidoList = pedidoList;
	}

	public int getCount() {
		return pedidoList.size();
	}

	public Object getItem(int positon) {
		return pedidoList.get(positon);
	}

	public long getItemId(int position) {
		return pedidoList.get(position).idPedido;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		final ItemRelatorioListaPedido pedido = pedidoList.get(position);
		
		final ViewHolder holder;

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			convertView = inflater.inflate(R.layout.modelo_item_pedido_list, null);

			holder = new ViewHolder();
			
			holder.numPedido = (TextView) convertView.findViewById(R.id.text_num_pedido_list);
			holder.nomeCliente = (TextView) convertView.findViewById(R.id.text_cliente_pedido_list);
			holder.dataemissao = (TextView) convertView.findViewById(R.id.text_data_pedido_list);
			holder.vlrBruto = (TextView) convertView.findViewById(R.id.text_valor_bruto_pedido_list);
			holder.valorDesconto = (TextView) convertView.findViewById(R.id.text_desconto_pedido_list);
			holder.valorLiquido = (TextView) convertView.findViewById(R.id.text_valor_liquido_pedido_list);
			
			convertView.setTag(holder);
			
		}
		else{
			holder = (ViewHolder) convertView.getTag();
		}
			
		
		holder.numPedido.setText(pedido.getNumeroPedido());

		holder.nomeCliente.setText(pedido.getNomeCliente());

		holder.dataemissao.setText(pedido.getDataEmissaoPedido());

		holder.vlrBruto.setText(pedido.getValorBruto());

		
		// calcular desconto
		
		holder.valorDesconto.setText(pedido.getValorDesconto());

		holder.valorLiquido.setText(pedido.getValorLiquido());

		

		return convertView;
		
	}
	
	static class ViewHolder {
		TextView numPedido;
		TextView nomeCliente;
		TextView dataemissao;
		TextView vlrBruto;
		TextView valorDesconto;
		TextView valorLiquido;
		
    }
}
