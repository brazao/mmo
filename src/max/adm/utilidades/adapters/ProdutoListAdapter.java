package max.adm.utilidades.adapters;

import java.io.ByteArrayInputStream;
import java.util.List;

import max.adm.R;
import max.adm.entidade.Produto;
import max.adm.entidade.ProdutoReduzido;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ProdutoListAdapter extends BaseAdapter {

	private Context context;
	private List<ProdutoItemParaListar> produtos;
	LayoutInflater inflater;

	public ProdutoListAdapter(Context context,
		List<ProdutoItemParaListar> produtos) {
		this.context = context;
		this.produtos = produtos;
		inflater = LayoutInflater.from(context);
	}

	public int getCount() {
		return produtos.size();
	}

	public Produto getItem(int position) {
		return produtos.get(position).getProduto();
	}

	public long getItemId(int position) {
		return this.getItem(position).getCodigo();
	}
	


	public View getView(int position, View convertView, ViewGroup parent) {
		final ProdutoItemParaListar produto = produtos.get(position);
		
		final ViewHolder holder;

		if (convertView == null) {
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.modelo_item_produto, null);
			
			holder = new ViewHolder();

			holder.imagemProduto = (ImageView) convertView.findViewById(R.id.imgReferencia);
			holder.codigo = (TextView) convertView.findViewById(R.id.text_codigo_referencia);
			holder.descricao = (TextView) convertView.findViewById(R.id.textDescricaoReferencia);
			holder.textPadroes = (TextView) convertView.findViewById(R.id.textPadroes);
			holder.textTam = (TextView) convertView.findViewById(R.id.textGrades);
			holder.textPreco = (TextView) convertView.findViewById(R.id.textPreco);
			holder.bt_mais_info = (ImageView) convertView.findViewById(R.id.bt_mais_info_prod);
			holder.textTituloPreco = (TextView) convertView.findViewById(R.id.text_titulo_preco);
			
			
			convertView.setTag(holder);
			
		}
		
		else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		
			holder.codigo.setText(produto.getProduto().getMascaraCodigo());

			
			holder.descricao.setText(produto.getProduto().getDescricao());

			
			
			byte[] img = produto.getImagem();

			if (img != null) {
				
				ByteArrayInputStream is = new ByteArrayInputStream(img);
				Drawable drw = Drawable.createFromStream(is, "articleImage");
				holder.imagemProduto.setImageDrawable(drw);

			}

			
			String padroes = "";
			String tamanhos = "";
			String preco = "";

			if (produto.getReduzidos() != null) {
				if (img == null){
					holder.imagemProduto.setImageResource(R.drawable.vazio);
				}
				for (int i = 0; i < produto.getReduzidos().size(); i++) {
					List<ProdutoReduzido> reds = produto.getReduzidos();
					if (reds != null) {
						if (reds.get(i) != null) {
							if (reds.get(i).getPadrao() != null) {
								padroes += reds.get(i).getPadrao()
										.getDescricao() + '\n';
							}
							if (reds.get(i).getTamanho() != null) {
								tamanhos += reds.get(i).getTamanho()
										.getDescricao() + '\n';
							}
//							if (reds.get(i).getPreco() != 0) {
//								preco += "R$ "
//										+ (MascaraDecimal
//												.mascaradecimal(
//														reds.get(i).getPreco(),
//														MascaraDecimal.DECIMAL_DUAS_CASAS))
//										+ '\n';
//							}
						}

					}

				}// fim do for
				holder.textPadroes.setText(padroes);
				holder.textTam.setText(tamanhos);

			}

			
			final String mostraPreco = preco;

			

			if (produto.isMostraPreco()) {
				holder.textPreco.setText(mostraPreco);
				holder.textTituloPreco.setText("Preço:");
				holder.bt_mais_info.setImageResource(R.drawable.menos_dados_medio);

			}
			else {
				holder.textPreco.setText("");
				holder.textTituloPreco.setText("");
				holder.bt_mais_info.setImageResource(R.drawable.mais_dados_medio);
			}

			holder.bt_mais_info.setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {

					if (produto.isMostraPreco() == false) {
						produto.setMostraPreco(true);
						holder.textPreco.setText(mostraPreco);
						holder.textTituloPreco.setText("Preço:");
						holder.bt_mais_info
								.setImageResource(R.drawable.menos_dados_medio);

					} else {
						produto.setMostraPreco(false);
						holder.textPreco.setText("");
						holder.textTituloPreco.setText("");
						holder.bt_mais_info.setImageResource(R.drawable.mais_dados_medio);
					}

				}
			});

			return convertView;
		
	}
	
	static class ViewHolder {
		TextView codigo;
		TextView descricao;
		ImageView imagemProduto;
		TextView textPadroes;
		TextView textTam;
		TextView textPreco;
		ImageView bt_mais_info;
		TextView textTituloPreco;
		
    }

}
