package max.adm.utilidades.adapters;

import java.util.List;

import max.adm.R;
import max.adm.entidade.TabelaPrecoProdutoReduzido;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.google.common.collect.Lists;

public class ProdutoListPedidoAdapter extends BaseAdapter implements Filterable {

	private Context context;
	private List<ProdutoItemPedidoParaListar> produtos;
	private List<ProdutoItemPedidoParaListar> copy;
	private LayoutInflater inflater;

	public ProdutoListPedidoAdapter(Context context, List<ProdutoItemPedidoParaListar> produtos) {
		this.context = context;
		this.produtos = produtos;
		this.copy = Lists.newLinkedList(produtos);
		inflater = LayoutInflater.from(context);
	}

	public int getCount() {
		return produtos.size();
	}

	public ProdutoItemPedidoParaListar getItem(int position) {
		return produtos.get(position);
	}

	public long getItemId(int position) {
		return produtos.get(position).getProduto().getCodigo();
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final ProdutoItemPedidoParaListar produto = produtos.get(position);
		final ViewHolder holder;

		if (convertView == null) {
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.modelo_item_produto_para_pedido, null);
			holder = new ViewHolder();
			holder.codigo = (TextView) convertView.findViewById(R.id.text_codigo_referencia_para_pedido);
			holder.descricao = (TextView) convertView.findViewById(R.id.textDescricaoReferencia_para_pedido);
			holder.textPadroes = (TextView) convertView.findViewById(R.id.textPadroes_para_pedido);
			holder.textTam = (TextView) convertView.findViewById(R.id.textGrades_para_pedido);
			holder.textPreco = (TextView) convertView.findViewById(R.id.textPreco_para_pedido);
			holder.textTitloPreco = (TextView) convertView.findViewById(R.id.text_titulo_preco_para_pedido);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.codigo.setText(produto.getProduto().getMascaraCodigo());
		holder.descricao.setText(produto.getProduto().getDescricao());
		holder.textPadroes.setText(getPadroes(produto.getReduzidos()));
		holder.textTam.setText(getTamanhos(produto.getReduzidos()));
		holder.textPreco.setText(getPrecos(produto.getReduzidos()));
		holder.textTitloPreco.setText("Preço");
		// final String mostraPreco = preco;

		return convertView;

	}

	private class ViewHolder {

		TextView codigo;
		TextView descricao;
		TextView textPadroes;
		TextView textTam;
		TextView textPreco;
		TextView textTitloPreco;
		// LinearLayout guardaImagem;

	}

	private String getPadroes(List<TabelaPrecoProdutoReduzido> reds) {
		StringBuilder sb = new StringBuilder();
		for (TabelaPrecoProdutoReduzido r : reds) {
			sb.append(r.getProdutoReduzido().getPadrao().getDescricao()).append("\n");
		}
		return sb.toString();
	}

	private String getTamanhos(List<TabelaPrecoProdutoReduzido> reds) {
		StringBuilder sb = new StringBuilder();
		for (TabelaPrecoProdutoReduzido r : reds) {
			sb.append(r.getProdutoReduzido().getTamanho().getCodigo()).append("\n");
		}
		return sb.toString();
	}

	private String getPrecos(List<TabelaPrecoProdutoReduzido> tabReds) {
		StringBuilder sb = new StringBuilder();
		for (TabelaPrecoProdutoReduzido r : tabReds) {
			sb.append(r.getPreco().formatToScreen()).append("\n");
		}
		return sb.toString();
	}

	@Override
	public Filter getFilter() {
		Filter filter = new Filter() {

			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				if (results != null && results.count > 0) {
					produtos = (List<ProdutoItemPedidoParaListar>) results.values;
					notifyDataSetChanged();
				} else {
					produtos = Lists.newLinkedList(copy);
					notifyDataSetInvalidated();
				}

			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults filterResults = new FilterResults();
				List<ProdutoItemPedidoParaListar> results = Lists.newLinkedList();
				if (constraint != null) {
					if (copy != null && !copy.isEmpty()) {
						for (ProdutoItemPedidoParaListar prod : copy) {
							if (prod.getProduto().getMascaraCodigo().toUpperCase().contains(constraint.toString().toUpperCase())
									|| prod.getProduto().getDescricao().toUpperCase().contains(constraint.toString().toUpperCase())) {
								results.add(prod);
							}
						}
						filterResults.values = results;
						filterResults.count = results.size();
					}
				}
				return filterResults;
			}
		};
		return filter;
	}

}
