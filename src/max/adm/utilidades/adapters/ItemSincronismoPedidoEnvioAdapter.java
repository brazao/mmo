package max.adm.utilidades.adapters;

import java.text.SimpleDateFormat;
import java.util.List;

import max.adm.Dinheiro;
import max.adm.R;
import max.adm.utilidades.MascaraDecimal;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

public class ItemSincronismoPedidoEnvioAdapter extends BaseAdapter {

	private Context context;
	private List<ItemSincronismoPedidoEnvio> pedidos;
	LayoutInflater inflater;
	private SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

	public ItemSincronismoPedidoEnvioAdapter(Context ctx, List<ItemSincronismoPedidoEnvio> pedidos) {
		this.context = ctx;
		this.pedidos = pedidos;
		this.inflater = LayoutInflater.from(ctx);
	}

	public int getCount() {

		return pedidos.size();
	}

	public Object getItem(int position) {

		return pedidos.get(position);
	}

	public long getItemId(int position) {
		return pedidos.get(position).pedido.getCPE_ID();

	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final ItemSincronismoPedidoEnvio pedido = pedidos.get(position);

		final ItemTela tela;

		if (convertView == null) {

			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.modelo_item_sincronismo_envio_pedido, null);

			tela = new ItemTela();

			tela.codigo = (TextView) convertView.findViewById(R.id.text_num_pedido_sincroniza_envio);
			tela.data = (TextView) convertView.findViewById(R.id.text_data_pedido_sincronismo_envio);
			tela.cliente = (TextView) convertView.findViewById(R.id.text_cliente_pedido_sincroniza_envio);
			tela.valorBruto = (TextView) convertView.findViewById(R.id.text_valor_bruto_pedido_sincronismo_envio);
			tela.desconto = (TextView) convertView.findViewById(R.id.text_desconto_pedido_sincronismo_envio);
			tela.valorLiquido = (TextView) convertView.findViewById(R.id.text_valor_liquido_pedido_sincronismo_envio);
			tela.marcar = (CheckBox) convertView.findViewById(R.id.checkBox_sincroniza_pedido);

			convertView.setTag(tela);
		} else {
			tela = (ItemTela) convertView.getTag();
		}

		tela.codigo.setText(String.valueOf(pedido.pedido.getCPE_NUMERO_PEDIDO()));

		tela.data.setText(df.format(pedido.pedido.getCPE_DATA_EMISSAO()));

		tela.cliente.setText(pedido.pedido.getPESSOANOME());

		tela.valorBruto.setText(Dinheiro.newInstance(pedido.pedido.getValor()).formatToScreen());

		tela.desconto.setText(MascaraDecimal.mascaradecimal(pedido.pedido.getCPE_PERC_DESCONTO().doubleValue(), MascaraDecimal.DECIMAL_DUAS_CASAS) + "%");

		tela.valorLiquido.setText(MascaraDecimal.mascaradecimal(pedido.pedido.getValorLiquido().doubleValue(), MascaraDecimal.DECIMAL_DUAS_CASAS));

		tela.marcar.setChecked(pedido.marcado);

		tela.marcar.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				if (tela.marcar.isChecked()) {
					pedido.marcado = true;
				} else {
					pedido.marcado = false;
				}
			}
		});

		return convertView;
	}

	static class ItemTela {

		TextView codigo;
		TextView data;
		TextView cliente;
		TextView valorBruto;
		TextView desconto;
		TextView valorLiquido;
		CheckBox marcar;

	}

}
