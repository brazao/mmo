package max.adm.utilidades.adapters;

import java.math.BigDecimal;
import java.util.List;

import max.adm.Dinheiro;
import max.adm.R;
import max.adm.entidade.PedidoItens;
import max.adm.pedido.GrupoDeItensDoPedido;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.common.collect.Lists;

public class ItemDoPedidoAdapter extends BaseAdapter {
	private List<GrupoDeItensDoPedido>  itens = Lists.newLinkedList();
	private Context context;

	public ItemDoPedidoAdapter(Context context, List<GrupoDeItensDoPedido>  itens) {
		this.itens = itens;
		this.context = context;
	}

	public int getCount() {
		return this.itens.size();
	}

	public List<PedidoItens> getItem(int position) {
		return itens.get(position).getItens();
	}

	public long getItemId(int position) {
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final List<PedidoItens> its = itens.get(position).getItens();

		final Dados dados;

		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.modelo_item_pedido, null);

		dados = new Dados();
		dados.referencia = (TextView) convertView.findViewById(R.id.text_codigo_item_pedido);
		dados.descricao = (TextView) convertView.findViewById(R.id.text_descricao_item_pedido);
		dados.padrao = (TextView) convertView.findViewById(R.id.text_padrao_item_pedido);
		dados.total = (TextView) convertView.findViewById(R.id.text_total_item_pedido);
		dados.quantidade = (TextView) convertView.findViewById(R.id.text_quantidade_item_pedido);
		convertView.setTag(dados);		
		if (its.size() > 0) {
			double total = 0;
			int quantidade = 0;
			for (PedidoItens it : its) {
				total += it.getValorTotal();
				quantidade+= it.getQuantidade();
			}
			PedidoItens itemDoPedido = its.get(0);
			dados.referencia.setText(itemDoPedido.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto().getMascaraCodigo());
			dados.descricao.setText(itemDoPedido.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getProduto().getDescricao());
			dados.padrao.setText(itemDoPedido.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getPadrao().getDescricao());
			Dinheiro tot = Dinheiro.newInstance(new BigDecimal(total));
			dados.total.setText(tot.formatToScreen());
			dados.quantidade.setText(String.valueOf(quantidade));
		}

		return convertView;
	}

	static class Dados {
		TextView referencia;
		TextView descricao;
		TextView padrao;
		TextView total;
		TextView quantidade;

	}

}
