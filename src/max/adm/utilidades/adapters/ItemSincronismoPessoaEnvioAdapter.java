package max.adm.utilidades.adapters;

import java.util.List;

import max.adm.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

public class ItemSincronismoPessoaEnvioAdapter extends BaseAdapter {
	
	private Context context;
	private List<ItemSincronismoPessoaEnvio> pessoas;
	LayoutInflater inflater;
	
	public ItemSincronismoPessoaEnvioAdapter(Context ctx, List<ItemSincronismoPessoaEnvio> pessoas){
		this.context = ctx;
		this.pessoas = pessoas;
		this.inflater = LayoutInflater.from(ctx);
	}

	public int getCount() {
		return pessoas.size();
	}

	public Object getItem(int position) {
		return pessoas.get(position);
	}

	public long getItemId(int position) {
		return pessoas.get(position).pessoa.getCodigo();
	}

	public View getView(int position, View view, ViewGroup parent) {
		final ItemSincronismoPessoaEnvio pessoa = pessoas.get(position);
		
		final ItemTela tela;
		
		if (view == null){
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.modelo_item_sincronismo_envio, null);
			
			tela = new ItemTela();
			
			tela.codigo = (TextView) view.findViewById(R.id.text_cod_pessoa_sincroniza);
			tela.razao = (TextView) view.findViewById(R.id.text_razao_sincriniza);
			tela.fantasia = (TextView) view.findViewById(R.id.text_fatasia_sincriniza);
			tela.cnpj = (TextView) view.findViewById(R.id.text_cnpj_sincroniza);
			tela.marcar = (CheckBox) view.findViewById(R.id.checkBox_sincroniza_pessoa);
			
			view.setTag(tela);
		}
		else{
			tela = (ItemTela) view.getTag();
		}
		
		
		tela.codigo.setText(String.valueOf(pessoa.pessoa.getCodigo()));
		
		tela.razao.setText(pessoa.pessoa.getRazaoSocial());
		
		tela.fantasia.setText(pessoa.pessoa.getNomeFantasia());
		
		tela.cnpj.setText(pessoa.pessoa.getCpfCgc());
		
		tela.marcar.setChecked(pessoa.marcado);
		
		tela.marcar.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				if(tela.marcar.isChecked()){
					pessoa.marcado = true;
				}
				else{
					pessoa.marcado = false;
				}
			}
		});

		return view;
	}
	
	static class ItemTela{
		
		TextView codigo;
		TextView razao;
		TextView fantasia;
		TextView cnpj;
		CheckBox marcar;
		
	}

}
