package max.adm.utilidades;

import static com.google.common.base.Preconditions.*;

import max.adm.R;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

/**
 * @author Eduardo Sutil
 *
 */
public class Notificacao {

	private NotificationManager nm;
	private Notification notification;
	private static Intent intent;
	private final int idNotificacao;
	public static int TIPO_INFO = 1;
	public static int TIPO_FALHA = 2;
	public static int TIPO_SUCESSO = 3;

	private Notificacao(Context ctx, String titulo, String msg, int idNotificacao, int tipo) {

		this.idNotificacao = idNotificacao;
		nm = (NotificationManager) ctx.getSystemService(IntentService.NOTIFICATION_SERVICE);
		if (tipo == TIPO_INFO) {
			notification = new Notification(R.drawable.ic_notifica, msg, System.currentTimeMillis());
			notification.flags = Notification.FLAG_NO_CLEAR;
			PendingIntent contentIntent = PendingIntent.getActivity(ctx, idNotificacao, intent, 0);
			notification.tickerText = msg;
			notification.setLatestEventInfo(ctx, titulo, msg, contentIntent);
		} else if (tipo == TIPO_FALHA) {
			notification = new Notification(R.drawable.ic_notifica_erro, msg, System.currentTimeMillis());
			PendingIntent contentIntent = PendingIntent.getActivity(ctx, this.idNotificacao, intent, this.idNotificacao);
			notification.flags = Notification.FLAG_AUTO_CANCEL;
			notification.setLatestEventInfo(ctx, titulo, msg, contentIntent);

		} else if (tipo == TIPO_SUCESSO) {
			notification = new Notification(R.drawable.ic_notifica_ok, msg, System.currentTimeMillis());
			PendingIntent contentIntent = PendingIntent.getActivity(ctx, this.idNotificacao, intent, 0);
			notification.tickerText = msg;
			notification.flags = Notification.FLAG_AUTO_CANCEL;
			notification.setLatestEventInfo(ctx, titulo, msg, contentIntent);
		}
		nm.notify(this.idNotificacao, notification);
	}

	
	static Notificacao newNotificacaoInfo(Context ctx, String titulo, String msg, Intent i, int idNotificacao) {
		checkNotNull(ctx, "Contexto nao pode ser nulo para criar notifcacao.");
		checkNotNull(titulo, "Titulo da mensagem nao pode ser nulo para criar notifcacao.");
		checkNotNull(ctx, "mensagem nao pode ser nulo para criar notifcacao.");
		checkNotNull(i, "intent nao pode ser nula.");
		intent = i;
		return new Notificacao(ctx, titulo, msg, idNotificacao, TIPO_INFO);
	}
	
	static Notificacao newNotificacaoFalha(Context ctx, String titulo, String msg, Intent i, int idNotificacao) {
		checkNotNull(ctx, "Contexto nao pode ser nulo para criar notifcacao.");
		checkNotNull(titulo, "Titulo da mensagem nao pode ser nulo para criar notifcacao.");
		checkNotNull(ctx, "mensagem nao pode ser nulo para criar notifcacao.");
		checkNotNull(i, "intent nao pode ser nula.");
		intent = i;
		return new Notificacao(ctx, titulo, msg, idNotificacao, TIPO_FALHA);
	}
	
	static Notificacao newNotificacaoSucesso(Context ctx, String titulo, String msg, Intent i, int idNotificacao){
		checkNotNull(ctx, "Contexto nao pode ser nulo para criar notifcacao.");
		checkNotNull(titulo, "Titulo da mensagem nao pode ser nulo para criar notifcacao.");
		checkNotNull(ctx, "mensagem nao pode ser nulo para criar notifcacao.");
		checkNotNull(i, "intent nao pode ser nula.");
		intent = i;
		return new Notificacao(ctx, titulo, msg, idNotificacao, TIPO_SUCESSO);
	}

	public void cancelaNotificacao() {
		nm.cancel(this.idNotificacao);
	}

	public void atualizaNotificacao(Context ctx, String titulo, String msg) {
		PendingIntent contentIntent = PendingIntent.getActivity(ctx, this.idNotificacao, intent, 0);
		notification.tickerText = msg;
		notification.setLatestEventInfo(ctx, titulo, msg, contentIntent);
		nm.notify(552, notification);
	}

	

}
