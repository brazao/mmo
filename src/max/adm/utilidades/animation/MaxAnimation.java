package max.adm.utilidades.animation;

import max.adm.R;
import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;

public class MaxAnimation {
	
	public static Animation fadeInFast(Context ctx) {
		Animation fade = AnimationUtils.loadAnimation(ctx, android.R.anim.fade_in);
		fade.setDuration(100);
		return fade;
	}
	
	public static Animation fadeIn(Context ctx) {
		Animation fadeIn = AnimationUtils.loadAnimation(ctx, android.R.anim.fade_in);
		fadeIn.setDuration(3000);
		return fadeIn;
	}

	public static Animation fadeOut(Context ctx) {
		Animation fade = AnimationUtils.loadAnimation(ctx, android.R.anim.fade_out);
		fade.setDuration(1500);
		return fade;
	}
	
	public static Animation fadeOut(Context ctx, int duration) {
		Animation fade = AnimationUtils.loadAnimation(ctx, android.R.anim.fade_out);
		fade.setDuration(duration);
		return fade;
	}
	
	public static Animation slideUp(Context ctx) {
		Animation slide = AnimationUtils.loadAnimation(ctx, R.anim.slide_up);
		slide.setDuration(200);
		return slide;
	}
	
	public static Animation slideUp(Context ctx, int duration) {
		Animation slide = AnimationUtils.loadAnimation(ctx, R.anim.slide_up);
		slide.setDuration(duration);
		return slide;
	}
	
	public static Animation slideDown(Context ctx) {
		Animation slide = AnimationUtils.loadAnimation(ctx, R.anim.slide_down);
		slide.setDuration(200);
		return slide;
	}
	
	public static Animation slideDown(Context ctx, int duration) {
		Animation slide = AnimationUtils.loadAnimation(ctx, R.anim.slide_down);
		slide.setDuration(duration);
		return slide;
	}
	
	public static Animation slideLeft(Context ctx) {
		Animation slide = AnimationUtils.loadAnimation(ctx, R.anim.slide_left);
		slide.setDuration(200);
		return slide;
	}
	
	public static Animation slideLeft(Context ctx, int duration) {
		Animation slide = AnimationUtils.loadAnimation(ctx, R.anim.slide_left);
		slide.setDuration(duration);
		return slide;
	}
	
	public static Animation slideRigth(Context ctx) {
		Animation slide = AnimationUtils.loadAnimation(ctx, R.anim.slide_rigth);
		slide.setDuration(200);
		return slide;
	}
	
	public static Animation slideOutRigth(Context ctx, int duration) {
		Animation slide = AnimationUtils.loadAnimation(ctx, R.anim.slide_out_rigth);
		slide.setDuration(duration);
		return slide;
	}
	

	public static Animation slideOutDown(Context ctx, int duration) {
		Animation slide = AnimationUtils.loadAnimation(ctx, R.anim.slide_out_down);
		slide.setDuration(duration);
		return slide;
	}
	
	public static Animation slideLeftTrusty(Context ctx) {
		Animation slide = AnimationUtils.loadAnimation(ctx, R.anim.slide_left_trusty);
		slide.setDuration(400);
		return slide;
	}
	
	public static Animation slideRigthTrusty(Context ctx) {
		Animation slide = AnimationUtils.loadAnimation(ctx, R.anim.slide_rigth_trusty);
		slide.setDuration(400);
		return slide;
	} 
	
	public static Animation blink(Context ctx, int duration) {
		Animation blink = AnimationUtils.loadAnimation(ctx, R.anim.blink);
		blink.setDuration(duration);
		return blink;
	}

    public static RotateAnimation rotate(int duration) {
        RotateAnimation rotate = new RotateAnimation(360, 0);
        rotate.setDuration(duration);
        return rotate;
    }


}
