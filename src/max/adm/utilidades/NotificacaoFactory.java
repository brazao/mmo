package max.adm.utilidades;

import android.content.Context;
import android.content.Intent;
/**
 * 
 * @author Eduardo Sutil
 *
 */
public enum NotificacaoFactory {
	
	INSTANCE;
	
	private int idNotificacao=0;
	
	/**
	 * 
	 * @param ctx - contexto do service
	 * @param titulo - titulo da notificacao
	 * @param msg - mensagem da notificacao
	 * @param i - intent
	 * @param tipo - tipo da notificacao (1=informacao, 2-falha, 3-sucesso)
	 * @return Notifcacao.
	 */
	public Notificacao newNotificacaoInfo(Context ctx, String titulo, String msg, Intent i){
		this.idNotificacao++;
		return Notificacao.newNotificacaoInfo(ctx, titulo, msg, i, this.idNotificacao);
	}
	
	public Notificacao newNotificacaoFalha(Context ctx, String titulo, String msg, Intent i){
		this.idNotificacao++;
		return Notificacao.newNotificacaoFalha(ctx, titulo, msg, i, this.idNotificacao);
	}
	
	public Notificacao newNotificacaoSucesso(Context ctx, String titulo, String msg, Intent i){
		this.idNotificacao++;
		return Notificacao.newNotificacaoSucesso(ctx, titulo, msg, i, this.idNotificacao);
	}

}
