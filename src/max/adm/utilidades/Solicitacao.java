package max.adm.utilidades;



import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

public class Solicitacao implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	private int loja;
    private String chaveLicenca;
    
    private Solicitacao(int loja, String chaveLicenca){
    	this.loja = loja;
    	this.chaveLicenca = chaveLicenca;
    }
    
    public static Solicitacao newSolicitacao(int loja, String chaveLicenca){
    	checkNotNull(chaveLicenca, "chave da Licenca nao pode ser nula");
    	checkArgument(loja > 0, "O numero da loja deve ser maior que 0 para se criar uma solicitacao");
    	return new Solicitacao(loja, chaveLicenca);
    }

    public String getChaveLicenca() {
		return chaveLicenca;
	}

	public void setChaveLicenca(String chaveLicenca) {
		this.chaveLicenca = chaveLicenca;
	}

	public int getLoja() {
        return loja;
    }

    public void setLoja(int loja) {
        this.loja = loja;
    }

}
