package max.adm.utilidades;

public class ValidacaoCpfCnpj {

	// 02998301000181
	public static boolean CNPJ(String str_cnpj) {

		int soma = 0, dig = 0;
		String tiraMascara = "";

		if (str_cnpj.length() < 1)
			return false;

		for (int i = 0; i < str_cnpj.length(); i++) {
			if (!str_cnpj.substring(i, i + 1).equals(" ") && !str_cnpj.substring(i, i + 1).equals("-") && !str_cnpj.substring(i, i + 1).equals(".") && !str_cnpj.substring(i, i + 1).equals("/"))
				tiraMascara += str_cnpj.substring(i, i + 1);
		}

		str_cnpj = tiraMascara;

		if (str_cnpj.length() != 14) {
			return false;
		}

		if (str_cnpj.equals("00000000000000")) {
			return false;
		}

		String cnpj_calc = str_cnpj.substring(0, 12);

		System.out.println("ja era pra ter retornado");

		char[] chr_cnpj = str_cnpj.toCharArray();

		/* Primeira parte */
		for (int i = 0; i < 4; i++)
			if (chr_cnpj[i] - 48 >= 0 && chr_cnpj[i] - 48 <= 9)
				soma += (chr_cnpj[i] - 48) * (6 - (i + 1));
		for (int i = 0; i < 8; i++)
			if (chr_cnpj[i + 4] - 48 >= 0 && chr_cnpj[i + 4] - 48 <= 9)
				soma += (chr_cnpj[i + 4] - 48) * (10 - (i + 1));
		dig = 11 - (soma % 11);

		cnpj_calc += (dig == 10 || dig == 11) ? "0" : Integer.toString(dig);

		/* Segunda parte */
		soma = 0;
		for (int i = 0; i < 5; i++)
			if (chr_cnpj[i] - 48 >= 0 && chr_cnpj[i] - 48 <= 9)
				soma += (chr_cnpj[i] - 48) * (7 - (i + 1));
		for (int i = 0; i < 8; i++)
			if (chr_cnpj[i + 5] - 48 >= 0 && chr_cnpj[i + 5] - 48 <= 9)
				soma += (chr_cnpj[i + 5] - 48) * (10 - (i + 1));
		dig = 11 - (soma % 11);
		cnpj_calc += (dig == 10 || dig == 11) ? "0" : Integer.toString(dig);

		return str_cnpj.equals(cnpj_calc);
	}

	public static boolean CPF(String cpfNum) {
		String tiraMascara = "";

		if (cpfNum.length() < 1)
			return false;

		for (int i = 0; i < cpfNum.length(); i++) {
			if (!cpfNum.substring(i, i + 1).equals(" ") && !cpfNum.substring(i, i + 1).equals("-") && !cpfNum.substring(i, i + 1).equals(".") && !cpfNum.substring(i, i + 1).equals("/"))
				tiraMascara += cpfNum.substring(i, i + 1);
			System.out.println(cpfNum.substring(i, i + 1));
		}
		cpfNum = tiraMascara;

		int[] cpf = new int[cpfNum.length()]; // define o valor com o tamanho da
												// string
		int resultP = 0;
		int resultS = 0;

		if (cpfNum.length() != 11)
			return false;

		if (cpfNum.equals("00000000000")) {
			return false;
		}

		// converte a string para um array de integer
		for (int i = 0; i < cpf.length; i++) {
			cpf[i] = Integer.parseInt(cpfNum.substring(i, i + 1));
		}

		// calcula o primeiro n�mero(DIV) do cpf
		for (int i = 0; i < 9; i++) {
			resultP += cpf[i] * (i + 1);
		}
		int divP = resultP % 11;

		// se o resultado for diferente ao 10� digito do cpf retorna falso
		if (divP != cpf[9]) {
			return false;
		} else {
			// calcula o segundo n�mero(DIV) do cpf
			for (int i = 0; i < 10; i++) {
				resultS += cpf[i] * (i);
			}
			int divS = resultS % 11;

			// se o resultado for diferente ao 11� digito do cpf retorna falso
			if (divS != cpf[10]) {
				return false;
			}
		}

		// se tudo estiver ok retorna verdadeiro
		return true;
	}

	public static String poeMascaraCpfCnpj(String str) {
		if (str != null) {
			String aux = "";
			str = ValidaString.tiraMascara(str);
			if (str.length() == 11) {
				String part1 = str.substring(0, 3);
				String part2 = str.substring(3, 6);
				String part3 = str.substring(6, 9);
				String part4 = str.substring(9, 11);
				aux = part1 + "." + part2 + "." + part3 + "-" + part4;
				return aux;
			}
			if (str.length() == 14) {
				String parte1 = str.substring(0, 2);
				String parte2 = str.substring(2, 5);
				String parte3 = str.substring(5, 8);
				String parte4 = str.substring(8, 12);
				String parte5 = str.substring(12, 14);
				aux = parte1 + "." + parte2 + "." + parte3 + "/" + parte4 + "-" + parte5;

				return aux;
			}
		}
		return str;
	}
}
