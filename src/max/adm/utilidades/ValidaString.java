package max.adm.utilidades;

public class ValidaString {
	
	
		
		public static boolean Telefone (String tel){
			
			if(tel == null){
				return false;
			}

			String telSemMascara = "";
			for (int i = 0; i<tel.length();i++){
				if (!tel.substring(i, i+1).equals("(") && !tel.substring(i, i+1).equals(")") && !tel.substring(i, i+1).equals(" ") && !tel.substring(i, i+1).equals("-")){
					telSemMascara += tel.substring(i, i+1);
				}
			}
			
			if (telSemMascara.length() > 0 && telSemMascara.length() < 10)
				return false;
			
			return true;
		}
		
		public static boolean Email (String email){
			
			if (!email.equals("")){
				if (!email.contains("@") || !email.contains(".") || email.contains(" ")){
					return false;
				}
				else if(email.substring(email.length()-1, email.length()).equals(".")){
					return false;
				}
			}
			
			return true;
		}
		
		public static boolean CampoVazio(String campo){
			
			if (campo.equals("") || campo.equals(null))
				return true;
			
			return false;
		}
		
		public static boolean cep(String cep){
			cep = tiraMascara(cep);
			
			if (cep.length() != 8){
				return false;
			}
			
			return true;
		}
		
		public static String tiraMascara(String string){
			String stringSemMascara = "";
			for (int i= 0; i<string.length(); i++){
		    	   if (!string.substring(i, i+1).equals(" ") && !string.substring(i, i+1).equals("-") && !string.substring(i, i+1).equals(".")&& !string.substring(i, i+1).equals("/") && !string.substring(i, i+1).equals("(") && !string.substring(i, i+1).equals(")"))
		    		  
		    		   stringSemMascara += string.substring(i, i+1);
		       }
			return stringSemMascara;
		}
		
		public static String poeMascaraCep(String cep){
			if (cep != null){
				if (cep.length() == 8){
					String part1 = "";
					String part2 = "";
					part1 = cep.substring(0, 5);
					part2 = cep.substring(5, 8);
					cep = part1+"-"+part2;
				}
			}
			
			return cep;
		}
	





}
