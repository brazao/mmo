package max.adm;

public enum ValorAtividade {
	menu(1), parametro(2), criaLogin(3), primeiroPasso(4), SincronismoReceber(5), PessoaCadastro(6), Endereco(7), CidadeList(8), PessoaList(9), DadosFinanceiros(10), MensagemResposta(11), MensagemShow(
			12), PedidoCadastro(13), PedidoInsereItes(14), PedidoPesquisaProduto(15), EscolhaPadraoTamanho(16), AutenticaLicecaRepres(17), TabelaPrecoMultActivity(18), TabelaPrecoSingleActivity(19), EditarItemPedidoAcitivity(
			20), PedidoInsereDadosCapa(21), DateActivity(22), CodigoBarra(23), EditarItemPedidoShowroom(24), AssinaturaActivity(25), PedidoShowActivity(26), PedidoResumoItens(27);

	ValorAtividade(int codigo) {
		this.codigo = codigo;
	}

	public int get() {
		return this.codigo;
	}

	private int codigo;

}
