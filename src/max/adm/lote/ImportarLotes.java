package max.adm.lote;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import max.adm.Servico;
import max.adm.entidade.Lote;
import max.adm.repositorio.RepositorioLote;
import max.adm.service.ConexaoHttp;
import max.adm.utilidades.Notificacao;
import max.adm.utilidades.NotificacaoFactory;
import android.content.Context;
import android.content.Intent;

import com.google.common.reflect.TypeToken;

public class ImportarLotes {
	
	private Context ctx;
	private String url;
	
	private Notificacao notificacao;
	
	private ImportarLotes(Context ctx, String url) {
		this.ctx = ctx;
		this.url = url;
	}
	
	public static ImportarLotes newInstance(Context ctx, String url){
		checkNotNull(ctx);
		checkNotNull(url);
		return new ImportarLotes(ctx, url);
	}
	
	public boolean execute(){
		notifica();
		try{
			ConexaoHttp conexao = ConexaoHttp.newInstance(url+Servico.buscaLotes.get());
			List<Lote> lotes = conexao.executaRequisicao(Lote.class, new TypeToken<List<Lote>>() {}.getType());
			RepositorioLote repo = new RepositorioLote(ctx);
			repo.deletar(null, null);
			repo.salvarLista(lotes);
			repo.fechar();
			notificaSucesso();
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			notificaFalha();
			return false;
		}
	}

	
	private void notifica(){
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoInfo(this.ctx, "Lotes", "Importando Lotes", new Intent());
	}
	
	private void notificaFalha(){
		notificacao.cancelaNotificacao();
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoFalha(this.ctx, "Lotes", "Falha ao importar", new Intent());
	}

	private void notificaSucesso(){
		notificacao.cancelaNotificacao();
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoSucesso(this.ctx, "Lotes", "importação concluida", new Intent());
	}
	
}
