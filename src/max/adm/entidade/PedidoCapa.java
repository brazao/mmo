package max.adm.entidade;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import max.adm.dominio.Entidade;
import max.adm.pedido.PedidoDoc;
import max.adm.relatorios.ItemRelatorioListaPedido;
import max.adm.repositorio.RepositorioPedidoCapa;
import android.content.Context;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
public class PedidoCapa extends Entidade {
	
	public static String NOME_TABELA = "PEDIDOCAPA";

	public static String ID = "CPE_ID";
	public static String NUMEROPEDIDO = "CPE_NUMERO_PEDIDO";
	public static String NUMEROENTREGA = "CPE_NR_ENTREGA";
	public static String CODIGOCOLECAO = "COL_CODIGO";
	public static String DATAEMISSAO = "CPE_DATA_EMISSAO";
	public static String DATAPREVISAO = "CPE_DATA_PREVISAO_ENT";
	public static String VALOR = "CPE_VALOR";
	public static String PERCENTUALDESCONTO = "CPE_PERC_DESCONTO";
	public static String STATUS = "CPE_STATUS";
	public static String VALORLIQUIDO = "CPE_VALORlIQUIDO";
	public static String OBSERVACAO = "CPE_OBS";
	public static String SYNC = "CPE_SYNC";
	public static String EMPRESA = "FIA_EMPRESA_EMP_ID";
	public static String EMPRESANUMERO = "EMPRESANUMERO";
	public static String TIPOCOBRANCA = "TIPOCOBRANCA_ID";
	public static String TIPOCOBRANCACODIGO = "TIPOCOBRANCACODIGO";
	public static String TIPOCOBRANCADESCRICAO = "TIPOCOBRANCADESCRICAO";
	public static String TIPOCOBRANCA2 = "TIPOCOBRANCA2_ID";
	public static String TIPOCOBRANCACODIGO2 = "TIPOCOBRANCACODIGO2";
	public static String TIPOCOBRANCADESCRICAO2 = "TIPOCOBRANCADESCRICAO2";
	public static String PESSOA = "FIA_PESSOA_PES_ID";
	public static String PESSOACODIGO = "PESSOACODIGO";
	public static String PESSOANOME = "PESSOANOME";
	public static String TABELADEPRECO = "TABELA_PRECO_TABELAPRECO_ID";
	public static String TABELADEPRECONUMERO = "TABELADEPRECONUMERO";
	public static String TABELADEPRECODESCRICAO = "TABELADEPRECODESCRICAO";
	public static String CONDICAODEPAGAMENTO = "CONDICAOPGTO_CONDICAOPGTO_ID";
	public static String CONDICAODEPAGAMENTOCODIGO = "CONDICAODEPAGAMENTOCODIGO";
	public static String CONDICAODEPAGAMENTODESCRICAO = "CONDICAODEPAGAMENTODESCRICAO";
	public static String ASSINATURA = "ASSINATURA";

	public static String[] COLUNAS = {ID, NUMEROPEDIDO, NUMEROENTREGA, CODIGOCOLECAO, DATAEMISSAO, DATAPREVISAO, VALOR, PERCENTUALDESCONTO, STATUS, VALORLIQUIDO, OBSERVACAO, SYNC, EMPRESA,
			EMPRESANUMERO, TIPOCOBRANCA, TIPOCOBRANCACODIGO, TIPOCOBRANCADESCRICAO, TIPOCOBRANCA2, TIPOCOBRANCACODIGO2, TIPOCOBRANCADESCRICAO2, PESSOA, PESSOACODIGO, PESSOANOME, TABELADEPRECO,
			TABELADEPRECONUMERO, TABELADEPRECODESCRICAO, CONDICAODEPAGAMENTO, CONDICAODEPAGAMENTOCODIGO, CONDICAODEPAGAMENTODESCRICAO, ASSINATURA};
	
	private static final long serialVersionUID = 1L;

	private int id;
	private Empresa empresa;
	private int numeroBloco;
	private int numeroPedido;
	private int numeroEntrega;
	private int codigoColecao;
	private Date dataEmissao;
	private Date dataPrevisao;
	private double percentualDesconto;
	private String status;
	private String observacao;
	private boolean sync;
	private TipoCobranca tipoCobranca;
	private TipoCobranca tipoCobranca2;
	private Pessoa pessoa;
	private TabelaDePreco tabelaDePreco;
	private CondicaoPagamento condicaoDePagamento;
	private List<PedidoItens> itens = new ArrayList<PedidoItens>();
	private byte[] assinatura;

	PedidoCapa(Builder b) {
		this.id = b.id;
		this.numeroPedido = b.numeroPedido;
		this.numeroEntrega = b.numeroEntrega;
		this.codigoColecao = b.codigoColecao;
		this.dataEmissao = b.dataEmissao;
		this.dataPrevisao = b.dataPrevisao;
		this.percentualDesconto = b.percentualDesconto;
		this.status = b.status;
		this.observacao = b.observacao;
		this.sync = b.sync;
		this.empresa = b.empresa;
		this.tipoCobranca = b.tipoCobranca;
		this.tipoCobranca2 = b.tipoCobranca2;
		this.pessoa = b.pessoa;
		this.tabelaDePreco = b.tabelaDePreco;
		this.condicaoDePagamento = b.condicaoDePagamento;
		this.numeroBloco = b.numeroBloco;
		this.itens = b.itens;
		this.assinatura = b.assinatura;
	}

	public static Builder builder() {
		return new Builder();
	}

	public void addOrAlterItem(PedidoItens item) {
		checkNotNull(item, "nao e possivel inserir item nulo no pedido.");
		if (!itens.contains(item)) {
			itens.add(item);

		} else {
			for (int i = 0; i < itens.size(); i++) {
				if (item.equals(itens.get(i))) {
					itens.get(i).setQuantidade(item.getQuantidade());
				}
			}
		}

	}

	public void setAssinatura(byte[] assinatura) {
		this.assinatura = assinatura;
	}

	public byte[] getAssinatura() {
		return assinatura;
	}

	public List<PedidoItens> getItens() {
		return itens;
	}

	public void setItens(List<PedidoItens> itens) {
		this.itens = itens;
	}

	public int getId() {
		return id;
	}

	public int getNumeroBloco() {
		return numeroBloco;
	}

	public int getNumeroPedido() {
		return numeroPedido;
	}

	public int getNumeroEntrega() {
		return numeroEntrega;
	}

	public int getCodigoColecao() {
		return codigoColecao;
	}

	public Date getDataEmissao() {
		return dataEmissao;
	}

	public Date getDataPrevisao() {
		return dataPrevisao;
	}

	public double getValor() {
		double valor = 0;
		for (PedidoItens item : this.itens) {
			valor += item.getValorTotal();
		}
		return valor;
	}

	public double getPercentualDesconto() {
		return percentualDesconto;
	}

	public void setPercentualDesconto(double percentualDesconto) {
		Preconditions.checkArgument(percentualDesconto >= 0 && percentualDesconto <= 100, "percentual de desconto no pedido deve ser maior que 0 e menor que 100");
		this.percentualDesconto = percentualDesconto;

	}

	public String getDataEmissaoToString() {
		StringBuilder sb = new StringBuilder();
		if (this.dataEmissao != null) {
			sb.append(this.dataEmissao.getDay()).append("/").append(this.dataEmissao.getMonth()).append("/").append(this.dataEmissao.getYear());
		}
		return sb.toString();
	}

	public String getDataEntregaToString() {
		StringBuilder sb = new StringBuilder();
		if (this.dataEmissao != null) {
			sb.append(this.dataEmissao.getDay()).append("/").append(this.dataEmissao.getMonth()).append("/").append(this.dataEmissao.getYear());
		}
		return sb.toString();
	}

	public String getStatus() {
		return status;
	}

	public double getValorLiquido() {
		return this.getValor() - this.getValorDesconto();
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public boolean isSync() {
		return sync;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public TipoCobranca getTipoCobranca() {
		return tipoCobranca;
	}

	public TipoCobranca getTipoCobranca2() {
		return tipoCobranca2;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public TabelaDePreco getTabelaDePreco() {
		return tabelaDePreco;
	}

	public CondicaoPagamento getCondicaoDePagamento() {
		return condicaoDePagamento;
	}

	public int getQuantidadePecas() {
		int quantidade = 0;
		for (PedidoItens item : itens) {
			quantidade += item.getQuantidade();
		}
		return quantidade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PedidoCapa other = (PedidoCapa) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public double getValorDesconto() {
		double valorDesc = this.getValor() * (this.percentualDesconto / 100);
		return valorDesc;
	}

	public static void updateLista(Context ctx, List<PedidoDoc> pedidos) {
		RepositorioPedidoCapa repo = new RepositorioPedidoCapa(ctx);
		repo.updateLista(pedidos);
		repo.fechar();
	}

	public void setSync(boolean b) {
		this.sync = b;
	}

	public void setId(int id) {
		this.id = id;
	}

	public static List<ItemRelatorioListaPedido> listarParaTela(Context ctx, int idLoja) {
		RepositorioPedidoCapa repo = new RepositorioPedidoCapa(ctx);
		List<ItemRelatorioListaPedido> pedidos = repo.listarParaTela(idLoja);
		repo.fechar();
		return pedidos;
	}

	@Override
	public String toString() {

		return Objects.toStringHelper(this).add("id", this.id).add("numero", this.numeroPedido).add("numero da entrega", numeroEntrega).add("codigo colecao", this.codigoColecao)
				.add("data de Emissao", dataEmissao).add("data de previsao", this.dataPrevisao).add("percentual desconto", this.percentualDesconto).add("status", this.status)
				.add("observacao", this.observacao).add("syncronizado", this.sync).add("empresa", this.empresa).add("tipo de cobranca 1", this.tipoCobranca).add("tipo de cob 2", this.tipoCobranca2)
				.add("pessoa", this.pessoa).add("tabela de preco", this.tabelaDePreco).add("condicao de pagamento", this.condicaoDePagamento).add("qtde itesn", this.itens.size())
				.add("assinatura", assinatura).toString();

	}	
	
	public static class Builder implements Serializable {

		private static final long serialVersionUID = 1L;
		
		int id;
		Empresa empresa;
		int numeroPedido;
		int numeroEntrega;
		int codigoColecao;
		int numeroBloco;
		Date dataEmissao;
		Date dataPrevisao;
		double percentualDesconto;
		String status;
		String observacao;
		boolean sync;
		TipoCobranca tipoCobranca;
		TipoCobranca tipoCobranca2;
		Pessoa pessoa;
		TabelaDePreco tabelaDePreco;
		CondicaoPagamento condicaoDePagamento;
		List<PedidoItens> itens = new ArrayList<PedidoItens>();
		byte[] assinatura;

		public Builder() {

		}

		public Builder limparItens() {
			if (itens != null) {
				itens.clear();
			}
			return this;
		}

		public Builder addItem(PedidoItens item) {
			if (item != null && this.tabelaDePreco != null) {
				//if (item.getProdutoReduzido().getTabelaDePreco().equals(this.tabelaDePreco)) {
					this.itens.add(item);
				//}
			}
			return this;
		}

		public TabelaDePreco getTabelaDePreco() {
			return tabelaDePreco;
		}

		public List<PedidoItens> getItens() {
			return itens;
		}

		public int getNumeroPedido() {
			return numeroPedido;
		}

		public Builder comId(int id) {
			this.id = id;
			return this;
		}
		public Builder comEmpresa(Empresa empresa) {
			this.empresa = empresa;
			return this;
		}

		public Builder comNumeroPedido(int numeroPedido) {
			checkArgument(numeroPedido > 0);
			this.numeroPedido = numeroPedido;
			return this;
		}
		public Builder comNumeroEntrega(int numeroEntrega) {
			this.numeroEntrega = 1;
			return this;
		}
		public Builder comCodigoColecao(int codigoColecao) {
			this.codigoColecao = codigoColecao;
			return this;
		}
		public Builder comDataEmissao(Date dataEmissao) {
			this.dataEmissao = dataEmissao;
			return this;
		}
		public Builder comDataPrevisao(Date dataPrevisao) {
			this.dataPrevisao = dataPrevisao;
			return this;
		}

		public Builder comPercentualDesconto(double percentualDesconto) {
			checkArgument(percentualDesconto >= 0 && percentualDesconto < 100);
			this.percentualDesconto = percentualDesconto;
			return this;
		}

		public Builder comStatus(String status) {
			checkArgument(status != null);
			this.status = status;
			return this;
		}

		public Builder comObservacao(String observacao) {
			this.observacao = observacao;
			return this;
		}
		public Builder comSync(boolean sync) {
			this.sync = sync;
			return this;
		}
		public Builder comSync(String sync) {
			if (sync.equals("S")) {
				this.sync = true;
			} else {
				this.sync = false;
			}
			return this;
		}

		public Builder comTipoCobranca(TipoCobranca tipoCobranca) {
			checkNotNull(tipoCobranca);
			this.tipoCobranca = tipoCobranca;
			return this;
		}
		public Builder comTipoCobranca2(TipoCobranca tipoCobranca2) {
			this.tipoCobranca2 = tipoCobranca2;
			return this;
		}

		public Builder comPessoa(Pessoa pessoa) {
			checkNotNull(pessoa);
			this.pessoa = pessoa;
			return this;
		}
		public Builder comTabelaDePreco(TabelaDePreco tabelaDePreco) {
			checkNotNull(tabelaDePreco);
			this.tabelaDePreco = tabelaDePreco;
			return this;
		}
		public Builder comCondicaoDePagamento(CondicaoPagamento condicaoDePagamento) {
			checkNotNull(condicaoDePagamento);
			this.condicaoDePagamento = condicaoDePagamento;
			return this;
		}
		public Builder comItens(List<PedidoItens> itens) {
			checkNotNull(itens);
			checkArgument(!itens.isEmpty());
			this.itens = itens;
			return this;
		}

		public Builder comNumeroBloco(int numeroBloco) {
			this.numeroBloco = numeroBloco;
			return this;
		}
		public Builder comAssinatura(byte[] assinatura) {
			this.assinatura = assinatura;
			return this;
		}

		public PedidoCapa build() {
			return new PedidoCapa(this);
		}

	}

}
