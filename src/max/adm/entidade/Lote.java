package max.adm.entidade;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Date;

import max.adm.dominio.Entidade;

import com.google.common.base.Objects;

public class Lote extends Entidade {

	private static final long serialVersionUID = 1L;
	
	private long id;
	private int numeroLote;
	private Date dataPrevisao;
	private Date dataFechamento;
	
	private Lote(int numeroLote, Date dataPrevisao, Date dataFechamento) {
		this.numeroLote = numeroLote;
		this.dataPrevisao = dataPrevisao;
		this.dataFechamento = dataFechamento;
	}
	
	private Lote(long id, int numeroLote, Date dataPrevisao, Date dataFechamento) {
		this.id = id;
		this.numeroLote = numeroLote;
		this.dataPrevisao = dataPrevisao;
		this.dataFechamento = dataFechamento;
	}
	
	public static Lote newInstance(int numeroLote, Date dataPrevisao, Date dataFechamento){
		checkArgument(numeroLote > 0);
		checkNotNull(dataPrevisao);
		return new Lote(numeroLote, dataPrevisao, dataFechamento);
	}
	
	public static Lote newInstance(long id, int numeroLote, Date dataPrevisao, Date dataFechamento){
		checkArgument(numeroLote > 0);
		checkNotNull(dataPrevisao);
		return new Lote(id, numeroLote, dataPrevisao, dataFechamento);
	}
	
	public long getId() {
		return id;
	}

	public int getNumeroLote() {
		return numeroLote;
	}

	public Date getDataPrevisao() {
		return dataPrevisao;
	}

	public Date getDataFechamento() {
		return dataFechamento;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Lote){
			Lote other = (Lote) o;
			return Objects.equal(this.numeroLote, other.numeroLote);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(this.numeroLote);
	}
	
	@Override
	public String toString() {
		return super.toString();
	}

}
