package max.adm.entidade;

import java.util.List;

import android.content.Context;
import max.adm.dominio.Entidade;
import max.adm.repositorio.RepositorioEmail;

public class Email extends Entidade {

	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "EMAIL";

	public static String CODIGO = "CODIGO";
	public static String EMAIL = "EMAIL";
	public static String PESSOA = "PESSOA_ID";

	public static String[] COLUNAS = { CODIGO, EMAIL, PESSOA };

	private int codigo;
	private String email;
	private transient Pessoa pessoa;

	public Email() {
	}

	public Email(String email, Pessoa pessoa) {
		this.email = email;
		this.pessoa = pessoa;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Email other = (Email) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}

	public static List<Email> buscaEmails(Context ctx, int idPessoa) {
		RepositorioEmail repo = new RepositorioEmail(ctx);
		List<Email> emails = repo.buscaPorPessoa(idPessoa);
		repo.fechar();
		return emails;
	}

}
