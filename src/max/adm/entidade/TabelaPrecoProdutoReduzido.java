package max.adm.entidade;

import max.adm.Dinheiro;
import max.adm.dominio.Entidade;

public class TabelaPrecoProdutoReduzido extends Entidade {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "TABELAPRECO_PRODUTOREDUZIDO";

	public static final String PRECO = "PRECO";
	public static final String PRODUTOREDUZIDO = "REDUZIDOPRODUTO_ID";
	public static final String TABELAPRECO = "TABELAPRECO_ID";
	public static final String EMPRESA = "EMPRESA_ID";

	public static final String[] COLUNAS = { PRECO, PRODUTOREDUZIDO, TABELAPRECO, EMPRESA };

	private Empresa empresa;
	private Dinheiro preco;
	private ProdutoReduzido produtoReduzido;
	private TabelaDePreco tabelaDePreco;
	
	
	private TabelaPrecoProdutoReduzido(Empresa empresa, Dinheiro preco, ProdutoReduzido produtoReduzido,
			TabelaDePreco tabelaDePreco) {
		this.empresa = empresa;
		this.preco = preco;
		this.produtoReduzido = produtoReduzido;
		this.tabelaDePreco = tabelaDePreco;
	}
	
	public static TabelaPrecoProdutoReduzido newInstance(Empresa empresa, Dinheiro preco, ProdutoReduzido produtoReduzido,
			TabelaDePreco tabelaDePreco) {
		return new TabelaPrecoProdutoReduzido(empresa, preco, produtoReduzido, tabelaDePreco);
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Dinheiro getPreco() {
		return preco;
	}

	public void setPreco(Dinheiro preco) {
		this.preco = preco;
	}

	public ProdutoReduzido getProdutoReduzido() {
		return produtoReduzido;
	}

	public void setProdutoReduzido(ProdutoReduzido produtoReduzido) {
		this.produtoReduzido = produtoReduzido;
	}

	public TabelaDePreco getTabelaDePreco() {
		return tabelaDePreco;
	}

	public void setTabelaDePreco(TabelaDePreco tabelaDePreco) {
		this.tabelaDePreco = tabelaDePreco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((empresa == null) ? 0 : empresa.hashCode());
		result = prime * result + ((preco == null) ? 0 : preco.hashCode());
		result = prime * result + ((produtoReduzido == null) ? 0 : produtoReduzido.hashCode());
		result = prime * result + ((tabelaDePreco == null) ? 0 : tabelaDePreco.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TabelaPrecoProdutoReduzido other = (TabelaPrecoProdutoReduzido) obj;
		if (empresa == null) {
			if (other.empresa != null)
				return false;
		} else if (!empresa.equals(other.empresa))
			return false;
		if (preco == null) {
			if (other.preco != null)
				return false;
		} else if (!preco.equals(other.preco))
			return false;
		if (produtoReduzido == null) {
			if (other.produtoReduzido != null)
				return false;
		} else if (!produtoReduzido.equals(other.produtoReduzido))
			return false;
		if (tabelaDePreco == null) {
			if (other.tabelaDePreco != null)
				return false;
		} else if (!tabelaDePreco.equals(other.tabelaDePreco))
			return false;
		return true;
	}

}
