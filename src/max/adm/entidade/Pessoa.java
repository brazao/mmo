package max.adm.entidade;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import max.adm.dominio.Entidade;
import max.adm.endereco.TipoEndereco;
import max.adm.pessoa.repositorio.RepositorioPessoa;
import max.adm.telefone.TipoTelefone;
import android.content.Context;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class Pessoa extends Entidade implements Comparable<Pessoa> {
	
	public static String NOME_TABELA = "PESSOA";

	public static String CODIGO = "CODIGO";
	public static String RAZAOSOCIAL = "RAZAO_NOME";
	public static String NOMEFANTASIA = "NOME_FANTASIA";
	public static String OBSERVACAO = "OBSERVACAO";
	public static String CGCCPF = "CPFCGC";
	public static String INSCRG = "RG_INSCRICAO";
	public static String DATACADASTRO = "DATA_CADASTRO";
	public static String SYNC = "SYNC";
	public static String CONTATO = "CONTATO";
	public static String EMPRESA = "EMPRESA_ID";

	public static String[] COLUNAS = { CODIGO, RAZAOSOCIAL, NOMEFANTASIA, OBSERVACAO, CGCCPF, INSCRG,
			DATACADASTRO, SYNC, CONTATO, EMPRESA };

	private static final long serialVersionUID = 1L;

	private Integer codigo;
	private String razaoSocial;
	private String nomeFantasia;
	private String observacao;
	private String cpfCgc;
	private String rgInscricao;
	private Date dataCadastro;
	private boolean sync;
	private String contato;
	private Empresa empresa;
	private List<Email> emails;
	private List<Endereco> enderecos;
	private List<Telefone> telefones;

	public static Builder builder(FaixaCodigoCliente codigo) {
		return new Builder(codigo);
	}

	public static Builder builder(int codigo) {
		return new Builder(codigo);
	}

	Pessoa(Builder b) {
		this.codigo = b.codigo();
		this.empresa = b.empresa();
		this.razaoSocial = b.razaoSocial();
		this.nomeFantasia = b.nomeFantasia();
		this.observacao = b.observacao();
		this.cpfCgc = b.cpfCgc();
		this.rgInscricao = b.rgInscricao();
		this.dataCadastro = b.dataCadastro();
		this.contato = b.contato();
		this.enderecos = b.enderecos();
		this.telefones = b.telefones();
		this.emails = b.emails();
		this.sync = b.sync();
	}

	public void setSync(boolean sync) {
		this.sync = sync;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public String getObservacao() {
		return observacao;
	}

	public String getCpfCgc() {
		return cpfCgc;
	}

	public String getRgInscricao() {
		return rgInscricao;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public String getContato() {
		return contato;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public List<Telefone> getTelefones() {
		return telefones;
	}

	public List<Email> getEmails() {
		return emails;
	}

	public boolean isSync() {
		return sync;
	}

	@Override
	public Pessoa clone() {
		return this.clone();
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.codigo, this.empresa);
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Pessoa) {
			Pessoa other = (Pessoa) o;
			return Objects.equal(this.codigo, other.codigo) && Objects.equal(this.empresa, other.codigo);
		}
		return false;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("codigo", codigo).add("empresa", empresa)
				.add("razaoSocial", razaoSocial).add("nomeFantasia", nomeFantasia)
				.add("observacao", observacao).add("cpfCgc", cpfCgc).add("rgInscricao", rgInscricao)
				.add("dataCadastro", dataCadastro)
				.add("String contato", contato).add("enderecos", enderecos).add("telefones", telefones)
				.add("emails", emails).add("sync", sync).toString();
	}

	@Override
	public int compareTo(Pessoa another) {
		return this.nomeFantasia.compareTo(another.nomeFantasia);
	}

	public Pessoa salvar(Context ctx) {
		RepositorioPessoa repo = new RepositorioPessoa(ctx);
		repo.salvar(this);
		return this;
	}

	public static class Builder {
		private int codigo;
		private Empresa empresa;
		private String razaoSocial;
		private String nomeFantasia;
		private String observacao;
		private String cpfCgc;
		private String rgInscricao;
		private Date dataCadastro;
		private String contato;
		private Map<TipoEndereco, Endereco> enderecos = Maps.newHashMap();
		private Map<TipoTelefone, Telefone> telefones = Maps.newHashMap();
		private List<Email> emails = Lists.newLinkedList();
		private boolean sync;

		public Builder(int codigo) {
			checkArgument(codigo > 0);
			this.codigo = codigo;
		}

		public Builder(FaixaCodigoCliente codigo) {
			checkNotNull(codigo);
			checkArgument(!codigo.isUtilizado(), "nao é possivel instanciar Pessoa com codigo utilizado");
			checkArgument(codigo.getCodigoCliente() > 0);
			this.codigo = codigo.getCodigoCliente();
		}

		public Builder comEmpresa(Empresa empresa) {
			checkNotNull(empresa);
			this.empresa = empresa;
			return this;
		}
		
		public Builder comRazaoSocial(String razaoSocial) {
			System.out.println(Objects.toStringHelper(this).add("cod", codigo).add("nome", razaoSocial)
					.toString());
			checkNotNull(razaoSocial);
			this.razaoSocial = razaoSocial;
			return this;
		}
		
		public Builder comNomeFantasia(String nomeFantasia) {
			this.nomeFantasia = nomeFantasia;
			return this;
		}

		public Builder comObservacao(String observacao) {
			this.observacao = observacao;
			return this;
		}

		public Builder comCgcCpf(String cpfCgc) {
			checkNotNull(cpfCgc);
			this.cpfCgc = cpfCgc;
			return this;
		}

		public Builder comInscRg(String rgInscricao) {
			checkNotNull(rgInscricao);
			this.rgInscricao = rgInscricao;
			return this;
		}

		public Builder comDataCadastro(Date dataCadastro) {
			checkNotNull(dataCadastro);
			this.dataCadastro = dataCadastro;
			return this;
		}

		public Builder comContato(String contato) {
			this.contato = contato;
			return this;
		}

		public Builder addEndereco(Endereco endereco) {
			checkNotNull(endereco);
			enderecos.put(endereco.getTipoEndereco(), endereco);
			return this;
		}

		public Builder addTelefone(Telefone telefone) {
			checkNotNull(telefone);
			telefones.put(telefone.getTipoTelefone(), telefone);
			return this;
		}

		public Builder addEmail(Email email) {
			checkNotNull(email);
			if (!emails.contains(email)) {
				emails.add(email);
			}
			return this;
		}

		public Builder sincronozado(boolean sincronizado) {
			this.sync = sincronizado;
			return this;
		}

		public Builder comEnderecos(List<Endereco> enderecos) {
			if (enderecos != null)
				for (Endereco e : enderecos)
					this.enderecos.put(e.getTipoEndereco(), e);
			return this;
		}

		public Builder comTelefones(List<Telefone> fones) {
			checkNotNull(fones);
			for (Telefone t : fones)
				this.telefones.put(t.getTipoTelefone(), t);
			return this;
		}

		public Builder comEmails(List<Email> emails) {
			this.emails = emails;
			return this;
		}

		Integer codigo() {
			return this.codigo;
		}

		Empresa empresa() {
			return this.empresa;
		}

		String razaoSocial() {
			return this.razaoSocial;
		}

		String nomeFantasia() {
			return this.nomeFantasia;
		}

		String observacao() {
			return this.observacao;
		}

		String cpfCgc() {
			return this.cpfCgc;
		}

		String rgInscricao() {
			return this.rgInscricao;
		}

		Date dataCadastro() {
			return this.dataCadastro;
		}

		List<Endereco> enderecos() {
			return new ArrayList<Endereco>(this.enderecos.values());
		}

		List<Telefone> telefones() {
			return new ArrayList<Telefone>(this.telefones.values());
		}

		String contato() {
			return this.contato;
		}

		List<Email> emails() {
			return this.emails;
		}

		boolean sync() {
			return this.sync;
		}

		public Pessoa build() {
			return new Pessoa(this);
		}

	}

}
