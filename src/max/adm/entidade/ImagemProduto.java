package max.adm.entidade;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.List;

import max.adm.dominio.Entidade;
import max.adm.repositorio.RepositorioProImagem;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.Log;

public class ImagemProduto extends Entidade {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "IMAGEMPRODUTO";

	public static final String CODIGO = "CODIGO";
	public static final String PRODUTO = "PRODUTO_ID";
	public static final String IMAGEM = "IMAGEM";

	public static String[] COLUNAS = { CODIGO, PRODUTO, IMAGEM };

	private int codigo;
	private Produto produto;
	private byte[] imagem;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public byte[] getImagem() {
		return imagem;
	}

	public void setImagem(byte[] imagem) {
		this.imagem = imagem;
	}

	public static ImagemProduto buscaPorProduto(Context ctx, int codigoProduto) {
		RepositorioProImagem repoImg = new RepositorioProImagem(ctx);
		ImagemProduto imagemProduto = repoImg.buscaPorProduto(codigoProduto);
		repoImg.fechar();
		return imagemProduto;
	}

	public static boolean salvarLista(Context ctx, List<ImagemProduto> imagens) {
		RepositorioProImagem repo = new RepositorioProImagem(ctx);
		boolean ok = repo.salvarLista(imagens);
		repo.fechar();
		return ok;
	}

	public int salvar(Context ctx) {
		RepositorioProImagem repo = new RepositorioProImagem(ctx);
		this.codigo = repo.salvar(this);
		repo.fechar();
		return this.codigo;
	}

	public Bitmap getBitmap() {
		try {
			final int IMAGE_MAX_SIZE = 150000; // 1.5MP
			ByteArrayInputStream in = new ByteArrayInputStream(this.imagem);
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(in, null, o);
			in.close();
			int scale = 1;
			while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) > IMAGE_MAX_SIZE) {
				scale++;
			}
			Bitmap b = null;
			in = new ByteArrayInputStream(this.imagem);
			if (scale > 1) {
				scale--;
				// larger than target
				o = new BitmapFactory.Options();
				o.inSampleSize = scale;
				b = BitmapFactory.decodeStream(in, null, o);

				// resize to desired dimensions
				int height = b.getHeight();
				int width = b.getWidth();

				double y = Math.sqrt(IMAGE_MAX_SIZE / (((double) width) / height));
				double x = (y / height) * width;

				Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x, (int) y, true);
				b.recycle();
				b = scaledBitmap;

				System.gc();
			} else {
				b = BitmapFactory.decodeStream(in);
			}
			in.close();
			return b;
		} catch (Throwable e) {
			Log.e("img", e.getMessage());
			return null;
		}
	}

	public static Bitmap getBitmap(byte[] bytes) {
		try {
			final int IMAGE_MAX_SIZE = 150000; // 1.5MP
			ByteArrayInputStream in = new ByteArrayInputStream(bytes);
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(in, null, o);
			in.close();
			int scale = 1;
			while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) > IMAGE_MAX_SIZE) {
				scale++;
			}
			Bitmap b = null;
			in = new ByteArrayInputStream(bytes);
			if (scale > 1) {
				scale--;
				// larger than target
				o = new BitmapFactory.Options();
				o.inSampleSize = scale;
				b = BitmapFactory.decodeStream(in, null, o);

				// resize to desired dimensions
				int height = b.getHeight();
				int width = b.getWidth();

				double y = Math.sqrt(IMAGE_MAX_SIZE / (((double) width) / height));
				double x = (y / height) * width;

				Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x, (int) y, true);
				b.recycle();
				b = scaledBitmap;

				System.gc();
			} else {
				b = BitmapFactory.decodeStream(in);
			}
			in.close();
			return b;
		} catch (Throwable e) {
			Log.e("img", e.getMessage());
			return null;
		}
	}

	public Bitmap getMiniBitmap() {
		try {
			final int IMAGE_MAX_SIZE = 10000; // 0.1MP
			ByteArrayInputStream in = new ByteArrayInputStream(this.imagem);
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(in, null, o);
			in.close();
			int scale = 1;
			while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) > IMAGE_MAX_SIZE) {
				scale++;
			}
			Bitmap b = null;
			in = new ByteArrayInputStream(this.imagem);
			if (scale > 1) {
				scale--;
				// larger than target
				o = new BitmapFactory.Options();
				o.inSampleSize = scale;
				b = BitmapFactory.decodeStream(in, null, o);

				// resize to desired dimensions
				int height = b.getHeight();
				int width = b.getWidth();

				double y = Math.sqrt(IMAGE_MAX_SIZE / (((double) width) / height));
				double x = (y / height) * width;

				Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x, (int) y, true);
				b.recycle();
				b = scaledBitmap;

				System.gc();
			} else {
				b = BitmapFactory.decodeStream(in);
			}
			in.close();
			return b;
		} catch (Throwable e) {
			Log.e("img", e.getMessage());
			return null;
		}
	}

	public static Drawable getMiniDrawable(int codigoProduto) {
		String path = String.format("%s/%s/%d/mini/%dmini.jpg", Environment.getExternalStorageDirectory()
				.getPath(), "maxMobile/produtos/", codigoProduto, codigoProduto);
		return Drawable.createFromPath(path);

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codigo;
		result = prime * result + Arrays.hashCode(imagem);
		result = prime * result + ((produto == null) ? 0 : produto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImagemProduto other = (ImagemProduto) obj;
		if (codigo != other.codigo)
			return false;
		if (!Arrays.equals(imagem, other.imagem))
			return false;
		if (produto == null) {
			if (other.produto != null)
				return false;
		} else if (!produto.equals(other.produto))
			return false;
		return true;
	}
}
