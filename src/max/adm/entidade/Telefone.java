package max.adm.entidade;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import max.adm.dominio.Entidade;
import max.adm.telefone.TipoTelefone;

public class Telefone extends Entidade {
	
	public static String NOME_TABELA = "TELEFONE";

	public static String CODIGO = "CODIGO";
	public static String NUMEROTELEFONE = "NUMERO_TELEFONE";
	public static String TIPOTELEFONE = "TIPO_TELEFONE";
	public static String PESSOA = "PESSOA_ID";
	public static String EMPRESA = "EMPRESA_ID";
	
	public static String[] COLUNAS = {CODIGO, NUMEROTELEFONE, TIPOTELEFONE, PESSOA, EMPRESA}; 

	private static final long serialVersionUID = 1L;

	private Integer codigo;
	private String numeroTelefone;
	private TipoTelefone tipoTelefone;
	
	private Empresa empresa;
	private Pessoa pessoa;

	public Integer getCodigo() {
		return codigo;
	}

	public String getNumeroTelefone() {
		return numeroTelefone;
	}

	public TipoTelefone getTipoTelefone() {
		return tipoTelefone;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	private Telefone(Builder builder) {
		this.codigo = builder.codigo;
		this.numeroTelefone = builder.numero;
		this.tipoTelefone = builder.tipoTelefone;
		this.empresa = builder.empresa;
	}

	public static Builder builder(Empresa empresa, TipoTelefone tipoTelefone) {
		return new Builder(empresa,  tipoTelefone);
	}

	public static Builder builder(Integer codigo, Empresa empresa,
			TipoTelefone tipoTelefone) {
		return new Builder(codigo, empresa,  tipoTelefone);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((empresa == null) ? 0 : empresa.hashCode());
		result = prime * result + ((numeroTelefone == null) ? 0 : numeroTelefone.hashCode());
		result = prime * result + ((tipoTelefone == null) ? 0 : tipoTelefone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Telefone other = (Telefone) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (empresa == null) {
			if (other.empresa != null)
				return false;
		} else if (!empresa.equals(other.empresa))
			return false;
		if (numeroTelefone == null) {
			if (other.numeroTelefone != null)
				return false;
		} else if (!numeroTelefone.equals(other.numeroTelefone))
			return false;
		if (tipoTelefone != other.tipoTelefone)
			return false;
		return true;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public static class Builder {
		private Integer codigo;
		private Integer ddd = 0;
		private TipoTelefone tipoTelefone;
		private String numero;
		private Empresa empresa;

		private Builder(Integer codigo, Empresa empresa, TipoTelefone tipoTelefone) {
			checkNotNull(codigo);
			checkNotNull(tipoTelefone);
			checkArgument(codigo > 0);
			this.codigo = codigo;
			this.tipoTelefone = tipoTelefone;
			this.empresa = empresa;
		}

		private Builder(Empresa empresa, TipoTelefone tipoTelefone) {
			checkNotNull(tipoTelefone);
			this.tipoTelefone = tipoTelefone;
			this.empresa = empresa;
		}

		public Builder withNumero(String numero) {
			checkNotNull(numero, "Número não pode ser nulo.");
			//checkArgument(numero.matches("\\d*"), "Número deve conter somente dígitos: "+numero);
			this.numero = numero;
			return this;
		}

		public Builder withDdd(int ddd) {
			checkArgument(ddd > 0, "DDD deve ser maior que zero.");
			this.ddd = ddd;
			return this;
		}

		public Telefone build() {
			checkState(numero != null);
			checkState(ddd != null);
			checkState(empresa != null);
			return new Telefone(this);
		}
	}
}
