package max.adm.entidade;

import java.util.List;

import com.google.common.base.Objects;

import android.content.Context;
import max.adm.classificacaoPessoa.RepositorioClassificacaoPessoa;
import max.adm.dominio.Entidade;

public class ClassifcacaoPessoa extends Entidade {

	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "CLASSIFICACAOPESSOA";

	public static String CODIGO = "CODIGO";
	public static String DESCRICAO = "DESCRICAO";

	public static String[] COLUNAS = { CODIGO, DESCRICAO };

	private String descricao;
	private String codigo;

	public String toString() {
		return this.descricao;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.codigo, this.descricao);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ClassifcacaoPessoa) {
			ClassifcacaoPessoa other = (ClassifcacaoPessoa) obj;
			return Objects.equal(this.descricao, other.descricao) && Objects.equal(this.codigo, other.codigo);
		}
		return false;
	}

	public static List<ClassifcacaoPessoa> listar(Context ctx) {
		RepositorioClassificacaoPessoa repoClass = new RepositorioClassificacaoPessoa(ctx);
		List<ClassifcacaoPessoa> classificacoes = repoClass.listar();
		repoClass.fechar();

		return classificacoes;

	}

	public void salvar(Context ctx) {
		RepositorioClassificacaoPessoa repoClass = new RepositorioClassificacaoPessoa(ctx);
		this.codigo = repoClass.salvar(this);
		repoClass.fechar();
	}

	public static boolean salvarLista(Context ctx, List<ClassifcacaoPessoa> classificacoes) {
		RepositorioClassificacaoPessoa repoClass = new RepositorioClassificacaoPessoa(ctx);
		boolean ok = repoClass.salvarLista(classificacoes);
		repoClass.fechar();
		return ok;
	}

}
