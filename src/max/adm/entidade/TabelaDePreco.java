package max.adm.entidade;

import java.util.List;

import max.adm.dominio.Entidade;
import max.adm.tabelaDePreco.RepositorioTabelaDePreco;
import android.content.Context;

public class TabelaDePreco extends Entidade {

	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "TABELAPRECO";

	public static String CODIGO = "CODIGO";
	public static String DESCRICAO = "DESCRICAO";
	public static String PERCENTUALDESCONTO = "PERC_DESCONTO";
	public static String EMPRESA = "EMPRESA_ID";

	public static String[] COLUNAS = { CODIGO, DESCRICAO, PERCENTUALDESCONTO, EMPRESA };

	private Integer codigo;
	private String descricao;
	private Double percentualDesconto;
	private Empresa empresa;
	
	public String toString(){
		return String.valueOf(this.codigo)+" "+this.descricao;
	}
	
	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getPercentualDesconto() {
		return percentualDesconto;
	}

	public void setPercentualDesconto(Double percentualDesconto) {
		this.percentualDesconto = percentualDesconto;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((empresa == null) ? 0 : empresa.hashCode());
		long temp;
		temp = Double.doubleToLongBits(percentualDesconto);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TabelaDePreco other = (TabelaDePreco) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (empresa == null) {
			if (other.empresa != null)
				return false;
		} else if (!empresa.equals(other.empresa))
			return false;
		if (Double.doubleToLongBits(percentualDesconto) != Double
				.doubleToLongBits(other.percentualDesconto))
			return false;
		return true;
	}

	public int salvar(Context ctx) {
		RepositorioTabelaDePreco repoTabPreco = new RepositorioTabelaDePreco(ctx);
		this.codigo = repoTabPreco.salvar(this);
		repoTabPreco.fechar();

		return this.codigo;

	}

	public static List<TabelaDePreco> listar(Context ctx) {
		RepositorioTabelaDePreco repoTabPreco = new RepositorioTabelaDePreco(
				ctx);

		List<TabelaDePreco> tabelas = repoTabPreco.listar();
		repoTabPreco.fechar();
		return tabelas;
	}

	public static List<TabelaDePreco> listaPorEmpresa(Empresa emp, Context ctx) {
		RepositorioTabelaDePreco repoTabPreco = new RepositorioTabelaDePreco(ctx);

		List<TabelaDePreco> tabelas = repoTabPreco.listar(emp);
		repoTabPreco.fechar();
		return tabelas;

	}

	public static boolean salvarLista(Context ctx, List<TabelaDePreco> tabelas) {
		RepositorioTabelaDePreco repo = new RepositorioTabelaDePreco(ctx);
		boolean ok = repo.salvarLista(tabelas);
		repo.fechar();
		return ok;
	}

}
