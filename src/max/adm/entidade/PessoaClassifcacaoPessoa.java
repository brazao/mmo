package max.adm.entidade;

import java.util.List;

import com.google.common.base.Objects;

import max.adm.dominio.Entidade;
import max.adm.repositorio.RepositorioPessoaClassificacaoPessoa;
import android.content.Context;

public class PessoaClassifcacaoPessoa extends Entidade {

	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "PESSOA_CLASSIFICACAOPESSOA";

	public static final String EMPRESA = "EMPRESA_ID";
	public static final String PESSOA = "PESSOA_ID";
	public static final String CLASSIFICACAOPESSOA = "CLASSIFICACAOPESSOA_ID";

	public static String[] COLUNAS = { EMPRESA, PESSOA, CLASSIFICACAOPESSOA };

	private Empresa empresa;
	private Pessoa pessoa;
	private ClassifcacaoPessoa classifcacaoPessoa;

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public ClassifcacaoPessoa getClassifcacaoPessoa() {
		return classifcacaoPessoa;
	}

	public void setClassifcacaoPessoa(ClassifcacaoPessoa classifcacaoPessoa) {
		this.classifcacaoPessoa = classifcacaoPessoa;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classifcacaoPessoa == null) ? 0 : classifcacaoPessoa.hashCode());
		result = prime * result + ((empresa == null) ? 0 : empresa.hashCode());
		result = prime * result + ((pessoa == null) ? 0 : pessoa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		
		if (obj instanceof PessoaClassifcacaoPessoa) {
			PessoaClassifcacaoPessoa other = (PessoaClassifcacaoPessoa) obj;
			return Objects.equal(this.classifcacaoPessoa, other) && 
					Objects.equal(this.empresa, other.empresa) &&
					Objects.equal(this.pessoa, other.pessoa);
		}
		
		return false;
	}

	public static List<PessoaClassifcacaoPessoa> listar(Context ctx) {
		RepositorioPessoaClassificacaoPessoa repoClass = new RepositorioPessoaClassificacaoPessoa(ctx);
		List<PessoaClassifcacaoPessoa> classificacoes = repoClass.listar();
		repoClass.fechar();

		return classificacoes;

	}

	public void salvar(Context ctx) {
		RepositorioPessoaClassificacaoPessoa repoClass = new RepositorioPessoaClassificacaoPessoa(ctx);
		repoClass.salvar(this);
		repoClass.fechar();
	}
}
