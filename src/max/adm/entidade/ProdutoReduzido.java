package max.adm.entidade;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import max.adm.dominio.Entidade;
import max.adm.produtoreduzido.repositorio.RepositorioProdutoReduzido;
import android.content.Context;

import com.google.common.base.Objects;

public class ProdutoReduzido extends Entidade {

	public static String NOME_TABELA = "PRODUTOREDUZIDO";

	public static final String RED_PRODUTO_KEY = "RED_PRODUTO_KEY";
	public static final String PRODUTO = "PRODUTO_ID";
	public static final String TAMANHO = "TAMANHO_ID";
	public static final String CODIGOBARRAS = "CODIGOBARRAS";
	public static final String EMPRESA = "EMPRESA_ID";
	public static final String PADRAO = "PADRAO_ID";

	public static String[] COLUNAS = { RED_PRODUTO_KEY, PRODUTO, TAMANHO, EMPRESA, PADRAO, CODIGOBARRAS };

	private static final long serialVersionUID = 1L;

	private Integer redProdutoKey;
	private Produto produto;
	private PadraoProduto padrao;
	private TamanhoProduto tamanho;
	private String codigoDeBarras;
	private Empresa empresa;

	private ProdutoReduzido(Builder b) {
		this.redProdutoKey = b.redProdutoKey;
		this.produto = b.produto;
		this.padrao = b.padrao;
		this.tamanho = b.tamanho;
		this.codigoDeBarras = b.codigoDeBarras;
		this.empresa = b.empresa;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static Builder builder(Integer redProdutoKey) {
		return new Builder(redProdutoKey);
	}

	public Integer getRed_produto_key() {
		return redProdutoKey;
	}

	public Produto getProduto() {
		return produto;
	}

	public PadraoProduto getPadrao() {
		return padrao;
	}

	public TamanhoProduto getTamanho() {
		return tamanho;
	}

	// public TabelaDePreco getTabelaDePreco() {
	// return tabelaDePreco;
	// }

	public String getCodigoDeBarras() {
		return codigoDeBarras;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ProdutoReduzido) {
			ProdutoReduzido other = (ProdutoReduzido) obj;
			return Objects.equal(this.redProdutoKey, other.redProdutoKey);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.redProdutoKey);
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("reduzido", redProdutoKey).add("produto", produto)
				.add("padrao", padrao).add("tamanho", tamanho).toString();
	}

	public static class Builder {
		private Integer redProdutoKey;
		private Produto produto;
		private PadraoProduto padrao;
		private TamanhoProduto tamanho;
		private String codigoDeBarras;
		private Empresa empresa;

		private Builder() {
		}

		private Builder(Integer redProdutoKey) {
			checkNotNull(redProdutoKey);
			checkArgument(redProdutoKey > 0);
			this.redProdutoKey = redProdutoKey;
		}

		public Builder comEmpresa(Empresa empresa) {
			checkNotNull(redProdutoKey);
			checkArgument(redProdutoKey > 0);
			this.empresa = empresa;
			return this;
		}

		public Builder comRedProdutoKey(Integer redProdutoKey) {
			checkNotNull(redProdutoKey);
			checkArgument(redProdutoKey > 0);
			this.redProdutoKey = redProdutoKey;
			return this;
		}

		public Builder comProduto(Produto produto) {
			checkNotNull(produto);
			this.produto = produto;
			return this;
		}

		public Builder comPadrao(PadraoProduto padrao) {
			checkNotNull(padrao);
			this.padrao = padrao;
			return this;
		}

		public Builder comTamanho(TamanhoProduto tamanho) {
			checkNotNull(tamanho);
			this.tamanho = tamanho;
			return this;
		}

		public Builder comCodigoDeBarras(String codigoDeBarras) {
			this.codigoDeBarras = codigoDeBarras;
			return this;
		}

		public ProdutoReduzido build() {
			checkState(redProdutoKey != null);
			checkState(produto != null);
			checkState(padrao != null);
			checkState(tamanho != null);
			checkState(empresa != null);
			return new ProdutoReduzido(this);
		}
	}

	public void putPadrao(PadraoProduto padrao) {
		checkNotNull(padrao);
		this.padrao = padrao;
	}

	public void putTamanho(TamanhoProduto tamanho) {
		checkNotNull(tamanho);
		this.tamanho = tamanho;
	}

	public ProdutoReduzido salvar(Context ctx) {
		RepositorioProdutoReduzido repo = new RepositorioProdutoReduzido(ctx);
		this.redProdutoKey = repo.salvar(this);
		repo.fechar();
		return this;
	}

}
