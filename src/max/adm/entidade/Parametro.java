package max.adm.entidade;

import java.util.Date;
import java.util.List;

import max.adm.dominio.Entidade;
import max.adm.empresa.RepositorioEmpresa;
import max.adm.parametro.ParametroMaximum;
import max.adm.parametro.RepositorioParametro;
import android.content.Context;

public class Parametro extends Entidade {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// dados da tabela de sncronismo
	public static String TABELA_SYNC = "SYNCRONISMO";

	public static String SYNC_ID = "SYNC_ID";
	public static String SYNC_EMPRESA = "EMPRESA";
	public static String SYNC_DATA = "SYNC_DATA";
	public static String SYNC_TIPO = "SYNC_TIPO";
	public static String SYNC_CLIENTE = "SYNC_CLIENTE";
	public static String SYNC_PEDIDO = "SYNC_PEDIDO";
	public static String SYNC_TABPRECO = "SYNC_TABPRECO";
	public static String SYNC_FAIXACODIGO = "SYNC_FAIXACODIGO";
	public static String SYNC_CEP = "SYNC_CEP";
	public static String SYNC_CONDICAOPGTO = "SYNC_CONDICAOPGTO";
	public static String SYNC_CONTARECEBER = "SYNC_CONTARECEBER";

	public static String NOME_TABELA = "PARAMETRO";

	public static String ID = "PAR_ID";
	public static String EMPRESA = "FIA_EMPRESA_EMP_ID";
	public static String CODIGOREPRESENTANTE = "PAR_CODIGOREPRESENTANTE";
	public static String NOMEREPRESENTANTE = "PAR_NOMEREPRESENTANTE";
	public static String SYNCAUTOMATICA = "PAR_SYNCAUTOMATICA";
	public static String URLWEBSERVICE = "PAR_URLWEBSERVICE";
	public static String EMAIL = "PAR_EMAIL";
	public static String SENHAEMAIL = "PAR_SENHAEMAIL";
	public static String SMTP = "PAR_SMTP";
	public static String ASSINATURA = "PAR_ASSINATURA";
	public static String PORTAEMAIL = "PAR_PORTAEMAIL";
	public static String SSL = "PAR_SSL";
	public static String REPRESENTANTEFAIXA = "PAR_REPRFAIXA";
	public static String TRABALHAPROREGIAO = "PAR_REGIAO";
	public static String IMAGEM = "PAR_IMAGEM";
	public static String USADADOSFINANCEIROS = "PAR_DADOS_FIN";
	public static String PRIMEIROACESSO = "PAR_PRIMEIRO_ACESSO";
	public static String EXIGETELEFONECADCLEINTE = "PAR_EXIGE_FONE_CAD_CIENTE";
	public static String EXIGEEMAILCADCLIENTE = "PAR_EXIGE_EMAIL_CAD_CLIENTE";
	public static String NUMEROMAXPECASPEDIDO = "PAR_NUMERO_MAX_PECAS_PEDIDO";
	public static String PERMITECNPJREPETIDO = "PAR_PERMITECNPJREPETIDO";
	public static String DUPLICACADASTROCLIENTE = "PAR_DUPLICACADASTROCLIENTE";
	public static String MULTITABELAPRECO = "PAR_MULTITABELAPRECO";
	public static String USALOTE = "PAR_USALOTE";
	public static String MOSTRAPRECONOPEDIDO = "PAR_MOSTRAPRECOPEDIDO";
	public static String CHAVE = "PAR_CHAVE";
	public static String MAXIMODESCONTO = "maximodesconto";

	public static String[] COLUNAS = {ID, EMPRESA, CODIGOREPRESENTANTE, NOMEREPRESENTANTE, SYNCAUTOMATICA, URLWEBSERVICE, EMAIL, SENHAEMAIL, SMTP, ASSINATURA, PORTAEMAIL, SSL, REPRESENTANTEFAIXA,
			TRABALHAPROREGIAO, IMAGEM, USADADOSFINANCEIROS, PRIMEIROACESSO, EXIGETELEFONECADCLEINTE, EXIGEEMAILCADCLIENTE, NUMEROMAXPECASPEDIDO, PERMITECNPJREPETIDO, DUPLICACADASTROCLIENTE,
			MULTITABELAPRECO, USALOTE, MOSTRAPRECONOPEDIDO, CHAVE, MAXIMODESCONTO};

	private int id;
	private Empresa empresa;
	private int codigoRepresentante;
	private String nomeRepresentante;
	private boolean syncAutomatica;
	private String urlWebService;
	private String email;
	private String senhaEmail;
	private String smtp;
	private String assinatura;
	private int portaEmail;
	private boolean ssl;
	private int representantefaixa;
	private boolean TrabalhaProRegiao;
	private byte[] imagem;
	private boolean usaDadosFinanceiros;
	private boolean primeiroAcesso;
	private boolean exigeEmailCadCliente;
	private boolean exigeTelefoneCadCleinte;
	private int numeroMaxPecasPedido;
	private boolean permiteCnpjRepetido;
	private boolean duplicaCadastroCliente;
	private boolean multiTabelaPreco;
	private boolean usaLote;
	private boolean mostraPrecoNoPedido;
	private String chave;
	private double maximoDesconto;
	
	public Parametro(){
		
	}

	private Parametro(ParametroMaximum par, Context ctx) {
		 
		 this.maximoDesconto = par.maximodesconto != null ? par.maximodesconto.doubleValue() : 0;
		 this.usaDadosFinanceiros = "S".equals(par.parDadosFin);
		 this.duplicaCadastroCliente = "S".equals(par.parDuplicacadastrocliente);
		 this.exigeEmailCadCliente = "S".equals(par.parExigeEmailCadCliente);
		 this.exigeTelefoneCadCleinte = "S".equals(par.parExigeFoneCadCiente);
		 RepositorioEmpresa repoEmp = new RepositorioEmpresa(ctx);
		 this.empresa = repoEmp.buscaEmpresa( par.parLjaCodigo.intValue());
		 repoEmp.fechar();
		 this.multiTabelaPreco = "S".equals(par.parMultitabelapreco);
		 this.permiteCnpjRepetido = "S".equals(par.parPermitecnpjrepetido);
		 this.TrabalhaProRegiao = "S".equals(par.parRegiao);
		 this.syncAutomatica = "S".equals(par.parSyncAutomatica);
		 this.usaLote = "S".equals(par.parUsaLotes);
	}
	
	public static Parametro newInstance(ParametroMaximum par, Context ctx) {
		return new Parametro(par, ctx);
	}
	
	public double getMaximoDesconto() {
		return maximoDesconto;
	}

	public void setMaximoDesconto(double maximoDesconto) {
		this.maximoDesconto = maximoDesconto;
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public boolean isMostraPrecoNoPedido() {
		return mostraPrecoNoPedido;
	}

	public void setMostraPrecoNoPedido(boolean mostraPrecoNoPedido) {
		this.mostraPrecoNoPedido = mostraPrecoNoPedido;
	}

	public boolean usaLote() {
		return usaLote;
	}

	public void setUsaLote(boolean usaLote) {
		this.usaLote = usaLote;
	}

	public boolean isMultiTabelaPreco() {
		return multiTabelaPreco;
	}

	public void setMultiTabelaPreco(boolean multiTabelaPreco) {
		this.multiTabelaPreco = multiTabelaPreco;
	}

	public boolean isPermiteCnpjRepetido() {
		return permiteCnpjRepetido;
	}

	public void setPermiteCnpjRepetido(boolean permiteCnpjRepetido) {
		this.permiteCnpjRepetido = permiteCnpjRepetido;
	}

	public boolean isDuplicaCadastroCliente() {
		return duplicaCadastroCliente;
	}

	public void setDuplicaCadastroCliente(boolean duplicaCadastroCliente) {
		this.duplicaCadastroCliente = duplicaCadastroCliente;
	}

	public String getSenhaEmail() {
		return senhaEmail;
	}

	public void setSenhaEmail(String senhaEmail) {
		this.senhaEmail = senhaEmail;
	}

	public int getCodigoRepresentante() {
		return codigoRepresentante;
	}

	public void setCodigoRepresentante(int codigoRepresentante) {
		this.codigoRepresentante = codigoRepresentante;
	}

	public String getNomeRepresentante() {
		return nomeRepresentante;
	}

	public void setNomeRepresentante(String nomeRepresentante) {
		this.nomeRepresentante = nomeRepresentante;
	}

	public boolean isSyncAutomatica() {
		return syncAutomatica;
	}

	public void setSyncAutomatica(boolean syncAutomatica) {
		this.syncAutomatica = syncAutomatica;
	}

	public int getNumeroMaxPecasPedido() {
		return numeroMaxPecasPedido;
	}

	public void setNumeroMaxPecasPedido(int numeroMaxPecasPedido) {
		this.numeroMaxPecasPedido = numeroMaxPecasPedido;
	}

	public boolean isExigeEmailCadCliente() {
		return exigeEmailCadCliente;
	}

	public void setExigeEmailCadCliente(boolean exigeEmailCadCliente) {
		this.exigeEmailCadCliente = exigeEmailCadCliente;
	}

	public boolean isExigeTelefoneCadCleinte() {
		return exigeTelefoneCadCleinte;
	}

	public void setExigeTelefoneCadCleinte(boolean exigeTelefoneCadCleinte) {
		this.exigeTelefoneCadCleinte = exigeTelefoneCadCleinte;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getUrlWebService() {
		return urlWebService;
	}

	public void setUrlWebService(String urlWebService) {
		this.urlWebService = urlWebService;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSmtp() {
		return smtp;
	}

	public void setSmtp(String smtp) {
		this.smtp = smtp;
	}

	public String getAssinatura() {
		return assinatura;
	}

	public void setAssinatura(String assinatura) {
		this.assinatura = assinatura;
	}

	public int getPortaEmail() {
		return portaEmail;
	}

	public void setPortaEmail(int portaEmail) {
		this.portaEmail = portaEmail;
	}

	public boolean isSsl() {
		return ssl;
	}

	public void setSsl(boolean ssl) {
		this.ssl = ssl;
	}

	public int getRepresentantefaixa() {
		return representantefaixa;
	}

	public void setRepresentantefaixa(int representantefaixa) {
		this.representantefaixa = representantefaixa;
	}

	public boolean isTrabalhaProRegiao() {
		return TrabalhaProRegiao;
	}

	public void setTrabalhaProRegiao(boolean trabalhaProRegiao) {
		TrabalhaProRegiao = trabalhaProRegiao;
	}

	public byte[] getImagem() {
		return imagem;
	}

	public void setImagem(byte[] imagem) {
		this.imagem = imagem;
	}

	public boolean isUsaDadosFinanceiros() {
		return usaDadosFinanceiros;
	}

	public void setUsaDadosFinanceiros(boolean usaDadosFinanceiros) {
		this.usaDadosFinanceiros = usaDadosFinanceiros;
	}

	public boolean isPrimeiroAcesso() {
		return primeiroAcesso;
	}

	public void setPrimeiroAcesso(boolean primeiroAcesso) {
		this.primeiroAcesso = primeiroAcesso;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Parametro other = (Parametro) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public Parametro buscaParametro(int id, Context ctx) {
		RepositorioParametro repositorio = new RepositorioParametro(ctx);
		Parametro par = repositorio.busca(id);
		repositorio.fechar();
		return par;

	}

	public void salvar(Context ctx) {
		RepositorioParametro repositorio = new RepositorioParametro(ctx);
		repositorio.salvar(this);
		repositorio.fechar();
	}

	public static boolean tabelaVazia(Context ctx) {
		RepositorioParametro repo = new RepositorioParametro(ctx);
		boolean vazia = !repo.contemRegistro();
		repo.fechar();
		return vazia;
	}

	public static Date verificaUltimoSincronismo(Context ctx, int lojaId) {
		Date dataUltimosincronismo = null;
		RepositorioParametro repoParam = new RepositorioParametro(ctx);
		dataUltimosincronismo = repoParam.verificaUltmoSincrnismo(lojaId);
		repoParam.fechar();

		return dataUltimosincronismo;
	}

	public static Parametro buscaPorLoja(Context ctx, int idDaLoja) {
		RepositorioParametro repoParam = new RepositorioParametro(ctx);
		Parametro p = repoParam.buscaPorLoja(idDaLoja);
		repoParam.fechar();

		return p;
	}

	/**
	 * Este metodo retorna uma lista com todos os parametros contidos no banco
	 * de dados.
	 * 
	 * @param ctx
	 * @return
	 */
	public static List<Parametro> listar(Context ctx) {
		RepositorioParametro repoParam = new RepositorioParametro(ctx);
		List<Parametro> params = repoParam.listar();
		repoParam.fechar();
		return params;

	}

	/**
	 * 
	 * @param ctx
	 *            Context - contexto da activity
	 * @param idEmpresa
	 *            Int - id da empresa que esta logado
	 * @return boolean * @true se o parametro estiver marcaso para replicar os
	 *         cadastros e @false se estiver marcado para NaO duplicar
	 */
	public static boolean replicaCadastro(Context ctx, int idEmpresa) {
		RepositorioParametro repoParam = new RepositorioParametro(ctx);
		boolean replicaCadastro = repoParam.replicaCadastro(idEmpresa);
		repoParam.fechar();
		return replicaCadastro;
	}

}
