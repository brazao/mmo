package max.adm.entidade;

import java.util.List;

import static com.google.common.base.Preconditions.*;
import max.adm.dominio.Entidade;
import max.adm.repositorio.RepositorioPadrao;
import android.content.Context;

public class PadraoProduto extends Entidade {

	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "PADRAOPRODUTO";

	public static String CODIGO = "CODIGO";
	public static String DESCRICAO = "DESCRICAO";
	public static String SIGLA = "SIGLA";
	
	public static String[] COLUNAS = {CODIGO, DESCRICAO, SIGLA};

	private int codigo;
	private String descricao;
	private String sigla;
	
	private PadraoProduto(int codigo, String descricao, String sigla){
		this.codigo = codigo;
		this.descricao = descricao;
		this.sigla = sigla;
	}
	
	public static PadraoProduto newPadrao(int codigo, String descricao, String sigla){
		checkArgument(codigo > 0, "codigo do Padao deve ser maior que 0");
		checkNotNull(descricao, "Descricao do padrao nao pode ser nula");
		return new PadraoProduto(codigo, descricao, sigla);
	}
	
	public int getCodigo() {
		return codigo;
	}

	public String toString(){
		return this.descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codigo;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PadraoProduto other = (PadraoProduto) obj;
		if (codigo != other.codigo)
			return false;
		return true;
	}

	public static boolean salvarLista(Context ctx, List<PadraoProduto> padroes){
		RepositorioPadrao repo = new RepositorioPadrao(ctx);
		boolean ok = repo.salvarLista(padroes);
		repo.fechar();
		return ok;
	}


	public PadraoProduto salvar(Context ctx) {
		RepositorioPadrao repo = new RepositorioPadrao(ctx);
		this.codigo = repo.salvar(this);
		repo.fechar();
		return this;
	}
	
	
	

}
