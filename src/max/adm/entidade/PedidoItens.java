package max.adm.entidade;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import max.adm.Dinheiro;
import max.adm.dominio.Entidade;
import max.adm.repositorio.RepositorioPedidoItem;
import android.content.Context;

import com.google.common.base.Objects;

public class PedidoItens extends Entidade {

	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "PEDIDOITENS";

	public static String ID = "PEDIDOITEM_ID";
	public static String RED_PRODUTO_KEY = "RED_PRODUTO_KEY";
	public static String QUANTIDADE = "PEDITEM_QUANTIDADE";
	public static String PRECO = "PEDITEM_PRECO";
	public static String NUMEROCONJUNTO = "PEDITEM_NRCONJUNTO";
	public static String PEDIDOCAPA = "PEDIDOCAPA_CPE_ID";
	public static String PRODUTOREDUZIDO = "PRO_REDUZIDO_PRODUTOS_REDUZIDO_ID";
	public static String DESCRICAO = "DESCRICAO";
	public static String TAMANHODESCRICAO = "TAMANHODESCRICAO";
	public static String PADRAOCODIGO = "PADRAOCODIGO";
	public static String PADRAODESCRICAO = "PADRAODESCRICAO";

	public static String[] COLUNAS = {ID, RED_PRODUTO_KEY, QUANTIDADE, PRECO, NUMEROCONJUNTO, PEDIDOCAPA, PRODUTOREDUZIDO, DESCRICAO, TAMANHODESCRICAO, PADRAOCODIGO, PADRAODESCRICAO};

	private int id;
	private int quantidade;
	private Dinheiro preco;
	private int numeroConjunto;
	private transient PedidoCapa pedidoCapa;
	private TabelaPrecoProdutoReduzido tabelaPrecoProdutoReduzido;

	private PedidoItens(int id, int quantidade, Dinheiro preco, int numeroConjunto, PedidoCapa pedido, TabelaPrecoProdutoReduzido tabelaPrecoProdutoReduzido) {
		this.id = id;
		this.quantidade = quantidade;
		this.preco = preco;
		this.numeroConjunto = numeroConjunto;
		this.pedidoCapa = pedido;
		this.tabelaPrecoProdutoReduzido = tabelaPrecoProdutoReduzido;
	}

	private PedidoItens(int quantidade, Dinheiro preco, int numeroConjunto, PedidoCapa pedido, TabelaPrecoProdutoReduzido tabelaPrecoProdutoReduzido) {

		this.quantidade = quantidade;
		this.preco = preco;
		this.numeroConjunto = numeroConjunto;
		this.pedidoCapa = pedido;
		this.tabelaPrecoProdutoReduzido = tabelaPrecoProdutoReduzido;
	}

	public PedidoItens(int quantidade, Dinheiro preco, int numeroConjunto, TabelaPrecoProdutoReduzido tabelaPrecoProdutoReduzido) {
		this.quantidade = quantidade;
		this.preco = preco;
		this.numeroConjunto = numeroConjunto;
		this.tabelaPrecoProdutoReduzido = tabelaPrecoProdutoReduzido;
	}

	public static PedidoItens newPedidoitens(int id, int quantidade, Dinheiro preco, int numeroConjunto, PedidoCapa pedido, TabelaPrecoProdutoReduzido tabelaPrecoProdutoReduzido) {
		checkArgument(id > 0, "id do item do pedido deve ser maior que 0.");
		checkArgument(quantidade > 0, "quantidade do item do pedido deve ser maior que 0");
		checkArgument(!preco.isZero(), "preco do item do pedido nao pode ser negativo");
		checkNotNull(tabelaPrecoProdutoReduzido, "nao e possivel inserir um item no pedido com o produtoRedizido nulo.");

		return new PedidoItens(id, quantidade, preco, numeroConjunto, pedido, tabelaPrecoProdutoReduzido);

	}

	public static PedidoItens newPedido(int quantidade, Dinheiro preco, int numeroConjunto, PedidoCapa pedido, TabelaPrecoProdutoReduzido tabelaPrecoProdutoReduzido) {
		checkArgument(quantidade > 0, "quantidade do item do pedido deve ser maior que 0");
		checkArgument(!preco.isZero(), "preco do item do pedido nao pode ser negativo");
		checkNotNull(pedido, "pedido do item do pedido nao pode ser nulo");
		checkNotNull(tabelaPrecoProdutoReduzido, "nao e possivel inserir um item no pedido com o produtoRedizido nulo.");
		return new PedidoItens(quantidade, preco, numeroConjunto, pedido, tabelaPrecoProdutoReduzido);
	}

	public static PedidoItens newItem(int quantidade, Dinheiro preco, int numeroConjunto, TabelaPrecoProdutoReduzido tabelaPrecoProdutoReduzido) {
		checkArgument(quantidade >= 0, "quantidade do item do pedido deve ser maior que 0");
		checkArgument(!preco.isZero(), "preco do item do pedido nao pode ser negativo");
		checkNotNull(tabelaPrecoProdutoReduzido, "nao e possivel inserir um item no pedido com o produtoRedizido nulo.");
		return new PedidoItens(quantidade, preco, numeroConjunto, tabelaPrecoProdutoReduzido);
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void addQuantidade(int quantidade) {
		checkArgument(quantidade >= 0, "nao e possivel adicionar quantidade negativa no pedido.");
		this.quantidade += quantidade;
	}

	public Dinheiro getPreco() {
		return preco;
	}

	public int getNumeroConjunto() {
		return numeroConjunto;
	}

	public void setNumeroConjunto(int numeroConjunto) {
		this.numeroConjunto = numeroConjunto;
	}

	public PedidoCapa getPedidoCapa() {
		return pedidoCapa;
	}

	public TabelaPrecoProdutoReduzido getTabelaPrecoProdutoReduzido() {
		return tabelaPrecoProdutoReduzido;
	}

	public double getValorTotal() {
		return (this.preco.getValor().doubleValue() * this.quantidade);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.tabelaPrecoProdutoReduzido);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof PedidoItens) {
			PedidoItens other = (PedidoItens) obj;
			return Objects.equal(this.tabelaPrecoProdutoReduzido, other.tabelaPrecoProdutoReduzido);
		}
		return false;

	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("id", id).add("quantidade", quantidade).add("preco", preco).add("pedido", pedidoCapa).add("reduzido", tabelaPrecoProdutoReduzido).toString();
	}

	public static double qtdePecasPorPedido(Context ctx, int idPedidoCapa) {
		RepositorioPedidoItem repo = new RepositorioPedidoItem(ctx);
		double qtdePecas = repo.qtdePecasPorPedido(idPedidoCapa);
		repo.fechar();

		return qtdePecas;
	}

	public boolean refAndPadraoIgual(PedidoItens it) {
		if (this.tabelaPrecoProdutoReduzido.getProdutoReduzido().getProduto().equals(it.tabelaPrecoProdutoReduzido.getProdutoReduzido().getProduto()) && this.tabelaPrecoProdutoReduzido.getProdutoReduzido().getPadrao().equals(it.tabelaPrecoProdutoReduzido.getProdutoReduzido().getPadrao())) {
			return true;
		}
		return false;
	}

	public void setQuantidade(int quantidade) {
		checkArgument(quantidade >= 0, "quantidade de itens no pedido nao pode ser negativa");
		this.quantidade = quantidade;
	}

	public void setPedidoCapa(PedidoCapa pedido) {
		this.pedidoCapa = pedido;

	}

	public static List<PedidoItens> buscaPorPedido(Context ctx, PedidoCapa pedido) {
		RepositorioPedidoItem repo = new RepositorioPedidoItem(ctx);
		List<PedidoItens> itens = repo.listarPorPedido(pedido);
		repo.fechar();
		return itens;
	}

}
