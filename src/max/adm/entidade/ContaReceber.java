package max.adm.entidade;

import java.util.Date;
import java.util.List;

import android.content.Context;
import max.adm.dominio.Entidade;
import max.adm.repositorio.RepositorioContaReceber;

public class ContaReceber extends Entidade {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "CONTARECEBER";

	public static String LJA_CODIGO = "EMPRESA_ID";
	public static String REC_SERIE = "REC_SERIE";
	public static String REC_NOTA = "REC_NOTA";
	public static String REC_PARCEL = "REC_PARCEL";
	public static String CLF_CODIGO = "PESSOA_ID";
	public static String REC_VALOR = "REC_VALOR";
	public static String REC_DTEMIS = "REC_DTEMIS";
	public static String REC_DTVENC = "REC_DTVENC";
	public static String REC_VALORPAGO = "REC_VALORPAGO";
	public static String REC_DTULTPGTO = "REC_DTULTPGTO";
	public static String TIPOCOBRANCA = "TIPOCOBRANCA_ID";
	public static String CPE_NUMERO_PEDIDO = "CPE_NUMERO_PEDIDO";
	public static String CPE_NR_ENTREGA = "CPE_NR_ENTREGA";
	public static String REC_POSCOBR = "REC_POSCOBR";
	public static String REC_BAIXA = "REC_BAIXA";
	public static String REC_JUROS = "REC_JUROS";
	public static String REC_MULTA = "REC_MULTA";
	public static String REC_DESCONTO = "REC_DESCONTO";

	public static String[] COLUNAS = { LJA_CODIGO, REC_SERIE, REC_NOTA, REC_PARCEL, CLF_CODIGO, REC_VALOR,
			REC_DTEMIS, REC_DTVENC, REC_VALORPAGO, REC_DTULTPGTO, TIPOCOBRANCA, CPE_NUMERO_PEDIDO,
			CPE_NR_ENTREGA, REC_POSCOBR, REC_BAIXA, REC_JUROS, REC_MULTA, REC_DESCONTO };

	private int lja_codigo;
	private String rec_serie;
	private int rec_nota;
	private int rec_parcel;
	private int clf_codigo;
	private double rec_valor;
	private Date rec_dtemis;
	private Date rec_dtvenc;
	private double rec_valorpago;
	private Date rec_dtultpgto;
	private String tipocobranca;
	private int cpe_numero_pedido;
	private int cpe_nr_entrega;
	private String rec_poscobr;
	private String rec_baixa;
	private double rec_juros;
	private double rec_multa;
	private double rec_desconto;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cpe_numero_pedido;
		result = prime * result + lja_codigo;
		result = prime * result + rec_nota;
		result = prime * result + rec_parcel;
		result = prime * result + ((rec_serie == null) ? 0 : rec_serie.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContaReceber other = (ContaReceber) obj;
		if (cpe_numero_pedido != other.cpe_numero_pedido)
			return false;
		if (lja_codigo != other.lja_codigo)
			return false;
		if (rec_nota != other.rec_nota)
			return false;
		if (rec_parcel != other.rec_parcel)
			return false;
		if (rec_serie == null) {
			if (other.rec_serie != null)
				return false;
		} else if (!rec_serie.equals(other.rec_serie))
			return false;
		return true;
	}

	public int getLja_codigo() {
		return lja_codigo;
	}

	public void setLja_codigo(int lja_codigo) {
		this.lja_codigo = lja_codigo;
	}

	public boolean comparaChavesPrimarias(ContaReceber contaReceber) {
		if (this == contaReceber)
			return true;
		if (contaReceber == null)
			return false;

		ContaReceber other = (ContaReceber) contaReceber;

		if (lja_codigo != other.lja_codigo)
			return false;
		if (rec_nota != other.rec_nota)
			return false;
		if (rec_parcel != other.rec_parcel)
			return false;
		if (rec_serie == null) {
			if (other.rec_serie != null)
				return false;
		} else if (!rec_serie.equals(other.rec_serie))
			return false;

		return true;
	}

	public String getRec_serie() {
		return rec_serie;
	}

	public void setRec_serie(String rec_serie) {
		this.rec_serie = rec_serie;
	}

	public int getRec_nota() {
		return rec_nota;
	}

	public void setRec_nota(int rec_nota) {
		this.rec_nota = rec_nota;
	}

	public int getRec_parcel() {
		return rec_parcel;
	}

	public void setRec_parcel(int rec_parcel) {
		this.rec_parcel = rec_parcel;
	}

	public int getClf_codigo() {
		return clf_codigo;
	}

	public void setClf_codigo(int clf_codigo) {
		this.clf_codigo = clf_codigo;
	}

	public double getRec_valor() {
		return rec_valor;
	}

	public void setRec_valor(double rec_valor) {
		this.rec_valor = rec_valor;
	}

	public Date getRec_dtemis() {
		return rec_dtemis;
	}

	public void setRec_dtemis(Date rec_dtemis) {
		this.rec_dtemis = rec_dtemis;
	}

	public Date getRec_dtvenc() {
		return rec_dtvenc;
	}

	public void setRec_dtvenc(Date rec_dtvenc) {
		this.rec_dtvenc = rec_dtvenc;
	}

	public double getRec_valorpago() {
		return rec_valorpago;
	}

	public void setRec_valorpago(double rec_valorpago) {
		this.rec_valorpago = rec_valorpago;
	}

	public Date getRec_dtultpgto() {
		return rec_dtultpgto;
	}

	public void setRec_dtultpgto(Date rec_dtultpgto) {
		this.rec_dtultpgto = rec_dtultpgto;
	}

	public String getTipocobranca() {
		return tipocobranca;
	}

	public void setTipocobranca(String tipocobranca) {
		this.tipocobranca = tipocobranca;
	}

	public int getCpe_numero_pedido() {
		return cpe_numero_pedido;
	}

	public void setCpe_numero_pedido(int cpe_numero_pedido) {
		this.cpe_numero_pedido = cpe_numero_pedido;
	}

	public int getCpe_nr_entrega() {
		return cpe_nr_entrega;
	}

	public void setCpe_nr_entrega(int cpe_nr_entrega) {
		this.cpe_nr_entrega = cpe_nr_entrega;
	}

	public String getRec_poscobr() {
		return rec_poscobr;
	}

	public void setRec_poscobr(String rec_poscobr) {
		this.rec_poscobr = rec_poscobr;
	}

	public String getRec_baixa() {
		return rec_baixa;
	}

	public void setRec_baixa(String rec_baixa) {
		this.rec_baixa = rec_baixa;
	}

	public double getRec_juros() {
		return rec_juros;
	}

	public void setRec_juros(double rec_juros) {
		this.rec_juros = rec_juros;
	}

	public double getRec_multa() {
		return rec_multa;
	}

	public void setRec_multa(double rec_multa) {
		this.rec_multa = rec_multa;
	}

	public double getRec_desconto() {
		return rec_desconto;
	}

	public void setRec_desconto(double rec_desconto) {
		this.rec_desconto = rec_desconto;
	}

	public static ContaReceber buscarPrimeiraCompra(Context ctx, int codigoCliente) {
		RepositorioContaReceber repoContaReceber = new RepositorioContaReceber(ctx);
		ContaReceber primeiraCompra = repoContaReceber.buscaPrimeiraCompra(codigoCliente);
		repoContaReceber.fechar();
		return primeiraCompra;

	}

	public static int atrasoMedio(Context ctx, int codigoPessoa, int codigoloja) {
		RepositorioContaReceber repoContaReceber = new RepositorioContaReceber(ctx);
		int atrasoMedio = repoContaReceber.atrasoMedio(codigoPessoa, codigoloja);
		repoContaReceber.fechar();
		return atrasoMedio;

	}

	public static int prazoMedio(Context ctx, int codigoPessoa, int codigoLoja) {
		RepositorioContaReceber repoContaReceber = new RepositorioContaReceber(ctx);
		int prazoMedio = repoContaReceber.prazoMedio(codigoPessoa, codigoLoja);
		repoContaReceber.fechar();
		return prazoMedio;

	}

	public static Date vencentoUltimoAtraso(Context ctx, int codigoPessoa, int codigoLoja) {
		RepositorioContaReceber repoContaReceber = new RepositorioContaReceber(ctx);
		Date vencimentoUltimoAtraso = repoContaReceber.vencimentoUltimoAtraso(codigoPessoa, codigoLoja);
		repoContaReceber.fechar();

		return vencimentoUltimoAtraso;
	}

	/**
	 * 
	 * @param ctx
	 * @param codigoCliente
	 * @param codigoLoja
	 * @returno retornara uma lista com apenas as data venc, data pagamento,
	 *          data emissao, valor, valor pago, baixa
	 */
	public static List<ContaReceber> listarContas(Context ctx, int codigoCliente, int codigoLoja) {
		RepositorioContaReceber repoContareceber = new RepositorioContaReceber(ctx);
		List<ContaReceber> contas = repoContareceber.listarContas(codigoCliente, codigoLoja);
		repoContareceber.fechar();
		return contas;
	}

	public static void deleteAll(Context ctx) {
		RepositorioContaReceber repo = new RepositorioContaReceber(ctx);
		repo.deleteAll();
		repo.fechar();
	}
}
