package max.adm.entidade;

import java.util.List;

import max.adm.dominio.Entidade;
import max.adm.tipoCobranca.RepositorioTipoCobranca;
import android.content.Context;

public class TipoCobranca extends Entidade {

	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "TIPO_COBRANCA";

	public static String CODIGO = "CODIGO";
	public static String DESCRICAO = "DESCRICAO";

	public static String[] COLUNAS = { CODIGO, DESCRICAO };

	private int codigo;
	private String descricao;

	public String toString() {
		return descricao;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codigo;
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoCobranca other = (TipoCobranca) obj;
		if (codigo != other.codigo)
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		return true;
	}

	public static List<TipoCobranca> listar(Context ctx) {
		RepositorioTipoCobranca repo = new RepositorioTipoCobranca(ctx);
		List<TipoCobranca> tposCobranca = repo.listar();
		repo.fechar();
		return tposCobranca;
	}

	public static boolean salvarLista(Context ctx,
			List<TipoCobranca> tiposCobrancas) {
		RepositorioTipoCobranca repo = new RepositorioTipoCobranca(ctx);
		boolean ok = repo.salvarLista(tiposCobrancas);
		repo.fechar();
		return ok;
	}

}
