package max.adm.entidade;

import java.util.ArrayList;
import java.util.List;

import max.adm.dominio.Entidade;
import max.adm.empresa.RepositorioEmpresa;
import android.content.Context;

public class Empresa extends Entidade {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/*
	//nome tabela Replica Loja
	public static String TABELA_REPLICA = "REPLICA_LOJA";
	
	// colunas da tabela replica
	public static String ID_REPLICA = "ID_REPLICA";
	public static String LOJA_REPLICA = "LOJA";
	public static String REPLICA_PARA = "REPLICA_PARA";
	
	public static String[] COLUNAS_REPLICA = {ID_REPLICA, LOJA_REPLICA, REPLICA_PARA};
	 */
	
	public static String NOME_TABELA = "EMPRESA";

	public static String NUMEROLOJA = "CODIGO";
	public static String CNPJ = "CNPJ";
	public static String RAZAOSOCIAL = "RAZAO_SOCIAL";
	public static String LOGRADOURO = "ENDERECO";
	public static String COMPLEMENTO = "COMPLEMENTO";
	public static String NUMEROENDERECO = "NUMERO_ENDERECO";
	public static String CEP = "CEP";
	public static String INSCESTADUAL = "INSCRICAO_ESTADUAL";
	public static String TELEFONE = "TELEFONE";
	public static String CONTATO = "CONTATO";
	public static String REGIMETRIBUTARIO = "REGIME_TRIBUTARIO";
	public static String CIDADE = "CIDADE_ID";

	public static String[] COLUNAS = { NUMEROLOJA, CNPJ, RAZAOSOCIAL, LOGRADOURO,
			COMPLEMENTO, NUMEROENDERECO, CEP, INSCESTADUAL, TELEFONE, CONTATO,
			REGIMETRIBUTARIO, CIDADE };

	private Integer numeroLoja;
	private String cnpj;
	private String razaoSocial;
	private String logradouro;
	private String complemento;
	private String numeroEndereco;
	private String cep;
	private String inscEstadual;
	private String telefone;
	private String contato;
	private String regimeTributario;
	private Cidade cidade;
	
	public String toString(){
		return "Loja "+String.valueOf(this.numeroLoja);
	}

	public Integer getNumeroLoja() {
		return numeroLoja;
	}

	public void setNumeroLoja(int codigo) {
		this.numeroLoja = codigo;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getNumeroEndereco() {
		return numeroEndereco;
	}

	public void setNumeroEndereco(String numero) {
		this.numeroEndereco = numero;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getInscEstadual() {
		return inscEstadual;
	}

	public void setInscEstadual(String inscEstadual) {
		this.inscEstadual = inscEstadual;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public String getRegimeTributario() {
		return regimeTributario;
	}

	public void setRegimeTributario(String regimeTributario) {
		this.regimeTributario = regimeTributario;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (numeroLoja ^ (numeroLoja >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Empresa other = (Empresa) obj;
		if (Integer.valueOf(numeroLoja) != Integer.valueOf(other.numeroLoja))
			return false;
		return true;
	}
	
	
	public int salvar(Context ctx){
		RepositorioEmpresa repoEmpresa = new RepositorioEmpresa(ctx);
		this.numeroLoja = repoEmpresa.salvar(this);
		repoEmpresa.fechar();
		return this.numeroLoja;
	}
	
	
	public static List<Empresa> listar(Context ctx){
		RepositorioEmpresa repoEmpresa = new RepositorioEmpresa(ctx);
		List<Empresa> empresas= new ArrayList<Empresa>();
		
		empresas = repoEmpresa.listar();
		
		
		repoEmpresa.fechar();
		
		return empresas;
		
	}
	
//	public static List<Empresa> buscaLojasReplicacao(Context ctx, int codigoLoja){
//		RepositorioEmpresa repoEmpresa = new RepositorioEmpresa(ctx);
//		List<Empresa> empresas= new ArrayList<Empresa>();
//		
//		empresas = repoEmpresa.buscaLojasReplicacao(codigoLoja);
//		
//		repoEmpresa.fechar();
//		
//		return empresas;
//		
//	}
	
	public static boolean tabelaVazia(Context ctx){
		RepositorioEmpresa repo = new RepositorioEmpresa(ctx);
		boolean vazia = repo.tabelaVazia();
		repo.fechar();
		return vazia;
	}
	
	

}
