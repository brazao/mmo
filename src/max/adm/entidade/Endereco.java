package max.adm.entidade;

import java.util.List;

import max.adm.dominio.Entidade;
import max.adm.endereco.RepositorioEndereco;
import max.adm.endereco.TipoEndereco;
import android.content.Context;

public class Endereco extends Entidade {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "ENDERECO";

	public static String CODIGO = "CODIGO";
	public static String TIPO_LOGRADOURO = "TIPO_LOGRADOURO";
	public static String NUMERO = "NUMERO";
	public static String CEP = "CEP";
	public static String COMPLEMENTO = "COMPLEMENTO";
	public static String BAIRRO = "BAIRRO";
	public static String CIDADE = "CIDADE_ID";
	public static String PESSOA = "PESSOA_ID";
	public static String TIPOENDERECO = "TIPOENDERECO_ID";
	public static String EMPRESA = "EMPRESA_ID";
	public static String LOGRADOURO = "LOGRADOURO";

	public static String COLUNAS[] = { CODIGO, TIPO_LOGRADOURO, NUMERO, CEP, COMPLEMENTO, BAIRRO, CIDADE, PESSOA,
			EMPRESA, TIPOENDERECO, LOGRADOURO };

	private int codigo;
	private String tipoLogradouro;
	private String numero;
	private String cep;
	private String complemento;
	private String bairro;
	private Cidade cidade;
	private transient Pessoa pessoa;
	private Empresa empresa;
	private TipoEndereco tipoEndereco;
	private String logradouro;

	public static List<Endereco> buscaEnderecos(Context ctx, int idPessoa) {
		RepositorioEndereco repo = new RepositorioEndereco(ctx);
		List<Endereco> enderecos = repo.buscaPorPessoa(idPessoa);
		repo.fechar();
		return enderecos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bairro == null) ? 0 : bairro.hashCode());
		result = prime * result + ((cep == null) ? 0 : cep.hashCode());
		result = prime * result + ((cidade == null) ? 0 : cidade.hashCode());
		result = prime * result + codigo;
		result = prime * result + ((complemento == null) ? 0 : complemento.hashCode());
		result = prime * result + ((empresa == null) ? 0 : empresa.hashCode());
		result = prime * result + ((logradouro == null) ? 0 : logradouro.hashCode());
		result = prime * result + ((tipoLogradouro == null) ? 0 : tipoLogradouro.hashCode());
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		result = prime * result + ((tipoEndereco == null) ? 0 : tipoEndereco.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Endereco other = (Endereco) obj;
		if (bairro == null) {
			if (other.bairro != null)
				return false;
		} else if (!bairro.equals(other.bairro))
			return false;
		if (cep == null) {
			if (other.cep != null)
				return false;
		} else if (!cep.equals(other.cep))
			return false;
		if (cidade == null) {
			if (other.cidade != null)
				return false;
		} else if (!cidade.equals(other.cidade))
			return false;
		if (codigo != other.codigo)
			return false;
		if (complemento == null) {
			if (other.complemento != null)
				return false;
		} else if (!complemento.equals(other.complemento))
			return false;
		if (empresa == null) {
			if (other.empresa != null)
				return false;
		} else if (!empresa.equals(other.empresa))
			return false;
		if (logradouro == null) {
			if (other.logradouro != null)
				return false;
		} else if (!logradouro.equals(other.logradouro))
			return false;
		if (tipoLogradouro == null) {
			if (other.tipoLogradouro != null)
				return false;
		} else if (!tipoLogradouro.equals(other.tipoLogradouro))
			return false;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		if (tipoEndereco != other.tipoEndereco)
			return false;
		return true;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public TipoEndereco getTipoEndereco() {
		return tipoEndereco;
	}

	public void setTipoEndereco(TipoEndereco tipoEndereco) {
		this.tipoEndereco = tipoEndereco;
	}

	public String getTipoLogradouro() {
		return tipoLogradouro;
	}

	public void setTipoLogradouro(String tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	@Override
	public String toString() {
		return "Endereco [codigo=" + codigo + ", logradouro=" + tipoLogradouro + ", numero=" + numero + ", cep="
				+ cep + ", complemento=" + complemento + ", bairro=" + bairro + ", cidade=" + cidade
				+ ", empresa=" + empresa + ", tipoEndereco=" + tipoEndereco + ", enderecoNome="
				+ logradouro + "]";
	}
	
}
