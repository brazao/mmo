package max.adm.entidade;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import max.adm.dominio.Entidade;
import max.adm.repositorio.RepositorioTamanho;
import android.content.Context;

import com.google.common.collect.ComparisonChain;

public class TamanhoProduto extends Entidade implements
		Comparable<TamanhoProduto> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "TAMANHOPRODUTO";

	public static String CODIGO = "CODIGO";
	public static String DESCRICAO = "DESCRICAO";
	public static String TAMANHOCLASSIFICACAO = "CLASSIFICACAO";

	public static String[] COLUNAS = { CODIGO, DESCRICAO, TAMANHOCLASSIFICACAO };

	private String codigo;
	private String descricao;
	private int tamanhoClassificacao;

	private TamanhoProduto(String codigo, String descricao,
			int tamanhoClassificacao) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.tamanhoClassificacao = tamanhoClassificacao;

	}

	public static TamanhoProduto newTamanho(String descricao, int tamanhoClassificacao) {
		checkArgument(tamanhoClassificacao >= 0, "Classificao do tamanho nao pode ser negativa.");
		return new TamanhoProduto(String.valueOf(0), descricao, tamanhoClassificacao);

	}

	public static TamanhoProduto newTamanho(String id, String descricao, int tamanhoClassificacao) {
		checkArgument(tamanhoClassificacao >= 0, "Classificao do tamanho nao pode ser negativa.");
		return new TamanhoProduto(id, descricao, tamanhoClassificacao);

	}

	public String getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public int getTamanhoClassificacao() {
		return tamanhoClassificacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + tamanhoClassificacao;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TamanhoProduto other = (TamanhoProduto) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (tamanhoClassificacao != other.tamanhoClassificacao)
			return false;
		return true;
	}

	public static List<TamanhoProduto> listar(Context ctx) {
		RepositorioTamanho repo = new RepositorioTamanho(ctx);
		List<TamanhoProduto> tams = repo.listar();
		repo.fechar();
		return tams;
	}

	public static boolean salvarLista(Context ctx, List<TamanhoProduto> tamanhos) {
		RepositorioTamanho repo = new RepositorioTamanho(ctx);
		boolean ok = repo.salvarLista(tamanhos);
		repo.fechar();
		return ok;
	}

	public TamanhoProduto salvar(Context ctx) {
		RepositorioTamanho repo = new RepositorioTamanho(ctx);
		this.codigo = repo.salvar(this);
		repo.fechar();
		return this;
	}

	@Override
	public int compareTo(TamanhoProduto another) {
		return ComparisonChain
				.start()
				.compare(this.tamanhoClassificacao,
						another.tamanhoClassificacao).result();

	}

}
