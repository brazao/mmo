package max.adm.entidade;

import java.util.List;

import android.content.Context;
import max.adm.dominio.Entidade;
import max.adm.repositorio.RepositorioCondicaoPagamento;

public class CondicaoPagamento extends Entidade {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "CONDICAOPGTO";

	public static String CODIGO = "CODIGO";
	public static String DESCRICAO = "DESCRICAO";
	
	public static String[] COLUNAS = {CODIGO, DESCRICAO}; 

	private int codigo;
	private String descricao;
	
	
	public String toString(){
		return String.valueOf(this.codigo)+" - "+this.descricao;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (codigo ^ (codigo >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CondicaoPagamento other = (CondicaoPagamento) obj;
		if (codigo != other.codigo)
			return false;
		return true;
	}
	
	public static List<CondicaoPagamento> listar(Context ctx){
		RepositorioCondicaoPagamento repo = new RepositorioCondicaoPagamento(ctx);
		List<CondicaoPagamento> condicoes = repo.listar();
		repo.fechar();
		return condicoes;
	}


}
