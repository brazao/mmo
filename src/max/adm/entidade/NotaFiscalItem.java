package max.adm.entidade;

import java.util.List;

import max.adm.dominio.Entidade;
import max.adm.repositorio.RepositorioNotaFiscalItem;
import android.content.Context;

public class NotaFiscalItem extends Entidade {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "NOTAFISCALITEM";

	public static String REDPRODUTOKEY = "RED_PRODUTO_KEY";
	public static String QUANTIDADE = "QUANTIDADE";
	public static String PRECOUNITARIO = "PRECO_UNITARIO";
	public static String NOTAFISCAL = "NOTAFISCALCAPA_ID";
	
	public static String[] COLUNAS = {REDPRODUTOKEY, QUANTIDADE, PRECOUNITARIO, NOTAFISCAL};

	private int redProdutoKey;
	private double quantidade;
	private double precoUnitario;
	private transient NotaFiscalCapa notaFiscal;

	public int getRedProdutoKey() {
		return redProdutoKey;
	}

	public void setRedProdutoKey(int redProdutoKey) {
		this.redProdutoKey = redProdutoKey;
	}

	public double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(double quantidade) {
		this.quantidade = quantidade;
	}

	public double getPrecoUnitario() {
		return precoUnitario;
	}

	public void setPrecoUnitario(double precoUnitario) {
		this.precoUnitario = precoUnitario;
	}

	public NotaFiscalCapa getNotaFiscal() {
		return notaFiscal;
	}

	public void setNotaFiscal(NotaFiscalCapa notaFiscal) {
		this.notaFiscal = notaFiscal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(precoUnitario);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(quantidade);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + redProdutoKey;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotaFiscalItem other = (NotaFiscalItem) obj;
		if (Double.doubleToLongBits(precoUnitario) != Double.doubleToLongBits(other.precoUnitario))
			return false;
		if (Double.doubleToLongBits(quantidade) != Double.doubleToLongBits(other.quantidade))
			return false;
		if (redProdutoKey != other.redProdutoKey)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "NotaFiscalItem [redProdutoKey=" + redProdutoKey + ", quantidade=" + quantidade
				+ ", precoUnitario=" + precoUnitario + "]";
	}

	/**
	 * 
	 * @param ctx Cotext contexto da activity
	 * @param idCapaNota int
	 * @return List<ItemNotaFisca> retorna uma lista com itens da nota com o id que foi passado por parametro.
	 */
	public static List<NotaFiscalItem> buscaPorNota(Context ctx, int idCapaNota){
		RepositorioNotaFiscalItem repoItemNota = new RepositorioNotaFiscalItem(ctx);
		List<NotaFiscalItem> itensNota = repoItemNota.buscaItensPorNota(idCapaNota);
		repoItemNota.fechar();
		
		return itensNota;
		
	}
	
//	public static List<ItemNotaFiscal> buscaItensPorNota(Context ctx, int idCapaNota){
//		RepositorioItemNotaFiscal repoItens = new RepositorioItemNotaFiscal();
//		List<ItemNotaFiscal> itens = repoItens.buscaItensPorNota(idCapaNota);
//		repoItens.fechar();
//		return itens;
//	}

}
