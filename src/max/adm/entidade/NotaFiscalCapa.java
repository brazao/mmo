package max.adm.entidade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import max.adm.Dinheiro;
import max.adm.dominio.Entidade;
import max.adm.repositorio.RepositorioNotaFiscalCapa;

public class NotaFiscalCapa extends Entidade {

	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "NOTAFISCALCAPA";

	public static String CODIGO = "CODIGO";
	public static String NUMERONOTA = "NUMERO";
	public static String PESSOA = "PESSOA_ID";
	public static String DATAEMISSAO = "DATA_EMISSAO";
	public static String EMPRESA = "EMPRESA_ID";
	public static String PEDIDONUMERO = "PEDIDO_NUMERO";
	public static String SERIE = "SERIE";
	public static String VALORTOTAL = "VALOR_TOTAL";
	
	public static String[] COLUNAS  = {CODIGO, NUMERONOTA, SERIE ,PESSOA ,PEDIDONUMERO ,DATAEMISSAO ,EMPRESA, VALORTOTAL };

	private int id;
	private int numeroNota;
	private String serie;
	private int pessoa;
	private int numeroPedido;
	private Date dataemissao;
	private int empresa;
	private Dinheiro valorTotal;
	private List<NotaFiscalItem> itens = new ArrayList<NotaFiscalItem>();
	
	public List<NotaFiscalItem> getItens() {
		return itens;
	}

	public void setItens(List<NotaFiscalItem> itens) {
		this.itens = itens;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNumeroNota() {
		return numeroNota;
	}

	public void setNumeroNota(int numeroNota) {
		this.numeroNota = numeroNota;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public int getPessoa() {
		return pessoa;
	}

	public void setPessoa(int pessoa) {
		this.pessoa = pessoa;
	}

	public int getNumeroPedido() {
		return numeroPedido;
	}

	public void setNumeroPedido(int numeroPedido) {
		this.numeroPedido = numeroPedido;
	}

	public Date getDataemissao() {
		return dataemissao;
	}

	public void setDataemissao(Date dataemissao) {
		this.dataemissao = dataemissao;
	}

	public Dinheiro getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Dinheiro valorTotal) {
		this.valorTotal = valorTotal;
	}

	public int getEmpresa() {
		return empresa;
	}

	public void setEmpresa(int empresa) {
		this.empresa = empresa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotaFiscalCapa other = (NotaFiscalCapa) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	public static NotaFiscalCapa buscaUltimaCompra(Context ctx, int codigoCliente){

		RepositorioNotaFiscalCapa repoNota = new RepositorioNotaFiscalCapa(ctx);
		NotaFiscalCapa notaCapa = repoNota.buscaUltimaCompra(codigoCliente);
		repoNota.fechar();
		return notaCapa;
	}
	
	public static NotaFiscalCapa buscaPrimeiraCompra(Context ctx, int codigoCliente){

		RepositorioNotaFiscalCapa repoNota = new RepositorioNotaFiscalCapa(ctx);
		NotaFiscalCapa notaCapa = repoNota.buscaPrimeiraCompra(codigoCliente);
		repoNota.fechar();
		return notaCapa;
	}

	public static void deleteAll(Context ctx) {
		RepositorioNotaFiscalCapa repoNota = new RepositorioNotaFiscalCapa(ctx);
		repoNota.deleteAll();
		repoNota.fechar();
	}	

}
