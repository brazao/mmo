package max.adm.entidade;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.Date;

import max.adm.repositorio.RepositorioPedidoCapa;
import android.content.Context;

public class FaixaCodigoPedido {

	public static final String TABELA_FAIXA = "FAIXACODIGOPEDIDO";

	public static String PESSOAID = "PESSOA_ID";
	public static String PEDIDOID = "PEDIDO_ID";
	public static String CODIGOCLIENTE = "CODCLIENTE";
	public static String DATAPEDIDO = "DATAPEDIDO";
	public static String VALOR = "VALOR";
	public static String EMPRESAID = "EMPRESA_ID";

	private int pessoaId;
	private int pedidoId;
	private int codCliente;
	private Date dataPedido;
	private Double valor;
	private int empresaId;
	
	private FaixaCodigoPedido(int empresaId, int pedidoId) {
		this.empresaId = empresaId;
		this.pedidoId = pedidoId;
	}
	
	public static FaixaCodigoPedido newInstance(Context ctx, int codigoLoja){
		RepositorioPedidoCapa repo = new RepositorioPedidoCapa(ctx);
		int codigo = repo.leCodigoDisponivel(codigoLoja);
		repo.fechar();
		checkArgument(codigo > 0, "codigo do pedido deve ser maior que zero");
		return new FaixaCodigoPedido(codigoLoja, codigo);
	}

	public int getPessoaId() {
		return pessoaId;
	}

	public void setPessoaId(int pessoaId) {
		this.pessoaId = pessoaId;
	}

	public int getPedidoId() {
		return pedidoId;
	}

	public void setPedidoId(int pedidoId) {
		this.pedidoId = pedidoId;
	}

	public int getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(int codCliente) {
		this.codCliente = codCliente;
	}

	public Date getDataPedido() {
		return dataPedido;
	}

	public void setDataPedido(Date dataPedido) {
		this.dataPedido = dataPedido;
	}

	public int getEmpresaId() {
		return empresaId;
	}

	public void setEmpresaId(int empresaId) {
		this.empresaId = empresaId;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

}
