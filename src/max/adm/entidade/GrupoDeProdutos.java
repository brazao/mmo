package max.adm.entidade;

import java.util.List;

import max.adm.dominio.Entidade;
import max.adm.repositorio.RepositorioGrupoDeProdutos;
import android.content.Context;

public class GrupoDeProdutos extends Entidade {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "GRUPOPRODUTO";

	public static String CODIGO = "CODIGO";
	public static String DESCRICAO = "DESCRICAO";

	public static String[] COLUNAS = { CODIGO, DESCRICAO };

	private int codigo;
	private String descricao;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codigo;
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoDeProdutos other = (GrupoDeProdutos) obj;
		if (codigo != other.codigo)
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		return true;
	}

	public static boolean salvarLista(Context ctx, List<GrupoDeProdutos> grupos) {
		RepositorioGrupoDeProdutos repo = new RepositorioGrupoDeProdutos(ctx);
		boolean ok = repo.salvarLista(grupos);
		repo.fechar();
		return ok;
	}

	public GrupoDeProdutos salvar(Context ctx) {
		RepositorioGrupoDeProdutos repo = new RepositorioGrupoDeProdutos(ctx);
		this.codigo = repo.salvar(this);
		repo.fechar();
		return this;
	}

}
