package max.adm.entidade;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.Serializable;

import com.google.common.base.Objects;

public class FaixaCodigoCliente implements Serializable {
	
	public static String TABELA_FAIXA_CODIGO = "FAIXACODIGOCLIENTE";

	public static String CODIGO = "CODIGO";
	public static String PESSOA = "PESSOA_ID";
	public static String TIPO = "TIPO";
	public static String UTILIZADO = "UTILIZADO";
	public static String EMPRESA = "EMPRESA_ID";

	public static String[] COLUNAS_FAIXA = {CODIGO, PESSOA, TIPO, UTILIZADO, EMPRESA};

	private static final long serialVersionUID = 1L;

	private int codigoLoja;
	private int codigoRepresentante;
	private int codigoCliente;
	private boolean utilizado;

	private FaixaCodigoCliente(int codigoLoja, int codigoRepresentante,
			int codigoCliente, boolean utilizado) {
		this.codigoLoja = codigoLoja;
		this.codigoRepresentante = codigoRepresentante;
		this.codigoCliente = codigoCliente;
		this.utilizado = utilizado;
	}

	public static FaixaCodigoCliente newInstance(int codigoLoja,
			int codigoRepresentante, int codigoCliente, boolean utilizado) {
		checkArgument(codigoLoja > 0, "codigo da loja deve ser maior que zero");
		checkArgument(codigoCliente > 0, "codigo do cliente deve ser maior que zero");

		return new FaixaCodigoCliente(codigoLoja, codigoRepresentante, codigoCliente, utilizado);
	}

	public int getCodigoLoja() {
		return codigoLoja;
	}

	public int getCodigoRepresentante() {
		return codigoRepresentante;
	}

	public int getCodigoCliente() {
		return codigoCliente;
	}

	public boolean isUtilizado() {
		return utilizado;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof FaixaCodigoCliente) {
			FaixaCodigoCliente other = (FaixaCodigoCliente) obj;
			return Objects.equal(this.codigoLoja, other.codigoLoja)
					&& Objects.equal(this.codigoCliente, other.codigoCliente)
					&& Objects.equal(this.codigoRepresentante,
							this.codigoRepresentante);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.codigoCliente, this.codigoLoja,
				this.codigoRepresentante);
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("loja", this.codigoLoja)
				.add("cod cliente", this.codigoCliente)
				.add("cod representante", this.codigoRepresentante).toString();
	}

}
