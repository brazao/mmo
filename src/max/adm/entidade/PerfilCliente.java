package max.adm.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.google.common.base.Objects;

public class PerfilCliente implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	private Integer codigoCliente;
	private Integer loja;
	private Date primeiraCompra;
	private Integer quantidadePecasPrimeiraCompra;
	private BigDecimal valorPrimeiraCompra;
	private Date antiPenultimaCompra;
	private BigDecimal quantidadePecasAntiPenultimaCompra;
	private BigDecimal valorAntiPenultimaCompra;
	private Date penultimaCompra;
	private BigDecimal quantidadePecasPenultimaCompra;
	private BigDecimal valorPenultimaCompra;
	private Date ultimaCompra;
	private Integer quantidadePecasUltimaCompra;
	private BigDecimal valorUltimaCompra;
	private BigDecimal totalEmCompras;
	private BigDecimal totalEmAberto;
	private BigDecimal totalVencido;
	private BigDecimal totalVencer;
	private BigDecimal valorMedioPorPedido;
	private BigDecimal percentualEmAberto;
	private Date dataSincronizacao;

	private PerfilCliente(Integer id, Integer codigoCliente, Integer loja, Date primeiraCompra,
			Integer quantidadePecasPrimeiraCompra, BigDecimal valorPrimeiraCompra,
			Date antiPenultimaCompra, BigDecimal quantidadePecasAntiPenultimaCompra,
			BigDecimal valorAntiPenultimaCompra, Date penultimaCompra,
			BigDecimal quantidadePecasPenultimaCompra, BigDecimal valorPenultimaCompra, Date ultimaCompra,
			Integer quantidadePecasUltimaCompra, BigDecimal valorUltimaCompra, BigDecimal totalEmCompras,
			BigDecimal totalEmAberto, BigDecimal totalVencido, BigDecimal totalVencer,
			BigDecimal valorMedioPorPedido, BigDecimal percentualEmAberto, Date dataSincronizacao) {
		this.id = id;
		this.codigoCliente = codigoCliente;
		this.loja = loja;
		this.primeiraCompra = primeiraCompra;
		this.quantidadePecasPrimeiraCompra = quantidadePecasPrimeiraCompra;
		this.valorPrimeiraCompra = valorPrimeiraCompra;
		this.antiPenultimaCompra = antiPenultimaCompra;
		this.quantidadePecasAntiPenultimaCompra = quantidadePecasAntiPenultimaCompra;
		this.valorAntiPenultimaCompra = valorAntiPenultimaCompra;
		this.penultimaCompra = penultimaCompra;
		this.quantidadePecasPenultimaCompra = quantidadePecasPenultimaCompra;
		this.valorPenultimaCompra = valorPenultimaCompra;
		this.ultimaCompra = ultimaCompra;
		this.quantidadePecasUltimaCompra = quantidadePecasUltimaCompra;
		this.valorUltimaCompra = valorUltimaCompra;
		this.totalEmCompras = totalEmCompras;
		this.totalEmAberto = totalEmAberto;
		this.totalVencido = totalVencido;
		this.totalVencer = totalVencer;
		this.valorMedioPorPedido = valorMedioPorPedido;
		this.percentualEmAberto = percentualEmAberto;
		this.dataSincronizacao = dataSincronizacao;
	}

	public static PerfilCliente newInstance(Integer id, Integer codigoCliente, Integer loja,
			Date primeiraCompra, Integer quantidadePecasPrimeiraCompra, BigDecimal valorPrimeiraCompra,
			Date antiPenultimaCompra, BigDecimal quantidadePecasAntiPenultimaCompra,
			BigDecimal valorAntiPenultimaCompra, Date penultimaCompra,
			BigDecimal quantidadePecasPenultimaCompra, BigDecimal valorPenultimaCompra, Date ultimaCompra,
			Integer quantidadePecasUltimaCompra, BigDecimal valorUltimaCompra, BigDecimal totalEmCompras,
			BigDecimal totalEmAberto, BigDecimal totalVencido, BigDecimal totalVencer,
			BigDecimal valorMedioPorPedido, BigDecimal percentualEmAberto, Date dataSincronizacao) {

		return new PerfilCliente(id, codigoCliente, loja, primeiraCompra, quantidadePecasPrimeiraCompra,
				valorPrimeiraCompra, antiPenultimaCompra, quantidadePecasAntiPenultimaCompra,
				valorAntiPenultimaCompra, penultimaCompra, quantidadePecasPenultimaCompra,
				valorPenultimaCompra, ultimaCompra, quantidadePecasUltimaCompra, valorUltimaCompra,
				totalEmCompras, totalEmAberto, totalVencido, totalVencer, valorMedioPorPedido,
				percentualEmAberto, dataSincronizacao);

	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof PerfilCliente) {
			PerfilCliente other = (PerfilCliente) obj;
			return Objects.equal(this.codigoCliente, other.codigoCliente)
					&& Objects.equal(this.loja, other.loja);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(codigoCliente, loja);
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("codigo cliente", codigoCliente).add("loja", loja)
				.add("primeiraCompra", primeiraCompra).add("valor primeira compra", valorPrimeiraCompra)
				.add("ultimaCompra", ultimaCompra).add("valor ultima compra", valorUltimaCompra)

				.toString();
	}

	public Date getDataSincronizacao() {
		return dataSincronizacao;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCodigoCliente() {
		return codigoCliente;
	}

	public Integer getLoja() {
		return loja;
	}

	public BigDecimal getTotalEmCompras() {
		return totalEmCompras;
	}

	public BigDecimal getTotalEmAberto() {
		return totalEmAberto;
	}

	public BigDecimal getTotalVencido() {
		return totalVencido;
	}

	public BigDecimal getTotalVencer() {
		return totalVencer;
	}

	public BigDecimal getValorMedioPorPedido() {
		return valorMedioPorPedido;
	}

	public BigDecimal getPercentualEmAberto() {
		return percentualEmAberto;
	}

	public long getId() {
		return id;
	}

	public Date getPrimeiraCompra() {
		return primeiraCompra;
	}

	public Integer getQuantidadePecasPrimeiraCompra() {
		return quantidadePecasPrimeiraCompra;
	}

	public BigDecimal getValorPrimeiraCompra() {
		return valorPrimeiraCompra;
	}

	public Date getAntiPenultimaCompra() {
		return antiPenultimaCompra;
	}

	public BigDecimal getQuantidadePecasAntiPenultimaCompra() {
		return quantidadePecasAntiPenultimaCompra;
	}

	public BigDecimal getValorAntiPenultimaCompra() {
		return valorAntiPenultimaCompra;
	}

	public Date getPenultimaCompra() {
		return penultimaCompra;
	}

	public BigDecimal getQuantidadePecasPenultimaCompra() {
		return quantidadePecasPenultimaCompra;
	}

	public BigDecimal getValorPenultimaCompra() {
		return valorPenultimaCompra;
	}

	public Date getUltimaCompra() {
		return ultimaCompra;
	}

	public Integer getQuantidadePecasUltimaCompra() {
		return quantidadePecasUltimaCompra;
	}

	public BigDecimal getValorUltimaCompra() {
		return valorUltimaCompra;
	}

}
