package max.adm.entidade;

import java.util.List;

import max.adm.cidade.RepositorioCidade;
import max.adm.dominio.Entidade;
import android.content.Context;

import com.google.common.base.Objects;

public class Cidade extends Entidade {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "CIDADE";

	public static String CODIGO = "CODIGO";
	public static String NOME = "NOME";
	public static String CEP = "CEP";
	public static String CODIGOIBGE = "CODIGO_IBGE";
	public static String ESTADO = "ESTADO_ID";
	
	public static String[] COLUNAS = {CODIGO,  NOME, CEP, CODIGOIBGE,  ESTADO};

	private int codigo;
	private String nome;
	private String cep;
	private int codigoIbge;
	private Estado estado;
	
	
	public String toString(){
		return nome+" - "+estado.getSigla();
	}
	
	public String toStringHelper(){
		return Objects.toStringHelper(this).add("codigo",codigo).add("nome", nome)
				.add("cep", cep).add("codIbge", codigoIbge).add("estado", estado).toString();
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public int getCodigoIbge() {
		return codigoIbge;
	}

	public void setCodigoIbge(int codigoIbge) {
		this.codigoIbge = codigoIbge;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codigo;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cidade other = (Cidade) obj;
		if (codigo != other.codigo)
			return false;
		return true;
	}

	public static boolean tabelaVazia(Context ctx){
		RepositorioCidade repoCidade = new RepositorioCidade(ctx);
		boolean vazia = repoCidade.tabelaVazia();
		repoCidade.fechar();
		return vazia;
	}
	
	
	public static List<Cidade> listar(Context ctx){
		RepositorioCidade repoCidade = new RepositorioCidade(ctx);
		List<Cidade> cidades = repoCidade.listarTodas();
		repoCidade.fechar();
		return cidades;
	}
	
	
	public static List<Cidade> buscaPorNome(Context ctx, String nome){
		RepositorioCidade repoCidade = new RepositorioCidade(ctx);
		
		List<Cidade> cidades = repoCidade.buscaPorNome(nome);
		repoCidade.fechar();
		return cidades;
		
		
	}
	

	public static Cidade busca(Context ctx, int id){
		RepositorioCidade repoCidade = new RepositorioCidade(ctx);
		Cidade cidade = repoCidade.busca(id);
		repoCidade.fechar();
		
		return cidade;
	} 
	
	public static boolean salvarLista(Context ctx, List<Cidade> cidades){
		RepositorioCidade repo = new RepositorioCidade(ctx);
		boolean ok =  repo.salvarLista(cidades);
		repo.fechar();
		return ok;
	}
	
	public int salvar(Context ctx){
		RepositorioCidade repo = new RepositorioCidade(ctx);
		int id = repo.salvar(this);
		repo.fechar();
		return id;
	}

	
	
}
