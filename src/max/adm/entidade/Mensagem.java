package max.adm.entidade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import max.adm.dominio.Entidade;
import max.adm.repositorio.RepositorioMensagem;
import android.content.Context;

public class Mensagem extends Entidade {

	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "MENSAGENS";

	public static String ID = "MENSAGEM_ID";
	public static String CODIGO = "MSG_CODIGO";
	public static String REMETENTE = "MSG_REMETENTE";
	public static String DATAENVIO = "MSG_ENVIADA";
	public static String DATALEITURA = "MSG_LIDA";
	public static String DATARESPOSTA = "MSG_RESPONDIDA";
	public static String CODIGOPESSOA = "CLF_CODIGO";
	public static String MENSAGEM = "MSG_MENSAGEM";
	public static String RESPOSTA = "MSG_RESPOSTA";
	public static String EMPRESA = "FIA_EMPRESA_EMP_ID";

	public static String[] COLUNAS = { ID, CODIGO, REMETENTE, DATAENVIO,
			DATALEITURA, DATARESPOSTA, CODIGOPESSOA, MENSAGEM, RESPOSTA,
			EMPRESA };

	private int id;
	private int codigo;
	private String remetente;
	private Date DataEnvio;
	private Date DataLeitura;
	private Date DataResposta;
	private int codigoPessoa;
	private String mensagem;
	private String resposta;
	private Empresa empresa;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getRemetente() {
		return remetente;
	}

	public void setRemetente(String remetente) {
		this.remetente = remetente;
	}

	public Date getDataEnvio() {
		return DataEnvio;
	}

	public void setDataEnvio(Date dataEnvio) {
		DataEnvio = dataEnvio;
	}

	public Date getDataLeitura() {
		return DataLeitura;
	}

	public void setDataLeitura(Date dataLeitura) {
		DataLeitura = dataLeitura;
	}

	public Date getDataResposta() {
		return DataResposta;
	}

	public void setDataResposta(Date dataResposta) {
		DataResposta = dataResposta;
	}

	public int getCodigoPessoa() {
		return codigoPessoa;
	}

	public void setCodigoPessoa(int codigoPessoa) {
		this.codigoPessoa = codigoPessoa;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getResposta() {
		return resposta;
	}

	public void setResposta(String resposta) {
		this.resposta = resposta;
	}

	

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mensagem other = (Mensagem) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
	/**
	 * 
	 * @param ctx
	 * @return retorna todas as mensagem contidas no banco de dados.
	 */
	public static List<Mensagem> listar(Context ctx){
		RepositorioMensagem repoMsg = new RepositorioMensagem(ctx);
		List<Mensagem> mensagens = repoMsg.listar();
		repoMsg.fechar();
		return mensagens;
		
	}
	
	public static void deletarVarias(Context ctx, List<Mensagem> msgs){
		RepositorioMensagem repoMsg = new RepositorioMensagem(ctx);
		
		List<String> ids = new ArrayList<String>();
		for (Mensagem m: msgs){
			ids.add(String.valueOf(m.getId()));
		}
		
		String[] idsCollection  =  ids.toArray(new String[]{});
		
		repoMsg.deletarVarias(idsCollection);
		repoMsg.fechar();
		
		
		
	}
	
	public static int excluir(Context ctx, int id){
		RepositorioMensagem repoMsg = new RepositorioMensagem(ctx);
		int count = repoMsg.deletar(id);
		repoMsg.fechar();
		return count;
	}
	
	public void salvar(Context ctx){
		RepositorioMensagem repo = new RepositorioMensagem(ctx);
		repo.salvar(this);
		repo.fechar();
	}

	

}
