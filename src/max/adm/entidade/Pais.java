package max.adm.entidade;

import android.content.Context;
import max.adm.dominio.Entidade;
import max.adm.repositorio.RepositorioPais;

public class Pais extends Entidade {

	private static final long serialVersionUID = 1L;

	public static String TABELA_NOME = "PAIS";

	public static String CODIGO = "CODIGO";
	public static String SIGLA = "SIGLA";
	public static String NOME = "NOME";
	//public static String CODIGOIBGE = "CODIGO_IBGE";
	public static String CODIGOBACEN = "CODIGO_BACEN";

	public static String[] COLUNAS = { CODIGO, SIGLA, NOME, CODIGOBACEN };

	private long codigo;
	private String sigla;
	private String nomePais;
	private int codigoBacen;

	public static int buscaIdBrasil(Context ctx) {
		RepositorioPais repo = new RepositorioPais(ctx);
		int id = repo.buscaIdBrasil();
		repo.fechar();
		return id;
	}
	

	public long getCodigo() {
		return codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNomePais() {
		return nomePais;
	}

	public void setNomePais(String nomePais) {
		this.nomePais = nomePais;
	}

	public int getCodigoBacen() {
		return codigoBacen;
	}

	public void setCodigoBacen(int codigoBacen) {
		this.codigoBacen = codigoBacen;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (codigo ^ (codigo >>> 32));
		result = prime * result + codigoBacen;
		result = prime * result
				+ ((nomePais == null) ? 0 : nomePais.hashCode());
		result = prime * result + ((sigla == null) ? 0 : sigla.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pais other = (Pais) obj;
		if (codigo != other.codigo)
			return false;
		if (codigoBacen != other.codigoBacen)
			return false;
		if (nomePais == null) {
			if (other.nomePais != null)
				return false;
		} else if (!nomePais.equals(other.nomePais))
			return false;
		if (sigla == null) {
			if (other.sigla != null)
				return false;
		} else if (!sigla.equals(other.sigla))
			return false;
		return true;
	}
}
