package max.adm.entidade;

import java.util.List;

import max.adm.dominio.Entidade;
import max.adm.repositorio.RepositorioProduto;
import android.content.Context;

import com.google.common.base.Objects;

public class Produto extends Entidade {

	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "PRODUTO";

	public static String CODIGO = "CODIGO";
	public static String MASCARACODIGO = "PRO_MASKCOD";
	public static String DESCRICAO = "DESCRICAO";
	public static String DESCRICAOREDUZIDA = "DESCRICAO_REDUZIDA";
	public static String UNIDADE = "UNIDADE";
	public static String GRUPODEPRODUTO = "GRUPOPRODUTO_ID";

	public static String[] COLUNAS = { CODIGO, MASCARACODIGO, DESCRICAO,
			DESCRICAOREDUZIDA, UNIDADE, GRUPODEPRODUTO };

	private Integer codigo;
	private String mascaraCodigo;
	private String descricao;
	private String descricaoReduzida;
	private String unidadeDeMedida;
	private GrupoDeProdutos grupoDeProduto;

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getMascaraCodigo() {
		return mascaraCodigo;
	}

	public void setMascaraCodigo(String mascaraCodigo) {
		this.mascaraCodigo = mascaraCodigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricaoReduzida() {
		return descricaoReduzida;
	}

	public void setDescricaoReduzida(String descricaoReduzida) {
		this.descricaoReduzida = descricaoReduzida;
	}

	public String getUnidadeDeMedida() {
		return unidadeDeMedida;
	}

	public void setUnidadeDeMedida(String unidadeDeMedida) {
		this.unidadeDeMedida = unidadeDeMedida;
	}

	public GrupoDeProdutos getGrupoDeProduto() {
		return grupoDeProduto;
	}

	public void setGrupoDeProduto(GrupoDeProdutos grupoDeProduto) {
		this.grupoDeProduto = grupoDeProduto;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(mascaraCodigo, descricao);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Produto) {
			Produto other = (Produto) obj;
			return Objects.equal(this.mascaraCodigo, other.mascaraCodigo)
					&& Objects.equal(this.mascaraCodigo, other.mascaraCodigo);
		}
		return false;
	}

	/**
	 * 
	 * @param ctx
	 *            Context - contexto da activity para a abertura do banco
	 * @param idTabelaPreco
	 *            int - id da tabela de preco pela qual sera filtrada
	 * @return lista de prdutos sendo que cada item da lista contera apenas id,
	 *         pro_maskcodigo e descricaoo, para otimizacao da consilta. Caso
	 *         necessite mais dados usar o metodo busca pelo id do prduto
	 */
	public static List<Produto> listarParaTela(Context ctx, int idTabelaPreco) {
		RepositorioProduto repoProd = new RepositorioProduto(ctx);

		List<Produto> produtos = repoProd.listarParaTela(idTabelaPreco);
		repoProd.fechar();

		return produtos;
	}

	public static List<Produto> listarPorTabelas(Context ctx, String[] tabelas) {
		RepositorioProduto repoProd = new RepositorioProduto(ctx);

		List<Produto> produtos = repoProd.listarPorTabelas(tabelas);
		repoProd.fechar();

		return produtos;
	}

	public static List<Integer> listarProCodigos(Context ctx) {
		RepositorioProduto repo = new RepositorioProduto(ctx);
		List<Integer> codigos = repo.listarProCodigos();
		repo.fechar();
		return codigos;
	}

	public Produto salvar(Context ctx) {
		RepositorioProduto repo = new RepositorioProduto(ctx);
		this.codigo = repo.salvar(this);
		repo.fechar();
		return this;
	}

	public void putGrupoProduto(GrupoDeProdutos grupoDeProdutos) {
		this.grupoDeProduto = grupoDeProdutos;

	}

}
