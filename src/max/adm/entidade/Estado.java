package max.adm.entidade;

import max.adm.dominio.Entidade;
import max.adm.estado.RepositorioEstado;
import android.content.Context;

import com.google.common.base.Objects;

public class Estado extends Entidade {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "ESTADO";

	public static String SIGLA = "SIGLA";
	public static String DESCRICAO = "DESCRICAO";
	public static String IBGE = "CODIGO_IBGE";
	public static String PAIS = "PAIS_ID";

	public static String[] COLUNAS = { SIGLA, DESCRICAO, IBGE, PAIS };

	private String sigla;
	private String descricao;
	private int codigoIbge;
	private Pais pais;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public int getIbge() {
		return codigoIbge;
	}

	public void setIbge(int ibge) {
		this.codigoIbge = ibge;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + codigoIbge;
		result = prime * result + ((pais == null) ? 0 : pais.hashCode());
		result = prime * result + ((sigla == null) ? 0 : sigla.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estado other = (Estado) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (codigoIbge != other.codigoIbge)
			return false;
		if (pais == null) {
			if (other.pais != null)
				return false;
		} else if (!pais.equals(other.pais))
			return false;
		if (sigla == null) {
			if (other.sigla != null)
				return false;
		} else if (!sigla.equals(other.sigla))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("descricao", descricao)
				.add("Sigla", sigla).add("cod", codigoIbge).add("pais", pais)
				.toString();
	}

	public static boolean tabelaVazia(Context ctx) {
		RepositorioEstado repo = new RepositorioEstado(ctx);
		boolean vazio = repo.tabelaVazia();
		repo.fechar();
		return vazio;
	}

	public static Estado buscaEstadoPorSigla(Context ctx, String sigla) {
		RepositorioEstado repo = new RepositorioEstado(ctx);
		Estado estado = repo.busca(sigla);
		repo.fechar();
		return estado;
	}

}
