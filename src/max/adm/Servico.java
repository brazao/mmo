package max.adm;

public enum Servico {

	verifica("/hello/teste"), LicencaService("/licenca/verificaLicenca"), primeiroImport("/primeiroImport/importar"), buscaEstado("/buscaEstadosRest/buscaEstados"), buscaCidades(
			"/buscaCidadesRest/buscaCidades"), buscalojas("/buscaLojasRest/buscaLojas"), buscaParametros("/parametroRest/buscaParametros"), qtdeCeps("/cepsRest/qtdeCeps"), buscaCeps(
			"/cepsRest/buscaCeps"), buscaFaixaDeCodCliente("/faixaCodigoClientesRest/buscaFaixa"), buscaFaixaDeCodPedido("/faixaCodigPedidoRest/buscaFaixa"), buscaClassificacoesCli(
			"/classificacaoClientes/buscaClassificacoes"), buscaClientes("/clientesRest/buscaClientes"), buscaPadroes("/padroesRest/buscaPadroes"), buscaTamanhos("/tamanhosRest/buscaTamanhos"), buscaMedidas(
			"/unidadeDeMedidaRest/buscaUnMedidas"), buscaGruposDeProdutos("/grupoProdutoRest/buscaGruposDeProdutos"), buscaProdutos("/produtoRest/buscaProdutos"), buscaTabelasPreco(
			"/tabelaPrecoRest/buscaTabelas"), getQuantidadeReduzidos("/reduzidoRest/quantidade"), buscaReduzidos("/reduzidoRest/buscaReduzidos"), buscaImagens("/proImagemRest/buscaImagens"), buscaLotes(
			"/loteRest/buscaLotes"), buscaCondicoesPagto("/condicaoDePagamentoRest/buscaCondicoesPagto"), buscaTipoCobrancas("/tipoCobrancaRest/buscaTipoCobrancas"),

	enviaClientes("/clienteRest/enviaClientes"), enviaPedidos("/pedidoRest/enviaPedidos"),

	buscaPessoasNovas("/ImportarPessoas/buscaPessoasNovas"),

	buscaContasReceber("/contaReceberService/buscaContasReceber"), verificaQuantidadeDeContasParaImportar("/contaReceberService/quantidadeDeContasReceber"), buscaNotas(
			"/notaFiscalService/buscaNotasFiscais"), qantidadeNotaFiscalImportar("/notaFiscalService/qtdeParaImportar"),

	buscaTabelasDePrecos("/produtoService/buscaTabelasPreco"), 
	buscaQuantidadeDeReduzidos("/produtoService/getQuantidadeReduzidos"), 
	buscaProdutosReduzidos("/produtoService/buscaReduzidos"), 
	buscaImagensDosProdutos("/produtoService/buscaImagens"), 
	
	buscaBancoDeDados("/bancoDeDadosRest/buscaBancoDeDados");

	Servico(String valor) {
		this.valor = valor;
	}

	public String get() {
		return this.valor;
	}

	String valor;

}
