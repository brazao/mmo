package max.adm.endereco;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import max.adm.cidade.RepositorioCidade;
import max.adm.database.DBHelper;
import max.adm.entidade.Endereco;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RepositorioEndereco {

	private Context context;

	private static final String NOME_TABELA = Endereco.NOME_TABELA;

	protected SQLiteDatabase db;

	public RepositorioEndereco(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public int salvar(Endereco endereco) {
		int id = endereco.getCodigo();
		if (id != 0) {
			atualizar(endereco);
		} else {
			id = inserir(endereco);
		}

		return id;
	}

	public int inserir(Endereco endereco) {
		ContentValues values = new ContentValues();
		values.put(Endereco.TIPO_LOGRADOURO, endereco.getLogradouro());
		values.put(Endereco.NUMERO, endereco.getNumero());
		values.put(Endereco.CEP, endereco.getCep());
		values.put(Endereco.COMPLEMENTO, endereco.getComplemento());
		values.put(Endereco.TIPOENDERECO, endereco.getTipoEndereco().getDescricao());
		values.put(Endereco.TIPO_LOGRADOURO, endereco.getTipoLogradouro());
		values.put(Endereco.BAIRRO, endereco.getBairro());
		values.put(Endereco.CIDADE, endereco.getCidade().getCodigo());
		values.put(Endereco.PESSOA, endereco.getPessoa().getCodigo());
		values.put(Endereco.LOGRADOURO, endereco.getLogradouro());

		int id = inserir(values);

		return id;
	}

	public int inserir(ContentValues values) {
		int id = (int) db.insert(Endereco.NOME_TABELA, "", values);
		return id;
	}

	public int atualizar(Endereco endereco) {
		ContentValues values = new ContentValues();
		values.put(Endereco.TIPO_LOGRADOURO, endereco.getTipoLogradouro());
		values.put(Endereco.NUMERO, endereco.getNumero());
		values.put(Endereco.CEP, endereco.getCep());
		values.put(Endereco.COMPLEMENTO, endereco.getComplemento());
		values.put(Endereco.TIPOENDERECO, endereco.getTipoEndereco().getDescricao());
		values.put(Endereco.BAIRRO, endereco.getBairro());
		values.put(Endereco.CIDADE, endereco.getCidade().getCodigo());
		values.put(Endereco.PESSOA, endereco.getPessoa().getCodigo());
		values.put(Endereco.LOGRADOURO, endereco.getLogradouro());

		String _id = String.valueOf(endereco.getCodigo());
		String where = Endereco.CODIGO + "=?";
		String[] whereArgs = new String[] { _id };
		int count = atualizar(values, where, whereArgs);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public int deletar(int id) {
		String where = Endereco.CODIGO + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[] { _id };
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletarPorPessoa(int idPessoa) {
		String where = Endereco.PESSOA + "=?";
		String _idPessoa = String.valueOf(idPessoa);
		String[] whereArgs = new String[] { _idPessoa };
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(NOME_TABELA, where, whereArgs);
		return count;
	}

	public List<Endereco> buscaPorPessoa(long idPessoa) {
		Cursor c = db.query(true, NOME_TABELA, Endereco.COLUNAS, Endereco.PESSOA + "=" + idPessoa, null,
				null, null, null, null);

		List<Endereco> enderecos = new ArrayList<Endereco>();

		if (c.moveToFirst()) {
			do {
				RepositorioCidade repoCidade = new RepositorioCidade(context);

				Endereco endereco = new Endereco();
				enderecos.add(endereco);

				endereco.setCodigo(c.getInt(c.getColumnIndex(Endereco.CODIGO)));
				endereco.setNumero(c.getString(c.getColumnIndex(Endereco.NUMERO)));
				endereco.setCep(c.getString(c.getColumnIndex(Endereco.CEP)));
				endereco.setComplemento(c.getString(c.getColumnIndex(Endereco.COMPLEMENTO)));
				endereco.setTipoEndereco(tipoEndereco(c.getString(c.getColumnIndex(Endereco.TIPOENDERECO))));
				endereco.setTipoLogradouro(c.getString(c.getColumnIndex(Endereco.TIPO_LOGRADOURO)));
				endereco.setBairro(c.getString(c.getColumnIndex(Endereco.BAIRRO)));
				endereco.setCidade(repoCidade.busca(c.getInt(c.getColumnIndex(Endereco.CIDADE))));
				endereco.setLogradouro(c.getString(c.getColumnIndex(Endereco.LOGRADOURO)));
				repoCidade.fechar();

			} while (c.moveToNext());
			c.close();
			return enderecos;

		}
		return null;
	}

	public List<String> buscaTipoLogradouros() {
		Cursor c = db.query(true, NOME_TABELA, new String[] { Endereco.TIPO_LOGRADOURO }, null, null, null, null,
				null, null);

		List<String> tipoLogradouros = new LinkedList<String>();

		while (c.moveToNext()) {
			tipoLogradouros.add(c.getString(c.getColumnIndex(Endereco.TIPO_LOGRADOURO)));
		}
		return tipoLogradouros;
	}

	public Cursor getCursor() {
		try {
			return db.query(NOME_TABELA, Endereco.COLUNAS, null, null, null, null, null);
		} catch (SQLException e) {
			Log.e("Endereco", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

	private TipoEndereco tipoEndereco(String tipo) {
		if (tipo != null) {
			if (tipo.equals(TipoEndereco.CO.getDescricao())) {
				return TipoEndereco.CO;
			}
			if (tipo.equals(TipoEndereco.EN.getDescricao())) {
				return TipoEndereco.EN;
			}
		}
		return TipoEndereco.PR;
	}

}
