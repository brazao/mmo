package max.adm.endereco;

public enum TipoEndereco {
	
	PR("Principal"), EN("Entrega"), CO("Cobrança");
	
	private TipoEndereco(String descricao){
		this.descricao = descricao;
	}
	
	public String getDescricao(){
		return this.descricao;
	}
	
	private String descricao;

}
