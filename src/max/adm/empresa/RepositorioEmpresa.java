package max.adm.empresa;

import java.util.ArrayList;
import java.util.List;

import max.adm.cidade.RepositorioCidade;
import max.adm.database.DBHelper;
import max.adm.entidade.Empresa;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RepositorioEmpresa {

	private Context context;

	private static final String NOME_TABELA = Empresa.NOME_TABELA;

	protected SQLiteDatabase db;

	public RepositorioEmpresa(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public int salvar(Empresa empresa) {
		int id = empresa.getNumeroLoja();
		if (id != 0) {
			atualizar(empresa);
		} else {
			id = inserir(empresa);
		}

		return id;
	}

	public int inserir(Empresa empresa) {
		ContentValues values = new ContentValues();
		values.put(Empresa.NUMEROLOJA, empresa.getNumeroLoja());
		values.put(Empresa.CNPJ, empresa.getCnpj());
		values.put(Empresa.RAZAOSOCIAL, empresa.getRazaoSocial());
		values.put(Empresa.LOGRADOURO, empresa.getLogradouro());
		values.put(Empresa.COMPLEMENTO, empresa.getComplemento());
		values.put(Empresa.NUMEROENDERECO, empresa.getNumeroEndereco());
		values.put(Empresa.CEP, empresa.getCep());
		values.put(Empresa.INSCESTADUAL, empresa.getInscEstadual());
		values.put(Empresa.TELEFONE, empresa.getTelefone());
		values.put(Empresa.CONTATO, empresa.getContato());
		values.put(Empresa.REGIMETRIBUTARIO, empresa.getRegimeTributario());
		if (empresa.getCidade() != null) {
			values.put(Empresa.CIDADE, empresa.getCidade().getCodigo());
		} else {
			values.put(Empresa.CIDADE, 0);
		}

		int id = inserir(values);

		return id;
	}

	public int inserir(ContentValues values) {
		int id = (int) db.insert(Empresa.NOME_TABELA, "", values);
		return id;
	}

	public int atualizar(Empresa empresa) {
		ContentValues values = new ContentValues();
		values.put(Empresa.CNPJ, empresa.getCnpj());
		values.put(Empresa.RAZAOSOCIAL, empresa.getRazaoSocial());
		values.put(Empresa.LOGRADOURO, empresa.getLogradouro());
		values.put(Empresa.COMPLEMENTO, empresa.getComplemento());
		values.put(Empresa.NUMEROENDERECO, empresa.getNumeroEndereco());
		values.put(Empresa.CEP, empresa.getCep());
		values.put(Empresa.INSCESTADUAL, empresa.getInscEstadual());
		values.put(Empresa.TELEFONE, empresa.getTelefone());
		values.put(Empresa.CONTATO, empresa.getContato());
		values.put(Empresa.REGIMETRIBUTARIO, empresa.getRegimeTributario());
		values.put(Empresa.CIDADE, empresa.getCidade().getCodigo());

		String _id = String.valueOf(empresa.getNumeroLoja());
		String where = Empresa.NUMEROLOJA + "=?";
		String[] whereArgs = new String[]{_id};
		int count = atualizar(values, where, whereArgs);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public int deletar(int id) {
		String where = Empresa.NUMEROLOJA + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[]{_id};
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(NOME_TABELA, where, whereArgs);
		return count;
	}

	public Empresa busca(int id) {
		Cursor c = db.query(true, NOME_TABELA, Empresa.COLUNAS, Empresa.NUMEROLOJA + "=" + id, null, null, null, null, null);

		if (c.moveToFirst()) {
			
			Empresa empresa = new Empresa();
			empresa.setCnpj(c.getString((c.getColumnIndex(Empresa.CNPJ))));
			empresa.setNumeroLoja(c.getInt(c.getColumnIndex(Empresa.NUMEROLOJA)));
			empresa.setRazaoSocial(c.getString((c.getColumnIndex(Empresa.RAZAOSOCIAL))));
			empresa.setLogradouro(c.getString((c.getColumnIndex(Empresa.LOGRADOURO))));
			empresa.setComplemento(c.getString((c.getColumnIndex(Empresa.COMPLEMENTO))));
			empresa.setNumeroEndereco(c.getString((c.getColumnIndex(Empresa.NUMEROENDERECO))));
			empresa.setCep(c.getString((c.getColumnIndex(Empresa.CEP))));
			empresa.setInscEstadual(c.getString((c.getColumnIndex(Empresa.INSCESTADUAL))));
			empresa.setTelefone(c.getString((c.getColumnIndex(Empresa.TELEFONE))));
			empresa.setContato(c.getString((c.getColumnIndex(Empresa.CONTATO))));
			empresa.setRegimeTributario(c.getString((c.getColumnIndex(Empresa.REGIMETRIBUTARIO))));
			RepositorioCidade repoCidade = new RepositorioCidade(context);
			empresa.setCidade(repoCidade.busca(c.getInt(c.getColumnIndex(Empresa.CIDADE))));
			repoCidade.fechar();

			c.close();

			return empresa;

		}
		c.close();
		return null;
	}

	// retorna cursor com todos os Empresaes
	public Cursor getCursor() {
		try {
			// select * frm carros
			return db.query(NOME_TABELA, Empresa.COLUNAS, null, null, null, null, null);
		} catch (SQLException e) {
			Log.e("Empresa", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<Empresa> listar() {
		Cursor c = getCursor();
		List<Empresa> empresas = new ArrayList<Empresa>();

		while (c.moveToNext()) {
			RepositorioCidade repoCidade = new RepositorioCidade(context);
			Empresa empresa = new Empresa();
			empresa.setNumeroLoja(c.getInt(c.getColumnIndex(Empresa.NUMEROLOJA)));
			empresa.setCnpj(c.getString((c.getColumnIndex(Empresa.CNPJ))));
			empresa.setRazaoSocial(c.getString((c.getColumnIndex(Empresa.RAZAOSOCIAL))));
			empresa.setLogradouro(c.getString((c.getColumnIndex(Empresa.LOGRADOURO))));
			empresa.setComplemento(c.getString((c.getColumnIndex(Empresa.COMPLEMENTO))));
			empresa.setNumeroEndereco(c.getString((c.getColumnIndex(Empresa.NUMEROENDERECO))));
			empresa.setCep(c.getString((c.getColumnIndex(Empresa.CEP))));
			empresa.setInscEstadual(c.getString((c.getColumnIndex(Empresa.INSCESTADUAL))));
			empresa.setTelefone(c.getString((c.getColumnIndex(Empresa.TELEFONE))));
			empresa.setContato(c.getString((c.getColumnIndex(Empresa.CONTATO))));
			empresa.setRegimeTributario(c.getString((c.getColumnIndex(Empresa.REGIMETRIBUTARIO))));

			empresa.setCidade(repoCidade.busca(c.getInt(c.getColumnIndex(Empresa.CIDADE))));
			repoCidade.fechar();
			
			empresas.add(empresa);
		}
		c.close();

		return empresas;
	}

//	public List<Empresa> buscaLojasReplicacao(int codigoLoja) {
//
//		String sql = "SELECT * FROM " + Empresa.NOME_TABELA + " inner join " + Empresa.TABELA_REPLICA + " on " + Empresa.NUMEROLOJA + " = " + Empresa.REPLICA_PARA + " where " + Empresa.LOJA_REPLICA
//				+ " = " + codigoLoja;
//
//		Cursor c = db.rawQuery(sql, null);
//		List<Empresa> empresas = new ArrayList<Empresa>();
//
//		if (c.moveToFirst()) {
//			do {
//				Empresa empresa = new Empresa();
//				empresas.add(empresa);
//
//				empresa.setId(c.getInt(c.getColumnIndex(Empresa.NUMEROLOJA)));
//				empresa.setNumeroLoja(c.getInt(c.getColumnIndex(Empresa.NUMEROLOJA)));
//				empresa.setCnpj(c.getString((c.getColumnIndex(Empresa.CNPJ))));
//				empresa.setRazaoSocial(c.getString((c.getColumnIndex(Empresa.RAZAOSOCIAL))));
//
//			} while (c.moveToNext());
//
//		}
//		c.close();
//		return empresas;
//
//	}

	public boolean tabelaVazia() {
		String sql = "SELECT (" + Empresa.NUMEROLOJA + " ) FROM " + Empresa.NOME_TABELA;

		Cursor c;
		try {

			c = db.rawQuery(sql, null);

		} catch (SQLException e) {
			Log.e("Tabela Empresa", "Erro ao fazer busca: " + e.toString());
			return true;
		}

		if (c.getCount() > 0) {
			c.close();
			return false;
		} else {
			c.close();
			return true;
		}

	}

	public void fechar() {
		if (db != null) {
			this.db.close();
		}
	}

	public Empresa buscaEmpresa(int numeroLoja) {
		Empresa empresa = new Empresa();
		Cursor c = db.query(NOME_TABELA, 
				Empresa.COLUNAS, 
				Empresa.NUMEROLOJA + " = " + numeroLoja, 
				null, null, null, null);
		
		if (c.moveToFirst()) {
			RepositorioCidade repoCidade = new RepositorioCidade(context);
			empresa.setNumeroLoja(c.getInt(c.getColumnIndex(Empresa.NUMEROLOJA)));
			empresa.setCnpj(c.getString((c.getColumnIndex(Empresa.CNPJ))));
			empresa.setRazaoSocial(c.getString((c.getColumnIndex(Empresa.RAZAOSOCIAL))));
			empresa.setLogradouro(c.getString((c.getColumnIndex(Empresa.LOGRADOURO))));
			empresa.setComplemento(c.getString((c.getColumnIndex(Empresa.COMPLEMENTO))));
			empresa.setNumeroEndereco(c.getString((c.getColumnIndex(Empresa.NUMEROENDERECO))));
			empresa.setCep(c.getString((c.getColumnIndex(Empresa.CEP))));
			empresa.setInscEstadual(c.getString((c.getColumnIndex(Empresa.INSCESTADUAL))));
			empresa.setTelefone(c.getString((c.getColumnIndex(Empresa.TELEFONE))));
			empresa.setContato(c.getString((c.getColumnIndex(Empresa.CONTATO))));
			empresa.setRegimeTributario(c.getString((c.getColumnIndex(Empresa.REGIMETRIBUTARIO))));

			empresa.setCidade(repoCidade.busca(c.getInt(c.getColumnIndex(Empresa.CIDADE))));
			repoCidade.fechar();
		}
		c.close();
		return empresa;
	}

	public static Empresa buscaEmpresa(int numeroLoja, SQLiteDatabase db) {
		Empresa empresa = new Empresa();
		Cursor c = db.query(NOME_TABELA, 
				Empresa.COLUNAS, 
				Empresa.NUMEROLOJA + " = " + numeroLoja, 
				null, null, null, null);
		
		if (c.moveToFirst()) {
			//RepositorioCidade repoCidade = new RepositorioCidade(context);
			empresa.setNumeroLoja(c.getInt(c.getColumnIndex(Empresa.NUMEROLOJA)));
			empresa.setCnpj(c.getString((c.getColumnIndex(Empresa.CNPJ))));
			empresa.setRazaoSocial(c.getString((c.getColumnIndex(Empresa.RAZAOSOCIAL))));
			empresa.setLogradouro(c.getString((c.getColumnIndex(Empresa.LOGRADOURO))));
			empresa.setComplemento(c.getString((c.getColumnIndex(Empresa.COMPLEMENTO))));
			empresa.setNumeroEndereco(c.getString((c.getColumnIndex(Empresa.NUMEROENDERECO))));
			empresa.setCep(c.getString((c.getColumnIndex(Empresa.CEP))));
			empresa.setInscEstadual(c.getString((c.getColumnIndex(Empresa.INSCESTADUAL))));
			empresa.setTelefone(c.getString((c.getColumnIndex(Empresa.TELEFONE))));
			empresa.setContato(c.getString((c.getColumnIndex(Empresa.CONTATO))));
			empresa.setRegimeTributario(c.getString((c.getColumnIndex(Empresa.REGIMETRIBUTARIO))));

//			empresa.setCidade(repoCidade.busca(c.getInt(c.getColumnIndex(Empresa.CIDADE))));
//			repoCidade.fechar();
		}
		c.close();
		return empresa;
	}
}
