package max.adm.tabelaDePreco;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.empresa.RepositorioEmpresa;
import max.adm.entidade.Empresa;
import max.adm.entidade.TabelaDePreco;
import max.adm.utilidades.MascaraDecimal;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.common.collect.Lists;

public class RepositorioTabelaDePreco {

	private Context context;

	private static final String NOME_TABELA = TabelaDePreco.NOME_TABELA;

	protected SQLiteDatabase db;

	public RepositorioTabelaDePreco(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public int salvar(TabelaDePreco tabelaDePreco) {
		int id = tabelaDePreco.getCodigo();
		if (id != 0) {
			atualizar(tabelaDePreco);
		} else {
			id = inserir(tabelaDePreco);
		}

		return id;
	}

	public static int salvar(TabelaDePreco tabelaDePreco, SQLiteDatabase db) {
		int id = tabelaDePreco.getCodigo();
		if (id != 0) {
			atualizar(tabelaDePreco, db);
		} else {
			id = inserir(tabelaDePreco, db);
		}

		return id;
	}

	public int inserir(TabelaDePreco tabelaDePreco) {
		ContentValues values = new ContentValues();
		values.put(TabelaDePreco.DESCRICAO, tabelaDePreco.getDescricao());
		values.put(TabelaDePreco.CODIGO, tabelaDePreco.getCodigo());
		values.put(TabelaDePreco.PERCENTUALDESCONTO, tabelaDePreco.getPercentualDesconto());
		values.put(TabelaDePreco.EMPRESA, tabelaDePreco.getEmpresa().getNumeroLoja());

		int id = inserir(values);

		return id;
	}

	public static int inserir(TabelaDePreco tabelaDePreco, SQLiteDatabase db) {
		ContentValues values = new ContentValues();
		values.put(TabelaDePreco.DESCRICAO, tabelaDePreco.getDescricao());
		values.put(TabelaDePreco.CODIGO, tabelaDePreco.getCodigo());
		values.put(TabelaDePreco.PERCENTUALDESCONTO, tabelaDePreco.getPercentualDesconto());
		values.put(TabelaDePreco.EMPRESA, tabelaDePreco.getEmpresa().getNumeroLoja());

		int id = inserir(values, db);

		return id;
	}

	public int inserir(ContentValues values) {
		int id = (int) db.insert(TabelaDePreco.NOME_TABELA, "", values);
		return id;
	}

	public static int inserir(ContentValues values, SQLiteDatabase db) {
		int id = (int) db.insert(TabelaDePreco.NOME_TABELA, "", values);
		return id;
	}

	public int atualizar(TabelaDePreco tabelaDePreco) {
		ContentValues values = new ContentValues();
		values.put(TabelaDePreco.DESCRICAO, tabelaDePreco.getDescricao());
		values.put(TabelaDePreco.CODIGO, tabelaDePreco.getCodigo());
		values.put(TabelaDePreco.PERCENTUALDESCONTO, tabelaDePreco.getPercentualDesconto());
		values.put(TabelaDePreco.EMPRESA, tabelaDePreco.getEmpresa().getNumeroLoja());

		String _id = String.valueOf(tabelaDePreco.getCodigo());
		String where = TabelaDePreco.CODIGO + "=?";
		String[] whereArgs = new String[] { _id };
		int count = atualizar(values, where, whereArgs);

		return count;
	}

	public static int atualizar(TabelaDePreco tabelaDePreco, SQLiteDatabase db) {
		ContentValues values = new ContentValues();
		values.put(TabelaDePreco.DESCRICAO, tabelaDePreco.getDescricao());
		values.put(TabelaDePreco.CODIGO, tabelaDePreco.getCodigo());
		values.put(TabelaDePreco.PERCENTUALDESCONTO, tabelaDePreco.getPercentualDesconto());
		values.put(TabelaDePreco.EMPRESA, tabelaDePreco.getEmpresa().getNumeroLoja());

		String _id = String.valueOf(tabelaDePreco.getCodigo());
		String where = TabelaDePreco.CODIGO + "=?";
		String[] whereArgs = new String[] { _id };
		int count = atualizar(values, where, whereArgs, db);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public static int atualizar(ContentValues valores, String where, String[] whereArgs, SQLiteDatabase db) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public int deletar(int id) {
		String where = TabelaDePreco.CODIGO + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[] { _id };
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(NOME_TABELA, where, whereArgs);
		return count;
	}

	public TabelaDePreco busca(int id) {
		Cursor c = db.query(true, NOME_TABELA, TabelaDePreco.COLUNAS, TabelaDePreco.CODIGO + "=" + id, null,
				null, null, null, null);

		if (c.moveToFirst()) {
			RepositorioEmpresa repoEmpresa = new RepositorioEmpresa(context);
			TabelaDePreco tabelaDePreco = new TabelaDePreco();
			tabelaDePreco.setDescricao(c.getString(c.getColumnIndex(TabelaDePreco.DESCRICAO)));
			tabelaDePreco.setCodigo(c.getInt(c.getColumnIndex(TabelaDePreco.CODIGO)));
			tabelaDePreco.setPercentualDesconto(c.getDouble(c.getColumnIndex(TabelaDePreco.PERCENTUALDESCONTO)));
			tabelaDePreco.setEmpresa(repoEmpresa.busca(c.getInt(c.getColumnIndex(TabelaDePreco.EMPRESA))));
			repoEmpresa.fechar();

			c.close();
			return tabelaDePreco;
		}
		c.close();
		return null;
	}

	// retorna cursor com todos os TabelaDePrecoes
	public Cursor getCursor() {
		try {
			// select * frm carros
			return db.query(NOME_TABELA, TabelaDePreco.COLUNAS, null, null, null, null, null);
		} catch (SQLException e) {
			Log.e("TabelaDePreco", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<TabelaDePreco> listar() {
		Cursor c = getCursor();
		List<TabelaDePreco> tabelaDePrecos = new ArrayList<TabelaDePreco>();
		RepositorioEmpresa repoEmpresa = new RepositorioEmpresa(context);
		while (c.moveToNext()) {

			TabelaDePreco tabelaDePreco = new TabelaDePreco();
			tabelaDePrecos.add(tabelaDePreco);

			tabelaDePreco.setDescricao(c.getString(c.getColumnIndex(TabelaDePreco.DESCRICAO)));
			tabelaDePreco.setCodigo(c.getInt(c.getColumnIndex(TabelaDePreco.CODIGO)));
			tabelaDePreco.setPercentualDesconto(c.getDouble(c
					.getColumnIndex(TabelaDePreco.PERCENTUALDESCONTO)));
			tabelaDePreco.setEmpresa(repoEmpresa.busca(c.getInt(c.getColumnIndex(TabelaDePreco.EMPRESA))));

		}

		return tabelaDePrecos;
	}

	public List<TabelaPrecoItemLista> listarParaTela(Empresa empresa) {
		Cursor c = db.query(NOME_TABELA, new String[] { TabelaDePreco.CODIGO, TabelaDePreco.DESCRICAO,
				TabelaDePreco.PERCENTUALDESCONTO }, TabelaDePreco.EMPRESA + " = " + empresa.getNumeroLoja(),
				null, null, null, null);

		List<TabelaPrecoItemLista> tabelas = Lists.newLinkedList();
		if (c.moveToFirst()) {
			do {
				TabelaPrecoItemLista tab = TabelaPrecoItemLista.newInstance(c.getInt(c
						.getColumnIndex(TabelaDePreco.CODIGO)), c.getString(c
						.getColumnIndex(TabelaDePreco.DESCRICAO)), MascaraDecimal
						.comDuasCasaDecimais(new BigDecimal(c.getDouble(c
								.getColumnIndex(TabelaDePreco.PERCENTUALDESCONTO)))));

				tabelas.add(tab);
			} while (c.moveToNext());
		}
		c.close();
		return tabelas;
	}

	public Cursor getCursor(Empresa empresa) {
		String sql = "SELECT * FROM " + TabelaDePreco.NOME_TABELA;
		if (empresa != null) {
			sql += " WHERE " + TabelaDePreco.EMPRESA + " = " + String.valueOf(empresa.getNumeroLoja());
		}

		try {
			// select * frm carros
			return db.rawQuery(sql, null);
		} catch (SQLException e) {
			Log.e("TabelaDePreco", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<TabelaDePreco> listar(Empresa empresa) {
		Cursor c = getCursor(empresa);
		List<TabelaDePreco> tabelaDePrecos = new ArrayList<TabelaDePreco>();

		while (c.moveToNext()) {
			TabelaDePreco tabelaDePreco = new TabelaDePreco();
			tabelaDePrecos.add(tabelaDePreco);

			tabelaDePreco.setCodigo(c.getInt(c.getColumnIndex(TabelaDePreco.CODIGO)));
			tabelaDePreco.setDescricao(c.getString(c.getColumnIndex(TabelaDePreco.DESCRICAO)));
			tabelaDePreco.setPercentualDesconto(c.getDouble(c
					.getColumnIndex(TabelaDePreco.PERCENTUALDESCONTO)));
			tabelaDePreco.setEmpresa(empresa);

		}
		c.close();
		return tabelaDePrecos;
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

	public boolean salvarLista(List<TabelaDePreco> tabelas) {
		for (TabelaDePreco tab : tabelas) {
			// busca id da loja
			Cursor cLoja = null;
			try {
				cLoja = db.query(Empresa.NOME_TABELA, new String[] { Empresa.NUMEROLOJA }, Empresa.NUMEROLOJA
						+ "= " + tab.getEmpresa().getNumeroLoja(), null, null, null, null);

				if (cLoja.getCount() > 0) {
					cLoja.moveToFirst();
					int idLoja = cLoja.getInt(cLoja.getColumnIndex(Empresa.NUMEROLOJA));
					tab.getEmpresa().setNumeroLoja(idLoja);
					salvar(tab);
				}
				cLoja.close();
			} catch (Exception e) {
				e.printStackTrace();
				if (cLoja != null && !cLoja.isClosed()) {
					cLoja.close();
				}
			}
		}
		return true;
	}

}
