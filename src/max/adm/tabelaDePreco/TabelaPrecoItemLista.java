package max.adm.tabelaDePreco;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.base.Objects;

public class TabelaPrecoItemLista {
	
	private int id;
	private String nomeTabela;
	private String desconto;
	private boolean escolhida;
	
	private TabelaPrecoItemLista(int id, String nomeTabela, String desconto){
		this.id = id;
		this.nomeTabela = nomeTabela;
		this.desconto = desconto;
	}
	
	public static TabelaPrecoItemLista newInstance(int id, String nomeTabela, String desconto){
		checkArgument(id > 0);
		checkNotNull(nomeTabela);
		return new TabelaPrecoItemLista(id, nomeTabela, desconto);
	}
	
	
	public int getId() {
		return id;
	}
	
	public String getNomeTabela() {
		return nomeTabela;
	}
	
	public String getDesconto() {
		return desconto+"%";
	}
	
	public boolean isEscolhida() {
		return escolhida;
	}
	
	public void setEscolhida(boolean escolhida) {
		this.escolhida = escolhida;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof TabelaPrecoItemLista){
			TabelaPrecoItemLista other = (TabelaPrecoItemLista) o;
			return Objects.equal(this.id, other.id);
		}
		return false;
	}

}
