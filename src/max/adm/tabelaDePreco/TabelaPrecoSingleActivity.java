package max.adm.tabelaDePreco;

import java.io.Serializable;
import java.util.List;

import max.adm.R;
import max.adm.entidade.Empresa;
import max.adm.entidade.TabelaDePreco;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;

import com.google.common.collect.Lists;

public class TabelaPrecoSingleActivity extends ListActivity {

	private List<TabelaDePreco> tabelasPrecoEscolhidas = Lists.newLinkedList();
	private List<TabelaPrecoItemLista> tabelasDisponiveis = Lists.newLinkedList();
	private Empresa empresa;
	private TabelaPrecoRadioButtonAdapter adapterRadioButton;
	private Button btOk;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setFinishOnTouchOutside(false);
		setContentView(R.layout.tabela_preco_list);
		empresa = (Empresa) getIntent().getExtras().getSerializable("EMPRESA");
		tabelasPrecoEscolhidas = (List<TabelaDePreco>) getIntent().getExtras().getSerializable("TABELAS");
		listar();
		montaTela();
		defineFuncoes();
	}

	private void listar() {
		RepositorioTabelaDePreco repo = new RepositorioTabelaDePreco(this);
		tabelasDisponiveis = repo.listarParaTela(empresa);
		repo.fechar();
		for (TabelaPrecoItemLista t : tabelasDisponiveis) {
			for (TabelaDePreco tab : tabelasPrecoEscolhidas) {
				if (t.getId() == tab.getCodigo()) {
					t.setEscolhida(true);
				}
			}
		}
	}

	private void montaTela() {
		adapterRadioButton = new TabelaPrecoRadioButtonAdapter(this, tabelasDisponiveis);
		setListAdapter(adapterRadioButton);
		btOk = (Button) findViewById(R.id.bt_ok_tabela_preco);
		btOk.setVisibility(View.INVISIBLE);
	}

	private void defineFuncoes() {

		this.getListView().setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> lista, View arg1, int position, long id) {
				montaListaDeTabelasParaRetornar(id);
				onBackPressed();
			}
		});
	}

	@Override
	public void onBackPressed() {
		Intent i = new Intent();
		Bundle b = new Bundle();
		b.putSerializable("TABELAS", (Serializable) tabelasPrecoEscolhidas);
		i.putExtras(b);
		setResult(RESULT_OK, i);
		finish();

	}

	private void montaListaDeTabelasParaRetornar(long id) {
		tabelasPrecoEscolhidas.clear();
		RepositorioTabelaDePreco repo = new RepositorioTabelaDePreco(this);
		tabelasPrecoEscolhidas.add(repo.busca((int) id));
		repo.fechar();
	}

}
