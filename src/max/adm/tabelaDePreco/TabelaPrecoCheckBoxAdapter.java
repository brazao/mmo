package max.adm.tabelaDePreco;

import java.util.List;

import max.adm.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.google.common.collect.Lists;

public class TabelaPrecoCheckBoxAdapter extends BaseAdapter {

	private Context ctx;
	private List<TabelaPrecoItemLista> tabelas = Lists.newLinkedList();

	public TabelaPrecoCheckBoxAdapter(Context context, List<TabelaPrecoItemLista> tabelas) {
		this.ctx = context;
		this.tabelas = tabelas;
	}

	@Override
	public int getCount() {
		return tabelas.size();
	}

	public List<TabelaPrecoItemLista> getLista() {
		return tabelas;
	}

	@Override
	public Object getItem(int position) {
		return tabelas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return tabelas.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final TabelaPrecoItemLista tabela = tabelas.get(position);
		final ViewHolder holder;

		LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.modelo_item_tabela_preco_checkbox, null);
		holder = new ViewHolder();
		holder.textNomeTabela = (TextView) convertView.findViewById(R.id.text_descricao_tabela);
		holder.desconto = (TextView) convertView.findViewById(R.id.text_desconto_tabela);
		holder.escolhida = (CheckBox) convertView.findViewById(R.id.checkBox_tabela_preco);
		convertView.setTag(holder);

		holder.textNomeTabela.setText(tabela.getNomeTabela());
		holder.desconto.setText(tabela.getDesconto());
		holder.escolhida.setChecked(tabela.isEscolhida());
		holder.escolhida.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					tabela.setEscolhida(true);
				} else {
					tabela.setEscolhida(false);
				}
			}
		});
		return convertView;
	}

	static class ViewHolder {
		TextView textNomeTabela;
		TextView desconto;
		CheckBox escolhida;
	}

}
