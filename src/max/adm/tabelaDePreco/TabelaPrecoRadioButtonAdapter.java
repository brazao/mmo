package max.adm.tabelaDePreco;

import java.util.List;

import max.adm.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.common.collect.Lists;

public class TabelaPrecoRadioButtonAdapter extends BaseAdapter {

	private Context ctx;
	private List<TabelaPrecoItemLista> tabelas = Lists.newLinkedList();

	public TabelaPrecoRadioButtonAdapter(Context context, List<TabelaPrecoItemLista> tabelas) {
		this.ctx = context;
		this.tabelas = tabelas;
	}

	@Override
	public int getCount() {
		return tabelas.size();
	}

	public List<TabelaPrecoItemLista> getLista() {
		return tabelas;
	}

	@Override
	public Object getItem(int position) {
		return tabelas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return tabelas.get(position).getId();
	}
	
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final TabelaPrecoItemLista tabela = tabelas.get(position);
		final ViewHolder holder;

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.modelo_item_tabela_preco_radiobutton, null);
			holder = new ViewHolder();
			holder.textNomeTabela = (TextView) convertView.findViewById(R.id.text_descricao_tabela);
			holder.desconto = (TextView) convertView.findViewById(R.id.text_desconto_tabela);
			convertView.setTag(holder);
		}
		else{
			holder = (ViewHolder) convertView.getTag();
		}
		holder.textNomeTabela = (TextView) convertView.findViewById(R.id.text_descricao_tabela);
		holder.desconto = (TextView) convertView.findViewById(R.id.text_desconto_tabela);
		convertView.setTag(holder);

		holder.textNomeTabela.setText(tabela.getNomeTabela());
		holder.desconto.setText(tabela.getDesconto());
		
		return convertView;
	}

	static class ViewHolder {
		TextView textNomeTabela;
		TextView desconto;
	}
	

}
