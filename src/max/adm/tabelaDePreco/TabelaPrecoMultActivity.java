package max.adm.tabelaDePreco;

import java.io.Serializable;
import java.util.List;

import max.adm.R;
import max.adm.entidade.Empresa;
import max.adm.entidade.TabelaDePreco;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.common.collect.Lists;

public class TabelaPrecoMultActivity extends ListActivity {

	private List<TabelaDePreco> tabelasPrecoEscolhidas = Lists.newLinkedList();
	private List<TabelaPrecoItemLista> tabelasDisponiveis = Lists.newLinkedList();
	private Empresa empresa;
	private TabelaPrecoCheckBoxAdapter adapterCheckBox;
	private Button btOk;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setFinishOnTouchOutside(false);
		setContentView(R.layout.tabela_preco_list);
		empresa = (Empresa) getIntent().getExtras().getSerializable("EMPRESA");
		tabelasPrecoEscolhidas = (List<TabelaDePreco>) getIntent().getExtras().getSerializable("TABELAS");
		listar();
		montaTela();
		defineFuncoes();
	}

	private void listar() {
		RepositorioTabelaDePreco repo = new RepositorioTabelaDePreco(this);
		tabelasDisponiveis = repo.listarParaTela(empresa);
		repo.fechar();
		for (TabelaPrecoItemLista t : tabelasDisponiveis) {
			for (TabelaDePreco tab : tabelasPrecoEscolhidas) {
				if (t.getId() == tab.getCodigo()) {
					t.setEscolhida(true);
				}
			}
		}
	}

	private void montaTela() {
		adapterCheckBox = new TabelaPrecoCheckBoxAdapter(this, tabelasDisponiveis);
		setListAdapter(adapterCheckBox);

		btOk = (Button) findViewById(R.id.bt_ok_tabela_preco);
	}

	private void defineFuncoes() {
		btOk.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}

	private boolean descontosOk() {
		boolean ok = true;
		if (!tabelasPrecoEscolhidas.isEmpty()) {
			double desconto = (tabelasPrecoEscolhidas.get(0).getPercentualDesconto());
			for (TabelaDePreco tab : tabelasPrecoEscolhidas) {
				if (((desconto > 0) && (tab.getPercentualDesconto() == 0)) || (desconto == 0 && tab.getPercentualDesconto() > 0)) {
					ok = false;
					break;
				}
			}
		}
		return ok;
	}

	@Override
	public void onBackPressed() {
		tabelasPrecoEscolhidas.clear();
		montaListaDeTabelasParaRetornar();
		if (descontosOk()) {
			Intent i = new Intent();
			Bundle b = new Bundle();
			b.putSerializable("TABELAS", (Serializable) tabelasPrecoEscolhidas);
			i.putExtras(b);
			setResult(RESULT_OK, i);
			finish();
		} else {
			Toast.makeText(TabelaPrecoMultActivity.this, "Tabelas com percentual de desconto divergentes.", Toast.LENGTH_LONG).show();
		}
	}

	private void montaListaDeTabelasParaRetornar() {
		tabelasPrecoEscolhidas.clear();
		RepositorioTabelaDePreco repo = new RepositorioTabelaDePreco(TabelaPrecoMultActivity.this);
		for (TabelaPrecoItemLista tab : adapterCheckBox.getLista()) {
			if (tab.isEscolhida()) {
				tabelasPrecoEscolhidas.add(repo.busca(tab.getId()));
			}
		}

		repo.fechar();
	}

}
