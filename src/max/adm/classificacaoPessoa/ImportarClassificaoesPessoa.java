package max.adm.classificacaoPessoa;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import com.google.common.reflect.TypeToken;

import max.adm.Servico;
import max.adm.entidade.ClassifcacaoPessoa;
import max.adm.service.ConexaoHttp;
import max.adm.utilidades.Notificacao;
import max.adm.utilidades.NotificacaoFactory;
import android.content.Context;
import android.content.Intent;

public class ImportarClassificaoesPessoa {
	
	private Context ctx;
	private String url;
	
	private Notificacao notificacao;
	
	public ImportarClassificaoesPessoa(Context ctx, String url) {
		this.ctx = ctx;
		this.url = url;
	}
	
	public static ImportarClassificaoesPessoa newInstance(Context ctx, String url){
		checkNotNull(ctx);
		checkNotNull(ctx);
		return new ImportarClassificaoesPessoa(ctx, url);
	}
	
	public boolean execute(){
		notifica();
		try{
			RepositorioClassificacaoPessoa repo = new RepositorioClassificacaoPessoa(ctx);
			repo.deletar(null, null);
			repo.fechar();
			ConexaoHttp conexao = ConexaoHttp.newInstance(url+Servico.buscaClassificacoesCli.get());
			List<ClassifcacaoPessoa> classificacoes = conexao.executaRequisicao(ClassifcacaoPessoa.class, new TypeToken<List<ClassifcacaoPessoa>>() {}.getType());
			ClassifcacaoPessoa.salvarLista(ctx, classificacoes);
			notificaSucesso();
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			notificaFalha();
			return false;
		}
	}
	
	private void notifica(){
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoInfo(this.ctx, "Classificações de clientes", "Importando Classificações de clientes", new Intent());
	}
	
	private void notificaFalha(){
		notificacao.cancelaNotificacao();
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoFalha(this.ctx, "Classificações de clientes", "Falha ao importar", new Intent());
	}

	private void notificaSucesso(){
		notificacao.cancelaNotificacao();
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoSucesso(this.ctx, "Classificações de clientes", "importação concluida", new Intent());
	}

}
