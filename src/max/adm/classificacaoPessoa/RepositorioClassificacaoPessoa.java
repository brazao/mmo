package max.adm.classificacaoPessoa;

import java.util.ArrayList;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.entidade.ClassifcacaoPessoa;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.common.collect.Lists;

public class RepositorioClassificacaoPessoa {

	private Context context;

	private static final String NOME_TABELA = ClassifcacaoPessoa.NOME_TABELA;

	protected SQLiteDatabase db;

	public RepositorioClassificacaoPessoa(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public String salvar(ClassifcacaoPessoa classificacaoPessoa) {
		String id = classificacaoPessoa.getCodigo();
		if (!id.equals("0") || !id.equals("") ) {
			atualizar(classificacaoPessoa);
		} else {
			id = String.valueOf(inserir(classificacaoPessoa));
		}

		return id;
	}

	public boolean salvarLista(List<ClassifcacaoPessoa> lista) {

		try {
			for (ClassifcacaoPessoa classificacaoPessoa : lista) {
				try {
					ContentValues values = new ContentValues();
					values.put(ClassifcacaoPessoa.CODIGO, classificacaoPessoa.getCodigo());
					values.put(ClassifcacaoPessoa.DESCRICAO, classificacaoPessoa.getDescricao());

					db.insert(ClassifcacaoPessoa.NOME_TABELA, "", values);
				} catch (Exception e) {
					Log.e("cassificacaoPessoa", "Nao foi possivel inserir, classificacao ja existe");
				}
			}
		} catch (Exception e) {
			return false;
		}

		return true;
	}

	public int inserir(ClassifcacaoPessoa classificacaoPessoa) {
		ContentValues values = new ContentValues();
		values.put(ClassifcacaoPessoa.CODIGO, classificacaoPessoa.getCodigo());
		values.put(ClassifcacaoPessoa.DESCRICAO, classificacaoPessoa.getDescricao());

		int id = inserir(values);

		return id;
	}

	public int inserir(ContentValues values) {
		int id = (int) db.insert(ClassifcacaoPessoa.NOME_TABELA, "", values);
		return id;
	}

	public int atualizar(ClassifcacaoPessoa classificacaoPessoa) {
		ContentValues values = new ContentValues();
		values.put(ClassifcacaoPessoa.CODIGO, classificacaoPessoa.getCodigo());
		values.put(ClassifcacaoPessoa.DESCRICAO, classificacaoPessoa.getDescricao());

		String _id = String.valueOf(classificacaoPessoa.getCodigo());
		String where = ClassifcacaoPessoa.CODIGO + "=?";
		String[] whereArgs = new String[] { _id };
		int count = atualizar(values, where, whereArgs);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public int deletar(int id) {
		String where = ClassifcacaoPessoa.CODIGO + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[] { _id };
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(NOME_TABELA, where, whereArgs);
		return count;
	}

	public ClassifcacaoPessoa busca(int id) {
		Cursor c = db.query(true, NOME_TABELA, ClassifcacaoPessoa.COLUNAS, ClassifcacaoPessoa.CODIGO + "="
				+ id, null, null, null, null, null);

		if (c.getCount() > 0) {
			// pocisiona no primeiro elemento
			c.moveToFirst();
			ClassifcacaoPessoa classificacaoPessoa = new ClassifcacaoPessoa();
			classificacaoPessoa.setCodigo(c.getString(c.getColumnIndex(ClassifcacaoPessoa.CODIGO)));
			classificacaoPessoa.setDescricao(c.getString(c.getColumnIndex(ClassifcacaoPessoa.DESCRICAO)));

			c.close();

			return classificacaoPessoa;

		}
		c.close();
		return null;
	}

	private Cursor getCursor() {
		try {
			return db.query(ClassifcacaoPessoa.NOME_TABELA, ClassifcacaoPessoa.COLUNAS, null, null, null,
					null, ClassifcacaoPessoa.CODIGO);

		} catch (SQLException e) {
			Log.e("ClassifcacaoPessoa", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<ClassifcacaoPessoa> listar() {
		Cursor c = getCursor();
		List<ClassifcacaoPessoa> classificacaoPessoas = new ArrayList<ClassifcacaoPessoa>();

		while (c.moveToNext()) {
			ClassifcacaoPessoa classificacaoPessoa = new ClassifcacaoPessoa();
			classificacaoPessoas.add(classificacaoPessoa);

			classificacaoPessoa.setCodigo(c.getString(c.getColumnIndex(ClassifcacaoPessoa.CODIGO)));
			classificacaoPessoa.setDescricao(c.getString(c.getColumnIndex(ClassifcacaoPessoa.DESCRICAO)));

		}

		return classificacaoPessoas;
	}

	public List<ClassifcacaoPessoa> listarTodas() {
		List<ClassifcacaoPessoa> classificacoes = Lists.newLinkedList();
		Cursor c = db.query(NOME_TABELA, ClassifcacaoPessoa.COLUNAS, null, null, null, null, null);
		if (c.moveToFirst()) {
			do {
				ClassifcacaoPessoa cla = new ClassifcacaoPessoa();
				cla.setCodigo(c.getString(c.getColumnIndex(ClassifcacaoPessoa.CODIGO)));
				cla.setDescricao(c.getString(c.getColumnIndex(ClassifcacaoPessoa.DESCRICAO)));
				classificacoes.add(cla);
			} while (c.moveToNext());
		}
		c.close();
		return classificacoes;
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

}
