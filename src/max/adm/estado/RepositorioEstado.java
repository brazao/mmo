package max.adm.estado;

import java.util.ArrayList;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.entidade.Estado;
import max.adm.repositorio.RepositorioPais;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RepositorioEstado {

	private Context context;

	private static final String NOME_TABELA = Estado.NOME_TABELA;

	protected SQLiteDatabase db;

	public RepositorioEstado(Context context) {
		db = new DBHelper(context).getWritableDatabase();
		this.context = context;
	}

	public boolean tabelaVazia() {
		Cursor c = getCursor();
		if (c.moveToFirst()) {
			c.close();
			return false;
		}
		c.close();
		return true;

	}

	public Estado busca(String sigla) {
		Cursor c = db.query(true, NOME_TABELA, Estado.COLUNAS, Estado.SIGLA + "='" + sigla + "'", null, null,
				null, null, null);

		if (c.moveToFirst()) {

			Estado estado = new Estado();
			estado.setDescricao(c.getString(c.getColumnIndex(Estado.DESCRICAO)));
			estado.setSigla(c.getString(c.getColumnIndex(Estado.SIGLA)));
			estado.setIbge(c.getInt(c.getColumnIndex(Estado.IBGE)));
			RepositorioPais repoPais = new RepositorioPais(context);
			estado.setPais(repoPais.busca(c.getInt(c.getColumnIndex(Estado.PAIS))));
			repoPais.fechar();
			c.close();
			return estado;

		}
		c.close();
		return null;
	}

	public List<String> buscaSiglas() {
		Cursor c = db.query(true, NOME_TABELA, new String[] { Estado.SIGLA }, 
				null, null, null, null, null, null);
		List<String> lista = new ArrayList<String>();
		while (c.moveToNext()) {
			lista.add(c.getString(c.getColumnIndex(Estado.SIGLA)));
		}
		return lista;
	}

	// retorna cursor com todos os Estadoes
	public Cursor getCursor() {
		try {
			// select * frm carros
			return db.query(NOME_TABELA, Estado.COLUNAS, null, null, null, null, null);
		} catch (SQLException e) {
			Log.e("Estado", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	private List<Estado> listar(Cursor cursor) {
		List<Estado> estados = new ArrayList<Estado>();

		while (cursor.moveToNext()) {
			RepositorioPais repoPais = new RepositorioPais(context);
			Estado estado = new Estado();
			estado.setDescricao(cursor.getString(cursor.getColumnIndex(Estado.DESCRICAO)));
			estado.setIbge(cursor.getInt(cursor.getColumnIndex(Estado.IBGE)));
			estado.setSigla(cursor.getString(cursor.getColumnIndex(Estado.SIGLA)));
			estado.setPais(repoPais.busca(cursor.getInt(cursor.getColumnIndex(Estado.PAIS))));
			repoPais.fechar();

			estados.add(estado);
		}

		return estados;
	}

	public List<Estado> listarTodos() {
		return listar(getCursor());
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

}
