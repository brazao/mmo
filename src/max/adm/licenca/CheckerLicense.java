package max.adm.licenca;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import max.adm.service.ConexaoHttp;
import max.adm.utilidades.Notificacao;
import max.adm.utilidades.NotificacaoFactory;
import android.content.Context;
import android.content.Intent;

import com.google.common.base.Strings;

public class CheckerLicense {

	private Context ctx;
	private String url;
	private String chave;
	private Notificacao notificacao;

	private CheckerLicense(Context ctx, String url, String chave) {
		this.ctx = ctx;
		this.chave = chave;
		this.url = url;
	}

	public static CheckerLicense newInstance(Context ctx, String url, String chave) {
		checkNotNull(ctx);
		checkArgument(!Strings.isNullOrEmpty(url));
		checkArgument(!Strings.isNullOrEmpty(chave));
		return new CheckerLicense(ctx, url, chave);
	}

	public boolean check() {

		try {
			ConexaoHttp conexao = ConexaoHttp.newInstance(url);
			String resposta = conexao.executaRequisicao(chave);
			System.out.println("resposta "+resposta);
			if("ok".equals(resposta)) {
				return true;
			}
			else {
				notificaFalha();
				return false;
			}
		} catch (Exception e) {
			notificaFalha();
			e.printStackTrace();
			return false;
		}

	}
	
	private void notificaFalha() {
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoFalha(this.ctx, "Licenca", "Entre em contato com a fábrica para verificar sua licença.", new Intent());

	}
}
