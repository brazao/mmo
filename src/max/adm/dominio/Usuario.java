package max.adm.dominio;

import java.util.List;

import android.content.Context;
import max.adm.repositorio.RepositorioUsuario;

public class Usuario extends Entidade {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static String NOME_TABELA = "USUARIO";

	public static String ID = "USU_ID";
	public static String LOGIN = "USU_LOGIN";
	public static String SENHA = "USU_SENHA";
	public static String NOME = "USU_NOME";
	public static String CODIGOREPRESENTANTE = "USU_CODIGO_REPRES";
	
	public static String[] COLUNAS = {ID, LOGIN, SENHA, NOME, CODIGOREPRESENTANTE};

	private int id;
	private String login;
	private String senha;
	private String nome;
	private String codigoRepresentante;

	public String getCodigoRepresentante() {
		return codigoRepresentante;
	}

	public void setCodigoRepresentante(String codigoRepresentante) {
		this.codigoRepresentante = codigoRepresentante;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	public void salvar(Context ctx){
		RepositorioUsuario repoUser = new RepositorioUsuario(ctx);
		repoUser.salvar(this);
		
		repoUser.fechar();
		
	}
	
	public static List<Usuario> listar(Context ctx){
		RepositorioUsuario repoUser = new RepositorioUsuario(ctx);
		List<Usuario> users = repoUser.listar();
		repoUser.fechar();
		
		
		return users;
		
	}
	

}
