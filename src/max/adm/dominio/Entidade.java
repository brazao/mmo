package max.adm.dominio;

import java.io.Serializable;

public abstract class Entidade implements Serializable, Cloneable {

	@Override
	protected Entidade clone()  {
		// TODO Auto-generated method stub
		try {
			return (Entidade) super.clone();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
		
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
    public abstract boolean equals(Object o);
    
    @Override
    public abstract int hashCode();

}
