package max.adm.dominio;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Date;

import max.adm.entidade.Empresa;

import com.google.common.base.Objects;

public class Sincronismo extends Entidade {
	
	private Sincronismo(Empresa empresa, Date data, String tipo, String cliente,
			String pedido, String cep, String condicaoPgto,
			String contaReceber, String faixaCodigo, String tabelaPreco) {
		
		this.empresa = empresa;
		this.data = data;
		this.tipo = tipo;
		this.cliente = cliente;
		this.pedido = pedido;
		this.cep = cep;
		this.condicaoPgto = condicaoPgto;
		this.contaReceber = contaReceber;
		this.faixaCodigo = faixaCodigo;
		this.tabelaPreco = tabelaPreco;
	}
	
	public static Sincronismo newSincronismo(Empresa empresa, Date data, String tipo, boolean cliente,
			boolean pedido, boolean cep, boolean condicaoPgto,
			boolean contaReceber, boolean faixaCodigo, boolean tabelaPreco){
		
			checkNotNull(empresa, "empresa não pode ser nula");
			checkNotNull(data, "data não pode ser nulo");
			checkNotNull(tipo, "tipo não pode ser nulo");
			checkArgument(tipo.equals("E") || tipo.equals("R"),  "tipo só pode ser igual a R ou E");
		
			String scliente="N";
			String spedido="N";
			String scep="N";
			String scondicaoPgto="N";
			String scontaReceber="N"; 
			String sfaixaCodigo="N"; 
			String stabelaPreco="N";	
			
		 if(cliente){scliente="S";}
		 if( pedido){spedido="S";}
		 if( cep){scep="S";}
		 if(condicaoPgto){scondicaoPgto="S";}
		 if( contaReceber){scontaReceber="S";}
		 if( faixaCodigo){sfaixaCodigo="S";}
		 if( tabelaPreco){stabelaPreco="S";}
		 
		 return new Sincronismo(empresa, data, tipo, scliente, spedido, scep, scondicaoPgto, scontaReceber, sfaixaCodigo, stabelaPreco);
	}

	private static final long serialVersionUID = 1L;
	
	private long id;
	private Empresa empresa;
	private Date data;
	private String tipo;
	private String cliente;
	private String pedido;
	private String cep;
	private String condicaoPgto;
	private String contaReceber;
	private String faixaCodigo;
	private String tabelaPreco;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getPedido() {
		return pedido;
	}

	public void setPedido(String pedido) {
		this.pedido = pedido;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCondicaoPgto() {
		return condicaoPgto;
	}

	public void setCondicaoPgto(String condicaoPgto) {
		this.condicaoPgto = condicaoPgto;
	}

	public String getContaReceber() {
		return contaReceber;
	}

	public void setContaReceber(String contaReceber) {
		this.contaReceber = contaReceber;
	}

	public String getFaixaCodigo() {
		return faixaCodigo;
	}

	public void setFaixaCodigo(String faixaCodigo) {
		this.faixaCodigo = faixaCodigo;
	}

	public String getTabelaPreco() {
		return tabelaPreco;
	}

	public void setTabelaPreco(String tabelaPreco) {
		this.tabelaPreco = tabelaPreco;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Sincronismo){
			Sincronismo other = (Sincronismo) obj;
			return Objects.equal(this.id, other.getId());
			
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.id);
	}


}
