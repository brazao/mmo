package max.adm.produto;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;

import max.adm.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.common.collect.Lists;

public class GalleryProdutoAutoCompleteAdapter extends GalleryProdutoAdapter implements Filterable {
	
	protected List<ItemGalleryProduto> itensNew = Lists.newLinkedList();

	public GalleryProdutoAutoCompleteAdapter(Context ctx, List<ItemGalleryProduto> itens) {
		super(ctx, itens);
	}

	@Override
	public Filter getFilter() {

		Filter myFilter = new Filter() {

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults filterResults = new FilterResults();
				List<ItemGalleryProduto> results = Lists.newLinkedList();
				if (constraint != null && itens != null) {
					System.out.println(constraint);
					for (ItemGalleryProduto item : itens) {
						if (item.getProduto().getDescricao().toUpperCase().contains(constraint.toString().toUpperCase()) ||
								item.getProduto().getMascaraCodigo().toUpperCase().contains(constraint.toString().toUpperCase())) {
							results.add(item);
						}
					}
					filterResults.values = results;
					filterResults.count = results.size();
				}
				return filterResults;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				if (results != null && results.count > 0) {
					itensNew = (List<ItemGalleryProduto>) results.values;
					notifyDataSetChanged();
				}
				else {
					itensNew = Lists.newLinkedList(itens);
					notifyDataSetInvalidated();
				}
				
			}
		};
		return myFilter;
	}

	@Override
	public int getCount() {
		return itensNew.size();
	}

	@Override
	public Object getItem(int position) {
		return itensNew.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ItemGalleryProduto item = itensNew.get(position);
		final Holder holder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.modelo_item_gallery_produto, null);

			holder = new Holder();
			holder.mascara = (TextView) convertView.findViewById(R.id.mascara_produto);
			holder.descricao = (TextView) convertView.findViewById(R.id.descricao_produto);
			holder.imagem = (LinearLayout) convertView.findViewById(R.id.imagem_produto);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		
		ImageView iv = new ImageView(ctx);
		holder.imagem.removeAllViews();
		holder.imagem.addView(iv);
		
		String path = String.format("%s/%s/%d/mini/%dmini.jpg", 
				Environment.getExternalStorageDirectory().getPath(), 
				"maxMobile/produtos", 
				item.getImagemProduto().getProduto().getCodigo(), 
				item.getImagemProduto().getProduto().getCodigo());
		File f = new File(path);
		Drawable drw;
		try {
			if (f.exists()) {
				Bitmap b = BitmapFactory.decodeFile(f.getAbsolutePath());
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				b.compress(Bitmap.CompressFormat.JPEG, 100, stream);
				item.getImagemProduto().setImagem(stream.toByteArray());
				drw = new BitmapDrawable(item.getImagemProduto().getBitmap());
				iv.setImageDrawable(drw);
				stream.close();
			} else {
				drw = ctx.getResources().getDrawable(R.drawable.vazio);
				iv.setImageDrawable(drw);
			}
			
		} catch (Throwable e) {
			Log.e("IMAGEM", e.getMessage());
		}
		
		holder.mascara.setText(item.getProduto().getMascaraCodigo());
		holder.descricao.setText(item.getProduto().getDescricao());
		return convertView;
	}

}
