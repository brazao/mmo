package max.adm.produto;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.Serializable;
import java.util.List;

import max.adm.entidade.ImagemProduto;
import max.adm.entidade.PedidoItens;
import max.adm.entidade.Produto;
import max.adm.entidade.ProdutoReduzido;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.Log;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;

public class ItemGalleryProduto implements Serializable {

	private static final long serialVersionUID = 1L;

	private ImagemProduto imagemProduto;
	private Produto produto;
	List<PedidoItens> itens = Lists.newLinkedList();

	private ItemGalleryProduto(ImagemProduto imagemProduto, Produto produto) {
		this.imagemProduto = imagemProduto;
		this.produto = produto;
	}

	public static ItemGalleryProduto newInstance(ImagemProduto imagemProduto) {
		checkNotNull(imagemProduto);
		return new ItemGalleryProduto(imagemProduto, imagemProduto.getProduto());
	}

	public ImagemProduto getImagemProduto() {
		return imagemProduto;
	}
	public Produto getProduto() {
		return produto;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof ItemGalleryProduto) {
			ItemGalleryProduto other = (ItemGalleryProduto) o;
			return Objects.equal(this.produto, other.produto);
		}
		return false;
	}

	@Override
	public String toString() {
		return String.format("%s %s", produto.getMascaraCodigo(), produto.getDescricao());
	}

	public List<Drawable> getDrawable() {
		List<Drawable> drawables = Lists.newLinkedList();
		String path = String.format("%s/%s/%d/normal", 
				Environment.getExternalStorageDirectory().getPath(), 
				"maxMobile/produtos", 
				this.imagemProduto.getProduto().getCodigo());

		Log.i("path ItemGaleryProduto", path);
		
		File f = new File(path);
		if (f.isDirectory()) {
			File[] listFiles = f.listFiles();
			if (listFiles != null && listFiles.length > 0) {
				for (File file : listFiles) {
					Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
					byte[] byteArray = stream.toByteArray();
					drawables.add(Drawable.createFromStream(new ByteArrayInputStream(byteArray), "articleImage"));
				}
			}
		}
		return drawables;
	}

	public void setItens(List<PedidoItens> itens) {
		if (itens != null)
			this.itens = itens;
	}

	public List<PedidoItens> getItens() {
		return itens;
	}

	public int getQuantidade() {
		int quantidade = 0;
		if (itens != null) {
			for (PedidoItens item : itens) {
				quantidade += item.getQuantidade();
			}
		}
		return quantidade;
	}

	public int getQuantidadePorReduzido(ProdutoReduzido red) {
		int quantidade = 0;
		if (itens != null) {
			for (PedidoItens item : itens) {
				if (item.getTabelaPrecoProdutoReduzido().getProdutoReduzido().equals(red)) {
					quantidade = item.getQuantidade();
					break;
				}
			}
		}
		return quantidade;
	}

}
