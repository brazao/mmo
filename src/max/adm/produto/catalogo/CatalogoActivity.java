package max.adm.produto.catalogo;

import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.google.common.reflect.TypeToken;

import max.adm.Dinheiro;
import max.adm.R;
import max.adm.Servico;
import max.adm.ValorAtividade;
import max.adm.controle.EscolhaPadraoTamanhoControle;
import max.adm.entidade.Empresa;
import max.adm.entidade.ImagemProduto;
import max.adm.entidade.Parametro;
import max.adm.entidade.PedidoCapa;
import max.adm.entidade.PedidoItens;
import max.adm.entidade.Produto;
import max.adm.entidade.ProdutoReduzido;
import max.adm.entidade.TabelaDePreco;
import max.adm.entidade.TabelaPrecoProdutoReduzido;
import max.adm.pedido.PedidoInsereDadosCapa;
import max.adm.produto.GalleryProdutoAdapter;
import max.adm.produto.ItemGalleryProduto;
import max.adm.produtoreduzido.repositorio.RepositorioProdutoReduzido;
import max.adm.repositorio.RepositorioProImagem;
import max.adm.repositorio.RepositorioProduto;
import max.adm.repositorio.RepositorioTabelaDePrecoProdutoReduzido;
import max.adm.service.ConexaoHttp;
import max.adm.utilidades.adapters.ProdutoItemPedidoParaListar;
import max.adm.utilidades.animation.MaxAnimation;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CatalogoActivity extends Activity {
	ImageView imagem;
	ImageView up;
	ImageView down;
	ImageView left;
	ImageView rigth;
	ImageView imgAdicionado;
	ImageView imgShowGallery;
	ImageView imgHideGallery;
	TextView codigo;
	TextView descricao;
	LinearLayout info;
	LinearLayout detalhes;
	LinearLayout resumo;
	GestureDetectorCompat detector;
	GestureDetectorCompat detectorGallery;

	TextView detCodigo;
	TextView detDescricao;
	TextView padrao;
	TextView tamanho;
	TextView preco;
	ImageView btShowSummary;
	ImageView btFechaPedido;
	TextView quantidade;
	TextView qtde;
	TextView subtotal;

	protected static int DIALOG_CARREGA_PRODS = 999;
	protected ProgressDialog progressDialog;
	protected List<ItemGalleryProduto> itens = new LinkedList<ItemGalleryProduto>();
	protected TabelaDePreco tabela;
	protected Parametro parametro;
	protected Empresa empresa;
	protected int SELECTED = 0;
	protected int IMGSELECTED = 0;
	protected List<Drawable> drawables = new LinkedList<Drawable>();

	protected Gallery gallery;
	protected GalleryProdutoAdapter adapter;

	protected Map<Produto, List<PedidoItens>> itensDoPedido = new HashMap<Produto, List<PedidoItens>>();

	@Override
	protected void onCreate(Bundle state) {
		super.onCreate(state);
		setContentView(R.layout.catalogo);
		
		criaEstruturaDePastas();
		
		if (state == null || !state.containsKey("RESTORE")) {
			tabela = (TabelaDePreco) getIntent().getExtras().getSerializable("TABELAPRECO");
			empresa = (Empresa) getIntent().getExtras().getSerializable("EMPRESA");
			parametro = (Parametro) getIntent().getExtras().getSerializable("PARAMETRO");
			new CarregaProdutos().execute("");
		}
		detector = new GestureDetectorCompat(this, new MyGestureListener());
		detectorGallery = new GestureDetectorCompat(this, new MyGestureListenerGallery());
	}
	
	public void criaEstruturaDePastas() {
		File f = new File(String.format("%s/%s", 
				Environment.getExternalStorageDirectory().getPath(), 
				"maxMobile/produtos"));
		
		RepositorioProImagem repositorioProImagem = new RepositorioProImagem(this);
		repositorioProImagem.apagaDiretorioPrincipal();
		
		if (!f.exists()) {
			Log.i("CatalagoActivity", "criando estrutura de pastas");
			List<ImagemProduto> imagensProduto = repositorioProImagem.listar();
			
			for (ImagemProduto imagemProduto : imagensProduto) {
				repositorioProImagem.criaDiretorio(String.valueOf(imagemProduto.getProduto().getCodigo()));
				repositorioProImagem.saveImage(imagemProduto);
				repositorioProImagem.saveImageMini(imagemProduto);
			}
			
			repositorioProImagem.fechar();
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle state) {
		state.putSerializable("TABELA", tabela);
		state.putSerializable("EMPRESA", empresa);
		state.putSerializable("PARAMETRO", parametro);
		state.putSerializable("ITENS", (Serializable) itens);
		state.putInt("SELECTED", SELECTED);

		state.putInt("IMGSELECTED", IMGSELECTED);
		state.putBoolean("RESTORE", true);
		state.putSerializable("ITENSDOPEDIDO", (Serializable) itensDoPedido);
		super.onSaveInstanceState(state);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onRestoreInstanceState(Bundle state) {
		tabela = (TabelaDePreco) state.getSerializable("TABELA");
		empresa = (Empresa) state.getSerializable("EMPRESA");
		parametro = (Parametro) state.getSerializable("PARAMETRO");
		itens = (List<ItemGalleryProduto>) state.getSerializable("ITENS");
		SELECTED = state.getInt("SELECTED");
		IMGSELECTED = state.getInt("IMGSELECTED");
		itensDoPedido = (Map<Produto, List<PedidoItens>>) state.getSerializable("ITENSDOPEDIDO");

		initiComponentes();
		handleComponentes();
		preencheTela(itens.get(SELECTED));
		hideComponents();
		detalhes.setVisibility(View.INVISIBLE);
		resumo.setVisibility(View.INVISIBLE);
		adapter = new GalleryProdutoAdapter(CatalogoActivity.this, itens);
		gallery.setAdapter(adapter);
		gallery.setVisibility(View.INVISIBLE);
		imgHideGallery.setVisibility(View.INVISIBLE);
		gallery.startAnimation(MaxAnimation.fadeOut(CatalogoActivity.this, 1500));
		imgShowGallery.startAnimation(MaxAnimation.blink(CatalogoActivity.this, 1000));
		super.onRestoreInstanceState(state);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data != null && requestCode == ValorAtividade.EscolhaPadraoTamanho.get()) {
			List<PedidoItens> itensPedido = (List<PedidoItens>) data.getExtras().getSerializable("ITENSPEDIDO");
			itens.get(SELECTED).setItens(itensPedido);
			itensDoPedido.put(itens.get(SELECTED).getProduto(), itens.get(SELECTED).getItens());
			removeQuantidadeZero();
			subtotal.setText(getValor().formatToScreen());
			quantidade.setText(String.valueOf(getQuantidade()));
			detalhes.setVisibility(View.INVISIBLE);
			
			if (itens.get(SELECTED).getItens().size() > 0) {
				imgAdicionado.setVisibility(View.VISIBLE);
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	protected void removeQuantidadeZero() {
		Iterator<Produto> iterator = itensDoPedido.keySet().iterator();
		while (iterator.hasNext()) {
			Produto key = iterator.next();
			Iterator<PedidoItens> it2 = itensDoPedido.get(key).iterator();
			while (it2.hasNext()) {
				PedidoItens item = it2.next();
				if (item.getQuantidade() == 0) {
					it2.remove();
				}
			}

			if (itensDoPedido.get(key).isEmpty()) {
				iterator.remove();
			}
		}
	}

	private void insereCapa() {
		PedidoCapa.Builder builder = PedidoCapa.builder();
		builder.comTabelaDePreco(tabela);
		for (Map.Entry<Produto, List<PedidoItens>> entry : itensDoPedido.entrySet()) {
			for (PedidoItens it : entry.getValue()) {
				builder.addItem(it);
			}
		}

		List<PedidoCapa.Builder> builders = new LinkedList<PedidoCapa.Builder>();
		builders.add(builder);

		Bundle b = new Bundle();
		b.putSerializable("EMPRESA", empresa);
		b.putSerializable("PARAMETRO", parametro);
		b.putSerializable("BUILDERS", (Serializable) builders);
		Intent i = new Intent(this, PedidoInsereDadosCapa.class);
		i.putExtras(b);
		startActivityForResult(i, ValorAtividade.PedidoInsereDadosCapa.get());
	}

	private class CarregaProdutos extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			showDialog(DIALOG_CARREGA_PRODS);
		}

		@Override
		protected String doInBackground(String... params) {
			buscaImagens();
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			dismissDialog(DIALOG_CARREGA_PRODS);
			initiComponentes();
			handleComponentes();
			hideComponents();
			
			if (itens != null && itens.size() > 0) {
				preencheTela(itens.get(0));
			}
			
			detalhes.setVisibility(View.INVISIBLE);
			resumo.setVisibility(View.INVISIBLE);
			adapter = new GalleryProdutoAdapter(CatalogoActivity.this, itens);
			gallery.setAdapter(adapter);
			gallery.setVisibility(View.INVISIBLE);
			imgHideGallery.setVisibility(View.INVISIBLE);
			gallery.startAnimation(MaxAnimation.fadeOut(CatalogoActivity.this, 1500));
			imgShowGallery.startAnimation(MaxAnimation.blink(CatalogoActivity.this, 1000));
		}

	}

	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == DIALOG_CARREGA_PRODS) {
			progressDialog = new ProgressDialog(this);
			progressDialog.setMessage("Carregando Produtos");
			progressDialog.setCancelable(false);
			return progressDialog;
		}
		return super.onCreateDialog(id);
	}

	private void buscaImagens() {
		RepositorioProImagem repo = new RepositorioProImagem(this);
		itens = repo.listarParaGaleria(tabela);
		repo.fechar();
	}

	protected void initiComponentes() {
		imagem = (ImageView) findViewById(R.id.imagem_produto);
		up = (ImageView) findViewById(R.id.up);
		down = (ImageView) findViewById(R.id.down);
		left = (ImageView) findViewById(R.id.left);
		rigth = (ImageView) findViewById(R.id.rigth);
		info = (LinearLayout) findViewById(R.id.info);
		descricao = (TextView) findViewById(R.id.descricao);
		codigo = (TextView) findViewById(R.id.codigo);
		detalhes = (LinearLayout) findViewById(R.id.detalhes);

		detCodigo = (TextView) findViewById(R.id.detCodigo);
		detDescricao = (TextView) findViewById(R.id.detDescricao);
		padrao = (TextView) findViewById(R.id.padrao);
		tamanho = (TextView) findViewById(R.id.tamanho);
		preco = (TextView) findViewById(R.id.preco);
		btFechaPedido = (ImageView) findViewById(R.id.btFechaPedido);
		btShowSummary = (ImageView) findViewById(R.id.btShowSummary);
		resumo = (LinearLayout) findViewById(R.id.resumo);
		subtotal = (TextView) findViewById(R.id.subtotal);
		quantidade = (TextView) findViewById(R.id.quantidade);
		imgAdicionado = (ImageView) findViewById(R.id.imgadicionado);
		qtde = (TextView) findViewById(R.id.qtde);
		gallery = (Gallery) findViewById(R.id.gallery);
		imgShowGallery = (ImageView) findViewById(R.id.imgShowGallery);
		imgHideGallery = (ImageView) findViewById(R.id.imgHideGallery);
	}

	protected void handleComponentes() {
		detalhes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				detalhes.setVisibility(View.INVISIBLE);
				detalhes.startAnimation(MaxAnimation.slideOutRigth(CatalogoActivity.this, 500));
			}
		});

		btFechaPedido.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (itensDoPedido.size() > 0) {
					insereCapa();
				} else {
					Toast.makeText(CatalogoActivity.this, "Não há itens no pedido.", Toast.LENGTH_LONG)
							.show();
				}
			}
		});

		btShowSummary.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				resumo.startAnimation(MaxAnimation.slideLeft(CatalogoActivity.this, 500));
				resumo.setVisibility(View.VISIBLE);
				resumo.setVisibility(View.INVISIBLE);
				resumo.startAnimation(MaxAnimation.fadeOut(CatalogoActivity.this, 3500));
			}
		});

		resumo.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				resumo.setVisibility(View.INVISIBLE);
				resumo.startAnimation(MaxAnimation.slideOutRigth(CatalogoActivity.this, 500));
			}
		});

		imgShowGallery.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showGallery();
			}
		});

		imgHideGallery.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				hideGallery();
			}
		});

		gallery.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> lista, View view, int position, long id) {
				ItemGalleryProduto item = (ItemGalleryProduto) lista.getItemAtPosition(position);
				preencheTela(item);
				SELECTED = position;
			}
		});
	}

	protected void preencheTela(ItemGalleryProduto item) {
		drawables = item.getDrawable();
		if (drawables.size() > 0) {
			imagem.setImageDrawable(drawables.get(0));
		} else {
			imagem.setImageDrawable(this.getResources().getDrawable(R.drawable.vazio));
		}
		if (item.getItens().size() > 0) {
			imgAdicionado.setVisibility(View.VISIBLE);
		} else {
			imgAdicionado.setVisibility(View.INVISIBLE);
		}
		descricao.setText(item.getProduto().getDescricaoReduzida());
		codigo.setText(item.getProduto().getMascaraCodigo());
		detalhes.setVisibility(View.INVISIBLE);
	}

	protected void showComponentes() {
		info.setVisibility(View.VISIBLE);
		if (drawables.size() > 1 && IMGSELECTED < drawables.size() - 1) {
			down.setVisibility(View.VISIBLE);
		}
		if (IMGSELECTED > 0 && drawables.size() > 0) {
			up.setVisibility(View.VISIBLE);
		}
		if (SELECTED > 0) {
			left.setVisibility(View.VISIBLE);
		}
		if (SELECTED < itens.size() - 1) {
			rigth.setVisibility(View.VISIBLE);
		}
	}

	protected void hideComponents() {
		info.startAnimation(MaxAnimation.fadeOut(this));
		info.setVisibility(View.INVISIBLE);
		if (drawables.size() > 1 && IMGSELECTED < drawables.size() - 1) {
			down.startAnimation(MaxAnimation.fadeOut(this));
		}
		if (IMGSELECTED > 0) {
			up.startAnimation(MaxAnimation.fadeOut(this));
		}
		if (SELECTED > 0) {
			left.startAnimation(MaxAnimation.fadeOut(this));
		}
		if (SELECTED < itens.size() - 1) {
			rigth.startAnimation(MaxAnimation.fadeOut(this));
		}
		up.setVisibility(View.INVISIBLE);
		left.setVisibility(View.INVISIBLE);
		rigth.setVisibility(View.INVISIBLE);
		down.setVisibility(View.INVISIBLE);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		detector.onTouchEvent(event);
		showComponentes();
		hideComponents();
		return super.onTouchEvent(event);
	}

	class MyGestureListenerGallery extends GestureDetector.SimpleOnGestureListener {

		@Override
		public boolean onFling(MotionEvent event1, MotionEvent event2, float x, float y) {
			if (x > 0 && y > 0) {
				if (x > y) {
				} else {
					hideGallery();
				}
			} else if (x < 0 && y > 0) {
				if ((x * -1) > y) {
				} else {
					hideGallery();
				}
			}
			return true;
		}

	}

	class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

		@Override
		public boolean onFling(MotionEvent event1, MotionEvent event2, float x, float y) {
			if (x > 0 && y > 0) {
				if (x > y) {
					moveToLeft();
				} else {
					moveToUp();
				}
			} else if (x < 0 && y < 0) {
				if (x > y) {
					moveToDown();
				} else {
					moveToRigth();
				}
			} else if (x > 0 && y < 0) {
				if (x > (y * -1)) {
					moveToLeft();
				} else {
					moveToDown();
				}
			} else if (x < 0 && y > 0) {
				if ((x * -1) > y) {
					moveToRigth();
				} else {
					moveToUp();
				}
			}
			return true;
		}

		@Override
		public boolean onDoubleTap(MotionEvent e) {
			escolherPadroesTamanhos(itens.get(SELECTED));
			return super.onDoubleTap(e);
		}

		@Override
		public void onLongPress(MotionEvent e) {
			showDetails();
			super.onLongPress(e);
		}
	}

	protected void moveToRigth() {
		if (SELECTED + 1 < itens.size()) {
			SELECTED++;
			imagem.startAnimation(MaxAnimation.slideRigth(this));
			preencheTela(itens.get(SELECTED));
			IMGSELECTED = 0;
		} else {
			imagem.startAnimation(MaxAnimation.slideRigthTrusty(this));
			imagem.startAnimation(MaxAnimation.slideLeftTrusty(this));
		}
	}

	protected void moveToLeft() {
		if (SELECTED - 1 >= 0) {
			SELECTED--;
			imagem.startAnimation(MaxAnimation.slideLeft(this));
			preencheTela(itens.get(SELECTED));
			IMGSELECTED = 0;
		}
	}

	protected void moveToDown() {
		if (IMGSELECTED + 1 < itens.get(SELECTED).getDrawable().size()) {
			IMGSELECTED++;
			imagem.startAnimation(MaxAnimation.slideDown(this));
			Drawable drawable = itens.get(SELECTED).getDrawable().get(IMGSELECTED);
			imagem.setImageDrawable(drawable);

		}
	}

	protected void moveToUp() {
		if (IMGSELECTED > 0 && itens.get(SELECTED).getDrawable().size() > 0) {
			IMGSELECTED--;
			imagem.startAnimation(MaxAnimation.slideUp(this));
			Drawable drawable = itens.get(SELECTED).getDrawable().get(IMGSELECTED);
			imagem.setImageDrawable(drawable);
		}
	}

	protected void showDetails() {
		ItemGalleryProduto item = itens.get(SELECTED);
		detalhes.startAnimation(MaxAnimation.slideLeft(CatalogoActivity.this, 500));
		detalhes.setVisibility(View.VISIBLE);
		
		RepositorioTabelaDePrecoProdutoReduzido repoTabelaDePrecoProdutoReduzido = 
				new RepositorioTabelaDePrecoProdutoReduzido(this);
		List<TabelaPrecoProdutoReduzido> tabelaPrecoProdutoReduzidos = repoTabelaDePrecoProdutoReduzido.buscaPorProdutoTabela(empresa.getNumeroLoja(), tabela.getCodigo(), item.getProduto().getCodigo());
		repoTabelaDePrecoProdutoReduzido.fechar();
		
		StringBuilder sbPadrao = new StringBuilder();
		StringBuilder sbTam = new StringBuilder();
		StringBuilder sbPreco = new StringBuilder();
		StringBuilder sbQuantidade = new StringBuilder();
		for (TabelaPrecoProdutoReduzido tabPrecoProdReduzido : tabelaPrecoProdutoReduzidos) {
			sbPadrao.append(tabPrecoProdReduzido.getProdutoReduzido().getPadrao().getDescricao()).append("\n");
			sbTam.append(tabPrecoProdReduzido.getProdutoReduzido().getTamanho().getCodigo()).append("\n");
			sbQuantidade.append(item.getQuantidadePorReduzido(tabPrecoProdReduzido.getProdutoReduzido())).append("\n");
		}
		detCodigo.setText(item.getProduto().getMascaraCodigo());
		detDescricao.setText(item.getProduto().getDescricaoReduzida());
		padrao.setText(sbPadrao.toString());
		tamanho.setText(sbTam.toString());
		preco.setText(sbPreco.toString());
		if (item.getItens().size() > 0) {
			qtde.setText(sbQuantidade.toString());
		} else {
			qtde.setText("");
		}

		detalhes.setVisibility(View.INVISIBLE);
		detalhes.startAnimation(MaxAnimation.fadeOut(CatalogoActivity.this, 5000));

	}

	private void escolherPadroesTamanhos(ItemGalleryProduto item) {
		RepositorioTabelaDePrecoProdutoReduzido repo = new RepositorioTabelaDePrecoProdutoReduzido(this);
		List<TabelaPrecoProdutoReduzido> tabelaPrecoReduzidos = repo.buscaPorProdutoTabela(
				empresa.getNumeroLoja(), item.getProduto().getCodigo(), tabela.getCodigo());
		repo.fechar();
		ProdutoItemPedidoParaListar produtoEscolhido = ProdutoItemPedidoParaListar.newInstance(
				item.getProduto(), tabelaPrecoReduzidos);
		Intent i = new Intent(this, EscolhaPadraoTamanhoControle.class);
		Bundle b = new Bundle();
		List<PedidoItens> itens = itensDoPedido.get(item.getProduto());
		if (itens != null) {
			b.putSerializable("ITENSPEDIDO", (Serializable) itens);
		}
		b.putSerializable("PRODUTO", produtoEscolhido);
		i.putExtras(b);
		startActivityForResult(i, ValorAtividade.EscolhaPadraoTamanho.get());

	}

	protected int getQuantidade() {
		int quantidade = 0;
		for (Map.Entry<Produto, List<PedidoItens>> entry : itensDoPedido.entrySet()) {
			for (PedidoItens it : entry.getValue()) {
				quantidade += it.getQuantidade();
			}
		}
		return quantidade;
	}

	protected Dinheiro getValor() {
		BigDecimal valor = new BigDecimal(0);
		for (Map.Entry<Produto, List<PedidoItens>> entry : itensDoPedido.entrySet()) {
			for (PedidoItens it : entry.getValue()) {
				valor = valor.add(new BigDecimal(it.getValorTotal()));
			}
		}
		return Dinheiro.newInstance(valor);
	}

	public void hideGallery() {
		if (gallery.isShown()) {
			gallery.startAnimation(MaxAnimation.slideOutDown(this, 500));
			gallery.setVisibility(View.INVISIBLE);
			imgHideGallery.startAnimation(MaxAnimation.slideOutDown(this, 500));
			imgHideGallery.setVisibility(View.INVISIBLE);

			imgShowGallery.setVisibility(View.VISIBLE);
			btFechaPedido.setVisibility(View.VISIBLE);
			btShowSummary.setVisibility(View.VISIBLE);
			imgShowGallery.startAnimation(MaxAnimation.slideDown(this, 500));
			btFechaPedido.startAnimation(MaxAnimation.slideDown(this, 500));
			btShowSummary.startAnimation(MaxAnimation.slideDown(this, 500));

		}
	}

	public void showGallery() {
		if (!gallery.isShown()) {
			gallery.startAnimation(MaxAnimation.slideDown(this, 500));
			gallery.setVisibility(View.VISIBLE);
			imgHideGallery.startAnimation(MaxAnimation.slideDown(this, 500));
			imgHideGallery.setVisibility(View.VISIBLE);

			imgShowGallery.setVisibility(View.INVISIBLE);
			btFechaPedido.setVisibility(View.INVISIBLE);
			btShowSummary.setVisibility(View.INVISIBLE);
			resumo.setVisibility(View.INVISIBLE);
		}
	}

}
