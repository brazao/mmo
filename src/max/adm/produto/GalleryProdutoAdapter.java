package max.adm.produto;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;

import max.adm.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.common.collect.Lists;

public class GalleryProdutoAdapter extends BaseAdapter {

	protected List<ItemGalleryProduto> itens = Lists.newLinkedList();
	protected Context ctx;

	public GalleryProdutoAdapter(Context ctx, List<ItemGalleryProduto> itens) {
		this.itens = itens;
		this.ctx = ctx;
	}

	@Override
	public int getCount() {
		return itens.size();
	}

	@Override
	public Object getItem(int position) {
		return itens.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ItemGalleryProduto item = itens.get(position);
		final Holder holder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.modelo_item_gallery_produto, null);

			holder = new Holder();
			holder.mascara = (TextView) convertView.findViewById(R.id.mascara_produto);
			holder.descricao = (TextView) convertView.findViewById(R.id.descricao_produto);
			holder.imagem = (LinearLayout) convertView.findViewById(R.id.imagem_produto);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		
		ImageView iv = new ImageView(ctx);
		holder.imagem.removeAllViews();
		holder.imagem.addView(iv);
		
		String path = String.format("%s/%s/%d/mini/%dmini.jpg", 
				Environment.getExternalStorageDirectory().getPath(), 
				"maxMobile/produtos/",
				item.getImagemProduto().getProduto().getCodigo() , 
				item.getImagemProduto().getProduto().getCodigo());
		
		Log.i("path adapter", path);
		
		File f = new File(path);
		Drawable drw;
		try {
			if (f.exists()) {
				Bitmap b = BitmapFactory.decodeFile(f.getAbsolutePath());
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				b.compress(Bitmap.CompressFormat.JPEG, 100, stream);
				item.getImagemProduto().setImagem(stream.toByteArray());
				drw = new BitmapDrawable(item.getImagemProduto().getBitmap());
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(60, 80);
				iv.setImageDrawable(drw);
				iv.setLayoutParams(params);
				stream.close();
			} else {
				drw = ctx.getResources().getDrawable(R.drawable.vazio);
				iv.setImageDrawable(drw);
			}
			
		} catch (Throwable e) {
			Log.e("IMAGEM", e.getMessage());
		}
		
		

		holder.mascara.setText(item.getProduto().getMascaraCodigo());
		holder.descricao.setText(item.getProduto().getDescricaoReduzida());
		return convertView;
	}

	static class Holder {
		TextView mascara;
		TextView descricao;
		LinearLayout imagem;
	}

	

}
