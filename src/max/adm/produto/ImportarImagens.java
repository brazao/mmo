package max.adm.produto;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;
import java.lang.reflect.Type;
import java.util.List;

import max.adm.Servico;
import max.adm.entidade.ImagemProduto;
import max.adm.repositorio.RepositorioProImagem;
import max.adm.repositorio.RepositorioProduto;
import max.adm.service.ConexaoHttp;
import max.adm.utilidades.Notificacao;
import max.adm.utilidades.NotificacaoFactory;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;

import com.google.common.reflect.TypeToken;

public class ImportarImagens {

	private Context ctx;
	private String url;

	private Notificacao notificacao;

	private ImportarImagens(Context ctx, String url) {
		this.ctx = ctx;
		this.url = url;
	}

	public static ImportarImagens newInstance(Context ctx, String url) {
		checkNotNull(ctx);
		checkNotNull(url);
		return new ImportarImagens(ctx, url);
	}

	public boolean execute() {
		notifica();
		try {
			RepositorioProImagem repoImg = new RepositorioProImagem(ctx);
			repoImg.deletar(null, null);
			repoImg.fechar();
			
			RepositorioProduto repo = new RepositorioProduto(ctx);
			List<Integer> codigos = repo.buscaCodigos();
			repo.fechar();
			int numErros = 0;
			
			File f = new File(String.format("%s/%s", Environment.getExternalStorageDirectory().getPath(), "maxMobile/produtos"));
			deleteImagens(f);
			for (Integer i : codigos) {
				try{
					String cod = String.valueOf(i);
					ConexaoHttp conexao = ConexaoHttp.newInstance(url + Servico.buscaImagens.get());
					Type tipo = new TypeToken<List<ImagemProduto>>() {
					}.getType();
					List<ImagemProduto> imgs = conexao.executaRequisicao(cod, ImagemProduto.class, tipo);
					RepositorioProImagem repositorioImagem = new RepositorioProImagem(ctx);
					repositorioImagem.salvarLista(imgs);
					repositorioImagem.fechar();
				}
				catch(Exception e){
					e.printStackTrace();
					numErros++;
					if(numErros>30){
						notificaFalha();
						return false;
					}
				}
			}
			notificaSucesso();
			return true;
		} 
		catch (Exception e) {
			e.printStackTrace();
			notificaFalha();
			return false;
		}

	}
	
	private void deleteImagens(File f) {
		if(f.isDirectory()){
			for(File child : f.listFiles()) {
				deleteImagens(child);
			}
		}
		f.delete();
	}

	private void notifica() {
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoInfo(this.ctx, "Imagens", "Importando imagens", new Intent());
	}

	private void notificaFalha() {
		notificacao.cancelaNotificacao();
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoFalha(this.ctx, "Imagens", "Falha ao importar", new Intent());
	}

	private void notificaSucesso() {
		notificacao.cancelaNotificacao();
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoSucesso(this.ctx, "Imagens", "importação concluida", new Intent());
	}

}
