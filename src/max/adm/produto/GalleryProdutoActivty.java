package max.adm.produto;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import max.adm.Dinheiro;
import max.adm.R;
import max.adm.ValorAtividade;
import max.adm.controle.EscolhaPadraoTamanhoControle;
import max.adm.entidade.Empresa;
import max.adm.entidade.ImagemProduto;
import max.adm.entidade.Parametro;
import max.adm.entidade.PedidoCapa;
import max.adm.entidade.PedidoItens;
import max.adm.entidade.Produto;
import max.adm.entidade.TabelaDePreco;
import max.adm.entidade.TabelaPrecoProdutoReduzido;
import max.adm.pedido.PedidoInsereDadosCapa;
import max.adm.repositorio.RepositorioProImagem;
import max.adm.repositorio.RepositorioTabelaDePrecoProdutoReduzido;
import max.adm.utilidades.adapters.ProdutoItemPedidoParaListar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class GalleryProdutoActivty extends Activity {

	private static int DIALOG_CARREGA_PRODS = 999;
	private ProgressDialog progressDialog;

	private List<ItemGalleryProduto> itens = Lists.newLinkedList();
	private TabelaDePreco tabela;
	ItemGalleryProduto itemSelecionado;

	private GalleryProdutoAdapter adapter;
	private GalleryProdutoAutoCompleteAdapter autoCompleteAdapter;
	private Gallery gallery;
	private TextView textMascaraProduto;
	private TextView textDescricarProduto;
	private TextView textPadrao;
	private TextView textTamanho;
	private TextView textPreco;
	private ImageView imagem;
	private AutoCompleteTextView autocomplete;
	private Button btAdd;
	private Button btDetalhes;
	private LinearLayout layoutImgs;
	private TextView quantidade;
	private TextView subTotal;
	private LinearLayout layoutSubTotal;
	private ImageView btMostraValor;
	private ImageView btCancel;
	private ImageView btOk;
	private Empresa empresa;
	private Parametro parametro;

	private Map<Produto, List<PedidoItens>> itensDoPedido = Maps.newHashMap();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gallery_produto);
		tabela = (TabelaDePreco) getIntent().getExtras().getSerializable("TABELAPRECO");
		empresa = (Empresa) getIntent().getExtras().getSerializable("EMPRESA");
		parametro = (Parametro) getIntent().getExtras().getSerializable("PARAMETRO");
		new CarregaProdutos().execute("");
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(autocomplete.getWindowToken(), 0);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		if (data != null && requestCode == ValorAtividade.EscolhaPadraoTamanho.get()) {
			List<PedidoItens> itensPedido = (List<PedidoItens>) data.getExtras().getSerializable("ITENSPEDIDO");
			itemSelecionado.setItens(itensPedido);
			itensDoPedido.put(itemSelecionado.getProduto(), itemSelecionado.getItens());
			removeQuantidadeZero();
			atualizaTela(itemSelecionado);
		}

		if (requestCode == ValorAtividade.PedidoInsereDadosCapa.get() && data != null) {
			String sair = data.getStringExtra("SAIR");
			if ("SIM".equals(sair)) {
				finish();
			}
		}

	}

	private void removeQuantidadeZero() {
		Iterator<Produto> iterator = itensDoPedido.keySet().iterator();
		while (iterator.hasNext()) {
			Produto key = iterator.next();
			Iterator<PedidoItens> it2 = itensDoPedido.get(key).iterator();
			while (it2.hasNext()) {
				PedidoItens item = it2.next();
				if (item.getQuantidade() == 0) {
					it2.remove();
				}
			}

			if (itensDoPedido.get(key).isEmpty()) {
				iterator.remove();
			}
		}
	}

	private int getQuantidade() {
		int quantidade = 0;
		for (Map.Entry<Produto, List<PedidoItens>> entry : itensDoPedido.entrySet()) {
			for (PedidoItens it : entry.getValue()) {
				quantidade += it.getQuantidade();
			}
		}
		return quantidade;
	}

	private Dinheiro getValor() {
		BigDecimal valor = new BigDecimal(0);
		for (Map.Entry<Produto, List<PedidoItens>> entry : itensDoPedido.entrySet()) {
			for (PedidoItens it : entry.getValue()) {
				valor = valor.add(new BigDecimal(it.getValorTotal()));
			}
		}
		return Dinheiro.newInstance(valor);
	}

	private void buscaImagens() {
		RepositorioProImagem repo = new RepositorioProImagem(this);
		itens = repo.listarParaGaleria(tabela);
		repo.fechar();
	}

	private void montaTela() {
		gallery = (Gallery) findViewById(R.id.gallery);
		textDescricarProduto = (TextView) findViewById(R.id.text_descricao_produto);
		textMascaraProduto = (TextView) findViewById(R.id.text_mascara_produto);
		imagem = (ImageView) findViewById(R.id.selected_imageview);
		textPadrao = (TextView) findViewById(R.id.text_padrao);
		textTamanho = (TextView) findViewById(R.id.text_tamanho);
		textPreco = (TextView) findViewById(R.id.text_preco);
		autocomplete = (AutoCompleteTextView) findViewById(R.id.autoCompleteProduto);
		btAdd = (Button) findViewById(R.id.btAdd);
		layoutImgs = (LinearLayout) findViewById(R.id.layoutMiniImages);
		btDetalhes = (Button) findViewById(R.id.btDetalhes);
		quantidade = (TextView) findViewById(R.id.totalpecas);
		subTotal = (TextView) findViewById(R.id.subtotal);
		layoutSubTotal = (LinearLayout) findViewById(R.id.layoutsubtotal);
		btMostraValor = (ImageView) findViewById(R.id.btmostraValor);
		btCancel = (ImageView) findViewById(R.id.btCancel);
		btOk = (ImageView) findViewById(R.id.btOk);
	}

	private void preencheTela() {
		adapter = new GalleryProdutoAdapter(this, itens);
		autoCompleteAdapter = new GalleryProdutoAutoCompleteAdapter(this, Lists.newLinkedList(itens));
		gallery.setAdapter(adapter);
		autocomplete.setAdapter(autoCompleteAdapter);
	}

	private void defineFuncoes() {
		gallery.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> lista, View view, int position, long id) {
				ItemGalleryProduto item = (ItemGalleryProduto) lista.getItemAtPosition(position);
				itemSelecionado = item;
				atualizaTela(item);
			}
		});

		autocomplete.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> lista, View view, int position, long id) {
				ItemGalleryProduto selected = (ItemGalleryProduto) lista.getItemAtPosition(position);
				System.out.println(selected);
				for (int i = 0; i < itens.size(); i++) {
					ItemGalleryProduto it = itens.get(i);
					if (selected.equals(it)) {
						gallery.setSelection(i);
						itemSelecionado = it;
						atualizaTela(it);
						break;
					}
				}
				autocomplete.getText().clear();
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(autocomplete.getWindowToken(), 0);
				getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			}
		});

		btAdd.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				addItem();
			}
		});

		btMostraValor.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (layoutSubTotal.getVisibility() == View.VISIBLE) {
					layoutSubTotal.setVisibility(View.INVISIBLE);
					btMostraValor.setImageResource(R.drawable.arrow_right_disabled);
				} else {
					layoutSubTotal.setVisibility(View.VISIBLE);
					btMostraValor.setImageResource(R.drawable.arrow_left_disabled);
				}

			}
		});

		btCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				cancelar();
			}
		});

		btOk.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				insereCapa();
			}
		});

		btDetalhes.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mostraDetalhesDoItem(itemSelecionado);
			}
		});

	}

	private void mostraDetalhesDoItem(ItemGalleryProduto item) {
		BigDecimal valor = new BigDecimal(0);
		StringBuilder sb = new StringBuilder();
		sb.append(item.getProduto().getDescricao()).append("\n\n");
		for (PedidoItens it : item.getItens()) {
			sb.append(String.format("%s %s %d unidade(s)\n", it.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getPadrao().getDescricao(), it.getTabelaPrecoProdutoReduzido().getProdutoReduzido().getTamanho().getCodigo(), it.getQuantidade()));
			valor = valor.add(new BigDecimal(it.getValorTotal()));
		}
		sb.append(String.format("\nTotal de peças: %d\n", item.getQuantidade()));
		sb.append(String.format("Valor: %s", Dinheiro.newInstance(valor).formatToScreen()));

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(String.format("%s", item.getProduto().getMascaraCodigo()));
		builder.setMessage(sb.toString());
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();

			}
		});
		builder.show();
	}

	private void atualizaTela(ItemGalleryProduto item) {
		Log.d("deb", "atualizando tela");
		layoutImgs.removeAllViews();
		Drawable drw = this.getResources().getDrawable(R.drawable.vazio);
		imagem.setImageDrawable(drw);
		if (item.getImagemProduto() != null) {
			try {
				String path = String.format("%s/%s/%d/normal", Environment.getExternalStorageDirectory().getPath(), "maxMobile/produtos", item.getImagemProduto().getImagem());
				Log.d("teste", path);
				File f = new File(path);

				if (f.isDirectory()) {
					File[] listFiles = f.listFiles();
					if (listFiles != null && listFiles.length > 0) {
						layoutImgs.removeAllViews();
						for (File file : listFiles) {
							addMiniImg(file);
						}
						Bitmap b = BitmapFactory.decodeFile(listFiles[0].getAbsolutePath());
						ByteArrayOutputStream stream = new ByteArrayOutputStream();
						b.compress(Bitmap.CompressFormat.JPEG, 100, stream);
						byte[] byteArray = stream.toByteArray();
						Bitmap bitmap = ImagemProduto.getBitmap(byteArray);
						drw = new BitmapDrawable(bitmap);
						imagem.setImageDrawable(drw);
					}
				}

			} catch (Throwable e) {
				Log.i(GalleryProdutoActivty.class.getName(), "Erro ao montar a imagem");
			}
		}

		RepositorioTabelaDePrecoProdutoReduzido repo = new RepositorioTabelaDePrecoProdutoReduzido(this);
		List<TabelaPrecoProdutoReduzido> tabPrecoProdReduzidos = repo.buscaPorProdutoTabela(empresa.getNumeroLoja(), tabela.getCodigo(), item.getProduto().getCodigo());
		repo.fechar();

		StringBuilder sbPadrao = new StringBuilder();
		StringBuilder sbTam = new StringBuilder();
		StringBuilder sbPreco = new StringBuilder();
		for (TabelaPrecoProdutoReduzido tabPrecoProdReduzido : tabPrecoProdReduzidos) {
			sbPadrao.append(tabPrecoProdReduzido.getProdutoReduzido().getPadrao().getDescricao()).append("\n");
			sbTam.append(tabPrecoProdReduzido.getProdutoReduzido().getTamanho().getCodigo()).append("\n");
		}
		textMascaraProduto.setText(item.getProduto().getMascaraCodigo());
		textDescricarProduto.setText(item.getProduto().getDescricao());
		textPadrao.setText(sbPadrao.toString());
		textTamanho.setText(sbTam.toString());
		textPreco.setText(sbPreco.toString());
		if (item.getQuantidade() > 0) {
			btDetalhes.setVisibility(View.VISIBLE);
			btDetalhes.setText(String.format("%d Peças", item.getQuantidade()));
			btAdd.setText("Alterar");
		} else {
			btDetalhes.setVisibility(View.INVISIBLE);
			btAdd.setText("Adicionar");
		}

		subTotal.setText(getValor().formatToScreen());
		quantidade.setText(String.valueOf(getQuantidade()));
	}
	private void addMiniImg(File img) {
		Bitmap b = BitmapFactory.decodeFile(img.getAbsolutePath());
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		b.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		byte[] byteArray = stream.toByteArray();
		Bitmap bitmap = ImagemProduto.getBitmap(byteArray);
		Drawable drw = new BitmapDrawable(bitmap);
		final ImageView view = new ImageView(this);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(80, 100);
		view.setLayoutParams(params);
		view.setImageDrawable(drw);
		layoutImgs.addView(view);

		view.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				imagem.setImageDrawable(view.getDrawable());

			}
		});
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == DIALOG_CARREGA_PRODS) {
			progressDialog = new ProgressDialog(GalleryProdutoActivty.this);
			progressDialog.setMessage("Carregando Produtos");
			progressDialog.setCancelable(false);
			return progressDialog;
		}
		return super.onCreateDialog(id);
	}

	private void escolherPadroesTamanhos(ItemGalleryProduto item) {
		RepositorioTabelaDePrecoProdutoReduzido repo = new RepositorioTabelaDePrecoProdutoReduzido(this);
		List<TabelaPrecoProdutoReduzido> tabelaPrecoReduzidos = 
				repo.buscaPorProdutoTabela(empresa.getNumeroLoja(),
						item.getProduto().getCodigo(), 
						tabela.getCodigo());
		repo.fechar();
		ProdutoItemPedidoParaListar produtoEscolhido = ProdutoItemPedidoParaListar.newInstance(item.getProduto(), tabelaPrecoReduzidos);
		Intent i = new Intent(this, EscolhaPadraoTamanhoControle.class);
		Bundle b = new Bundle();
		b.putSerializable("PRODUTO", produtoEscolhido);
		i.putExtras(b);
		startActivityForResult(i, ValorAtividade.EscolhaPadraoTamanho.get());

	}

	private void addItem() {
		if (itemSelecionado != null) {
			escolherPadroesTamanhos(itemSelecionado);
		}
	}

	private void cancelar() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Cancelar Pedido");
		builder.setMessage("Deseja cancelar a inserção do pedido e sair?");
		builder.setPositiveButton("Cancelar e sair", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
		builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		builder.show();
	}

	private void insereCapa() {
		PedidoCapa.Builder builder = PedidoCapa.builder();
		builder.comTabelaDePreco(tabela);
		for (Map.Entry<Produto, List<PedidoItens>> entry : itensDoPedido.entrySet()) {
			for (PedidoItens it : entry.getValue()) {
				builder.addItem(it);
			}
		}

		List<PedidoCapa.Builder> builders = Lists.newLinkedList();
		builders.add(builder);

		Bundle b = new Bundle();
		b.putSerializable("EMPRESA", empresa);
		b.putSerializable("PARAMETRO", parametro);
		b.putSerializable("BUILDERS", (Serializable) builders);
		Intent i = new Intent(this, PedidoInsereDadosCapa.class);
		i.putExtras(b);
		startActivityForResult(i, ValorAtividade.PedidoInsereDadosCapa.get());
	}

	private class CarregaProdutos extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			showDialog(DIALOG_CARREGA_PRODS);
		}

		@Override
		protected String doInBackground(String... params) {
			buscaImagens();
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			dismissDialog(DIALOG_CARREGA_PRODS);
			montaTela();
			preencheTela();
			defineFuncoes();
			if (itens != null && itens.size() > 0) {
				itemSelecionado = itens.get(0);
				atualizaTela(itemSelecionado);
			}
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(autocomplete.getWindowToken(), 0);
			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			layoutSubTotal.setVisibility(View.INVISIBLE);

		}

	}

}
