package max.adm;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import max.adm.utilidades.MascaraDecimal;

public class Dinheiro implements Serializable {

	private static final long serialVersionUID = 1L;

	private final BigDecimal valor;

	private static Dinheiro ZERO = new Dinheiro(new BigDecimal("0.00"));

	private Dinheiro(BigDecimal valor) {
		this.valor = valor;
	}

	public static Dinheiro newInstance(BigDecimal valor) {
		checkNotNull(valor, "dinheiro não pode ter valor nulo");
		if (valor.setScale(2, RoundingMode.HALF_EVEN).equals(new BigDecimal("0.00"))) {
			return ZERO;
		}
		return new Dinheiro(valor);
	}

	public BigDecimal getValor() {
		return valor;
	}

	public String formatToScreen() {
		return "R$ " + MascaraDecimal.comDuasCasaDecimais(this.valor);
	}

	public boolean isZero() {
		return this == ZERO;
	}

}
