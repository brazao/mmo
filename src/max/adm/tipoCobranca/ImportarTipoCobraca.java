package max.adm.tipoCobranca;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import com.google.common.reflect.TypeToken;

import max.adm.Servico;
import max.adm.entidade.TipoCobranca;
import max.adm.service.ConexaoHttp;
import max.adm.utilidades.Notificacao;
import max.adm.utilidades.NotificacaoFactory;
import android.content.Context;
import android.content.Intent;

public class ImportarTipoCobraca {
	
	private Context ctx;
	private String chave;
	private String url;
	
	private Notificacao notificacao;
	
	private ImportarTipoCobraca(Context ctx, String chave, String url) {
		this.ctx = ctx;
		this.chave = chave;
		this.url = url;
	}
	
	public static ImportarTipoCobraca newInstance(Context ctx, String chave, String url){
		checkNotNull(ctx);
		checkNotNull(chave);
		checkNotNull(url);
		return new ImportarTipoCobraca(ctx, chave, url);
	}
	
	public boolean execute(){
		notifica();
		try{
			ConexaoHttp conexao = ConexaoHttp.newInstance(url+Servico.buscaTipoCobrancas.get());
			List<TipoCobranca> tiposCobranca = conexao.executaRequisicao(chave, TipoCobranca.class, new TypeToken<List<TipoCobranca>>() {}.getType());
			RepositorioTipoCobranca repo = new RepositorioTipoCobranca(ctx);
			repo.deletar(null, null);
			repo.salvarLista(tiposCobranca);
			repo.fechar();
			notificaSucesso();
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			notificaFalha();
			return false;
		}
	}
	
	private void notifica(){
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoInfo(this.ctx, "Tipos de Cobrança", "Importando Tipos de Cobrança", new Intent());
	}
	
	private void notificaFalha(){
		notificacao.cancelaNotificacao();
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoFalha(this.ctx, "Tipos de Cobrança", "Falha ao importar", new Intent());
	}

	private void notificaSucesso(){
		notificacao.cancelaNotificacao();
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoSucesso(this.ctx, "Tipos de Cobrança", "importação concluida", new Intent());
	}

}
