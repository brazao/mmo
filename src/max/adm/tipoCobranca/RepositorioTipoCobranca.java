package max.adm.tipoCobranca;

import java.util.ArrayList;
import java.util.List;

import max.adm.database.DBHelper;
import max.adm.entidade.TipoCobranca;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RepositorioTipoCobranca {

	private static final String NOME_TABELA = TipoCobranca.NOME_TABELA;

	protected SQLiteDatabase db;

	public RepositorioTipoCobranca(Context context) {
		db = new DBHelper(context).getWritableDatabase();
	}

	public int salvar(TipoCobranca tipoCobranca) {
		int id = tipoCobranca.getCodigo();
		if (id != 0) {
			atualizar(tipoCobranca);
		} else {
			id = inserir(tipoCobranca);
		}

		return id;
	}

	public int inserir(TipoCobranca tipoCobranca) {
		ContentValues values = new ContentValues();
		values.put(TipoCobranca.CODIGO, tipoCobranca.getCodigo());
		values.put(TipoCobranca.DESCRICAO, tipoCobranca.getDescricao());

		int id = inserir(values);

		return id;
	}

	public int inserir(ContentValues values) {
		int id = (int) db.insert(TipoCobranca.NOME_TABELA, "", values);
		return id;
	}

	public int atualizar(TipoCobranca tipoCobranca) {
		ContentValues values = new ContentValues();
		values.put(TipoCobranca.CODIGO, tipoCobranca.getCodigo());
		values.put(TipoCobranca.DESCRICAO, tipoCobranca.getDescricao());

		String _id = String.valueOf(tipoCobranca.getCodigo());
		String where = TipoCobranca.CODIGO + "=?";
		String[] whereArgs = new String[] { _id };
		int count = atualizar(values, where, whereArgs);

		return count;
	}

	public int atualizar(ContentValues valores, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, valores, where, whereArgs);

		return count;
	}

	public int deletar(int id) {
		String where = TipoCobranca.CODIGO + "=?";
		String _id = String.valueOf(id);
		String[] whereArgs = new String[] { _id };
		int count = deletar(where, whereArgs);
		return count;
	}

	public int deletar(String where, String[] whereArgs) {
		int count = db.delete(NOME_TABELA, where, whereArgs);
		return count;
	}

	public TipoCobranca busca(int id) {
		Cursor c = db.query(true, NOME_TABELA, TipoCobranca.COLUNAS,
				TipoCobranca.CODIGO + "=" + id, null, null, null, null, null);

		if (c.getCount() > 0) {
			// pocisiona no primeiro elemento
			c.moveToFirst();
			TipoCobranca tipoCobranca = new TipoCobranca();
			tipoCobranca.setCodigo(c.getInt(c.getColumnIndex(TipoCobranca.CODIGO)));
			tipoCobranca.setDescricao(c.getString(c.getColumnIndex(TipoCobranca.DESCRICAO)));
			c.close();
			return tipoCobranca;

		}
		c.close();
		return null;
	}

	// retorna cursor com todos os TipoCobrancaes
	public Cursor getCursor() {
		try {
			return db.query(NOME_TABELA, TipoCobranca.COLUNAS, null, null, null, null, null);
		} catch (SQLException e) {
			Log.e("TipoCobranca", "Erro ao fazer busca: " + e.toString());
			return null;
		}

	}

	public List<TipoCobranca> listar() {
		Cursor c = getCursor();
		List<TipoCobranca> tipoCobrancas = new ArrayList<TipoCobranca>();
	
		if (c.moveToFirst()) {
			int indexId = c.getColumnIndex(TipoCobranca.CODIGO);
			int indexDescricao = c.getColumnIndex(TipoCobranca.DESCRICAO);
			
			do {
				TipoCobranca tipoCobranca = new TipoCobranca();
				tipoCobrancas.add(tipoCobranca);

				tipoCobranca.setCodigo(c.getInt(c.getColumnIndex(TipoCobranca.CODIGO)));
				tipoCobranca.setDescricao(c.getString(indexDescricao));
				
			} while (c.moveToNext());
		}
		c.close();
		return tipoCobrancas;
	}

	public void fechar() {
		if (db != null) {
			db.close();
		}
	}

	private ContentValues geraValues(TipoCobranca tipoCobranca){
		ContentValues values = new ContentValues();
		values.put(TipoCobranca.CODIGO, tipoCobranca.getCodigo());
		values.put(TipoCobranca.DESCRICAO, tipoCobranca.getDescricao());
		
		return values;
	}

	public boolean salvarLista(List<TipoCobranca> tiposCobrancas) {
		for (TipoCobranca tpCob : tiposCobrancas){
			try{
				ContentValues values = this.geraValues(tpCob);
				this.inserir(values);
			}
			catch (Exception e) {
				// tpCob j� existe;
			}
		}
			
		return true;
	}

}
