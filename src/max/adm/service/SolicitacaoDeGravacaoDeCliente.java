package max.adm.service;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.util.List;

import max.adm.entidade.Empresa;
import max.adm.entidade.Pessoa;

public class SolicitacaoDeGravacaoDeCliente implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<Pessoa> pessoas;
	private Integer representanteId;
	private Empresa empresa;

	private SolicitacaoDeGravacaoDeCliente(List<Pessoa> pessoas, Integer representanteId, Empresa empresa) {
		this.pessoas = pessoas;
		this.representanteId = representanteId;
		this.empresa = empresa;
	}

	public static SolicitacaoDeGravacaoDeCliente newInstance(List<Pessoa> pessoas, Integer representanteId, Empresa empresa) {
		checkNotNull(pessoas);
		checkNotNull(representanteId);
		checkNotNull(empresa);
		return new SolicitacaoDeGravacaoDeCliente(pessoas, representanteId, empresa);
	}

	public List<Pessoa> getPessoas() {
		return pessoas;
	}

	public void setPessoas(List<Pessoa> pessoas) {
		this.pessoas = pessoas;
	}

	public Integer getRepresentanteId() {
		return representanteId;
	}

	public void setChave(Integer representanteId) {
		this.representanteId = representanteId;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((representanteId == null) ? 0 : representanteId.hashCode());
		result = prime * result + ((empresa == null) ? 0 : empresa.hashCode());
		result = prime * result + ((pessoas == null) ? 0 : pessoas.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SolicitacaoDeGravacaoDeCliente other = (SolicitacaoDeGravacaoDeCliente) obj;
		if (representanteId == null) {
			if (other.representanteId != null)
				return false;
		} else if (!representanteId.equals(other.representanteId))
			return false;
		if (empresa == null) {
			if (other.empresa != null)
				return false;
		} else if (!empresa.equals(other.empresa))
			return false;
		if (pessoas == null) {
			if (other.pessoas != null)
				return false;
		} else if (!pessoas.equals(other.pessoas))
			return false;
		return true;
	}

}
