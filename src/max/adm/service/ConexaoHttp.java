package max.adm.service;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import max.adm.utilidades.Comprimir;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConexaoHttp {

	private String url;

	private ConexaoHttp(String url) {
		this.url = url;
	}

	public static ConexaoHttp newInstance(String url) {
		System.out.println(url);
		checkNotNull(url);
		return new ConexaoHttp(url);
	}

	public HttpPost criaReqisicao(String url) {
		try {
			HttpPost request = new HttpPost();
			request.setURI(new URI(url));
			return request;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public String enviaDados(HttpPost request) {

		try {
			HttpClient client = HttpClientFactory.getHttpClient();
			HttpResponse response = client.execute(request);
			//System.out.println(EntityUtils.toString(response.getEntity()));
			byte[] resposta = EntityUtils.toByteArray(response.getEntity());
			String resultado = Comprimir.byteArrayToObject(resposta, String.class);
			return resultado;

		} catch (Exception e) {
			e.printStackTrace();
			return "falha";
		}

	}
	
	public byte[] recebeDados(HttpEntity httpEntity) throws ClientProtocolException, IOException {
		
		HttpClient client = HttpClientFactory.getHttpClient();
		HttpPost httpPost = new HttpPost(url);
		httpPost.setEntity(httpEntity);
		
		HttpResponse response = client.execute(httpPost);
		
		byte[] resultado = EntityUtils.toByteArray(response.getEntity());
		
		return resultado;
	}

	public static boolean isConnected(Context context) {
		ConnectivityManager conn = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo net = conn.getActiveNetworkInfo();
		return (net != null && net.isConnected());
	}

	/**
	 * envia chave(String) e espera lista de objetos
	 */
	public <T> List<T> executaRequisicao(String param, Class<T> tipoListaRetorno, Type tipo) throws URISyntaxException, ClientProtocolException, IOException {
		HttpPost requisicao = new HttpPost();
		requisicao.setURI(new URI(url));
		HttpEntity entity = new StringEntity(param);
		requisicao.setEntity(entity);
		HttpClient client = HttpClientFactory.getHttpClient();
		HttpResponse response = client.execute(requisicao);
		byte[] resposta = EntityUtils.toByteArray(response.getEntity());
		return Comprimir.byteArrayToList(resposta, tipoListaRetorno, tipo);
	}

	/**
	 * apenas espera um objeto de retorno
	 */
	public <T> List<T> executaRequisicao(Class<T> tipoListaRetorno, Type tipo) throws URISyntaxException, ClientProtocolException, IOException {
		HttpPost requisicao = new HttpPost();
		requisicao.setURI(new URI(url));
		HttpClient client = HttpClientFactory.getHttpClient();
		HttpResponse response = client.execute(requisicao);
		byte[] resposta = EntityUtils.toByteArray(response.getEntity());
		return Comprimir.byteArrayToList(resposta, tipoListaRetorno, tipo);
	}

	/**
	 * envia objeto e espera uma lista de objetos
	 */
	public <T> List<T> executaRequisicao(byte[] param, Class<T> tipoListaRetorno, Type tipo) throws URISyntaxException, ClientProtocolException, IOException {
		HttpPost requisicao = new HttpPost();
		requisicao.setURI(new URI(url));
		ByteArrayEntity entity = new ByteArrayEntity(param);
		requisicao.setEntity(entity);
		HttpClient client = HttpClientFactory.getHttpClient();
		HttpResponse response = client.execute(requisicao);
		byte[] resposta = EntityUtils.toByteArray(response.getEntity());
		return Comprimir.byteArrayToList(resposta, tipoListaRetorno, tipo);
	}

	/**
	 * envia objeto ou lista e espera um objeto
	 */
	public <T> T executaRequisicaoObj(byte[] paramArray, Class<T> tipoRetorno) throws URISyntaxException, ClientProtocolException, IOException {
		HttpPost requisicao = new HttpPost();
		requisicao.setURI(new URI(url));
		ByteArrayEntity entity = new ByteArrayEntity(paramArray);
		requisicao.setEntity(entity);
		HttpClient client = HttpClientFactory.getHttpClient();
		HttpResponse response = client.execute(requisicao);
		byte[] resposta = EntityUtils.toByteArray(response.getEntity());
		return Comprimir.byteArrayToObject(resposta, tipoRetorno);
	}

	/**
	 * envia chave(String) e espera objeto
	 */
	public <T> T executaRequisicaoObj(String param, Class<T> tipoRetorno) throws URISyntaxException, ClientProtocolException, IOException {
		HttpPost requisicao = new HttpPost();
		requisicao.setURI(new URI(url));
		HttpEntity entity = new StringEntity(param);
		requisicao.setEntity(entity);
		HttpClient client = HttpClientFactory.getHttpClient();
		HttpResponse response = client.execute(requisicao);
		byte[] resposta = EntityUtils.toByteArray(response.getEntity());
		return Comprimir.byteArrayToObject(resposta, tipoRetorno);
	}
	
	
	/**
	 * 
	 * @param Uma String
	 * @return Uma string
	 */
	public String executaRequisicao(String str) throws URISyntaxException, ClientProtocolException, IOException {
		HttpPost requisicao = new HttpPost();
		requisicao.setURI(new URI(url));
		System.out.println(str);
		HttpEntity entity = new StringEntity(str);
		requisicao.setEntity(entity);
		HttpClient client = HttpClientFactory.getHttpClient();
		HttpResponse response = client.execute(requisicao);
		String resposta = EntityUtils.toString(response.getEntity());
		return resposta;
	}

}
