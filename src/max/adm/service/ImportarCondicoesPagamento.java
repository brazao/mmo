package max.adm.service;

import static com.google.common.base.Preconditions.checkNotNull;

import java.lang.reflect.Type;
import java.util.List;

import com.google.common.reflect.TypeToken;

import max.adm.Servico;
import max.adm.entidade.CondicaoPagamento;
import max.adm.repositorio.RepositorioCondicaoPagamento;
import max.adm.utilidades.Notificacao;
import max.adm.utilidades.NotificacaoFactory;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ImportarCondicoesPagamento {
	private String chave;
	private String url;
	private Context ctx;
	
	private Notificacao notificacao;
	
	
	private ImportarCondicoesPagamento(Context ctx, String chave, String url){
		this.ctx = ctx;
		this.chave = chave;
		this.url = url;
	}
	
	public static ImportarCondicoesPagamento newInstance(Context ctx, String chave, String url){
		checkNotNull(ctx, "não é possivel criar ImportarCondicoesPagamento com contexto nulo.");
		checkNotNull(chave);
		checkNotNull(url);
		return new ImportarCondicoesPagamento(ctx, chave, url);
	}
	
	public void execute(){
		Log.d("service", "importando cond Pgtos");
		notifica();
		String urlService = url + Servico.buscaCondicoesPagto.get();
		try {
			ConexaoHttp conexao = ConexaoHttp.newInstance(urlService);
			List<CondicaoPagamento> condicaoPagamentos = conexao.executaRequisicao(this.chave, CondicaoPagamento.class, new TypeToken<List<CondicaoPagamento>>() {}.getType());
			RepositorioCondicaoPagamento repo = new RepositorioCondicaoPagamento(ctx);
			repo.deleteAll();
			repo.salvarLista(condicaoPagamentos);
			repo.fechar();
			notificaSucesso();
		} catch (Exception e) {
			e.printStackTrace();
			notificaFalha();
		}
	}
	
	private void notifica(){
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoInfo(this.ctx, "Condições de Pagamentos", "Importando Condições de Pagamentos", new Intent());
	}
	
	private void notificaFalha(){
		notificacao.cancelaNotificacao();
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoFalha(this.ctx, "Condições de Pagamentos", "Falha ao importar", new Intent());
	}

	private void notificaSucesso(){
		notificacao.cancelaNotificacao();
		notificacao = NotificacaoFactory.INSTANCE.newNotificacaoSucesso(this.ctx, "Condições de Pagamentos", "importação concluida", new Intent());
	}

}
