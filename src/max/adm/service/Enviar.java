package max.adm.service;

import java.util.List;

import max.adm.Servico;
import max.adm.entidade.Empresa;
import max.adm.entidade.Parametro;
import max.adm.entidade.Pessoa;
import max.adm.pedido.PedidoDoc;
import max.adm.utilidades.Comprimir;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;

import android.content.Context;
import android.util.Log;

public class Enviar {

	public static boolean enviaClientes(Context ctx, List<Pessoa> pessoas, Empresa empresa) {

		try {
			Parametro parametro = Parametro.buscaPorLoja(ctx, empresa.getNumeroLoja());

			String url = parametro.getUrlWebService();

			ConexaoHttp conexao = ConexaoHttp.newInstance(url + Servico.enviaClientes.get());

			Integer representanteId = parametro.getCodigoRepresentante();

			SolicitacaoDeGravacaoDeCliente solicitacaoDeGravacaoDeCliente = SolicitacaoDeGravacaoDeCliente.newInstance(pessoas, representanteId, empresa);

			HttpPost httpPost = conexao.criaReqisicao(url + Servico.enviaClientes.get());

			byte[] byteEntity = Comprimir.transformToByteArray(solicitacaoDeGravacaoDeCliente);

			HttpEntity httpEntity = new ByteArrayEntity(byteEntity);

			httpPost.setEntity(httpEntity);

			Log.d("enviando dados", url + Servico.enviaClientes.get());

			String resultado = conexao.enviaDados(httpPost);
			System.out.println("resultado " + resultado);

			return resultado.equals("ok"); 
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	public static boolean enviaPedidos(Context ctx, List<PedidoDoc> pedidos, Empresa loja) {
		try {
			Parametro parametro = Parametro.buscaPorLoja(ctx, loja.getNumeroLoja());
			String url = parametro.getUrlWebService();
			ConexaoHttp conexao = ConexaoHttp.newInstance(url + Servico.enviaPedidos.get());
			Integer representanteId = parametro.getCodigoRepresentante();
			SolicitacaoDeGravacaoDePedido entity = SolicitacaoDeGravacaoDePedido.newInstance(pedidos, representanteId, parametro.getEmpresa());
			byte[] byteEntity = Comprimir.transformToByteArray(entity);
			Log.d("ENVIANDO", url + Servico.enviaPedidos.get());
			HttpPost requisicao = conexao.criaReqisicao(url + Servico.enviaPedidos.get());
			HttpEntity entidade = new ByteArrayEntity(byteEntity);
			requisicao.setEntity(entidade);
			Log.d("enviando dados", url + Servico.enviaPedidos.get());
			String resultado = conexao.enviaDados(requisicao);
			if (resultado.equals("ok")) {
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

}
