package max.adm.service;

import java.net.URI;

import max.adm.Servico;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import android.util.Log;

public class AutenticaLicecaRepres {
	
	public static boolean autentica(String urlServidor, String chave){
	
		try {
			HttpClient client = HttpClientFactory.getHttpClient();
			
			HttpPost request = new HttpPost();
			
			request.setURI(new URI(urlServidor+Servico.LicencaService.get()));
			
			HttpEntity entity = new StringEntity(chave);
			request.setEntity(entity);
			
			HttpResponse response = client.execute(request);
			
			String responseContext = EntityUtils.toString(response.getEntity());
			
			Log.d("Resposta Servidor", "Resposta: "+responseContext);
			
			if (responseContext.equals("ok")){
				return true;
			}
			else{
				return false;
			}
			
		} catch (Exception e) {
			Log.e("Resposta Servidor", "Erro: ", e);
			return false;
		}
	}

}
