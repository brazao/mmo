package max.adm.service;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.util.List;

import max.adm.entidade.Empresa;
import max.adm.pedido.PedidoDoc;

public class SolicitacaoDeGravacaoDePedido implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<PedidoDoc> pedidos;
	private Integer representanteId;
	private Empresa empresa;

	private SolicitacaoDeGravacaoDePedido(List<PedidoDoc> pedidos, Integer representanteId, Empresa empresa) {
		this.pedidos = pedidos;
		this.representanteId = representanteId;
		this.empresa = empresa;
	}

	public static SolicitacaoDeGravacaoDePedido newInstance(List<PedidoDoc> pedidos, Integer representanteId, Empresa empresa) {
		checkNotNull(pedidos);
		checkNotNull(representanteId);
		checkNotNull(empresa);
		return new SolicitacaoDeGravacaoDePedido(pedidos, representanteId, empresa);
	}

	public List<PedidoDoc> getPedidos() {
		return pedidos;
	}

	public Integer getRepresentanteId() {
		return representanteId;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

}
