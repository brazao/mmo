package max.adm.service;

import java.util.Date;

import max.adm.R;
import max.adm.Servico;
import max.adm.controle.CriaLoginControle;
import max.adm.controle.RepresentanteLicencaControle;
import max.adm.database.DBUtil;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ImportaBancoService {
	private String chave;
	private String url;

	private Context ctx;

	public ImportaBancoService(Context ctx, String url, String chave) {
		this.ctx = ctx;
		this.url = url;
		this.chave = chave;
		Log.d("service", "construiu Service");
	}

	Notification notification;

	public void importar() {
		Log.d("service", "executando handler");

		long tempoInicio = new Date().getTime();

		System.out.println("chave: " + chave);

		if (!buscaGravaBanco(url, chave)) {
			notificaFalha("Falha ao importar os dados!");
		}

		long tempoFinal = new Date().getTime();

		Log.d("TEMPO TOTAL", String.valueOf(((tempoFinal - tempoInicio) / 1000) / 60) + " minutos");

		notificaSucesso("Importação concluída.");

	}

	private boolean buscaGravaBanco(String url, String chave) {

		try {
			HttpEntity httpEntity = new StringEntity(chave);

			byte[] banco = ConexaoHttp.newInstance(url+Servico.buscaBancoDeDados).recebeDados(httpEntity);
			
			DBUtil.gravaBancoNaPasta(ctx, banco);
			
			return true;

		} catch (Exception e) {
			notificaFalha("Falha ao importar os dados.");
			return false;
		}

	}

	private void notificaSucesso(String msg) {

		notification = new Notification(R.drawable.ic_notifica_ok, msg, System.currentTimeMillis());
		Intent i = new Intent(this.ctx, CriaLoginControle.class);

		PendingIntent contentIntent = PendingIntent.getActivity(this.ctx, 551, i, 0);
		notification.tickerText = msg;
		notification.flags = Notification.FLAG_AUTO_CANCEL;
		notification.setLatestEventInfo(this.ctx, this.ctx.getApplicationContext().getString(R.string.app_name), msg, contentIntent);

	}

	private void notificaFalha(String msg) {

		notification = new Notification(R.drawable.ic_notifica_erro, msg, System.currentTimeMillis());
		Intent i = new Intent(this.ctx, RepresentanteLicencaControle.class);

		i.putExtra("ACTION", "notificação");
		i.putExtra("URL", url);

		PendingIntent contentIntent = PendingIntent.getActivity(this.ctx, 551, i, 0);
		notification.flags = Notification.FLAG_AUTO_CANCEL;
		notification.setLatestEventInfo(this.ctx, this.ctx.getApplicationContext().getString(R.string.app_name), msg, contentIntent);

	}

}
