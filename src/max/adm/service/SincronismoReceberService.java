package max.adm.service;

import java.util.Date;

import max.adm.dominio.Sincronismo;
import max.adm.entidade.Empresa;
import max.adm.entidade.Parametro;
import max.adm.lote.ImportarLotes;
import max.adm.perfilcliente.ImportarContasReceber;
import max.adm.produto.ImportarImagens;
import max.adm.tipoCobranca.ImportarTipoCobraca;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class SincronismoReceberService {

	private boolean importaTabelaPreco;
	private boolean importaFaixaDeCodigos;
	private boolean importaCeps;
	private boolean importaClientes;
	private boolean importaCondicoesPagamentos;
	private boolean importaContasReceber;
	private boolean importaLotes;
	private boolean importaImagens;
	private boolean importaParametros;
	private boolean importaTipoCobranca;

	private Empresa empresa;
	private Parametro param;
	private Context ctx;

	private SincronismoReceberService(Context ctx) {
		this.ctx = ctx;
	}

	public static SincronismoReceberService newSincronismoReceberService(Context ctx) {
		return new SincronismoReceberService(ctx);
	}

	public void execute(Intent intent) {
		importaTabelaPreco = intent.getExtras().getBoolean("importaTabelaPreco");
		importaFaixaDeCodigos = intent.getExtras().getBoolean("importaFaixaDeCodigos");;
		importaCeps = intent.getExtras().getBoolean("importaCeps");
		importaClientes = intent.getExtras().getBoolean("importaClientes");
		importaCondicoesPagamentos = intent.getExtras().getBoolean("importaCondicoesPagamentos");
		importaContasReceber = intent.getExtras().getBoolean("importaContasReceber");
		importaLotes = intent.getExtras().getBoolean("importaLotes");
		importaImagens = intent.getExtras().getBoolean("importaImagens");
		importaParametros = intent.getExtras().getBoolean("importaParametros");
		importaTipoCobranca = intent.getExtras().getBoolean("importaTipoCobranca");

		empresa = (Empresa) intent.getExtras().getSerializable("EMPRESA");
		param = (Parametro) intent.getExtras().getSerializable("PARAMETRO");

		if (importaFaixaDeCodigos) {
			Log.d("faixa codigo", "download faixa codigo");
			iniciaDownloadFaixaCodigo();
			iniciaDownloadFaixaCodigoPedido();
		}

		if (importaClientes) {
			Log.d("debug", "download clientes");
			iniciaDownlodsClientes();
		}
		if (importaCondicoesPagamentos) {
			Log.d("debug", "download condições de pagamentos");
			iniciaDownlodsCondicoesPgtos();
		}

		if (importaContasReceber) {
			Log.d("debug", "download dados financeiros");
			iniciaDownlodsContasreceber();
		}
		if (importaTabelaPreco) {
			Log.d("debug", "download produtos");
			iniciaDownloadProdutos();
		}

		if (importaLotes) {
			Log.d("debug", "downloads de lotes");
			iniciaDownloadLotes();
		}

		if (importaCeps) {
			Log.d("debug", "download ceps");
			iniciaDownloadCeps();
		}
		if (importaImagens) {
			iniciaDownloadImagens();
		}
		if (importaParametros) {
			iniciaDownloadParametros();
		}
		if (importaTipoCobranca) {
			iniciaDownloadTipoCobranca();
		}

		Sincronismo sync = Sincronismo.newSincronismo(empresa, new Date(), "R", importaClientes, false, importaCeps, importaCondicoesPagamentos, importaContasReceber, importaFaixaDeCodigos,
				importaTabelaPreco);

//		sync.salvar(this.ctx);
	}

	private void iniciaDownloadFaixaCodigo() {
		//ImportarFaixaCodigoCliente.newInstance(ctx, param.getUrlWebService(), param.getChave()).execute();
	}

	private void iniciaDownloadFaixaCodigoPedido() {
//		ImportarFaixaCodigoPedido.newInstance(ctx, param.getChave(), param.getUrlWebService()).execute();
	}

	private void iniciaDownlodsClientes() {
		//ImportarPessoas.newInstance(ctx, param.getChave(), param.getUrlWebService()).execute();
	}

	private void iniciaDownlodsCondicoesPgtos() {
		ImportarCondicoesPagamento.newInstance(ctx, param.getChave(), param.getUrlWebService()).execute();
	}

	private void iniciaDownlodsContasreceber() {
		ImportarContasReceber.newInstance(this.ctx, empresa).execute();
	}


	private void iniciaDownloadProdutos() {
//		ImportarReduzidos.newInstance(ctx, param.getChave(), param.getUrlWebService()).execute();
	}

	private void iniciaDownloadCeps() {
//		ImportarCeps.newInstance(this.ctx, param.getChave(), param.getUrlWebService()).execute();
	}

	private void iniciaDownloadLotes() {
		ImportarLotes.newInstance(ctx, param.getUrlWebService()).execute();
	}

	private void iniciaDownloadImagens() {
		ImportarImagens.newInstance(ctx, param.getUrlWebService()).execute();
	}

	private void iniciaDownloadParametros() {
//		ImportarParametros.newInstance(ctx, param.getChave(), param.getUrlWebService()).execute();
	}

	private void iniciaDownloadTipoCobranca() {
		ImportarTipoCobraca.newInstance(ctx, param.getChave(), param.getUrlWebService()).execute();
	}
}
