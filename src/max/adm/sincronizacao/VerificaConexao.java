package max.adm.sincronizacao;




import java.net.URI;


import max.adm.service.HttpClientFactory;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;




public class VerificaConexao {
	
	private Context context;
	

	public VerificaConexao(Context context)  {
		super();
		this.context = context;
	}
	
	
	public static boolean verificar(String url) {
		
		
		try {
			HttpClient client = HttpClientFactory.getHttpClient();
			HttpPost request = new HttpPost();
			request.setURI(new URI(url));
			HttpResponse response = client.execute(request);
			String responseContext = EntityUtils.toString(response.getEntity());
			
			Log.d("Resposta Servidor", "Resposta: "+responseContext);
			
			if (responseContext.equals("ok")){
				return true;
			}
			else{
				return false;
			}
			
		} catch (Exception e) {
			Log.e("Resposta Servidor", "Erro: ", e);
			return false;
		}
		
		
				
	}
	
	public boolean internetOk(){
		
		ConnectivityManager conn = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo net = conn.getActiveNetworkInfo();

		return (net != null && net.isConnected());
	}
	
	

}
